package portalfrontend;

import java.net.MalformedURLException;
import java.util.ArrayList;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.security.UserAndPassword;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;


public class CommonFooterMethods {
	public GeneralMethods Genf;
	public FrameWorkUtility Futil;
	
	
	public int afVerifyMuleSoft_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		
		try{
			Futil = new FrameWorkUtility();
			  Genf = new GeneralMethods();
			  int windowNum;
			  String winHandleBefore = Driver.getWindowHandle();
			  WebElement MuleSoftCopyright = Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='MULESOFT']/parent::a/following-sibling::small");
			  String CopyRightMsg = Futil.fgetData("Mule Soft Copyright", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			  StepNum = Genf.gfVerifyText(Extreport, Exttest, Driver, StepNum, ResultLocation, MuleSoftCopyright, "MuleSoft Copyright", CopyRightMsg);
			  WebElement MulesoftIcon= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='MULESOFT']");
			  StepNum = Genf.fJSClickOnObject(MulesoftIcon, "Click On Mulesoft Icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			  
			  try{
				  Thread.sleep(10000);
				  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
				  for( windowNum = 0;windowNum<tabs2.size();windowNum++){
					  if(tabs2.get(windowNum).equals(winHandleBefore)){}
					  else{
						  Driver.switchTo().window(tabs2.get(windowNum));
					  }
				  }
			  if((Driver.getCurrentUrl().contains("mulesoft.com"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Mulesoft Page", "Successfully verified the Mulesoft Page  with URL :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Successfully verified the Mulesoft Page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Mulesoft Page", "Failed To Verfiy the Mulesoft Page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed to verify the Mulesoft Page the URL is "+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Mulesoft Page", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Mulesoft Page", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Mulesoft Page");
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Mulesoft Page", "Verification failed for The Mulesoft Page", "No");
			  System.out.println("Verification failed for The Mulesoft Page");
			  StepNum = StepNum+1;	
		}
		 	 return  StepNum;
	}
	public int afVerifyGooglePlayStore_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement imgDwnldOnPlayStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='Get it on Google play']");
			  if(imgDwnldOnPlayStore!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", imgDwnldOnPlayStore);
				  //btnRegisterYourFree.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store From Footer", "Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer", "No");
				  System.out.println("Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store From Footer", "Failed To Click On  the button GET IT ON GOOGLE PLAY From Footer", "No");   
				  System.out.println("Failed To Click On  the button Download on the App Store From Footer"); 
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("com.parsifal.starz"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the play store page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed to verify the play store page for STARZ PLAY ARABIA the URL is "+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The play store for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for play store", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for play store");
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Google Play Store page", "Verification failed for The Google Play Store page", "No");
			  System.out.println("Verification failed for The Page Google Play Store");
			  StepNum = StepNum+1;	
		}
		 	 return  StepNum;
	}
	
	
	public int afVerifyAppStore_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		 try{
			 Futil = new FrameWorkUtility();
				Genf = new GeneralMethods();
			 WebElement imgDwnldOnAppStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='Download on the App Store']");
			  if(imgDwnldOnAppStore!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", imgDwnldOnAppStore);
				  //btnRegisterYourFree.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Download on the App Store from Footer", "Successfully Clicked on the button Download on the App Store from Footer", "No");
				  System.out.println("Successfully Clicked on the button Download on the App Store from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Download on the App Store from footer", "Failed To Click On  the button Download on the App Store from footer", "No");   
				  System.out.println("Failed To Click On  the button Download on the App Store from footer"); 
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("itunes.apple.com/ae/app/starzplay-arabia"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the Appstore page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed to verify the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Appstore", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
				  StepNum = StepNum+1;
				  
			  }
			 
		 }catch(Exception e){
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The AppStore page", "Verification failed for The AppStore page", "No");
			  System.out.println("Verification failed for The Page AppStore");
			  StepNum = StepNum+1;
			 
		 }
		  
		return  StepNum; 
		  
	}
	
	public int afVerifyYouTube_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement ImgYoutube= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialYoutube']/span");
			  if(ImgYoutube!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", ImgYoutube);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Youtube icon from footer", "Successfully Clicked on the Youtube icon from footer", "No");
				  System.out.println("Successfully Clicked on the Youtube icon from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Youtube icon from footer", "Failed To Click On  the Youtube icon from footer", "No");   
				  System.out.println("Failed To Click On  the Youtube icon from footer"); 
				  StepNum = StepNum+1;
				  //Assert.fail("Failed To Click On  the Instagram icon from footer");
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("youtube"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Youtube Page Has opened", "Successfully verified the Youtube page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Youtube page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Youtube Page Has opened", "Failed To Verfiy the Youtube page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Youtube page, And the URL is :"+Driver.getCurrentUrl());
				 //Assert.fail("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Youtube Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Youtube", "No");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Youtube");
				  //Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The YouTube page", "Verification failed for The YouTube page", "No");
			  System.out.println("Verification failed for The Page YouTube");
			  StepNum = StepNum+1;
			
		}
		 
		return   StepNum;
		  
	}
	
	public int afVerifyInstagram_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement ImgInstagram= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialInstagram']/span");
			  if(ImgInstagram!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", ImgInstagram);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Instagram icon from footer", "Successfully Clicked on the Instagram icon from footer", "No");
				  System.out.println("Successfully Clicked on the Instagram icon from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Instagram icon from footer", "Failed To Click On  the Instagram icon from footer", "No");   
				  System.out.println("Failed To Click On  the Instagram icon from footer"); 
				  Assert.fail("Failed To Click On  the Instagram icon from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("instagram.com/starzplayarabia/"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Instagram Page Has opened", "Successfully verified the Instagram page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Instagram page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Instagram Page Has opened", "Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Instagram Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram", "No");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Instagram page", "Verification failed for The Instagram page", "No");
			  System.out.println("Verification failed for The Page Instagram");
			  StepNum = StepNum+1;
		}
		 
		return   StepNum;
		 
	}
	
	public int afVerifyTwitter_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement ImgTwitter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialTwitter']/span");
			  if(ImgTwitter!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", ImgTwitter);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Twitter icon from footer", "Successfully Clicked on the Twitter icon from footer", "No");
				  System.out.println("Successfully Clicked on the Twitter icon from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Twitter icon from footer", "Failed To Click On  the Twitter icon from footer", "No");   
				  System.out.println("Failed To Click On  the Twitter icon from footer"); 
				  Assert.fail("Successfully Clicked on the Twitter icon from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("twitter.com/starzplayarabia"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Twitter Page Has opened", "Successfully verified the Twitter page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Twitter page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Twitter Page Has opened", "Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Twitter Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter", "No");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter");
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Twitter page", "Verification failed for The Twitter page", "No");
			  System.out.println("Verification failed for The Page Twitter");
			  StepNum = StepNum+1;
		}
		 
		return StepNum;
		  
	}
	
	public int afverifyFaceBook_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement ImgFacebook= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialFacebook']/span");
			  if(ImgFacebook!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", ImgFacebook);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Facebook icon from footer", "Successfully Clicked on the Facebook icon from footer", "No");
				  System.out.println("Successfully Clicked on the Facebook icon from footer"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Facebook icon from footer", "Failed To Click On  the Facebook icon from footer", "No");   
				  System.out.println("Failed To Click On  the Facebook icon from footer"); 
				  Assert.fail("Failed To Click On  the Facebook icon from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("facebook.com/starzplayarabia"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "No");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook page", "Verification failed for The Facebook page", "No");
			  System.out.println("Verification failed for The Page Facebook");
			  StepNum = StepNum+1;
			
		}
		 
		return StepNum;  
		  
	}
	
	
	public int afVerifyCareers_Footer(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement lnCareers= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Careers']");
			  if(lnCareers!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnCareers);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Careers from footer", "Successfully Clicked on the link Careers from footer", "No");
				  System.out.println("Successfully Clicked on the link Careers from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Careers from footer", "Failed To Click On  the link Careers from footer", "No");   
				  System.out.println("Failed To Click On the link Careers from footer"); 
				  Assert.fail("Failed To Click On the link Careers from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("starzplayarabia.bamboohr"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Career Page Has opened", "Successfully verified the Career page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Career page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Career Page Has opened", "Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Career Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Career", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Career");
				  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Career");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Careers page", "Verification failed for The Careers page", "No");
			  System.out.println("Verification failed for The Page Careers");
			  StepNum = StepNum+1;
			
		}
		 
		return StepNum; 	  
		  
	}
	
	public int afVerifyBlog_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{

		 try{
			 Futil = new FrameWorkUtility();
				Genf = new GeneralMethods();
				int windowNum;
				String winHandleBefore = Driver.getWindowHandle();
			 WebElement lnBlog= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Blog']");
			  if(lnBlog!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnBlog);
				  //btnRegisterYourFree.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Blog from footer", "Successfully Clicked on the link Blog from footer", "No");
				  System.out.println("Successfully Clicked on the Blog Policy from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Blog from footer", "Failed To Click On  the link Blog from footer", "No");   
				  System.out.println("Failed To Click On  the link Blog from footer"); 
				  Assert.fail("Failed To Click On  the link Blog from footer");
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  try{
				  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
				  for(windowNum=0;windowNum<tabs2.size();windowNum++){
					  if(tabs2.get(windowNum).equals(winHandleBefore)){}
					  else{
						  Driver.switchTo().window(tabs2.get(windowNum));
					  }
				  }
				  if((Driver.getCurrentUrl().contains("blog.starzplay"))){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Blog Page Has opened", "Successfully verified the Blog page with URL :"+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the Blog page with URL :"+Driver.getCurrentUrl());
					  StepNum = StepNum+1;
				  }
				  else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Blog Page Has opened", "Failed To Verfiy the Blog page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Failed To Verfiy the Blog page, And the URL is :"+Driver.getCurrentUrl());
					  Assert.fail("Failed To Verfiy the Blog page, And the URL is :"+Driver.getCurrentUrl());
					  StepNum = StepNum+1;
				  }
				  Driver.close();
				  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Blog Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Blog", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Blog");
				  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Blog");
				  StepNum = StepNum+1;
			  }
		 }catch(Exception e){
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Blog page", "Verification failed for The BLOG page", "No");
			  System.out.println("Verification failed for The Page Blog");
			  StepNum = StepNum+1;
		 }
		 return StepNum;    
		  
	}
	
	public int afVerifyPrivacyPolicy_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		try{
			int windowNum;
			String winHandleBefore = Driver.getWindowHandle();
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement lnPrivacyPolicy= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Privacy Policy']");
			  if(lnPrivacyPolicy!=null){
				  //JavascriptExecutor js = (JavascriptExecutor)Driver;
				  //js.executeScript("arguments[0].click();", lnPrivacyPolicy);
				  lnPrivacyPolicy.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Privacy Policy from footer", "Successfully Clicked on the link Privacy Policy from footer", "No");
				  System.out.println("Successfully Clicked on the link Privacy Policy from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Privacy Policy from footer", "Failed To Click On  the link Privacy Policy from footer", "No");   
				  System.out.println("Failed To Click On  the link Privacy Policy from footer"); 
				  Assert.fail("Failed To Click On  the link Privacy Policy from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(10000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  for(windowNum=0;windowNum<tabs2.size();windowNum++){
				  if(tabs2.get(windowNum).equals(winHandleBefore)){}
				  else{
					  Driver.switchTo().window(tabs2.get(windowNum));
				  }
			  }
			  //Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("/en/privacypolicy"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Privacy Policy Page Has opened", "Successfully verified the Privacy Policy page with URL :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Successfully verified the Privacy Policy page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy Page Has opened", "Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(winHandleBefore); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Privacy Policy", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Privacy Policy");
				  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Privacy Policy");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy page", "Verification failed for The Privacy Policy page", "No");
			  System.out.println("Verification failed for The Page Privacy Policy");
			  StepNum = StepNum+1;
		}
		 
		return StepNum;  
		  
	}
	
	public int afVerifyTermsAndAconditions_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		  try{
			  Futil = new FrameWorkUtility();
				Genf = new GeneralMethods();
				int windowNum;
				Genf.gfSleep(4);
				String winHandleBefore = Driver.getWindowHandle();
			  WebElement lnTermsNCond= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Terms & Conditions']");
			  if(lnTermsNCond!=null){
				  lnTermsNCond.click();
				  //JavascriptExecutor js = (JavascriptExecutor)Driver;
				  //js.executeScript("arguments[0].click();", lnTermsNCond);
				  
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Terms & Conditions from footer", "Successfully Clicked on Terms & Conditions from footer", "No");
				  System.out.println("Successfully Clicked on Terms & Conditions from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Terms & Conditions from footer", "Failed To Click Terms & Conditions from footer", "No");   
				  System.out.println("Failed To Click Terms & Conditions from footer"); 
				  Assert.fail("Failed To Click Terms & Conditions from footer");
				  StepNum = StepNum+1;
				   }
			  Genf.gfSleep(2);
			  Genf.gnWaitUntilPageLoad(Driver);
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  for(windowNum=0;windowNum<tabs2.size();windowNum++){
				  if(tabs2.get(windowNum).equals(winHandleBefore)){}
				  else{
					  Driver.switchTo().window(tabs2.get(windowNum));
				  }
			  }
			 // Driver.switchTo().window(tabs2.get(1));
			  WebElement lblTermsAndCond= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//h1[text()='Terms & Conditions']");
			  if((Driver.getCurrentUrl().contains("/en/termsandconditions"))&& (lblTermsAndCond!=null)){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to Terms & Conditions", "Successfully Loaded the Terms & Conditions page with URL :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Successfully Loaded the Terms & Conditions page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Terms & Conditions", "Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(winHandleBefore); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Terms & Conditions", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Terms & Conditions", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Terms & Conditions");
				  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Terms & Conditions");
				  StepNum = StepNum+1;
				  
			  }
			  
		  }catch(Exception e){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Terms & Conditions page", "Verification failed for The Terms & Conditions page", "No");
			  System.out.println("Verification failed for The Page Terms & Conditions");
			  StepNum = StepNum+1;
			  
		  }
		  
		return StepNum;  
		  
	}
	
	public int afVerifyCompanyPage_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			int windowNum;
			String winHandleBefore = Driver.getWindowHandle();
			WebElement lnCompany= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Company']");
			  if(lnCompany!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnCompany);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Company from footer", "Successfully Clicked on Company from footer", "No");
				  System.out.println("Successfully Clicked on Company from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Company from footer", "Failed To Click Company from footer", "No");   
				  System.out.println("Failed To Click On Company from footer"); 
				  Assert.fail("Failed To Click On Company from footer");
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(2);
			  Genf.gnWaitUntilPageLoad(Driver);
			  try{
				  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
				  for(windowNum=0;windowNum<tabs2.size();windowNum++){
					  if(tabs2.get(windowNum).equals(winHandleBefore)){}
					  else{
						  Driver.switchTo().window(tabs2.get(windowNum));
					  }
				  }
				  if((Driver.getCurrentUrl().contains("/en/aboutus"))){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to company aboutus", "Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl(), "No");
					  System.out.println("Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;
				  }
				  else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to company aboutus", "Failed To Load the aboutus page, And the URL is :"+Driver.getCurrentUrl(), "No");
					  System.out.println("Failed To Load the aboutus page, And the URL is :"+Driver.getCurrentUrl());
					  Assert.fail("Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl());
					  StepNum = StepNum+1;
				  }
				  if(tabs2.size()>1){
					Driver.close();
					Driver.switchTo().window(winHandleBefore); 
				  }
				  
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Page Company", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Company", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Company");
				  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Company");
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Company page", "Verification failed for The Company page", "No");
			  System.out.println("Verification failed for The Page Company");
			  StepNum = StepNum+1;
		}
		return StepNum;  
	}
	
	public int afVerifyPartnerWithUs_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		 try{
			 Futil = new FrameWorkUtility();
				Genf = new GeneralMethods();
			 WebElement lnPartnerWithUs= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Partner with us']");
			  if(lnPartnerWithUs!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnPartnerWithUs);
				  //btnLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Partner with us from footer", "Successfully Clicked on Partner with us from footer", "No");
				  System.out.println("Successfully Clicked on Partner with us from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Partner with us from footer", "Failed To Click Partner with us from footer", "No");   
				  System.out.println("Failed To Click On Partner with us from footer"); 
				  Assert.fail("Failed To Click On Partner with us from footer");
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  Genf.gnWaitUntilPageLoad(Driver);
			  if((Driver.getCurrentUrl().contains("/en/affiliatesubscription"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to Partner with us", "Successfully Loaded the Partner with us page with URL :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Successfully Loaded the Partner with us page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Partner with us", "Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			 
		 }catch(Exception e){
			 
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Partner with us page", "Verification failed for The Partner with us page, exception "+e, "No");
			  System.out.println("Verification failed for The Page Partner with us");
			  StepNum = StepNum+1;
			 
		 }
		  
		  return StepNum;
		  
	}
	
	public int afVerifyHelpCenter_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement btnHelpCenter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Help Center']");
			  if(btnHelpCenter!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnHelpCenter);
				  //btnLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Help Center from footer", "Successfully Clicked on Help Center from footer", "No");
				  System.out.println("Successfully Clicked on Help Center from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Help Center from footer", "Failed To Click On Clicked on Help Center from footer", "No");   
				  System.out.println("Failed To Click On Clicked on Help Center from footer"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(2);
			  Genf.gnWaitUntilPageLoad(Driver);
			 
			  WebElement lblHelpCenter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//h1[text()='Help Center']");
			  if(lblHelpCenter!=null){
				  
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The HELP CENTER Heading Label", "Successfully Verified the HELP CENTER Heading label", "Yes");
				  System.out.println("Successfully Verified the HELP CENTER Heading label");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The HELP CENTER Heading Label", "Failed To Verified the HELP CENTER Heading label", "No");   
				  System.out.println("Failed To Verified the HELP CENTER Heading label"); 
				  StepNum = StepNum+1;
			  }
			  Genf.gnWaitUntilPageLoad(Driver);
			  if((Driver.getCurrentUrl().contains("faq"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to faq", "Successfully Loaded the faq page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Loaded the Help Center page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to faq", "Failed To Load the faq page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the Why Help Center page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Help Center Page", "Verification failed for The Page Help Center", "No");
			  System.out.println("Verification failed for The Page Help Center");
			  StepNum = StepNum+1;
			}  
		return StepNum;
	}
	
	public int afVerifyWhyStarzPlayFromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement btnWhyStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Why STARZ PLAY?']");
			  if(btnWhyStarzPlay!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnWhyStarzPlay);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Why STARZ PLAY? from footer", "Successfully Clicked on Why STARZ PLAY? from footer", "No");
				  System.out.println("Successfully Clicked on Why STARZ PLAY? from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Why STARZ PLAY? from footer", "Failed To Click On Clicked on Why STARZ PLAY? from footer", "No");   
				  System.out.println("Failed To Click On Clicked on Why STARZ PLAY? from footer"); 
				  StepNum = StepNum+1;
				   }
			  Genf.gfSleep(5);
			  Genf.gnWaitUntilPageLoad(Driver);
			  
			  /*WebElement lblWhyStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[text()='WHY STARZ PLAY?']");
			  if(lblWhyStarzPlay!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The WHY STARZ PLAY? Label", "Successfully Verified the WHY STARZ PLAY? label", "Yes");
				  System.out.println("Successfully Verified the Why Starz Play Page");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The WHY STARZ PLAY? Label", "Failed To Verified the WHY STARZ PLAY? label", "No");   
				  System.out.println("Failed To Verified the WHY STARZ PLAY? label");  
				  StepNum = StepNum+1;
			  }*/
			  
			  if((Driver.getCurrentUrl().contains("whyStarzPlay"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to whyStarzPlay", "Successfully Loaded the whyStarzPlay page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Loaded the whyStarzPlay page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to whyStarzPlay", "Failed To Load the Why STARZ PLAY page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the Why STARZ PLAY page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  
			  
			
		}catch(Exception e)
		{
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Wahy Starz play? Page", "Verfication Failed, Please refer report", "Yes"); 
			 System.out.println("WHY STARZ Play? Page verification failed");  
			 Assert.fail("WHY STARZ Play? Page verification failed");
			 StepNum = StepNum+1; 		
		}	
		
		return StepNum;
		  
	}
	
	public int afLoginToPortal(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		  
		 try{
			 
			 Futil = new FrameWorkUtility();
			 Genf = new GeneralMethods();
			 WebElement btnLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//a[@id='login-button']");
			  if(btnLogin!=null){
				  btnLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login Button", "Successfully Clicked on the button Login", "No");
				  System.out.println("Successfully Clicked on the button Login");  
				  StepNum = StepNum+1;
				   }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login Button", "Failed To Click On  Clicked on the button Login", "Yes");   
				  System.out.println("Failed To Click On  Clicked on the button Login");  
				  StepNum = StepNum+1;
				   }
			  Thread.sleep(5000);
			  WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  //String EmailId = "ibrahim.poovakkattil@starzplayarabia.com";
				  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
				  EdtUserName.sendKeys(EmailId);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Email Address", "Successfully Entered The Email id :"+EmailId, "No");
				  System.out.println("Successfully Entered The Email id :"+EmailId);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Email Address", "Failed To Enter Email ID", "Yes");   
				  System.out.println("Failed To Enter Email ID");  
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  //String Password = "123456";
				  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Email id :"+Password, "No");
				  System.out.println("Successfully Entered The Email id :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			  	  StepNum = StepNum+1;
				  }
			  
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin!=null){
				  btnSubmitLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login Submit BUtton", "Successfully Clicked on the button Login", "No");
				  System.out.println("Successfully Clicked on the button Login");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login Submit Button", "Failed To Click On Clicked on the button Login", "Yes");   
				  System.out.println("Failed To Click On Clicked on the button Login");  
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(5000);
			  WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The user is logged in", "Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl(), "Yes");   
				  System.out.println("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl()); 
				  Assert.fail("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl());
				  StepNum = StepNum+1; 
			  	  }
			  /*WebElement lnkMOvies= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='movies-link']");
			  if(lnkMOvies!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Home Page", "Successfully Verified The home page by confirming The Link Movies", "Yes");
				  System.out.println("Successfully Verified The home page by confirming The Link Movies");  
				  StepNum = StepNum+1;   
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Home Page", "Failed To Verify the home page, The Link for Movies Not Present", "Yes");   
				  System.out.println("Failed To Verify the home page, The Link for Movies Not Present");  
				  StepNum = StepNum+1;   
			  }*/
			 
		 }catch(Exception e){
			 
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Login To Application", "Login Failed, Please refer the report", "Yes"); 
			 System.out.println("Login Failed");  
			 Assert.fail("Login Failed");
			 StepNum = StepNum+1;  
		 }
		  
		  return StepNum;
	  }
	
public void fManageAlert(WebDriver Driver){
	Genf = new GeneralMethods();
	boolean isAlertExist = Genf.isAlertPresent(Driver);
	if(isAlertExist){
		 Alert alert=Driver.switchTo().alert();
		 System.out.println(alert.getText());
		 alert.authenticateUsing(new UserAndPassword("starz","milkyway"));
		
	}

	
}

/*public void TC009_Guest_Landing_to_WhyStarzPlay(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  System.out.println("TC009_Guest_Landing_to_WhyStarzPlay: Execution Started");
	  Exttest = Extreport.startTest("TC009_Guest_Landing_to_WhyStarzPlay","A guest at Landing page goes to the Why STARZ PLAY? page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment Landing page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment Landing page", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
	  }
	 
	  WebElement btnWhyStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Why STARZ PLAY?']");
	  if(btnWhyStarzPlay!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", btnWhyStarzPlay);
		  //btnLogin.click();
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Why STARZ PLAY? from footer", "Successfully Clicked on Why STARZ PLAY? from footer", "No");
		  System.out.println("Successfully Clicked on Why STARZ PLAY? from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Why STARZ PLAY? from footer", "Failed To Click On Clicked on Why STARZ PLAY? from footer", "No");   
		  System.out.println("Failed To Click On Clicked on Why STARZ PLAY? from footer"); 
		   }
	  Genf.gfSleep(5);
	  
	  if((Driver.getCurrentUrl().contains("whyStarzPlay"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The URL Changed to whyStarzPlay", "Successfully Loaded the whyStarzPlay page with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully Loaded the whyStarzPlay page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The URL Changed to whyStarzPlay", "Failed To Load the Why STARZ PLAY page, And the URL is :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Load the Why STARZ PLAY page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  
	  WebElement lblWhyStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[text()='WHY STARZ PLAY?']");
	  if(lblWhyStarzPlay!=null){
		  
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 4. Verify The WHY STARZ PLAY? Label", "Successfully Verified the WHY STARZ PLAY? label", "Yes");
		  System.out.println("Successfully Verified the Why Starz Play Page");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 4. Verify The WHY STARZ PLAY? Label", "Failed To Verified the WHY STARZ PLAY? label", "No");   
		  System.out.println("Failed To Verified the WHY STARZ PLAY? label");  
	  }
	  
	  System.out.println("TC009_Guest_Landing_to_WhyStarzPlay : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
	  
}

public void TC010_Guest_Landing_to_Help_Center(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  
	  System.out.println("TC010_Guest_Landing_to_Help_Center: Execution Started");
	  Exttest = Extreport.startTest("TC010_Guest_Landing_to_Help_Center","A guest at Landing page goes to the Help Center page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment Landing page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment Landing page", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
	  }
	 
	  WebElement btnHelpCenter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Help Center']");
	  if(btnHelpCenter!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", btnHelpCenter);
		  //btnLogin.click();
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Help Center from footer", "Successfully Clicked on Help Center from footer", "No");
		  System.out.println("Successfully Clicked on Help Center from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Help Center from footer", "Failed To Click On Clicked on Help Center from footer", "No");   
		  System.out.println("Failed To Click On Clicked on Help Center from footer"); 
		   }
	  
	  Genf.gfSleep(5);
	  try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  if((Driver.getCurrentUrl().contains("faq"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The URL Changed to faq", "Successfully Loaded the faq page with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully Loaded the Help Center page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The URL Changed to faq", "Failed To Load the faq page, And the URL is :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Load the Why Help Center page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  
	  WebElement lblHelpCenter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//h1[text()='Help Center']");
	  if(lblHelpCenter!=null){
		  
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 4. Verify The HELP CENTER Heading Label", "Successfully Verified the HELP CENTER Heading label", "Yes");
		  System.out.println("Successfully Verified the HELP CENTER Heading label");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 4. Verify The HELP CENTER Heading Label", "Failed To Verified the HELP CENTER Heading label", "No");   
		  System.out.println("Failed To Verified the HELP CENTER Heading label");  
	  }
	  
	  System.out.println("TC010_Guest_Landing_to_Help_Center : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
	  
}

public void TC011_Guest_Landing_to_PartnerWithUs(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  
	  System.out.println("TC011_Guest_Landing_to_PartnerWithUs: Execution Started");
	  Exttest = Extreport.startTest("TC011_Guest_Landing_to_PartnerWithUs","A guest at the Landing page goes to the Partner with us page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment Landing page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment Landing page", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  Assert.fail("Failed To Launch And The title was"+Driver.getTitle());
	  }
	 
	  WebElement lnPartnerWithUs= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Partner with us']");
	  if(lnPartnerWithUs!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", lnPartnerWithUs);
		  //btnLogin.click();
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Partner with us from footer", "Successfully Clicked on Partner with us from footer", "No");
		  System.out.println("Successfully Clicked on Partner with us from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Partner with us from footer", "Failed To Click Partner with us from footer", "No");   
		  System.out.println("Failed To Click On Partner with us from footer"); 
		  Assert.fail("Failed To Click On Partner with us from footer");
		   }
	  
	  Genf.gfSleep(5);
	  
	  if((Driver.getCurrentUrl().contains("/en/affiliatesubscription"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The URL Changed to Partner with us", "Successfully Loaded the Partner with us page with URL :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Successfully Loaded the Partner with us page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The URL Changed to Partner with us", "Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl());
		  Assert.fail("Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  
	  System.out.println("TC011_Guest_Landing_to_PartnerWithUs : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
	  
}

public void TC012_Guest_Landing_to_Company(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  
	  System.out.println("TC012_Guest_Landing_to_Company: Execution Started");
	  Exttest = Extreport.startTest("TC012_Guest_Landing_to_Company","A guest at the Landing page goes to the Company page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment Landing page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment Landing page", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  Assert.fail("Failed To Launch And The title was"+Driver.getTitle());
	  }
	 
	  WebElement lnCompany= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Company']");
	  if(lnCompany!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", lnCompany);
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Company from footer", "Successfully Clicked on Company from footer", "No");
		  System.out.println("Successfully Clicked on Company from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Company from footer", "Failed To Click Company from footer", "No");   
		  System.out.println("Failed To Click On Company from footer"); 
		  Assert.fail("Failed To Click On Company from footer");
		   }
	  
	  Genf.gfSleep(5);
	  
	  if((Driver.getCurrentUrl().contains("/en/aboutus"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The URL Changed to company aboutus", "Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The URL Changed to company aboutus", "Failed To Load the aboutus page, And the URL is :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Load the aboutus page, And the URL is :"+Driver.getCurrentUrl());
		  Assert.fail("Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl());
	  }
	  
	  System.out.println("TC012_Guest_Landing_to_Company : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
	  
}

public void TC013_Guest_Landing_to_TermsAndAconditions(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  System.out.println("TC013_Guest_Landing_to_TermsAndAconditions: Execution Started");
	  Exttest = Extreport.startTest("TC013_Guest_Landing_to_TermsAndAconditions","A guest at the Landing page goes to the Terms & Conditions page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment Landing page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment Landing page", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  Assert.fail("Failed To Launch And The title was"+Driver.getTitle());
	  }
	  
	  WebElement lnTermsNCond= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Terms & Conditions']");
	  if(lnTermsNCond!=null){
		  lnTermsNCond.click();
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", lnTermsNCond);
		  
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Terms & Conditions from footer", "Successfully Clicked on Terms & Conditions from footer", "No");
		  System.out.println("Successfully Clicked on Terms & Conditions from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Terms & Conditions from footer", "Failed To Click Terms & Conditions from footer", "No");   
		  System.out.println("Failed To Click Terms & Conditions from footer"); 
		  Assert.fail("Failed To Click Terms & Conditions from footer");
		   }
	  Genf.gfSleep(5);
	  
	  try{
		  Thread.sleep(10000);
	  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
	  Driver.switchTo().window(tabs2.get(1));
	  WebElement lblTermsAndCond= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//h1[text()='Terms & Conditions']");
	  if((Driver.getCurrentUrl().contains("/en/termsandconditions"))&& (lblTermsAndCond!=null)){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The URL Changed to Terms & Conditions", "Successfully Loaded the Terms & Conditions page with URL :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Successfully Loaded the Terms & Conditions page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The URL Changed to Terms & Conditions", "Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl());
		  Assert.fail("Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  Driver.close();
	  Driver.switchTo().window(tabs2.get(0)); 
	  }catch(Exception e){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The URL Changed to Terms & Conditions", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Terms & Conditions", "No");
		  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Terms & Conditions");
		  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Terms & Conditions");
		  
	  }
	  System.out.println("TC013_Guest_Landing_to_TermsAndAconditions : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
	  
}

public void TC014_Guest_Landing_to_PrivacyPolicy(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  System.out.println("TC014_Guest_Landing_to_PrivacyPolicy: Execution Started");
	  Exttest = Extreport.startTest("TC014_Guest_Landing_to_PrivacyPolicy","A guest at the Landing page goes to the Privacy Policy page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  Assert.fail("Successfully Launched With Title :"+Driver.getTitle());
	  }
	 
	  WebElement lnPrivacyPolicy= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Privacy Policy']");
	  if(lnPrivacyPolicy!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", lnPrivacyPolicy);
		  //btnRegisterYourFree.click();
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Privacy Policy from footer", "Successfully Clicked on the link Privacy Policy from footer", "No");
		  System.out.println("Successfully Clicked on the link Privacy Policy from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Privacy Policy from footer", "Failed To Click On  the link Privacy Policy from footer", "No");   
		  System.out.println("Failed To Click On  the link Privacy Policy from footer"); 
		  Assert.fail("Failed To Click On  the link Privacy Policy from footer");
		   }
	  
	  try{
		  Thread.sleep(10000);
	  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
	  Driver.switchTo().window(tabs2.get(1));
	  if((Driver.getCurrentUrl().contains("/en/privacypolicy"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The Privacy Policy Page Has opened", "Successfully verified the Privacy Policy page with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully verified the Privacy Policy page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Privacy Policy Page Has opened", "Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl());
		  Assert.fail("Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  Driver.close();
	  Driver.switchTo().window(tabs2.get(0)); 
	  }catch(Exception e){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Privacy Policy Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Privacy Policy", "No");
		  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
		  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
	  }
	  System.out.println("TC014_Guest_Landing_to_PrivacyPolicy : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
}

public void TC015_Guest_Landing_to_Blog(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  System.out.println("TC015_Guest_Landing_to_Blog: Execution Started");
	  Exttest = Extreport.startTest("TC015_Guest_Landing_to_Blog","A guest at the Landing page goes to the Blog page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  Assert.fail("Failed To Launch And The title was"+Driver.getTitle());
	  }
	 
	  WebElement lnBlog= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Blog']");
	  if(lnBlog!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", lnBlog);
		  //btnRegisterYourFree.click();
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Blog from footer", "Successfully Clicked on the link Blog from footer", "No");
		  System.out.println("Successfully Clicked on the Blog Policy from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Blog from footer", "Failed To Click On  the link Blog from footer", "No");   
		  System.out.println("Failed To Click On  the link Blog from footer"); 
		  Assert.fail("Failed To Click On  the link Blog from footer");
		   }
	  
	  Genf.gfSleep(5);
	  if((Driver.getCurrentUrl().contains("blog.starzplay"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The Blog Page Has opened", "Successfully verified the Blog page with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully verified the Blog page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Blog Page Has opened", "Failed To Verfiy the Blog page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Failed To Verfiy the Blog page, And the URL is :"+Driver.getCurrentUrl());
		  Assert.fail("Failed To Verfiy the Blog page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  
	  
	  System.out.println("TC015_Guest_Landing_to_Blog : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
}

public void TC016_Guest_Landing_to_Careers(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  System.out.println("TC016_Guest_Landing_to_Careers: Execution Started");
	  Exttest = Extreport.startTest("TC016_Guest_Landing_to_Careers","A guest at the Landing page goes to the Careers page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())&&(Driver.getCurrentUrl().contains("error")!=true)){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl()); 
		  Assert.fail("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl());
	  }
	 
	  WebElement lnCareers= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Careers']");
	  if(lnCareers!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", lnCareers);
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Careers from footer", "Successfully Clicked on the link Careers from footer", "No");
		  System.out.println("Successfully Clicked on the link Careers from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Careers from footer", "Failed To Click On  the link Careers from footer", "No");   
		  System.out.println("Failed To Click On the link Careers from footer"); 
		  Assert.fail("Failed To Click On the link Careers from footer");
		   }
	  
	  try{
		  Thread.sleep(5000);
	  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
	  Driver.switchTo().window(tabs2.get(1));
	  if((Driver.getCurrentUrl().contains("starzplayarabia.bamboohr"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The Career Page Has opened", "Successfully verified the Career page with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully verified the Career page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Career Page Has opened", "Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl());
		  Assert.fail("Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  Driver.close();
	  Driver.switchTo().window(tabs2.get(0)); 
	  }catch(Exception e){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Career Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Career", "No");
		  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Career");
		  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Career");
	  }
	  System.out.println("TC016_Guest_Landing_to_Careers : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
}

public void TC017_Guest_Landing_to_FaceBook(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  System.out.println("TC017_Guest_Landing_to_FaceBook: Execution Started");
	  Exttest = Extreport.startTest("TC017_Guest_Landing_to_FaceBook","A guest at Landing page goes to the STARZ PLAY Facebook page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())&&(Driver.getCurrentUrl().contains("error")!=true)){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl()); 
		  Assert.fail("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl());
	  }
	 
	  WebElement ImgFacebook= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialFacebook']/span");
	  if(ImgFacebook!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", ImgFacebook);
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Facebook icon from footer", "Successfully Clicked on the Facebook icon from footer", "No");
		  System.out.println("Successfully Clicked on the Facebook icon from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Facebook icon from footer", "Failed To Click On  the Facebook icon from footer", "No");   
		  System.out.println("Failed To Click On  the Facebook icon from footer"); 
		  Assert.fail("Failed To Click On  the Facebook icon from footer");
		   }
	  
	  try{
		  Thread.sleep(5000);
	  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
	  Driver.switchTo().window(tabs2.get(1));
	  if((Driver.getCurrentUrl().contains("facebook.com/starzplayarabia"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The Facebook Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Facebook Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
		  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  Driver.close();
	  Driver.switchTo().window(tabs2.get(0)); 
	  }catch(Exception e){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "No");
		  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
		  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
	  }
	  System.out.println("TC017_Guest_Landing_to_FaceBook : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
}

public void TC018_Guest_Landing_to_Twitter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  System.out.println("TC018_Guest_Landing_to_Twitter: Execution Started");
	  Exttest = Extreport.startTest("TC018_Guest_Landing_to_Twitter","A guest at Landing page goes to the STARZ PLAY Twitter page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())&&(Driver.getCurrentUrl().contains("error")!=true)){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl()); 
		  Assert.fail("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl());
	  }
	 
	  WebElement ImgTwitter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialTwitter']/span");
	  if(ImgTwitter!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", ImgTwitter);
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Twitter icon from footer", "Successfully Clicked on the Twitter icon from footer", "No");
		  System.out.println("Successfully Clicked on the Twitter icon from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Twitter icon from footer", "Failed To Click On  the Twitter icon from footer", "No");   
		  System.out.println("Failed To Click On  the Twitter icon from footer"); 
		  Assert.fail("Successfully Clicked on the Twitter icon from footer");
		   }
	  
	  try{
		  Thread.sleep(5000);
	  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
	  Driver.switchTo().window(tabs2.get(1));
	  if((Driver.getCurrentUrl().contains("twitter.com/starzplayarabia"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The Twitter Page Has opened", "Successfully verified the Twitter page with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully verified the Twitter page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Twitter Page Has opened", "Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl());
		  Assert.fail("Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  Driver.close();
	  Driver.switchTo().window(tabs2.get(0)); 
	  }catch(Exception e){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Twitter Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter", "No");
		  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter");
		  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter");
	  }
	  System.out.println("TC018_Guest_Landing_to_Twitter : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
}

public void TC019_Guest_Landing_to_Instagram(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility(); 
	  Genf = new GeneralMethods();
	  System.out.println("TC019_Guest_Landing_to_Instagram: Execution Started");
	  Exttest = Extreport.startTest("TC019_Guest_Landing_to_Instagram","A guest at Landing page goes to the STARZ PLAY Instagram page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())&&(Driver.getCurrentUrl().contains("error")!=true)){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl()); 
		  Assert.fail("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl());
	  }
	 
	  WebElement ImgInstagram= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialInstagram']/span");
	  if(ImgInstagram!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", ImgInstagram);
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Instagram icon from footer", "Successfully Clicked on the Instagram icon from footer", "No");
		  System.out.println("Successfully Clicked on the Instagram icon from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Instagram icon from footer", "Failed To Click On  the Instagram icon from footer", "No");   
		  System.out.println("Failed To Click On  the Instagram icon from footer"); 
		  Assert.fail("Failed To Click On  the Instagram icon from footer");
		   }
	  
	  try{
		  Thread.sleep(5000);
	  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
	  Driver.switchTo().window(tabs2.get(1));
	  if((Driver.getCurrentUrl().contains("instagram.com/starzplayarabia/"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The Instagram Page Has opened", "Successfully verified the Instagram page with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully verified the Instagram page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Instagram Page Has opened", "Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
		  Assert.fail("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  Driver.close();
	  Driver.switchTo().window(tabs2.get(0)); 
	  }catch(Exception e){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Instagram Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram", "No");
		  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
		  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
	  }
	  System.out.println("TC019_Guest_Landing_to_Instagram : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
}

public void TC020_Guest_Landing_to_YouTube(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility(); 
	  Genf = new GeneralMethods();
	  System.out.println("TC020_Guest_Landing_to_YouTube: Execution Started");
	  Exttest = Extreport.startTest("TC020_Guest_Landing_to_YouTube","A guest at Landing page goes to the STARZ PLAY YouTube page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())&&(Driver.getCurrentUrl().contains("error")!=true)){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl()); 
		  Assert.fail("Failed To Launch And The title was"+Driver.getTitle() +" And The url is "+Driver.getCurrentUrl());
	  }
	 
	  WebElement ImgInstagram= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialYoutube']/span");
	  if(ImgInstagram!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", ImgInstagram);
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Youtube icon from footer", "Successfully Clicked on the Youtube icon from footer", "No");
		  System.out.println("Successfully Clicked on the Youtube icon from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Youtube icon from footer", "Failed To Click On  the Youtube icon from footer", "No");   
		  System.out.println("Failed To Click On  the Youtube icon from footer"); 
		  //Assert.fail("Failed To Click On  the Instagram icon from footer");
		   }
	  
	  try{
		  Thread.sleep(5000);
	  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
	  Driver.switchTo().window(tabs2.get(1));
	  if((Driver.getCurrentUrl().contains("youtube"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The Youtube Page Has opened", "Successfully verified the Youtube page with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully verified the Youtube page with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Youtube Page Has opened", "Failed To Verfiy the Youtube page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Failed To Verfiy the Youtube page, And the URL is :"+Driver.getCurrentUrl());
		 //Assert.fail("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
	  }
	  Driver.close();
	  Driver.switchTo().window(tabs2.get(0)); 
	  }catch(Exception e){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Youtube Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Youtube", "No");
		  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Youtube");
		  //Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
	  }
	  System.out.println("TC020_Guest_Landing_to_YouTube : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
}

public void TC021_Guest_Landing_to_AppStore_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  System.out.println("TC021_Guest_Landing_to_AppStore_FromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC021_Guest_Landing_to_AppStore_FromFooter","A guest at Landing page goes to the App Store page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
	  }
	 
	  WebElement imgDwnldOnAppStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='Download on the App Store']");
	  if(imgDwnldOnAppStore!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", imgDwnldOnAppStore);
		  //btnRegisterYourFree.click();
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Download on the App Store from Footer", "Successfully Clicked on the button Download on the App Store from Footer", "No");
		  System.out.println("Successfully Clicked on the button Download on the App Store from footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Download on the App Store from footer", "Failed To Click On  the button Download on the App Store from footer", "No");   
		  System.out.println("Failed To Click On  the button Download on the App Store from footer"); 
		   }
	  
	  try{
		  Thread.sleep(5000);
	  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
	  Driver.switchTo().window(tabs2.get(1));
	  if((Driver.getCurrentUrl().contains("itunes.apple.com/ae/app/starzplay-arabia"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the Appstore page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed to verify the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());
	  }
	  Driver.close();
	  Driver.switchTo().window(tabs2.get(0)); 
	  }catch(Exception e){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Appstore", "No");
		  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
		  
	  }
	  
	  System.out.println("TC021_Guest_Landing_to_AppStore_FromFooter : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
}


public void TC022_Guest_Landing_to_GooglePlayStore_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  System.out.println("TC022_Guest_Landing_to_GooglePlayStore_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC022_Guest_Landing_to_GooglePlayStore_fromFooter","A guest at Landing page goes to the Google Play Store page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
	  }
	 
	  WebElement imgDwnldOnPlayStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='Get it on Google play']");
	  if(imgDwnldOnPlayStore!=null){
		  JavascriptExecutor js = (JavascriptExecutor)Driver;
		  js.executeScript("arguments[0].click();", imgDwnldOnPlayStore);
		  //btnRegisterYourFree.click();
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On GET IT ON GOOGLE PLAY Store From Footer", "Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer", "No");
		  System.out.println("Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer");   
	  }else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On GET IT ON GOOGLE PLAY Store From Footer", "Failed To Click On  the button GET IT ON GOOGLE PLAY From Footer", "No");   
		  System.out.println("Failed To Click On  the button Download on the App Store From Footer"); 
		   }
	  
	  try{
		  Thread.sleep(5000);
	  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
	  Driver.switchTo().window(tabs2.get(1));
	  if((Driver.getCurrentUrl().contains("com.parsifal.starz"))){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
		  System.out.println("Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the play store page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
		  System.out.println("Failed to verify the play store page for STARZ PLAY ARABIA the URL is "+Driver.getCurrentUrl());
	  }
	  Driver.close();
	  Driver.switchTo().window(tabs2.get(0)); 
	  }catch(Exception e){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Verify The play store for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for play store", "No");
		  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for play store");
		  
	  }
	  
	  System.out.println("TC022_Guest_Landing_to_GooglePlayStore_fromFooter : Execution Finished");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  Driver.close();
}

public void TC023_User_Landing_to_WhyStarzPlay_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC023_User_Landing_to_WhyStarzPlay_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC023_User_Landing_to_WhyStarzPlay_fromFooter","A User at Landing page goes to the Why STARZ PLAY? page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyWhyStarzPlayFromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC023_User_Landing_to_WhyStarzPlay_fromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	   
}

public void TC024_User_Landing_to_Help_Center_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC024_User_Landing_to_Help_Center_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC024_User_Landing_to_Help_Center_fromFooter","A User at Landing page goes to the Help Center page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyHelpCenter_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC024_User_Landing_to_Help_Center_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC025_User_Landing_to_Partener_WithUS_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC025_User_Landing_to_Partener_WithUS_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC025_User_Landing_to_Partener_WithUS_fromFooter","A User at the Landing page goes to the Partner with us page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyPartnerWithUs_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC025_User_Landing_to_Partener_WithUS_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC026_User_Landing_to_Company_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC026_User_Landing_to_Company_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC026_User_Landing_to_Company_fromFooter","A User at the Landing page goes to the Company page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyCompanyPage_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC026_User_Landing_to_Company_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC027_User_Landing_to_TermsAndConditions_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC027_User_Landing_to_TermsAndConditions_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC027_User_Landing_to_TermsAndConditions_fromFooter","A User at the Landing page goes to the Terms & Conditions page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyTermsAndAconditions_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC027_User_Landing_to_TermsAndConditions_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC028_User_Landing_to_PrivacyPolicy_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC028_User_Landing_to_PrivacyPolicy_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC028_User_Landing_to_PrivacyPolicy_fromFooter","A User at the Landing page goes to the Blog page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyPrivacyPolicy_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC028_User_Landing_to_PrivacyPolicy_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC029_User_Landing_to_Blog_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC029_User_Landing_to_Blog_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC029_User_Landing_to_Blog_fromFooter","A User at the Landing page goes to the Blog page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyBlog_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC029_User_Landing_to_Blog_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC030_User_Landing_to_Careers_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC030_User_Landing_to_Careers_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC030_User_Landing_to_Careers_fromFooter","A User at the Landing page goes to the Careers page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyCareers_Footer(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC030_User_Landing_to_Careers_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC031_User_Landing_to_Facebook_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC031_User_Landing_to_Facebook_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC031_User_Landing_to_Facebook_fromFooter","A User at Landing page goes to the STARZ PLAY Facebook page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afverifyFaceBook_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC031_User_Landing_to_Facebook_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC032_User_Landing_to_Twitter_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC032_User_Landing_to_Twitter_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC032_User_Landing_to_Twitter_fromFooter","A User at Landing page goes to the STARZ PLAY Twitter page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyTwitter_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC032_User_Landing_to_Twitter_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC033_User_Landing_to_Instagram_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC033_User_Landing_to_Instagram_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC033_User_Landing_to_Instagram_fromFooter","A User at Landing page goes to the STARZ PLAY Instagram page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyInstagram_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC033_User_Landing_to_Instagram_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC034_User_Landing_to_YouTube_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC034_User_Landing_to_YouTube_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC034_User_Landing_to_YouTube_fromFooter","A User at Landing page goes to the STARZ PLAY YouTube page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyYouTube_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC034_User_Landing_to_YouTube_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC035_User_Landing_to_AppStore_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC035_User_Landing_to_AppStore_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC035_User_Landing_to_AppStore_fromFooter","A User at Landing page goes to the App Store page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyAppStore_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC035_User_Landing_to_AppStore_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}

public void TC036_User_Landing_to_GooglePlayStore_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
{
	  Futil = new FrameWorkUtility();
	  Genf = new GeneralMethods();
	  int StepNum = 1;
	  System.out.println("TC036_User_Landing_to_GooglePlayStore_fromFooter: Execution Started");
	  Exttest = Extreport.startTest("TC036_User_Landing_to_GooglePlayStore_fromFooter","A User at Landing page goes to the Google Play Store page (from the footer)");
	  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
	  if(AppTitle.equals(Driver.getTitle())){
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
		  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  StepNum = StepNum+1;
	  }
	  else{
		  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
		  StepNum = StepNum+1;
	  }
	  
	   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
	   StepNum = afVerifyGooglePlayStore_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
	   
	   System.out.println("TC036_User_Landing_to_GooglePlayStore_fromFooter : Execution Finished");
	   Extreport.flush();
	   Extreport.endTest(Exttest);
	   Driver.close();
	   
}*/

	
}

