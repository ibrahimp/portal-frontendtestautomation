package portalfrontend;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CommonHeaderMethods {
	
	public GeneralMethods Genf;
	public FrameWorkUtility Futil;
	public CommonSignUpMethods CSUpMenthods;
	public CommonMoPMethods CMoPMethods;
	
	public int afVerifyGoToHomeByLogo(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement lblStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='starzplay']");
			  if(lblStarzPlay!=null){
				  lblStarzPlay.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Starz Play logo", "Successfully Clicked on Starz Play logo", "No");
				  System.out.println("Successfully Clicked on Starz Play Logo"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Starz Play logo", "Failed To Clicked on Starz Play logo", "No");   
				  System.out.println("Failed To Clicked on Starz Play logo"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  if(Driver.getCurrentUrl().contains("en/start")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Starz Play Home page", "Successfully Verified the Home Page, The home Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Verified the Home Page With URL :"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Starz Play Home page", "Failed To Verify home page And The URL was"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verify home page And The URL was"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Home Page", "Verification failed for The Page Home", "No");
			  System.out.println("Verification failed for The Page Home");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyMoviesPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement lnkMovies= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='movies-link']");
			  if(lnkMovies!=null){
				  lnkMovies.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Movies Link", "Successfully Clicked on Movies Link", "No");
				  System.out.println("Successfully Clicked on Movies Link"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Movies Link", "Failed To Clicked on Click On Movies Link", "No");   
				  System.out.println("Failed To Clicked on Click On Movies Link"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  if(Driver.getCurrentUrl().contains("en/movies")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Movies page", "Successfully Verified the Movies Page, The Movies Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Verified the movies Page With URL :"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Starz Play Movies page", "Failed To Verify Movies page And The URL was"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verify Movies page And The URL was"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Movies Page", "Verification failed for The Page Movies", "No");
			  System.out.println("Verification failed for The Page Movies");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifySeriesPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement lnkMovies= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='series-link']");
			  if(lnkMovies!=null){
				  lnkMovies.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Series Link", "Successfully Clicked on Series Link", "No");
				  System.out.println("Successfully Clicked on Series Link"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Series Link", "Failed To Clicked on Click On Series Link", "No");   
				  System.out.println("Failed To Clicked on Click On Series Link"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  if(Driver.getCurrentUrl().contains("en/series")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Series page", "Successfully Verified the Series Page, The Series Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Verified the Series Page With URL :"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Starz Play Series page", "Failed To Verify Series page And The URL was"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verify Series page And The URL was"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Series Page", "Verification failed for The Page Series", "No");
			  System.out.println("Verification failed for The Page Series");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyArabicPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement lnkArabic= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='arabic-link']");
			  if(lnkArabic!=null){
				  lnkArabic.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On ARABIC Link", "Successfully Clicked on ARABIC Link", "No");
				  System.out.println("Successfully Clicked on ARABIC Link"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On ARABIC Link", "Failed To Clicked on Click On ARABIC Link", "No");   
				  System.out.println("Failed To Clicked on Click On ARABIC Link"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  if(Driver.getCurrentUrl().contains("en/arabic")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify ARABIC page", "Successfully Verified the ARABIC Page, The ARABIC Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Verified the ARABIC Page With URL :"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Starz Play ARABIC page", "Failed To Verify ARABIC page And The URL was"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verify ARABIC page And The URL was"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The ARABIC Page", "Verification failed for The Page ARABIC", "No");
			  System.out.println("Verification failed for The Page ARABIC");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyKidsPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement lnkKids= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='kids-link']");
			  if(lnkKids!=null){
				  lnkKids.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On KIDS Link", "Successfully Clicked on KIDS Link", "No");
				  System.out.println("Successfully Clicked on KIDS Link"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On KIDS Link", "Failed To Clicked on Click On KIDS Link", "No");   
				  System.out.println("Failed To Clicked on Click On KIDS Link"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  if(Driver.getCurrentUrl().contains("en/kids")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify KIDS page", "Successfully Verified the KIDS Page, The KIDS Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Verified the KIDS Page With URL :"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Starz Play KIDS page", "Failed To Verify KIDS page And The URL was"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verify KIDS page And The URL was"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The KIDS Page", "Verification failed for The Page KIDS", "No");
			  System.out.println("Verification failed for The Page KIDS");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifySearchFields(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement btnSearch= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//li[@id='btn-search']");
			  if(btnSearch!=null){
				  btnSearch.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Search Icon", "Successfully Clicked on Search Icon", "No");
				  System.out.println("Successfully Clicked on Search Icon"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Search Icon", "Failed To Clicked on Click On Search Icon", "No");   
				  System.out.println("Failed To Clicked on Click On Search Icon"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  WebElement edtSearchInputArea= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='input-search']");
			  if(edtSearchInputArea.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Search input area is enabled", "Successfully Verified the Search input area is enabled", "Yes");
				  System.out.println("Successfully Verified the Search input area is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Search input area is enabled", "The search input area is not enabled", "Yes");
				  System.out.println("The search input area is not enabled"); 
				  StepNum = StepNum+1;
			  }
			  String strDefaultText = edtSearchInputArea.getAttribute("placeholder");
			  
			  if(strDefaultText.equals("Titles, people")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The deafault Text Titles, people is displayed", "Successfully Verified the default text Titles, people", "Yes");
				  System.out.println("Successfully Verified the default text Titles, people"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The deafault Text Titles, people is displayed", "The default text Titles, people not displayed", "Yes");
				  System.out.println("The default text Titles, people not displayed"); 
				  StepNum = StepNum+1;
			  }
			  
			  
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Search Fields", "Verification failed for The Search Fields", "No");
			  System.out.println("Verification failed for The Search Fields");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	
	public int afVerifySettingsFields(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  btnSettings.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Settings Icon", "Successfully Clicked on Settings Icon", "No");
				  System.out.println("Successfully Clicked on Settings Icon"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Settings Icon", "Failed To Clicked on Settings Icon", "No");   
				  System.out.println("Failed To Clicked on Click On Settings Icon"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  WebElement lnkSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='dropSettings']");
			  if(lnkSettings.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Settings is enabled", "Successfully Verified the Settings is enabled", "Yes");
				  System.out.println("Successfully Verified the Settings is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Settings is enabled", "The Settings is not enabled", "Yes");
				  System.out.println("The Settings is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  WebElement lnkMyLibrary= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='watchSettings']");
			  if(lnkMyLibrary.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify My Libarary is enabled", "Successfully Verified the My Libarary is enabled", "Yes");
				  System.out.println("Successfully Verified the My Libarary is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify My Libarary is enabled", "The My Libarary is not enabled", "Yes");
				  System.out.println("The My Libarary is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  WebElement lnkLanguage= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='DropdownUser-item-link lang']");
			  if(lnkLanguage.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Language is enabled", "Successfully Verified the Language is enabled", "Yes");
				  System.out.println("Successfully Verified the Language is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Language is enabled", "The Language is not enabled", "Yes");
				  System.out.println("The Language is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  WebElement lnkArabic= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='Controls-item-link language' and @hreflang='ar']");
			  if(lnkArabic.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Arabic is enabled", "Successfully Verified the Arabic is enabled", "Yes");
				  System.out.println("Successfully Verified the Arabic is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Arabic is enabled", "The Arabic is not enabled", "Yes");
				  System.out.println("The Arabic is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  WebElement lnkEN= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='Controls-item-link language' and @hreflang='en']");
			  if(lnkEN.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify EN is enabled", "Successfully Verified the EN is enabled", "Yes");
				  System.out.println("Successfully Verified the EN is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify EN is enabled", "The EN is not enabled", "Yes");
				  System.out.println("The EN is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  WebElement lnkFR= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='Controls-item-link language' and @hreflang='fr']");
			  if(lnkFR.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify FR is enabled", "Successfully Verified the FR is enabled", "Yes");
				  System.out.println("Successfully Verified the FR is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify FR is enabled", "The FR is not enabled", "Yes");
				  System.out.println("The FR is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  WebElement lnkLogout= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='logoutSettings']");
			  if(lnkLogout.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Log out is enabled", "Successfully Verified the Log out is enabled", "Yes");
				  System.out.println("Successfully Log out the Language is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Log out is enabled", "The Log out is not enabled", "Yes");
				  System.out.println("The Log out is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Settings Fields", "Verification failed for The Settings Fields", "No");
			  System.out.println("Verification failed for The Settings Fields");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	
	public int afNavigateToSettingsPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  btnSettings.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Settings Icon", "Successfully Clicked on Settings Icon", "No");
				  System.out.println("Successfully Clicked on Settings Icon"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Settings Icon", "Failed To Clicked on Settings Icon", "No");   
				  System.out.println("Failed To Clicked on Click On Settings Icon"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(2);
			  WebElement lnkSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='dropSettings']");
			  if(lnkSettings.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Settings Dropdown item is enabled", "Successfully Verified the Settings Dropdown item is enabled", "Yes");
				  System.out.println("Successfully Verified the Settings Dropdown item is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Settings Dropdown item is enabled", "The Settings Dropdown item is not enabled", "Yes");
				  System.out.println("The Settings Dropdown item is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  Genf.gfSleep(5);
			  if(lnkSettings.isEnabled()){
				  JavascriptExecutor executor = (JavascriptExecutor)Driver;
				  executor.executeScript("arguments[0].click();", lnkSettings);
				  //lnkSettings.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on Settings Link", "Successfully Clicked on Settings Link", "Yes");
				  System.out.println("Successfully Clicked on Settings Link"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on Settings Link", "Failed to Click on Settings Link", "Yes");
				  System.out.println("Failed to Click on Settings Link"); 
				  StepNum = StepNum+1;
			  }
			  
			  Genf.gfSleep(5);
			  WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			  if(Driver.getCurrentUrl().contains("settings") && iconChangePassword!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Settings page", "Successfully Verified the Settings Page, The Settings Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Verified the Settings Page, The Settings Page loaded with URL :"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Settings page", "Failed To Verify Settings page And The URL was"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verify Settings page And The URL was"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Settings Page", "Verification failed for The Settings Page", "No");
			  System.out.println("Verification failed for The Settings Page");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afNavigateToCreditCardPaymentPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			CSUpMenthods = new CommonSignUpMethods();
			CMoPMethods = new CommonMoPMethods();
			StepNum = afNavigateToSettingsPage( Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnPayment= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changePayment']");
			  if(btnPayment!=null){
				  btnPayment.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Add/Change Payment ", "Successfully Clicked Add/Change Payment", "No");
				  System.out.println("Successfully Clicked on Add/Change Payment"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Add/Change Payment", "Failed To Clicked on Add/Change Payment", "No");   
				  System.out.println("Failed To Clicked on Click On Add/Change Payment"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(3);
			  StepNum = CSUpMenthods.afVerifyRegistrationPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
			  StepNum = CMoPMethods.afMoPCreditCard_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Navigate To Payment Page", "Navigation Failed", "No");
			  System.out.println("Verification failed for The Settings Page");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyMyLibraryPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  btnSettings.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Settings Icon", "Successfully Clicked on Settings Icon", "No");
				  System.out.println("Successfully Clicked on Settings Icon"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Settings Icon", "Failed To Clicked on Settings Icon", "No");   
				  System.out.println("Failed To Clicked on Click On Settings Icon"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(2);
			  WebElement lnkMyLibrary= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='watchSettings']");
			  if(lnkMyLibrary.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify My Libarary is enabled", "Successfully Verified the My Libarary is enabled", "Yes");
				  System.out.println("Successfully Verified the My Libarary is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify My Libarary is enabled", "The My Libarary is not enabled", "Yes");
				  System.out.println("The My Libarary is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  
			  if(lnkMyLibrary.isEnabled()){
				  lnkMyLibrary.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on My Library Link", "Successfully Clicked on My Library Link", "Yes");
				  System.out.println("Successfully Clicked on My Library Link"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on My Library Link", "Failed to Click on My Library Link", "Yes");
				  System.out.println("Failed to Click on My Library Link"); 
				  StepNum = StepNum+1;
			  }
			  
			  Genf.gfSleep(5);
			  
			  if(Driver.getCurrentUrl().contains("watchlist")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify My Library page", "Successfully Verified the My Library Page, The My Library Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Verified the My Library Page, The My Library Page loaded with URL :"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify My Library page", "Failed To Verify My Library page And The URL was"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verify My Library page And The URL was"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The My Library Page", "Verification failed for The My Library Page", "No");
			  System.out.println("Verification failed for The My Library Page");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afChangeLanguageToArabic(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  btnSettings.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Settings Icon", "Successfully Clicked on Settings Icon", "No");
				  System.out.println("Successfully Clicked on Settings Icon"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Settings Icon", "Failed To Clicked on Settings Icon", "No");   
				  System.out.println("Failed To Clicked on Click On Settings Icon"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(2);
			  WebElement lnkArabic= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='Controls-item-link language' and @hreflang='ar']");
			  if(lnkArabic.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Arabic is enabled", "Successfully Verified the Arabic is enabled", "Yes");
				  System.out.println("Successfully Verified the Arabic is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Arabic is enabled", "The Arabic is not enabled", "Yes");
				  System.out.println("The Arabic is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  
			  if(lnkArabic.isEnabled()){
				  lnkArabic.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on AR Link Under Settings", "Successfully Clicked on AR Link", "Yes");
				  System.out.println("Successfully Clicked on AR Link"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on AR Link Under Settings", "Failed to Click on AR Link", "Yes");
				  System.out.println("Failed to Click on AR Link"); 
				  StepNum = StepNum+1;
			  }
			  
			  Genf.gfSleep(5);
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  if(Driver.getCurrentUrl().contains("ar")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The language changed to Arabic", "Successfully Verified the landing page has changes to Arabic, Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully Verified the landing page has changes to Arabic, Page loaded with URL :"+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1;
				  }
				  else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The language changed to Arabic", "Failed To Verify the landing page has changed to Arabic page And The URL was"+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Failed To Verify the landing page has changed to Arabic page And The URL was"+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1;
				  }
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Arabic", "Failed To Load the Arabic home page. The wait message lasted for more than 50 seconds", "Yes");
				  System.out.println("Failed To Load the Arabic home page. The wait message lasted for more than 50 seconds");
				  StepNum = StepNum+1;
			  }
			  
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Landing Page in Arabic", "Verification failed for The Landing Page in Arabic", "No");
			  System.out.println("Verification failed for The Language Changes to Arabic");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	
	public int afChangeLanguageToFrench(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  btnSettings.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Settings Icon", "Successfully Clicked on Settings Icon", "No");
				  System.out.println("Successfully Clicked on Settings Icon"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Settings Icon", "Failed To Clicked on Settings Icon", "No");   
				  System.out.println("Failed To Clicked on Click On Settings Icon"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(2);
			  WebElement lnkFR= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='Controls-item-link language' and @hreflang='fr']");
			  if(lnkFR.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify FR is enabled", "Successfully Verified the FR is enabled", "Yes");
				  System.out.println("Successfully Verified the FR is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify FR is enabled", "The FR is not enabled", "Yes");
				  System.out.println("The FR is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  
			  if(lnkFR.isEnabled()){
				  lnkFR.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on FR Link Under Settings", "Successfully Clicked on FR Link", "Yes");
				  System.out.println("Successfully Clicked on FR Link"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on FR Link Under Settings", "Failed to Click on FR Link", "Yes");
				  System.out.println("Failed to Click on FR Link"); 
				  StepNum = StepNum+1;
			  }
			  
			  Genf.gfSleep(5);
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  if(Driver.getCurrentUrl().contains("fr")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The language changed to French", "Successfully Verified the landing page has changes to French, Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully Verified the landing page has changes to french, Page loaded with URL :"+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1;
				  }
				  else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The language changed to french", "Failed To Verify the landing page has changed to French page And The URL was"+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Failed To Verify the landing page has changed to French page And The URL was"+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1;
				  }
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to French", "Failed To Load the French home page. The wait message lasted for more than 50 seconds", "Yes");
				  System.out.println("Failed To Load the French home page. The wait message lasted for more than 50 seconds");
				  StepNum = StepNum+1;
			  } 
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Landing French Page", "Verification failed for The Landing French Page", "No");
			  System.out.println("Verification failed for The Language Changes to French");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	
	public int afLogout(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  btnSettings.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Settings Icon", "Successfully Clicked on Settings Icon", "No");
				  System.out.println("Successfully Clicked on Settings Icon"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Settings Icon", "Failed To Clicked on Settings Icon", "No");   
				  System.out.println("Failed To Clicked on Click On Settings Icon"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(2);
			  WebElement lnkLogout= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='logoutSettings']");
			  if(lnkLogout.isEnabled()){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Log out is enabled", "Successfully Verified the Log out is enabled", "Yes");
				  System.out.println("Successfully verified Log out is enabled"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Log out is enabled", "The Log out is not enabled", "Yes");
				  System.out.println("The Log out is not enabled or visible"); 
				  StepNum = StepNum+1;
			  }
			  
			  
			  if(lnkLogout.isEnabled()){
				  lnkLogout.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on Log Out Link Under Settings", "Successfully Clicked on Log Out Link", "Yes");
				  System.out.println("Successfully Clicked on Log Out Link"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on Log Out Link Under Settings", "Failed to Click on Log Out Link", "Yes");
				  System.out.println("Failed to Click on Log Out Link"); 
				  StepNum = StepNum+1;
			  }
			  
			  Genf.gfSleep(5);
			  WebElement btnLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//a[@id='login-button']");
			  if(btnLogin!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The User has Loged Out", "Successfully Verified that The user has been logged out, Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Verify The User has Loged Out, Page loaded with URL :"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The User has Loged Out", "User not logged out The URL is"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("User not logged out The URL is"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Logout", "Verification failed for The Logout", "No");
			  System.out.println("Verification failed for The Logout");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}

	
	public int afGuestChangeLanguageTorabic(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement btnLanguageSelector= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageSelector']");
			  if(btnLanguageSelector!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnLanguageSelector);
				  btnLanguageSelector.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Language Selector", "Successfully Clicked on the Language Selector", "No");
				  System.out.println("Successfully Clicked on the Language Selector");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Language Selector", "Failed To Click On Language Selector", "No");   
				  StepNum = StepNum+1;
				   }
			  
			  WebElement lnkArabic= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageAll']/li[1]/a");
			  if(lnkArabic!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnkArabic);
				  //lnkArabic.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Select Arabic", "Successfully Selected The language Arabic", "No");
				  System.out.println("Successfully Selecetd language Arabic");    
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Arabic", "Failed To Select The language arabic", "No");   
				  StepNum = StepNum+1;
				   } 
			  
			  Genf.gfSleep(5);
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  if((Driver.getCurrentUrl().contains("ar"))){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to Arabic", "Successfully Loaded the Arabic page with URL :"+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully Verified the Arabic page"); 
					  StepNum = StepNum+1;
				  }
				  else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Arabic", "Failed To Load the arabic page, And the URL is :"+Driver.getCurrentUrl(), "No");
					  StepNum = StepNum+1;
				  }
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Arabic", "Failed To Load the Arabic home page. The wait message lasted for more than 50 seconds", "Yes");
				  System.out.println("Failed To Load the Arabic home page. The wait message lasted for more than 50 seconds");
				  StepNum = StepNum+1;
			  }
			  
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Landing Page in Arabic", "Verification failed for The Landing Page in Arabic", "No");
			  System.out.println("Verification failed for The Language Changes to Arabic");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afGuestChangeLanguageToFrench(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement btnLanguageSelector= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageSelector']");
			  if(btnLanguageSelector!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnLanguageSelector);
				  btnLanguageSelector.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Language Selector", "Successfully Clicked on the Language Selector", "No");
				  System.out.println("Successfully Clicked on the Language Selector");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Language Selector", "Failed To Click On Language Selector", "No");   
				  System.out.println("Failed To Click On Language Selector"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(2);
			  
			  WebElement lnkFrench= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageAll']/li[3]/a");
			  if(lnkFrench!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnkFrench);
				  //lnkArabic.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Select French", "Successfully Selected The language French", "No");
				  System.out.println("Successfully Selecetd language French");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select French", "Failed To Select The language French", "No");   
				  System.out.println("Failed To Select The language French"); 
				  StepNum = StepNum+1;
				   } 
			  Genf.gfSleep(5);
			  @SuppressWarnings("unused")
			boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  
			  if((Driver.getCurrentUrl().contains("fr"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to French", "Successfully Loaded the French page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Verified the French page");
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to French", "Failed To Load the French page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The French Page", "Verification failed for The French Page", "No");
			  System.out.println("Verification failed for The Language Changes to French");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afGuestChangeLanguageToEnglish(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement btnLanguageSelector= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageSelector']");
			  if(btnLanguageSelector!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnLanguageSelector);
				  btnLanguageSelector.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Language Selector", "Successfully Clicked on the Language Selector", "No");
				  System.out.println("Successfully Clicked on the Language Selector");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Language Selector", "Failed To Click On Language Selector", "No");   
				  System.out.println("Failed To Click On Language Selector"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(2);
			  
			  WebElement lnkEnglish= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageAll']/li[2]/a");
			  if(lnkEnglish!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnkEnglish);
				  //lnkArabic.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Select English", "Successfully Selected The language English", "No");
				  System.out.println("Successfully Selecetd language English");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select English", "Failed To Select The language English", "No");   
				  System.out.println("Failed To Select The language English"); 
				  StepNum = StepNum+1;
				   } 
			  
			  @SuppressWarnings("unused")
			boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  
			  if((Driver.getCurrentUrl().contains("en"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to English", "Successfully Loaded the English page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Verified the English page");
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to English", "Failed To Load the English page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The English Page", "Verification failed for The English Page", "No");
			  System.out.println("Verification failed for The Language Changes to English");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLandingToLogin(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement btnLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//a[@id='login-button']");
			  if(btnLogin!=null){
				  /*JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnLogin);*/
				  btnLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login Button", "Successfully Clicked on the button Login", "No");
				  System.out.println("Successfully Clicked on the button Login");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login Button", "Failed To Click On  Clicked on the button Login", "No");   
				  System.out.println("Failed To Click on the button Login");  
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(3);
			  if(Driver.getCurrentUrl().contains("login")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The login page", "Successfully Verified the Login Page", "Yes");
				  System.out.println("Successfully Verified the Login Page");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login page", "Failed To Verify the Login Page, and the loaded URL is :"+Driver.getCurrentUrl(), "Yes");   
				  System.out.println("Failed To Verify the Login Page");   
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Login Page", "Verification failed for The Login Page, the loaded URL is :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Verification failed for The Login Page, The loaded URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	
	public int afLandingToSignUp(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='signup-button']");
			  if(btnSignUp!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnSignUp);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SignUp Button", "Successfully Clicked on the button SignUp", "No");
				  System.out.println("Successfully Clicked on the button SignUp");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SignUp Button", "Failed To Click On the button SignUp", "No");   
				  System.out.println("Failed To Click One button SignUp");  
				  StepNum = StepNum+1;
				   }
			  Genf.gfSleep(5);
			  WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Sign Up page", "Successfully Verified the Sign Up Page", "Yes");
				  System.out.println("Successfully Verified the Sign Up Page");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Sign Up page", "Failed To Verify the Sign Up Page", "No");   
				  System.out.println("Failed To Verify the Sign Up Page");   
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Sign Up Page", "Verification failed for The Sign Up Page", "No");
			  System.out.println("Verification failed for The Sign Up Page");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
}
