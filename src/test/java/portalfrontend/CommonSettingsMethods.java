package portalfrontend;

import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CommonSettingsMethods {
	
	public GeneralMethods Genf;
	public FrameWorkUtility Futil; 
	public CommonHeaderMethods CHM;
	public CommonLoginMethods CLoginMethods;
	public int afVerifyFirstNameFieldFunctions(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement edtFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='firstName']");
			StepNum = Genf.fVerifyObjectExistance(edtFirstName, "Verify FirstName Field", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(edtFirstName, "New First Name", "Enter First Name", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fVerifyValueFromObject(edtFirstName, "New First Name", "Verify the newly entered first name is saved", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fClearDataFromEditBox(edtFirstName, "Clear data from first name field", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fVerifyValueFromObject(edtFirstName, "", "Verify the first Name field is empty", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The First Name field and its related functionality", "Verification failed", "Yes");
			  System.out.println("Verification failed for The First Name field functinality");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afVerifyLastNameFieldFunctions(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement edtLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='lastName']");
			StepNum = Genf.fVerifyObjectExistance(edtLastName, "Verify Last Name Field", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(edtLastName, "New Last Name", "Enter First Name", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fVerifyValueFromObject(edtLastName, "New Last Name", "Verify the newly entered first name is saved", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fClearDataFromEditBox(edtLastName, "Clear data from Last name field", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fVerifyValueFromObject(edtLastName, "", "Verify the Last Name field is empty", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Last Name field and its related functionality", "Verification failed", "Yes");
			  System.out.println("Verification failed for Last Name field functinality");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditEmail_AllEmpty(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fJSClickOnObject(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Change Email", "There is an error in the form. Please check and resubmit.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email without any data", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email without any data");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int EditEmail_Incorrect_oldEmail_Empty_Email1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fJSClickOnObject(iconChangeEmail, "Click on Change email icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			Genf.gfSleep(5);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, "incorrect@test.com", "Enter Incorrect old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_email_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Incorrect old Email", "The email you have entered does not match the email linked to your account");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with Incorrect_oldEmail_Empty_Email1AndEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with Incorrect_oldEmail_Empty_Email1AndEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int EditEmail_Incorrect_oldEmailAndEmail1_Empty_Email2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			Genf.gfSleep(3);
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, "incorrect@test.com", "Enter Incorrect old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "incorrectNew.test.com", "Enter Incorrect new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_email_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Incorrect old Email", "The email you have entered does not match the email linked to your account");
			WebElement objerrEmailIDNew= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newEmail_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNew, "Incorrect new Email format", "Please enter a valid email address");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with Incorrect_oldEmailAndEmail1_Empty_Email2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with Incorrect_oldEmailAndEmail1_Empty_Email2");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int EditEmail_IncorrectoldEmail_EmptyEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, "incorrect@test.com", "Enter Incorrect old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "incorrectNew.test.com", "Enter Incorrect new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_email_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Incorrect old Email", "The email you have entered does not match the email linked to your account");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "Please enter a valid email address");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with IncorrectoldEmail_EmptyEmail1_IncorrectEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with IncorrectoldEmail_EmptyEmail1_IncorrectEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, "incorrect@test.com", "Enter Incorrect old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "incorrectNew.test.com", "Enter Incorrect new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "incorrectNew.test.com", "Enter Incorrect new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_email_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Incorrect old Email", "The email you have entered does not match the email linked to your account");
			WebElement objerrEmailIDNew= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newEmail_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNew, "Incorrect new Email format", "Please enter a valid email address");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "Please enter a valid email address");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2a");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_IncorrectoldEmail_CorrectEmail1_EmptyEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			Genf.gfSleep(3);
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, "incorrect@test.com", "Enter Incorrect old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "correctNew@test.com", "Enter correct new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Change Email", "There is an error in the form. Please check and resubmit.");
			WebElement objerrEmailID1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_email_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID1, "Incorrect old Email", "The email you have entered does not match the email linked to your account");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "Please enter a valid email address");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with IncorrectoldEmail_CorrectEmail1_EmptyEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with IncorrectoldEmail_CorrectEmail1_EmptyEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int EditEmail_IncorrectoldEmail_EmptyEmail1_correctEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, "incorrect@test.com", "Enter Incorrect old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "correctNew@test.com", "Enter correct new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Change Email", "There is an error in the form. Please check and resubmit.");
			WebElement objerrEmailIDold= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_email_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDold, "Incorrect old Email", "The email you have entered does not match the email linked to your account");
			WebElement objerrEmailIDNew= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newEmail_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNew, "Incorrect new Email format", "Please enter a valid email address");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "The emails do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with IncorrectoldEmail_EmptyEmail1_correctEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with IncorrectoldEmail_EmptyEmail1_correctEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_IncorrectoldEmail_MisMatchingEmail1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, "incorrect@test.com", "Enter Incorrect old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "MismatchingNew@test.com", "Enter correct new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "MismatchingRetype@test.com", "Enter correct new email id format in email2, which is mis matching ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_email_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Incorrect old Email", "The email you have entered does not match the email linked to your account");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "The emails do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with IncorrectoldEmail_MisMatchingEmail1AndEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with IncorrectoldEmail_MisMatchingEmail1AndEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_IncorrectoldEmail_MatchingEmail1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, "incorrect@test.com", "Enter Incorrect old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "matchingNew@test.com", "Enter correct new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "matchingNew@test.com", "Enter correct new email id format in email2, which is mis matching ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_email_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Incorrect old Email", "The email you have entered does not match the email linked to your account");
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with IncorrectoldEmail_MisMatchingEmail1AndEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with IncorrectoldEmail_MatchingEmail1AndEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_Correct_oldEmail_Empty_Email1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			Genf.gfSleep(5);
			String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Change Email", "There is an error in the form. Please check and resubmit.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with Incorrect_oldEmail_Empty_Email1AndEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with Correct_oldEmail_Empty_Email1AndEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int EditEmail_CorrectoldEmail_IncorrectEmail1_Empty_Email2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "incorrectNew.test.com", "Enter Incorrect new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailIDNew= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newEmail_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNew, "Incorrect new Email format", "Please enter a valid email address");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with CorrectoldEmail_IncorrectEmail1_Empty_Email2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with CorrectoldEmail_IncorrectEmail1_Empty_Email2");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int EditEmail_CorrectoldEmail_EmptyEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "incorrectNew.test.com", "Enter Incorrect new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "Please enter a valid email address");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with CorrectoldEmail_EmptyEmail1_IncorrectEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with CorrectoldEmail_EmptyEmail1_IncorrectEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_CorrectoldEmail_IncorrectEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "incorrectNew.test.com", "Enter Incorrect new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "incorrectNew.test.com", "Enter Incorrect new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailIDNew= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newEmail_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNew, "Incorrect new Email format", "Please enter a valid email address");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "Please enter a valid email address");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with CorrectoldEmail_IncorrectEmail1_IncorrectEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with CorrectoldEmail_IncorrectEmail1_IncorrectEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_CorrectoldEmail_CorrectEmail1_EmptyEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "correctNew@test.com", "Enter correct new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Change Email", "There is an error in the form. Please check and resubmit.");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "Please enter a valid email address");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with CorrectoldEmail_CorrectEmail1_EmptyEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with CorrectoldEmail_CorrectEmail1_EmptyEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int EditEmail_CorrectoldEmail_EmptyEmail1_correctEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "correctNew@test.com", "Enter correct new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Change Email", "There is an error in the form. Please check and resubmit.");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "The emails do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with CorrectoldEmail_EmptyEmail1_correctEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with CorrectoldEmail_EmptyEmail1_correctEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_CorrectoldEmail_MisMatchingEmail1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "MismatchingNew@test.com", "Enter correct new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "MismatchingRetype@test.com", "Enter correct new email id format in email2, which is mis matching ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailIDNewRetype= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Mismatching new Emails", "The emails do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with CorrectoldEmail_MisMatchingEmail1AndEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with CorrectoldEmail_MisMatchingEmail1AndEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			String EmailIdNew = Futil.fgetData("Email - Registered", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, EmailIdNew, "Enter already registered email id", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, EmailIdNew, "Enter already registered email id in retype field ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			if(Ready){
				StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailSubmit, "Already Registered new Email", "We have found an account in our system with the email you have entered. If you have previously created an account, please log in with your password or reset your password.");
			}
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2", "Verification failed", "Yes");
			System.out.println("Verification failed for the error message while submitting change email with CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2");
			StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String OldEmail,String NewEmail) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, OldEmail, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			//String EmailIdNew = Futil.fgetData("Email - Registered", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, NewEmail, "Enter new email id", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, NewEmail, "Enter new email id in retype field ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			//WebElement objerrEmailSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			if(Ready){
				WebElement objEmailConfirm= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='confirm_email']");
				Genf.fVerifyValueFromObject(objEmailConfirm, NewEmail, "Verify The New Email Id is saved", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
				//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailSubmit, "Already Registered new Email", "We have found an account in our system with the email you have entered. If you have previously created an account, please log in with your password or reset your password.");
			}else{
				Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Change the email id", " failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
				System.out.println("Change email id failed,the system is very slow. The wait message lasted for more than 50 seconds");
			    StepNum = StepNum+1;
			}
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Change the email id", " failed", "Yes");
			System.out.println("Change email id failed");
			StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			//WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			//StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "incorrectNew.test.com", "Enter Incorrect new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "incorrectNew.test.com", "Enter Incorrect new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			//WebElement objerrEmailIDNew= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newEmail_change']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNew, "Incorrect new Email format", "Please enter a valid email address");
			WebElement objerrEmailIDNewRetype= Genf.setObject(2, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Incorrect new Email Retype format", "Please enter a valid email address");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			//WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			//StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "incorrectNew.test.com", "Enter Incorrect new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "correctNew@test.com", "Enter Correct new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			//WebElement objerrEmailIDNew= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newEmail_change']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNew, "Incorrect new Email format", "Please enter a valid email address");
			WebElement objerrEmailIDNewRetype= Genf.setObject(2, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Correct new Email Retype format", "The emails do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			//WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "incorrectNew.test.com", "Enter Incorrect new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			//StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "correctNew@test.com", "Enter Correct new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Change Email", "There is an error in the form. Please check and resubmit.");
			WebElement objerrEmailIDNew= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newEmail_change']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNew, "Incorrect new Email format", "Please enter a valid email address");
			//WebElement objerrEmailIDNewRetype= Genf.setObject(2, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Correct new Email Retype format", "The emails do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changeemail']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangeEmail, "Click on Change email icon", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement popupChangeEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']");
			StepNum = Genf.fVerifyObjectExistance(popupChangeEmail, "Verify change email popup", "Yes", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(5);
			//WebElement EdtOldEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email_change']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtOldEmail, EmailId, "Enter correct old email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement EdtEmail1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newEmail_change']");
			StepNum = Genf.fEnterDataToEditBox(EdtEmail1, "CorrectNew@test.com", "Enter correct new email id format", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//WebElement EdtEmail2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newemailRetype_change']");
			//StepNum = Genf.fEnterDataToEditBox(EdtEmail2, "correctNew@test.com", "Enter Correct new email id format in email2 ", "No", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changeEmail']//button[@type='submit']");
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemail_submit']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Change Email", "There is an error in the form. Please check and resubmit.");
			//WebElement objerrEmailIDNew= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newEmail_change']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNew, "Incorrect new Email format", "Please enter a valid email address");
			//WebElement objerrEmailIDNewRetype= Genf.setObject(2, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='error_newemailRetype_change']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailIDNewRetype, "Correct new Email Retype format", "The emails do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change email with EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change email with EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2");
			  StepNum = StepNum+1;
		}
		return   StepNum; 
	}
	
	public int afEditPassword_AllEmpty(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			Genf.gfSleep(2);
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "This field is required.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "This field is required.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password without any data", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password without any data");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_EmptyOld_ShortNew_EmptyRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			Genf.gfSleep(2);
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123", "Enter Short password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "This field is required.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with EmptyOdl_ShortNew_EmptyRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with EmptyOdl_ShortNew_EmptyRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_EmptyOdl_EmptyNew_ShortRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			Genf.gfSleep(2);
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123", "Enter Short password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "This field is required.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The passwords do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with EmptyOdl_EmptyNew_ShortRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with EmptyOdl_EmptyNew_ShortRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_EmptyOdl_ShortNew_ShortRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123", "Enter Short password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123", "Enter Short password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			//WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "Please enter at least 6 characters.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with EmptyOdl_ShortNew_ShortRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with EmptyOdl_ShortNew_ShortRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_EmptyOdl_MismatchingNewandRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			//WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123456", "Enter password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123678", "Enter mismatching password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			//WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The passwords do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with EmptyOdl_MismatchingNewandRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with EmptyOdl_MismatchingNewandRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_IncorrectOdl_EmptyNew_EmptyRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			//WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			//WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, "1234561111", "Enter Incorrect old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123456", "Enter password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123678", "Enter mismatching password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "This field is required.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "This field is required.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with IncorrectOdl_EmptyNew_EmptyRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with IncorrectOdl_EmptyNew_EmptyRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_IncorrectOdl_ShortNew_EmptyRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			//WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, "1234561111", "Enter Incorrect old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123", "Enter short password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123678", "Enter mismatching password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "This field is required.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with IncorrectOdl_ShortNew_EmptyRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with IncorrectOdl_ShortNew_EmptyRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_IncorrectOdl_EmptyNew_ShortRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			//WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, "1234561111", "Enter Incorrect old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123", "Enter short password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123", "Enter short password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "This field is required.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The passwords do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with IncorrectOdl_EmptyNew_ShortRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with IncorrectOdl_EmptyNew_ShortRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_IncorrectOdl_ShortNew_ShortRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			Genf.gfSleep(3);
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, "1234561111", "Enter Incorrect old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123", "Enter short password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123", "Enter short password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			//WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The passwords do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with IncorrectOdl_ShortNew_ShortRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with IncorrectOdl_ShortNew_ShortRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_IncorrectOdl_MismatchingNewAndRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, "1234561111", "Enter Incorrect old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123678", "Enter password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123000", "Enter mismatching password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			//WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The passwords do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with IncorrectOdl_MismatchingNewAndRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with IncorrectOdl_MismatchingNewAndRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_CorrectOdl_EmptyNew_EmptyRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			//WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			//WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			String oldPassword = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, oldPassword, "Enter correct old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123456", "Enter password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123678", "Enter mismatching password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "This field is required.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "This field is required.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with CorrectOdl_EmptyNew_EmptyRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with CorrectOdl_EmptyNew_EmptyRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_CorrectOdl_ShortNew_EmptyRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			//WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			String oldPassword = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, oldPassword, "Enter correct old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123", "Enter short password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123678", "Enter mismatching password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "This field is required.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with CorrectOdl_ShortNew_EmptyRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with CorrectOdl_ShortNew_EmptyRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_CorrectOdl_EmptyNew_ShortRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			//WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			String oldPassword = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, oldPassword, "Enter correct old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123", "Enter short password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123", "Enter short password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "This field is required.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The passwords do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with CorrectOdl_EmptyNew_ShortRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with CorrectOdl_EmptyNew_ShortRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_CorrectOdl_ShortNew_ShortRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			String oldPassword = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, oldPassword, "Enter correct old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123", "Enter short password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123", "Enter short password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			//WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The passwords do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with CorrectOdl_ShortNew_ShortRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with CorrectOdl_ShortNew_ShortRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_CorrectOdl_MismatchingNewAndRetype(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			String oldPassword = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, oldPassword, "Enter Incorrect old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, "123678", "Enter password in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, "123000", "Enter mismatching password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			//WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The passwords do not match. Please try again.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with CorrectOdl_MismatchingNewAndRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with CorrectOdl_MismatchingNewAndRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_CorrectOdl_NewAndRetype_SameAsOLD(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			String oldPassword = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, oldPassword, "Enter correct old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, oldPassword, "Enter same old in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, oldPassword, "Enter same old password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			//WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='new-password-is-the-same']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The new password can't be the same as the old one");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with CorrectOdl_MismatchingNewAndRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with CorrectOdl_MismatchingNewAndRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
	
	public int afEditPassword_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String Email,String OldPassword,String NewPassword) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			CHM  = new CommonHeaderMethods();
			CLoginMethods = new CommonLoginMethods();
			WebElement iconChangePassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='button_changepass']");
			StepNum = Genf.fClickOnObjectWithAction(iconChangePassword, "Click on Change Password icon", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(5);
			WebElement EdtOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			WebElement EdtNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword']");
			WebElement EdtRetypeNewPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordRetype']");
			//String oldPassword = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtOldPassword, OldPassword, "Enter correct old password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtNewPassword, NewPassword, "Enter same old in New password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = Genf.fEnterDataToEditBox(EdtRetypeNewPassword, NewPassword, "Enter same old password in Retype password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			WebElement btnSubmit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='changepassword-send']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnSubmit, "Click on Submit", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(4);
			StepNum = CHM.afLogout(Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = CHM.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
			StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation, Email, NewPassword);
			StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
			//WebElement objerrOldPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrOldPassword, "Old Password", "This field is required.");
			//WebElement objerrPassword1= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='newPassword-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword1, "New Password", "Please enter at least 6 characters.");
			//WebElement objerrPassword2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='new-password-is-the-same']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword2, "Retype New Password", "The new password can't be the same as the old one");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message while submitting change password with CorrectOdl_MismatchingNewAndRetype", "Verification failed", "Yes");
			  System.out.println("Verification failed for the error message while submitting change password with CorrectOdl_MismatchingNewAndRetype");
			  StepNum = StepNum+1;
		}
		return   StepNum;  
	}
}

