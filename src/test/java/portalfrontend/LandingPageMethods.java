	package portalfrontend;

import java.net.MalformedURLException;
import java.util.ArrayList;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.security.UserAndPassword;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;


public class LandingPageMethods {
	public GeneralMethods Genf;
	public FrameWorkUtility Futil;
	public CommonFooterMethods CFMethods;
	public CommonHeaderMethods CHMethods;
	public CommonLoginMethods CLoginMethods;
	public CommonSignUpMethods CSignUpMethods;
	public CommonMoPMethods CMoPMethods;
	public void TC001_GuestLandingHome_GoToHome(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC001_GuestLandingHome_GoToHome: Execution Started");
		  Exttest = Extreport.startTest("TC001_GuestLandingHome_GoToHome","This Test case is to verify A guest at the Landing page goes to the Home Page (from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  WebElement lblStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='starzplay']");
		  if(lblStarzPlay!=null){
			  lblStarzPlay.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Starz Play logo", "Successfully Clicked on Starz Play logo", "No");
			  System.out.println("Successfully Clicked on Starz Play Logo");  
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login Button", "Failed To Click On  Clicked on Starz Play logo", "No");   
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		  WebElement lblHero= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='car-hero']");
		  if(lblHero!=null){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Starz Play Home page", "Successfully Verified the Home Page, The home Page for guest loaded with Title :"+Driver.getTitle(), "Yes");
			  System.out.println("Verified the guest Home Page With Title :"+Driver.getTitle());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC001_GuestLandingHome_GoToHome : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC002_GuestLandingHome_GotoExplore(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC002_GuestLandingHome_GotoExplore: Execution Started");
		  Exttest = Extreport.startTest("TC002_GuestLandingHome_GotoExplore","A guest at the Landing page clicks EXPLORE (from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  Genf.gfSleep(5);
		  
		  WebElement lnkExplore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='explore-button']");
		  if(lnkExplore!=null){
			  /*JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", lnkExplore);*/
			  lnkExplore.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Explore", "Successfully Clicked on the Explore", "No");
			  System.out.println("Successfully Clicked on the button Exploe"); 
			  StepNum = StepNum+1;   
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Explore", "Failed To Click On  Explore", "No");   
			  System.out.println("Failed to click on the button explore"); 
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		 
		  Genf.gfSleep(5);
		  if(Driver.getCurrentUrl().contains("en/start")){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Starz Play Home page", "Successfully Verified the Home Page, The home Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Verified the Home Page With URL :"+Driver.getCurrentUrl()); 
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Starz Play Home page", "Failed To Verify home page And The URL was"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Failed To Verify home page And The URL was"+Driver.getCurrentUrl()); 
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC002_GuestLandingHome_GotoExplore : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC003_Guest_Landing_to_Login(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC003_Guest_Landing_to_Login: Execution Started");
		  Exttest = Extreport.startTest("TC003_Guest_Landing_to_Login","A guest at the Landing page goes to the Log In page (from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		 
		  WebElement btnLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//a[@id='login-button']");
		  if(btnLogin!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", btnLogin);
			  //btnLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login Button", "Successfully Clicked on the button Login", "No");
			  System.out.println("Successfully Clicked on the button Login");   
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login Button", "Failed To Click On  Clicked on the button Login", "No");   
			  System.out.println("Failed To Click On  Clicked on the button Login");  
			  StepNum = StepNum+1;
			   }
		  
		  WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
		  if(EdtUserName!=null){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The login page", "Successfully Verified the Login Page", "Yes");
			  System.out.println("Successfully Verified the Login Page");   
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login page", "Failed To Verify the Login Page", "No");   
			  System.out.println("Failed To Verify the Login Page");   
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC003_Guest_Landing_to_Login : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC007_Guest_LanguageTo_Arabic(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC007_Guest_LanguageTo_Arabic: Execution Started");
		  Exttest = Extreport.startTest("TC007_Guest_LanguageTo_Arabic","A guest at the Landing page change language to Arabic");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		
		  Genf.gfSleep(5);
		  
		  WebElement btnLanguageSelector= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageSelector']");
		  if(btnLanguageSelector!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", btnLanguageSelector);
			  //btnLanguageSelector.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Language Selector", "Successfully Clicked on the Language Selector", "No");
			  System.out.println("Successfully Clicked on the Language Selector");  
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Language Selector", "Failed To Click On Language Selector", "No");   
			  StepNum = StepNum+1;
			   }
		  
		  WebElement lnkArabic= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageAll']/li[1]/a");
		  if(lnkArabic!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", lnkArabic);
			  //lnkArabic.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Select Arabic", "Successfully Selected The language Arabic", "No");
			  System.out.println("Successfully Selecetd language Arabic");   
			  StepNum = StepNum+1;
		      }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Arabic", "Failed To Select The language arabic", "No");   
			  StepNum = StepNum+1;
			   } 
		  
		  Genf.gfSleep(5);
		  
		  @SuppressWarnings("unused")
		WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
		  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
		  if(Ready){
			  if((Driver.getCurrentUrl().contains("ar"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to Arabic", "Successfully Loaded the Arabic home page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Verified the Arabic home page"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Arabic", "Failed To Load the arabic home page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  StepNum = StepNum+1;
			  }
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Arabic", "Failed To Load the Arabic home page. The wait message lasted for more than 50 seconds", "Yes");
			  System.out.println("Failed To Load the Arabic home page. The wait message lasted for more than 50 seconds");
			  StepNum = StepNum+1;
		  }
		  System.out.println("TC007_Guest_LanguageTo_Arabic : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC008_Guest_LanguageTo_French(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC008_Guest_LanguageTo_French: Execution Started");
		  Exttest = Extreport.startTest("TC008_Guest_LanguageTo_French","A guest at the Landing page change language to French");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		 
		  Genf.gfSleep(5);
		  
		  WebElement btnLanguageSelector= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageSelector']");
		  if(btnLanguageSelector!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", btnLanguageSelector);
			  //btnLanguageSelector.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Language Selector", "Successfully Clicked on the Language Selector", "No");
			  System.out.println("Successfully Clicked on the Language Selector");   
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Language Selector", "Failed To Click On Language Selector", "No");   
			  System.out.println("Failed To Click On Language Selector"); 
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		  
		  WebElement lnkFrench= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='languageAll']/li[3]/a");
		  if(lnkFrench!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", lnkFrench);
			  //lnkArabic.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Select French", "Successfully Selected The language French", "No");
			  System.out.println("Successfully Selecetd language French");    
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select French", "Failed To Select The language French", "No");   
			  System.out.println("Failed To Select The language French"); 
			  StepNum = StepNum+1;
			   } 
		  
		  Genf.gfSleep(5);
		  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
		  if(Ready){
			  if((Driver.getCurrentUrl().contains("fr"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to French", "Successfully Loaded the French home page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Verified the French home page"); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to French", "Failed To Load the French home page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  StepNum = StepNum+1;
				  System.out.println("Failed To Load the French home page, And the URL is :"+Driver.getCurrentUrl());
			  }
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to French", "Failed To Load the French home page. The wait message lasted for more than 50 seconds", "Yes");
			  System.out.println("Failed To Load the French home page. The wait message lasted for more than 50 seconds");
			  StepNum = StepNum+1;
		  }
		  
		  
		  System.out.println("TC008_Guest_LanguageTo_French : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC004_Guest_Landing_to_StartFreePeriod(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC004_Guest_Landing_to_StartFreePeriod: Execution Started");
		  Exttest = Extreport.startTest("TC004_Guest_Landing_to_StartFreePeriod","A guest at Landing page clicks START YOUR FREE PERIOD (from the bottom)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  WebElement btnRegisterYourFree= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='main']/div/a[text()='START YOUR FREE TRIAL']");
		  if(btnRegisterYourFree!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", btnRegisterYourFree);
			  //btnRegisterYourFree.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On START YOUR FREE MONTH Button from Bottom", "Successfully Clicked on the button START YOUR FREE MONTH from Bottom", "No");
			  System.out.println("Successfully Clicked on the button START YOUR FREE MONTH from Bottom"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On START YOUR FREE MONTH Button from Bottom", "Failed To Click On  button START YOUR FREE MONTH from Bottom", "No");   
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		  
		  if((Driver.getCurrentUrl().contains("/registration/userinfo"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to Regstration Page", "Successfully Loaded the Registration page with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully Verified the Registration Page");  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Regstration Page", "Failed To Load the Registration page page, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed to verify the registration Page the URL is "+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC004_Guest_Landing_to_StartFreePeriod : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC005_Guest_Landing_to_AppStore(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC005_Guest_Landing_to_AppStore: Execution Started");
		  Exttest = Extreport.startTest("TC005_Guest_Landing_to_AppStore","A guest at Landing page goes to the App Store page (from the body in the landing page)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  //WebElement imgDwnldOnAppStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[text()='Watch on all your screens ']//ancestor::div[1]//img[@alt='Download on the App Store']");
		  WebElement imgDwnldOnAppStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='amazing']//img[@alt='Download on the App Store']");
		  if(imgDwnldOnAppStore!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", imgDwnldOnAppStore);
			  //btnRegisterYourFree.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Download on the App Store Under Watch on all your screens", "Successfully Clicked on the button Download on the App Store from Watch on all your screens", "No");
			  System.out.println("Successfully Clicked on the button Download on the App Store from Watch on all your screens");  
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Download on the App Store Under Watch on all your screens", "Failed To Click On  the button Download on the App Store from Watch on all your screens", "No");   
			  System.out.println("Failed To Click On  the button Download on the App Store from Watch on all your screens"); 
			  StepNum = StepNum+1;
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("itunes.apple.com/ae/app/starzplay-arabia"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl()); 
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the Appstore page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed to verify the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Appstore", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
			  StepNum = StepNum+1;
			  
		  }
		  
		  System.out.println("TC005_Guest_Landing_to_AppStore : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC006_Guest_Landing_to_GooglePlayStore(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC006_Guest_Landing_to_GooglePlayStore: Execution Started");
		  Exttest = Extreport.startTest("TC006_Guest_Landing_to_GooglePlayStore","A guest at Landing page goes to the Google Play Store page (from the body in the landing page)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  //WebElement imgDwnldOnPlayStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[text()='Watch on all your screens ']//ancestor::div[1]//img[@alt='Get it on Google play']");
		  WebElement imgDwnldOnPlayStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='amazing']//img[@alt='Get it on Google play']");
		  if(imgDwnldOnPlayStore!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", imgDwnldOnPlayStore);
			  //btnRegisterYourFree.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store Under Watch on all your screens", "Successfully Clicked on the button GET IT ON GOOGLE PLAY from Watch on all your screens", "No");
			  System.out.println("Successfully Clicked on the button GET IT ON GOOGLE PLAY from Watch on all your screens"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store Under Watch on all your screens", "Failed To Click On  the button GET IT ON GOOGLE PLAY from Watch on all your screens", "No");   
			  System.out.println("Failed To Click On  the button Download on the App Store from Watch on all your screens"); 
			  StepNum = StepNum+1;
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("com.parsifal.starz"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Play Store for Starzplay Arabia Has opened", "Successfully verified the Play Store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Play Store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl()); 
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Play Store for Starzplay Arabia Has opened", "Failed To Verfiy the Play Store page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed to verify the Play Store page for STARZ PLAY ARABIA the URL is "+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Play Store", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Play Store");
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC006_Guest_Landing_to_GooglePlayStore : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC009_Guest_Landing_to_WhyStarzPlay(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC009_Guest_Landing_to_WhyStarzPlay: Execution Started");
		  Exttest = Extreport.startTest("TC009_Guest_Landing_to_WhyStarzPlay","A guest at Landing page goes to the Why STARZ PLAY? page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		 
		  WebElement btnWhyStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Why STARZ PLAY?']");
		  if(btnWhyStarzPlay!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", btnWhyStarzPlay);
			  //btnLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Why STARZ PLAY? from footer", "Successfully Clicked on Why STARZ PLAY? from footer", "No");
			  System.out.println("Successfully Clicked on Why STARZ PLAY? from footer");   
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Why STARZ PLAY? from footer", "Failed To Click On Clicked on Why STARZ PLAY? from footer", "No");   
			  System.out.println("Failed To Click On Clicked on Why STARZ PLAY? from footer");
			  StepNum = StepNum+1;
			   }
		  Genf.gfSleep(5);
		  
		  if((Driver.getCurrentUrl().contains("whyStarzPlay"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to whyStarzPlay", "Successfully Loaded the whyStarzPlay page with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully Loaded the whyStarzPlay page with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to whyStarzPlay", "Failed To Load the Why STARZ PLAY page, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed To Load the Why STARZ PLAY page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  
		  /*WebElement lblWhyStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[text()='WHY STARZ PLAY?']");
		  if(lblWhyStarzPlay!=null){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The WHY STARZ PLAY? Label", "Successfully Verified the WHY STARZ PLAY? label", "Yes");
			  System.out.println("Successfully Verified the Why Starz Play Page");   
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The WHY STARZ PLAY? Label", "Failed To Verified the WHY STARZ PLAY? label", "No");   
			  System.out.println("Failed To Verified the WHY STARZ PLAY? label"); 
			  StepNum = StepNum+1;
		  }*/
		  
		  System.out.println("TC009_Guest_Landing_to_WhyStarzPlay : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
		  
	}
	
	public void TC010_Guest_Landing_to_Help_Center(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC010_Guest_Landing_to_Help_Center: Execution Started");
		  Exttest = Extreport.startTest("TC010_Guest_Landing_to_Help_Center","A guest at Landing page goes to the Help Center page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		 
		  WebElement btnHelpCenter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Help Center']");
		  if(btnHelpCenter!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", btnHelpCenter);
			  //btnLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Help Center from footer", "Successfully Clicked on Help Center from footer", "No");
			  System.out.println("Successfully Clicked on Help Center from footer"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Help Center from footer", "Failed To Click On Clicked on Help Center from footer", "No");   
			  System.out.println("Failed To Click On Clicked on Help Center from footer"); 
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		  try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
		  if((Driver.getCurrentUrl().contains("faq"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to faq", "Successfully Loaded the faq page with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully Loaded the Help Center page with URL :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to faq", "Failed To Load the faq page, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed To Load the Why Help Center page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  
		  WebElement lblHelpCenter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//h1[text()='Help Center']");
		  if(lblHelpCenter!=null){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The HELP CENTER Heading Label", "Successfully Verified the HELP CENTER Heading label", "Yes");
			  System.out.println("Successfully Verified the HELP CENTER Heading label");   
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The HELP CENTER Heading Label", "Failed To Verified the HELP CENTER Heading label", "No");   
			  System.out.println("Failed To Verified the HELP CENTER Heading label"); 
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Launch And The URL was"+Driver.getCurrentUrl());
		  }
		  
		  System.out.println("TC010_Guest_Landing_to_Help_Center : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
		  
	}
	
	public void TC011_Guest_Landing_to_PartnerWithUs(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  
		  System.out.println("TC011_Guest_Landing_to_PartnerWithUs: Execution Started");
		  Exttest = Extreport.startTest("TC011_Guest_Landing_to_PartnerWithUs","A guest at the Landing page goes to the Partner with us page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  WebElement lnPartnerWithUs= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Partner with us']");
		  if(lnPartnerWithUs!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", lnPartnerWithUs);
			  //btnLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Partner with us from footer", "Successfully Clicked on Partner with us from footer", "No");
			  System.out.println("Successfully Clicked on Partner with us from footer");  
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Partner with us from footer", "Failed To Click Partner with us from footer", "No");   
			  System.out.println("Failed To Click On Partner with us from footer"); 
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Click On Partner with us from footer");
			   }
		  
		  Genf.gfSleep(5);
		  
		  if((Driver.getCurrentUrl().contains("/en/affiliatesubscription"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to Partner with us", "Successfully Loaded the Partner with us page with URL :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Successfully Loaded the Partner with us page with URL :"+Driver.getCurrentUrl()); 
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Partner with us", "Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl());
		  }
		  
		  System.out.println("TC011_Guest_Landing_to_PartnerWithUs : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
		  
	}
	
	public void TC012_Guest_Landing_to_Company(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  
		  System.out.println("TC012_Guest_Landing_to_Company: Execution Started");
		  Exttest = Extreport.startTest("TC012_Guest_Landing_to_Company","A guest at the Landing page goes to the Company page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		 
		  WebElement lnCompany= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Company']");
		  if(lnCompany!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", lnCompany);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Company from footer", "Successfully Clicked on Company from footer", "No");
			  System.out.println("Successfully Clicked on Company from footer");   
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Company from footer", "Failed To Click Company from footer", "Yes");   
			  System.out.println("Failed To Click On Company from footer"); 
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Click On Company from footer");
			   }
		  
		  Genf.gfSleep(5);
		  
		  if((Driver.getCurrentUrl().contains("/en/aboutus"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to company aboutus", "Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to company aboutus", "Failed To Load the aboutus page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Failed To Load the aboutus page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl());
		  }
		  
		  System.out.println("TC012_Guest_Landing_to_Company : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC013_Guest_Landing_to_TermsAndAconditions(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  int windowNum;
		  System.out.println("TC013_Guest_Landing_to_TermsAndAconditions: Execution Started");
		  Exttest = Extreport.startTest("TC013_Guest_Landing_to_TermsAndAconditions","A guest at the Landing page goes to the Terms & Conditions page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, "HtmlUnit", AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String winHandleBefore = Driver.getWindowHandle();
		  WebElement lnTermsNCond= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Terms & Conditions']");
		  if(lnTermsNCond!=null){
			  lnTermsNCond.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Terms & Conditions from footer", "Successfully Clicked on Terms & Conditions from footer", "No");
			  System.out.println("Successfully Clicked on Terms & Conditions from footer");  
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Terms & Conditions from footer", "Failed To Click Terms & Conditions from footer", "No");   
			  System.out.println("Failed To Click Terms & Conditions from footer"); 
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Click Terms & Conditions from footer");
			   }
		  Genf.gfSleep(5);
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  for(windowNum=0;windowNum<tabs2.size();windowNum++){
			  if(tabs2.get(windowNum).equals(winHandleBefore)){}
			  else{
				  Driver.switchTo().window(tabs2.get(windowNum));
			  }
		  }
		  WebElement lblTermsAndCond= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//h1[text()='Terms & Conditions']");
		  if((Driver.getCurrentUrl().contains("/en/termsandconditions"))&& (lblTermsAndCond!=null)){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to Terms & Conditions", "Successfully Loaded the Terms & Conditions page with URL :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Successfully Loaded the Terms & Conditions page with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Terms & Conditions", "Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl());
		  }
		  Driver.close();
		  Driver.switchTo().window(winHandleBefore); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Terms & Conditions", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Terms & Conditions", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Terms & Conditions");
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Terms & Conditions");
			  
		  }
		  System.out.println("TC013_Guest_Landing_to_TermsAndAconditions : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();  
	}
	
	public void TC014_Guest_Landing_to_PrivacyPolicy(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  int windowNum;
		  System.out.println("TC014_Guest_Landing_to_PrivacyPolicy: Execution Started");
		  Exttest = Extreport.startTest("TC014_Guest_Landing_to_PrivacyPolicy","A guest at the Landing page goes to the Privacy Policy page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, "HtmlUnit", AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String winHandleBefore = Driver.getWindowHandle();
		  WebElement lnPrivacyPolicy= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Privacy Policy']");
		  if(lnPrivacyPolicy!=null){
			  lnPrivacyPolicy.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Privacy Policy from footer", "Successfully Clicked on the link Privacy Policy from footer", "No");
			  System.out.println("Successfully Clicked on the link Privacy Policy from footer");   
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Privacy Policy from footer", "Failed To Click On  the link Privacy Policy from footer", "No");   
			  System.out.println("Failed To Click On  the link Privacy Policy from footer"); 
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Click On  the link Privacy Policy from footer");
			   }
		  
		  try{
			  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  for(windowNum=0;windowNum<tabs2.size();windowNum++){
				  if(tabs2.get(windowNum).equals(winHandleBefore)){}
				  else{
					  Driver.switchTo().window(tabs2.get(windowNum));
				  }
			  }
			 
		  if((Driver.getCurrentUrl().contains("/en/privacypolicy"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Privacy Policy Page Has opened", "Successfully verified the Privacy Policy page with URL :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Successfully verified the Privacy Policy page with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy Page Has opened", "Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl());
		  }
		  Driver.close();
		  Driver.switchTo().window(winHandleBefore); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Privacy Policy", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
		  }
		  System.out.println("TC014_Guest_Landing_to_PrivacyPolicy : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC015_Guest_Landing_to_Blog(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC015_Guest_Landing_to_Blog: Execution Started");
		  Exttest = Extreport.startTest("TC015_Guest_Landing_to_Blog","A guest at the Landing page goes to the Blog page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyBlog_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC015_Guest_Landing_to_Blog : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC016_Guest_Landing_to_Careers(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC016_Guest_Landing_to_Careers: Execution Started");
		  Exttest = Extreport.startTest("TC016_Guest_Landing_to_Careers","A guest at the Landing page goes to the Careers page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  WebElement lnCareers= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Careers']");
		  if(lnCareers!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", lnCareers);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Careers from footer", "Successfully Clicked on the link Careers from footer", "No");
			  System.out.println("Successfully Clicked on the link Careers from footer");  
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Careers from footer", "Failed To Click On  the link Careers from footer", "No");   
			  System.out.println("Failed To Click On the link Careers from footer"); 
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Click On the link Careers from footer");
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("starzplayarabia.bamboohr"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Career Page Has opened", "Successfully verified the Career page with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Career page with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Career Page Has opened", "Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl());
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Career Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Career", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Career");
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Career");
		  }
		  System.out.println("TC016_Guest_Landing_to_Careers : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC017_Guest_Landing_to_FaceBook(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC017_Guest_Landing_to_FaceBook: Execution Started");
		  Exttest = Extreport.startTest("TC017_Guest_Landing_to_FaceBook","A guest at Landing page goes to the STARZ PLAY Facebook page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		 
		  WebElement ImgFacebook= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialFacebook']/span");
		  if(ImgFacebook!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", ImgFacebook);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Facebook icon from footer", "Successfully Clicked on the Facebook icon from footer", "No");
			  System.out.println("Successfully Clicked on the Facebook icon from footer");  
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Facebook icon from footer", "Failed To Click On  the Facebook icon from footer", "No");   
			  System.out.println("Failed To Click On  the Facebook icon from footer"); 
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Click On  the Facebook icon from footer");
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("facebook.com/starzplayarabia"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "No");
			  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
		  }
		  System.out.println("TC017_Guest_Landing_to_FaceBook : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC018_Guest_Landing_to_Twitter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC018_Guest_Landing_to_Twitter: Execution Started");
		  Exttest = Extreport.startTest("TC018_Guest_Landing_to_Twitter","A guest at Landing page goes to the STARZ PLAY Twitter page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  WebElement ImgTwitter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialTwitter']/span");
		  if(ImgTwitter!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", ImgTwitter);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Twitter icon from footer", "Successfully Clicked on the Twitter icon from footer", "No");
			  System.out.println("Successfully Clicked on the Twitter icon from footer");
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Twitter icon from footer", "Failed To Click On  the Twitter icon from footer", "No");   
			  System.out.println("Failed To Click On  the Twitter icon from footer"); 
			  Driver.close();
			  Assert.fail("Successfully Clicked on the Twitter icon from footer");
			  StepNum = StepNum+1;
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("twitter.com/starzplayarabia"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Twitter Page Has opened", "Successfully verified the Twitter page with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Twitter page with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Twitter Page Has opened", "Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl());
			  Driver.close();
			  Assert.fail("Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Twitter Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter", "No");
			  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter");
			  Driver.close();
			  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter");
			  StepNum = StepNum+1;
		  }
		  System.out.println("TC018_Guest_Landing_to_Twitter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC019_Guest_Landing_to_Instagram(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility(); 
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC019_Guest_Landing_to_Instagram: Execution Started");
		  Exttest = Extreport.startTest("TC019_Guest_Landing_to_Instagram","A guest at Landing page goes to the STARZ PLAY Instagram page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  WebElement ImgInstagram= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialInstagram']/span");
		  if(ImgInstagram!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", ImgInstagram);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Instagram icon from footer", "Successfully Clicked on the Instagram icon from footer", "No");
			  System.out.println("Successfully Clicked on the Instagram icon from footer"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Instagram icon from footer", "Failed To Click On  the Instagram icon from footer", "No");   
			  System.out.println("Failed To Click On  the Instagram icon from footer"); 
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Click On  the Instagram icon from footer");
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("instagram.com/starzplayarabia/"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Instagram Page Has opened", "Successfully verified the Instagram page with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Instagram page with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Instagram Page Has opened", "Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Instagram Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram", "No");
			  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
		  }
		  System.out.println("TC019_Guest_Landing_to_Instagram : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC020_Guest_Landing_to_YouTube(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility(); 
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC020_Guest_Landing_to_YouTube: Execution Started");
		  Exttest = Extreport.startTest("TC020_Guest_Landing_to_YouTube","A guest at Landing page goes to the STARZ PLAY YouTube page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  WebElement ImgInstagram= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialYoutube']/span");
		  if(ImgInstagram!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", ImgInstagram);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Youtube icon from footer", "Successfully Clicked on the Youtube icon from footer", "No");
			  System.out.println("Successfully Clicked on the Youtube icon from footer"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Youtube icon from footer", "Failed To Click On  the Youtube icon from footer", "No");   
			  System.out.println("Failed To Click On  the Youtube icon from footer"); 
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Click On  the Instagram icon from footer");
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("youtube"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Youtube Page Has opened", "Successfully verified the Youtube page with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Youtube page with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Youtube Page Has opened", "Failed To Verfiy the Youtube page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Failed To Verfiy the Youtube page, And the URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Youtube Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Youtube", "No");
			  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Youtube");
			  StepNum = StepNum+1;
			  Driver.close();
			  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
		  }
		  System.out.println("TC020_Guest_Landing_to_YouTube : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC021_Guest_Landing_to_AppStore_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC021_Guest_Landing_to_AppStore_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC021_Guest_Landing_to_AppStore_FromFooter","A guest at Landing page goes to the App Store page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  WebElement imgDwnldOnAppStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='Download on the App Store']");
		  if(imgDwnldOnAppStore!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", imgDwnldOnAppStore);
			  //btnRegisterYourFree.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Download on the App Store from Footer", "Successfully Clicked on the button Download on the App Store from Footer", "No");
			  System.out.println("Successfully Clicked on the button Download on the App Store from footer"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Download on the App Store from footer", "Failed To Click On  the button Download on the App Store from footer", "No");   
			  System.out.println("Failed To Click On  the button Download on the App Store from footer"); 
			  StepNum = StepNum+1;
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("itunes.apple.com/ae/app/starzplay-arabia"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl()); 
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the Appstore page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed to verify the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Appstore", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
			  StepNum = StepNum+1;
			  
		  }
		  
		  System.out.println("TC021_Guest_Landing_to_AppStore_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC198_Guest_Landing_to_MuleSoft_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TCXXX_Guest_Landing_to_MuleSoft_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TCXXX_Guest_Landing_to_MuleSoft_fromFooter","A guest at Landing page goes to the Mulesoft page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyMuleSoft_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  /*WebElement imgDwnldOnPlayStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='Get it on Google play']");
		  if(imgDwnldOnPlayStore!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", imgDwnldOnPlayStore);
			  //btnRegisterYourFree.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store From Footer", "Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer", "No");
			  System.out.println("Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store From Footer", "Failed To Click On  the button GET IT ON GOOGLE PLAY From Footer", "No");   
			  System.out.println("Failed To Click On  the button Download on the App Store From Footer"); 
			  StepNum = StepNum+1;
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("com.parsifal.starz"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the play store page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed to verify the play store page for STARZ PLAY ARABIA the URL is "+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The play store for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for play store", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for play store");
			  StepNum = StepNum+1;
			  
		  }*/
		  
		  System.out.println("TCXXX_Guest_Landing_to_MuleSoft_fromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC022_Guest_Landing_to_GooglePlayStore_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC022_Guest_Landing_to_GooglePlayStore_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC022_Guest_Landing_to_GooglePlayStore_fromFooter","A guest at Landing page goes to the Google Play Store page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  WebElement imgDwnldOnPlayStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='Get it on Google play']");
		  if(imgDwnldOnPlayStore!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", imgDwnldOnPlayStore);
			  //btnRegisterYourFree.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store From Footer", "Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer", "No");
			  System.out.println("Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store From Footer", "Failed To Click On  the button GET IT ON GOOGLE PLAY From Footer", "No");   
			  System.out.println("Failed To Click On  the button Download on the App Store From Footer"); 
			  StepNum = StepNum+1;
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("com.parsifal.starz"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the play store page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed to verify the play store page for STARZ PLAY ARABIA the URL is "+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The play store for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for play store", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for play store");
			  StepNum = StepNum+1;
			  
		  }
		  
		  System.out.println("TC022_Guest_Landing_to_GooglePlayStore_fromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC267_Guest_Landing_to_SignUp_fromGreatEntertainment(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC267_Guest_Landing_to_SignUp_fromGreatEntertainment: Execution Started");
		  Exttest = Extreport.startTest("TC267_Guest_Landing_to_SignUp_fromGreatEntertainment","Guest at the landing page go to signup page by clicking START YOUR FREE TRIAL from the section Great Entertainment");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afVerifySignUpPage_FromGreatEntertainment(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC267_Guest_Landing_to_SignUp_fromGreatEntertainment : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC268_Guest_Landing_to_SignUp_fromKidsSection(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC268_Guest_Landing_to_SignUp_fromKidsSection: Execution Started");
		  Exttest = Extreport.startTest("TC268_Guest_Landing_to_SignUp_fromKidsSection","Guest at the landing page go to signup page by clicking START YOUR FREE TRIAL  from the section Kids");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afVerifySignUpPage_FromKids(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC268_Guest_Landing_to_SignUp_fromKidsSection : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC023_User_Landing_to_WhyStarzPlay_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC023_User_Landing_to_WhyStarzPlay_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC023_User_Landing_to_WhyStarzPlay_fromFooter","A User at Landing page goes to the Why STARZ PLAY? page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyWhyStarzPlayFromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC023_User_Landing_to_WhyStarzPlay_fromFooter : Execution Finished");
			  Extreport.flush();
			  Extreport.endTest(Exttest);
			  Driver.close();
		   
	}
	
	public void TC024_User_Landing_to_Help_Center_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC024_User_Landing_to_Help_Center_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC024_User_Landing_to_Help_Center_fromFooter","A User at Landing page goes to the Help Center page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyHelpCenter_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC024_User_Landing_to_Help_Center_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC025_User_Landing_to_Partener_WithUS_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC025_User_Landing_to_Partener_WithUS_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC025_User_Landing_to_Partener_WithUS_fromFooter","A User at the Landing page goes to the Partner with us page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		 
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyPartnerWithUs_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC025_User_Landing_to_Partener_WithUS_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC026_User_Landing_to_Company_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC026_User_Landing_to_Company_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC026_User_Landing_to_Company_fromFooter","A User at the Landing page goes to the Company page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyCompanyPage_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC026_User_Landing_to_Company_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC027_User_Landing_to_TermsAndConditions_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC027_User_Landing_to_TermsAndConditions_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC027_User_Landing_to_TermsAndConditions_fromFooter","A User at the Landing page goes to the Terms & Conditions page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, "HtmlUnit", AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyTermsAndAconditions_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC027_User_Landing_to_TermsAndConditions_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC028_User_Landing_to_PrivacyPolicy_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC028_User_Landing_to_PrivacyPolicy_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC028_User_Landing_to_PrivacyPolicy_fromFooter","A User at the Landing page goes to the Blog page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, "HtmlUnit", AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyPrivacyPolicy_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC028_User_Landing_to_PrivacyPolicy_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC029_User_Landing_to_Blog_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC029_User_Landing_to_Blog_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC029_User_Landing_to_Blog_fromFooter","A User at the Landing page goes to the Blog page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyBlog_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC029_User_Landing_to_Blog_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC030_User_Landing_to_Careers_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC030_User_Landing_to_Careers_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC030_User_Landing_to_Careers_fromFooter","A User at the Landing page goes to the Careers page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		 
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyCareers_Footer(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC030_User_Landing_to_Careers_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC031_User_Landing_to_Facebook_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC031_User_Landing_to_Facebook_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC031_User_Landing_to_Facebook_fromFooter","A User at Landing page goes to the STARZ PLAY Facebook page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afverifyFaceBook_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC031_User_Landing_to_Facebook_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC032_User_Landing_to_Twitter_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC032_User_Landing_to_Twitter_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC032_User_Landing_to_Twitter_fromFooter","A User at Landing page goes to the STARZ PLAY Twitter page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyTwitter_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC032_User_Landing_to_Twitter_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC033_User_Landing_to_Instagram_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC033_User_Landing_to_Instagram_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC033_User_Landing_to_Instagram_fromFooter","A User at Landing page goes to the STARZ PLAY Instagram page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyInstagram_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC033_User_Landing_to_Instagram_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC034_User_Landing_to_YouTube_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC034_User_Landing_to_YouTube_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC034_User_Landing_to_YouTube_fromFooter","A User at Landing page goes to the STARZ PLAY YouTube page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyYouTube_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC034_User_Landing_to_YouTube_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC035_User_Landing_to_AppStore_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC035_User_Landing_to_AppStore_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC035_User_Landing_to_AppStore_fromFooter","A User at Landing page goes to the App Store page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyAppStore_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC035_User_Landing_to_AppStore_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC036_User_Landing_to_GooglePlayStore_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC036_User_Landing_to_GooglePlayStore_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC036_User_Landing_to_GooglePlayStore_fromFooter","A User at Landing page goes to the Google Play Store page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyGooglePlayStore_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC036_User_Landing_to_GooglePlayStore_fromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
		   
	}
	
	public void TC199_User_Landing_to_MuleSoft_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CFMethods = new CommonFooterMethods();
		  int StepNum = 1;
		  System.out.println("TC199_User_Landing_to_MuleSoft_fromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC199_User_Landing_to_MuleSoft_fromFooter","A User at the Landing page goes to the Mulesoft page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CFMethods.afVerifyMuleSoft_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC199_User_Landing_to_MuleSoft_fromFooter : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC037_User_Landing_to_AppStore_FromBody(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  
		  int StepNum = 1;
		  System.out.println("TC037_User_Landing_to_AppStore_FromBody: Execution Started");
		  Exttest = Extreport.startTest("TC037_User_Landing_to_AppStore_FromBody","A User at Landing page goes to the App Store page (from the body in the landing page)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  WebElement imgDwnldOnAppStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='amazing']//img[@alt='Download on the App Store']");
		  if(imgDwnldOnAppStore!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", imgDwnldOnAppStore);
			  //btnRegisterYourFree.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Download on the App Store Under Watch on all your screens", "Successfully Clicked on the button Download on the App Store from Watch on all your screens", "No");
			  System.out.println("Successfully Clicked on the button Download on the App Store from Watch on all your screens"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Download on the App Store Under Watch on all your screens", "Failed To Click On  the button Download on the App Store from Watch on all your screens", "No");   
			  System.out.println("Failed To Click On  the button Download on the App Store from Watch on all your screens"); 
			  StepNum = StepNum+1;
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("itunes.apple.com/ae/app/starzplay-arabia"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the Appstore page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed to verify the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Appstore", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC037_User_Landing_to_AppStore_FromBody : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC038_User_Landing_to_GooglePlayStore_FromBody(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  int StepNum = 1;
		  System.out.println("TC038_User_Landing_to_GooglePlayStore_FromBody: Execution Started");
		  Exttest = Extreport.startTest("TC038_User_Landing_to_GooglePlayStore_FromBody","A User at Landing page goes to the Google Play Store page (from the body in the landing page)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  WebElement imgDwnldOnPlayStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[text()='Watch on all your screens ']//ancestor::div[1]//img[@alt='Get it on Google play']");
		  if(imgDwnldOnPlayStore!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", imgDwnldOnPlayStore);
			  //btnRegisterYourFree.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store Under Watch on all your screens", "Successfully Clicked on the button GET IT ON GOOGLE PLAY from Watch on all your screens", "No");
			  System.out.println("Successfully Clicked on the button GET IT ON GOOGLE PLAY from Watch on all your screens");  
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store Under Watch on all your screens", "Failed To Click On  the button GET IT ON GOOGLE PLAY from Watch on all your screens", "No");   
			  System.out.println("Failed To Click On  the button Download on the App Store from Watch on all your screens");
			  StepNum = StepNum+1;
			   }
		  
		  try{
			  Thread.sleep(5000);
		  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
		  Driver.switchTo().window(tabs2.get(1));
		  if((Driver.getCurrentUrl().contains("com.parsifal.starz"))){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Play Store for Starzplay Arabia Has opened", "Successfully verified the Play Store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Successfully verified the Play Store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Play Store for Starzplay Arabia Has opened", "Failed To Verfiy the Play Store page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed to verify the Play Store page for STARZ PLAY ARABIA the URL is "+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  Driver.close();
		  Driver.switchTo().window(tabs2.get(0)); 
		  }catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Play Store", "No");
			  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Play Store");
			  StepNum = StepNum+1;  
		  }
		  
		  System.out.println("TC038_User_Landing_to_GooglePlayStore_FromBody : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC039_User_Landing_to_HomePage_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC039_User_Landing_to_HomePage_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC039_User_Landing_to_HomePage_fromHeader","User goes to the Home page by clicking STARZ PLAY logo");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC039_User_Landing_to_HomePage_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC040_User_Landing_to_Movies_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC040_User_Landing_to_Movies_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC040_User_Landing_to_Movies_fromHeader","User goes to the Movies page by clicking Movies(from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afVerifyMoviesPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC040_User_Landing_to_Movies_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	
	public void TC041_User_Landing_to_Series_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC041_User_Landing_to_Series_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC041_User_Landing_to_Series_fromHeader","User goes to the Movies page by clicking Movies(from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afVerifySeriesPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC041_User_Landing_to_Series_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC042_User_Landing_to_ARABIC_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC042_User_Landing_to_ARABIC_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC042_User_Landing_to_ARABIC_fromHeader","Guest goes to the Arabic page by clicking Arabic(from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afVerifyArabicPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC042_User_Landing_to_ARABIC_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC043_User_Landing_to_KIDS_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC043_User_Landing_to_KIDS_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC043_User_Landing_to_KIDS_fromHeader","Guest goes to the Kids page by clicking Kids(from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afVerifyKidsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC043_User_Landing_to_KIDS_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC044_User_Landing_OpensSearchBar_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC044_User_Landing_OpensSearchBar_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC044_User_Landing_OpensSearchBar_fromHeader","User opens the Search bar");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afVerifySearchFields(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC044_User_Landing_OpensSearchBar_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC045_User_Landing_OpensSettings_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC045_User_Landing_OpensSettings_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC045_User_Landing_OpensSettings_fromHeader","User opens the Settings options");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afVerifySettingsFields(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC045_User_Landing_OpensSettings_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC046_User_Landing_GoToSettingsPage_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC046_User_Landing_GoToSettingsPage_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC046_User_Landing_GoToSettingsPage_fromHeader","User goes to the Settings page");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC046_User_Landing_GoToSettingsPage_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC047_User_Landing_GoToMyLibraryPage_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC047_User_Landing_GoToMyLibraryPage_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC047_User_Landing_GoToMyLibraryPage_fromHeader","User goes to the My Library page");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afVerifyMyLibraryPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC047_User_Landing_GoToMyLibraryPage_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC048_User_Landing_ChangeLanguageTo_Arabic_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC048_User_Landing_ChangeLanguageTo_Arabic_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC048_User_Landing_ChangeLanguageTo_Arabic_fromHeader","User changes language to Arabic(from the Settings drop down options in the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afChangeLanguageToArabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC048_User_Landing_ChangeLanguageTo_Arabic_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC049_User_Landing_ChangeLanguageTo_French_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC049_User_Landing_ChangeLanguageTo_French_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC049_User_Landing_ChangeLanguageTo_French_fromHeader","User changes language to French (from the Settings drop down options in the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC049_User_Landing_ChangeLanguageTo_French_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC050_User_Landing_Logout_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC050_User_Landing_Logout_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC050_User_Landing_Logout_fromHeader","User logs out from STARZ PLAY");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = afLoginToPortal( Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
		   StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CHMethods.afLogout(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC050_User_Landing_Logout_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC051_Guest_Landing_to_SignUp_NoUsrNameNoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC051_Guest_Landing_to_SignUp_NoUsrNameNoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC051_Guest_Landing_to_SignUp_NoUsrNameNoPwd","Guest tries to sign up with no username and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_NoUsrName_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC051_Guest_Landing_to_SignUp_NoUsrNameNoPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC052_Guest_Landing_to_SignUp_WrongEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC052_Guest_Landing_to_SignUp_WrongEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC052_Guest_Landing_to_SignUp_WrongEmailFormat_NoPwd","Guest tries to sign up with wrong email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC052_Guest_Landing_to_SignUp_WrongEmailFormat_NoPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC053_Guest_Landing_to_SignUp_WrongMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC053_Guest_Landing_to_SignUp_WrongMSISDNFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC053_Guest_Landing_to_SignUp_WrongMSISDNFormat_NoPwd","Guest tries to sign up with wrong MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC053_Guest_Landing_to_SignUp_WrongMSISDNFormat_NoPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC054_Guest_Landing_to_SignUp_ValidEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC054_Guest_Landing_to_SignUp_ValidEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC054_Guest_Landing_to_SignUp_ValidEmailFormat_NoPwd","Guest tries to sign up with correct email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_ValidEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC054_Guest_Landing_to_SignUp_ValidEmailFormat_NoPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC055_Guest_Landing_to_SignUp_ValidMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC055_Guest_Landing_to_SignUp_ValidMSISDNFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC055_Guest_Landing_to_SignUp_ValidMSISDNFormat_NoPwd","Guest tries to sign up with correct MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_ValidMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC055_Guest_Landing_to_SignUp_ValidMSISDNFormat_NoPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC056_Guest_Landing_to_SignUp_NoUserName_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC056_Guest_Landing_to_SignUp_NoUserName_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC056_Guest_Landing_to_SignUp_NoUserName_ShortPwd","Guest tries to sign up with no username and short password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_NoUserName_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC056_Guest_Landing_to_SignUp_NoUserName_ShortPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC057_Guest_Landing_to_SignUp_WrongEmailFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC057_Guest_Landing_to_SignUp_WrongEmailFormat_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC057_Guest_Landing_to_SignUp_WrongEmailFormat_ShortPwd","Guest tries to sign up with wrong email format and short password (less than 6 characters)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_WrongEmailFormat_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC057_Guest_Landing_to_SignUp_WrongEmailFormat_ShortPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC058_Guest_Landing_to_SignUp_WrongMSISDNFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC058_Guest_Landing_to_SignUp_WrongMSISDNFormat_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC058_Guest_Landing_to_SignUp_WrongMSISDNFormat_ShortPwd","Guest tries to sign up with wrong MSISDN format and short password (less than 6 characters)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_WrongMSISDNFFormat_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC058_Guest_Landing_to_SignUp_WrongMSISDNFormat_ShortPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC059_Guest_Landing_to_SignUp_ValidEmailFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC059_Guest_Landing_to_SignUp_ValidEmailFormat_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC059_Guest_Landing_to_SignUp_ValidEmailFormat_ShortPwd","Guest tries to sign up with correct email format and short password (less than 6 characters)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_ValidEmailFormat_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC059_Guest_Landing_to_SignUp_ValidEmailFormat_ShortPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC060_Guest_Landing_to_SignUp_ValidMSISDNFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC060_Guest_Landing_to_SignUp_ValidMSISDNFormat_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC060_Guest_Landing_to_SignUp_ValidMSISDNFormat_ShortPwd","Guest tries to sign up with correct MSISDN format and short password (less than 6 characters)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_ValidMSISDNFormat_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC060_Guest_Landing_to_SignUp_ValidMSISDNFormat_ShortPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC061_Guest_Landing_to_SignUp_AlreadyReg_Email(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC061_Guest_Landing_to_SignUp_AlreadyReg_Email: Execution Started");
		  Exttest = Extreport.startTest("TC061_Guest_Landing_to_SignUp_AlreadyReg_Email","Guest tries to sign up with an already registered email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_AlreadyRegEmail(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC061_Guest_Landing_to_SignUp_AlreadyReg_Email : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC062_Guest_Landing_to_SignUp_AlreadyReg_MSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC062_Guest_Landing_to_SignUp_AlreadyReg_MSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC062_Guest_Landing_to_SignUp_AlreadyReg_MSISDN","Guest tries to sign up with an already registered MSISDN");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_AlreadyRegMSISDN(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC062_Guest_Landing_to_SignUp_AlreadyReg_MSISDN : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC063_Guest_Landing_to_SignUp_FaceBook_Cancel(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC063_Guest_Landing_to_SignUp_FaceBook_Cancel: Execution Started");
		  Exttest = Extreport.startTest("TC063_Guest_Landing_to_SignUp_FaceBook_Cancel","Guest tries to sign up with Facebook and cancelling the Facebook log in");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Facebook_Cancel(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC063_Guest_Landing_to_SignUp_FaceBook_Cancel : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC064_Guest_Landing_to_SignUp_FaceBook_NotApprove(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  int StepNum = 1;
		  System.out.println("TC064_Guest_Landing_to_SignUp_FaceBook_NotApprove: Execution Started");
		  Exttest = Extreport.startTest("TC064_Guest_Landing_to_SignUp_FaceBook_NotApprove","Guest tries to sign up with Facebook and not approving STARZ PLAY application");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Facebook_NotApprove(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC064_Guest_Landing_to_SignUp_FaceBook_NotApprove : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC104_Guest_Landing_to_SignUp_Facebook_NotApprove_AccessToEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC104_Guest_Landing_to_SignUp_Facebook_NotApprove_AccessToEmail: Execution Started");
		  Exttest = Extreport.startTest("TC104_Guest_Landing_to_SignUp_Facebook_NotApprove_AccessToEmail","Guest tries to sign up with Facebook and not approving access to email address");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook_NotApprove_AccessToEmail(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC104_Guest_Landing_to_SignUp_Facebook_NotApprove_AccessToEmail : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC065_072_Guest_Landing_to_SignUp_Email_CreditCard(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC065_Guest_Landing_to_SignUp_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC065_Guest_Landing_to_SignUp_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		 
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC065_Guest_Landing_to_SignUp_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC066_Guest_Landing_to_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC066_Guest_Landing_to_SignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC066_Guest_Landing_to_SignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC067_Guest_Landing_to_SignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC067_Guest_Landing_to_SignUp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC067_Guest_Landing_to_SignUp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC068_Guest_Landing_to_SignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC068_Guest_Landing_to_SignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC068_Guest_Landing_to_SignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC069_Guest_Landing_to_SignUp_PaymentPage_MoPCreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC069_Guest_Landing_to_SignUp_PaymentPage_MoPCreditCard","Guest chooses Credit Card as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoPCreditCard_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC069_Guest_Landing_to_SignUp_PaymentPage_MoPCreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC070_Guest_Landing_to_SignUp_CreditCard_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC070_Guest_Landing_to_SignUp_CreditCard_ToArbic","Guest at Credit Card payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC070_Guest_Landing_to_SignUp_CreditCard_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC071_Guest_Landing_to_SignUp_CreditCard_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC071_Guest_Landing_to_SignUp_CreditCard_ToFrench","Guest at Credit Card Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC071_Guest_Landing_to_SignUp_CreditCard_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC072_Guest_Landing_to_SignUp_With_CreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC072_Guest_Landing_to_SignUp_With_CreditCard","Guest enters valid credit card details: First name, Last Name, Card Number, Exp Month, Exp Year, CVV");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC072_Guest_Landing_to_SignUp_With_CreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC073_080_Guest_Landing_to_SignUp_Email_Voucher(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap,String VoucherCode) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC073_Guest_Landing_to_SignUp_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC073_Guest_Landing_to_SignUp_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		 
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC073_Guest_Landing_to_SignUp_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC074_Guest_Landing_to_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC074_Guest_Landing_to_SignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC074_Guest_Landing_to_SignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC075_Guest_Landing_to_SignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC075_Guest_Landing_to_SignUp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC075_Guest_Landing_to_SignUp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC076_Guest_Landing_to_SignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC076_Guest_Landing_to_SignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC076_Guest_Landing_to_SignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC077_Guest_Landing_to_SignUp_PaymentPage_MoPVoucher: Execution Started");
		   Exttest = Extreport.startTest("TC077_Guest_Landing_to_SignUp_PaymentPage_MoPVoucher","Guest chooses STARZPLAY Voucher as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoPVoucher_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC077_Guest_Landing_to_SignUp_PaymentPage_MoPVoucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC078_Guest_Landing_to_SignUp_Voucher_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC078_Guest_Landing_to_SignUp_Voucher_ToArbic","Guest at STARZPLAY Voucher payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC078_Guest_Landing_to_SignUp_Voucher_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC079_Guest_Landing_to_SignUp_Voucher_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC079_Guest_Landing_to_SignUp_Voucher_ToFrench","Guest at STARZPLAY Voucher Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC079_Guest_Landing_to_SignUp_Voucher_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC080_Guest_Landing_to_SignUp_With_Voucher: Execution Started");
		   Exttest = Extreport.startTest("TC080_Guest_Landing_to_SignUp_With_Voucher","Guest enters valid STARZPLAY Voucher details");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithVoucher(Extreport, Exttest, Driver, StepNum, ResultLocation,VoucherCode);
		   System.out.println("TC080_Guest_Landing_to_SignUp_With_Voucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC105_TC107_Landing_to_SignUP_ProspectUser_Email_EndToEnd_Happy(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC105_ProspectUser_SignUp_with_Email: Execution Started");
		  Exttest = Extreport.startTest("TC105_ProspectUser_SignUp_with_Email","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC105_ProspectUser_SignUp_with_Email : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC106_ProspectUser_All_MOPs","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC106_ProspectUser_All_MOPs : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC107_ProspectUser_Navigate_to_Home","Guest goes to the Home page by clicking STARZ PLAY(from the header)");
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  Extreport.endTest(Exttest);
		  System.out.println("TC107_ProspectUser_Navigate_to_Home : Execution Finished  "); 
		  
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC108_TC110_Landing_to_SignUP_ProspectUser_Facebook_EndToEnd_Happy(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		
		int StepNum = 1;
		System.out.println("TC108_ProspectUser_SignUp_with_Facebook: Execution Started");
		  Exttest = Extreport.startTest("TC108_ProspectUser_SignUp_with_Facebook","Guest signs up with valid facebook credential");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserNameForProspectUserSignUP", "OneTimeData", "TC108_Landing_To_SignUp_ProspectUser_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPwdForProspectUserSignUP", "OneTimeData", "TC108_Landing_To_SignUp_ProspectUser_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook_ProspectUser(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC108_Landing_To_SignUp_ProspectUser_with_Facebook : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC109_ProspectUser_All_MOPs","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC109_ProspectUser_All_MOPs : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC110_ProspectUser_Naviagete_to_Home","Guest goes to the Home page by clicking STARZ PLAY(from the header)");
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  Extreport.endTest(Exttest);
		  System.out.println("TC110_ProspectUser_Naviagete_to_Home : Execution Finished  "); 
		  
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC111_TC128_Landing_to_SignUP_Email_CreditCard_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		int StepNum = 1;
		System.out.println("TC111_Landing_to_SignUp_with_Email: Execution Started");
		  Exttest = Extreport.startTest("TC111_Landing_to_SignUp_with_Email","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC111_Landing_to_SignUp_with_Email : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC112_SignUp_User_All_MOPs","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC112_SignUp_User_All_MOPs : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		   System.out.println("TC113_GuestAffiliateSignUp_MoPCreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC113_GuestAffiliateSignUp_MoPCreditCard","Guest chooses Credit Card as MoP at Payment page");
		   StepNum = CMoPMethods.afMoPCreditCard_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC113_GuestAffiliateSignUp_MoPCreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC114_GuestAffiliateSignUp_VerifyCreditCardPage: Execution Started");
		   Exttest = Extreport.startTest("TC114_GuestAffiliateSignUp_VerifyCreditCardPage","Guest should be directed to Credit Card Payment page");
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC114_GuestAffiliateSignUp_VerifyCreditCardPage : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC115_GuestAffiliateSignUpp_CreditCard_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC115_GuestAffiliateSignUpp_CreditCard_ToArbic","Guest at Credit Card payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC115_GuestAffiliateSignUpp_CreditCard_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC116_GuestAffiliateSignUpp_CreditCard_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC116_GuestAffiliateSignUpp_CreditCard_ToFrench","Guest at Credit Card Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC116_GuestAffiliateSignUpp_CreditCard_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC117_SignUpp_CreditCard_NoDetails: Execution Started");
		   Exttest = Extreport.startTest("TC117_SignUpp_CreditCard_NoDetails","Guest/User tries to complete payment with no details");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoDetails(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC117_SignUpp_CreditCard_NoDetails : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC118_SignUpp_CreditCard_NoFirstName: Execution Started");
		   Exttest = Extreport.startTest("TC118_SignUpp_CreditCard_NoFirstName","Guest/User tries to complete payment with missing First Name");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoFirstName(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC118_SignUpp_CreditCard_NoFirstName : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC119_SignUpp_CreditCard_NoLastName: Execution Started");
		   Exttest = Extreport.startTest("TC119_SignUpp_CreditCard_NoLastName","Guest/User tries to complete payment with missing last Name");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoLastName(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC119_SignUpp_CreditCard_NoLastName : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC120_SignUpp_CreditCard_NoCreditCardNumber: Execution Started");
		   Exttest = Extreport.startTest("TC120_SignUpp_CreditCard_NoCreditCardNumber","Guest/User tries to complete payment with missing Credit Card Number");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoCreditCardNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC120_SignUpp_CreditCard_NoCreditCardNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC121_SignUpp_CreditCard_NoExpiryMonth: Execution Started");
		   Exttest = Extreport.startTest("TC121_SignUpp_CreditCard_NoExpiryMonth","Guest/User tries to complete payment with missing Expiry Month");
		   StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CHMethods.afNavigateToCreditCardPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoExpiryMonth(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC121_SignUpp_CreditCard_NoExpiryMonth : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC122_SignUpp_CreditCard_NoExpiryYear: Execution Started");
		   Exttest = Extreport.startTest("TC122_SignUpp_CreditCard_NoExpiryYear","Guest/User tries to complete payment with missing Expiry Year");
		   StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CHMethods.afNavigateToCreditCardPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoExpiryYear(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC122_SignUpp_CreditCard_NoExpiryYear : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC123_SignUpp_CreditCard_NoCreditCardCVV: Execution Started");
		   Exttest = Extreport.startTest("TC123_SignUpp_CreditCard_NoCreditCardCVV","Guest/User tries to complete payment with missing Credit Card CVV");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoCVV(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC123_SignUpp_CreditCard_NoCreditCardCVV : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC124_SignUpp_CreditCard_WrongCreditCardFormat: Execution Started");
		   Exttest = Extreport.startTest("TC124_SignUpp_CreditCard_WrongCreditCardFormat","Guest/User tries to complete pament with invalid Credit Card Number");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_WrongCreditCardFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC124_SignUpp_CreditCard_WrongCreditCardFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC125_SignUpp_CreditCard_InvalidCreditCardExpiryDate: Execution Started");
		   Exttest = Extreport.startTest("TC125_SignUpp_CreditCard_InvalidCreditCardExpiryDate","Guest/User tries to complete payment with invalid Expiration Date");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidExpiryDate(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC125_SignUpp_CreditCard_InvalidCreditCardExpiryDate : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC126_SignUpp_CreditCard_InvalidCVVFormat: Execution Started");
		   Exttest = Extreport.startTest("TC126_SignUpp_CreditCard_InvalidCVVFormat","Guest/User tries to complete payment with invalid CVV format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidCVV(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC126_SignUpp_CreditCard_InvalidCVVFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC127_SignUpp_CreditCard_InvalidFirstNameFormat: Execution Started");
		   Exttest = Extreport.startTest("TC127_SignUpp_CreditCard_InvalidFirstNameFormat","Guest/User tries to complete payment with invalid First Name format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidFirstNameFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC127_SignUpp_CreditCard_InvalidFirstNameFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC128_SignUpp_CreditCard_InvalidLastNameFormat: Execution Started");
		   Exttest = Extreport.startTest("TC128_SignUpp_CreditCard_InvalidLastNameFormat","Guest/User tries to complete payment with invalid Last Name format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidLastNameFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC128_SignUpp_CreditCard_InvalidLastNameFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		  
		  Driver.close();
	}
	
	public void TC129_TC133_Landing_To_SignUP_Email_Voucher_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		int StepNum = 1;
		System.out.println("TC129_Landing_To_SignUp_with_Email: Execution Started");
		  Exttest = Extreport.startTest("TC129_Landing_To_SignUp_with_Email","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC129_Landing_To_SignUp_with_Email : Execution Finished  ");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		  Exttest = Extreport.startTest("TC130_SignUp_User_All_MOPs_verifcation","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC130_SignUp_User_All_MOPs_verifcation : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		   System.out.println("TC131_GuestAffiliateSignUp_MoPVoucher: Execution Started");
		   Exttest = Extreport.startTest("TC131_GuestAffiliateSignUp_MoPVoucher","Guest chooses STARZPLAY Voucher as MoP at Payment page");
		   StepNum = CMoPMethods.afMoPVoucher_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC131_GuestAffiliateSignUp_MoPVoucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC132_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode: Execution Started");
		   Exttest = Extreport.startTest("TC132_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode","Guest tries to complete process without entering Voucher Code");
		   StepNum = CMoPMethods.afSignUPWithVoucher_NoVoucherCode(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC132_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC133_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode: Execution Started");
		   Exttest = Extreport.startTest("TC133_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode","Guest tries to complete process with an invalid Voucher");
		   StepNum = CMoPMethods.afSignUPWithVoucher_InvalidVoucherCode(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC133_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
	  Driver.close();
}
	
	public void TC134_TC151_SignUP_Facebook_CreditCard_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		int StepNum = 1;
		System.out.println("TC134_Landing_To_SignUp_with_Facebook: Execution Started");
		  Exttest = Extreport.startTest("TC134_Landing_To_SignUp_with_Facebook","Guest signs up with Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserName", "LandingPageTest", "TC134_Landing_To_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPassword", "LandingPageTest", "TC134_Landing_To_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC134_Landing_To_SignUp_with_Facebook : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC135_SignUp_User_All_MOPs","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC135_SignUp_User_All_MOPs : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		   System.out.println("TC136_GuestAffiliateSignUp_MoPCreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC136_GuestAffiliateSignUp_MoPCreditCard","Guest chooses Credit Card as MoP at Payment page");
		   StepNum = CMoPMethods.afMoPCreditCard_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC136_GuestAffiliateSignUp_MoPCreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC137_GuestAffiliateSignUp_VerifyCreditCardPage: Execution Started");
		   Exttest = Extreport.startTest("TC137_GuestAffiliateSignUp_VerifyCreditCardPage","Guest should be directed to Credit Card Payment page");
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC137_GuestAffiliateSignUp_VerifyCreditCardPage : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC138_GuestAffiliateSignUpp_CreditCard_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC138_GuestAffiliateSignUpp_CreditCard_ToArbic","Guest at Credit Card payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC138_GuestAffiliateSignUpp_CreditCard_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC139_GuestAffiliateSignUpp_CreditCard_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC139_GuestAffiliateSignUpp_CreditCard_ToFrench","Guest at Credit Card Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC139_GuestAffiliateSignUpp_CreditCard_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC140_SignUpp_CreditCard_NoDetails: Execution Started");
		   Exttest = Extreport.startTest("TC140_SignUpp_CreditCard_NoDetails","Guest/User tries to complete payment with no details");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoDetails(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC141_SignUpp_CreditCard_NoDetails : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC142_SignUpp_CreditCard_NoFirstName: Execution Started");
		   Exttest = Extreport.startTest("TC142_SignUpp_CreditCard_NoFirstName","Guest/User tries to complete payment with missing First Name");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoFirstName(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC142_SignUpp_CreditCard_NoFirstName : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC142_SignUpp_CreditCard_NoLastName: Execution Started");
		   Exttest = Extreport.startTest("TC142_SignUpp_CreditCard_NoLastName","Guest/User tries to complete payment with missing last Name");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoLastName(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC142_SignUpp_CreditCard_NoLastName : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC143_SignUpp_CreditCard_NoCreditCardNumber: Execution Started");
		   Exttest = Extreport.startTest("TC143_SignUpp_CreditCard_NoCreditCardNumber","Guest/User tries to complete payment with missing Credit Card Number");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoCreditCardNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC143_SignUpp_CreditCard_NoCreditCardNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC144_SignUpp_CreditCard_NoExpiryMonth: Execution Started");
		   Exttest = Extreport.startTest("TC144_SignUpp_CreditCard_NoExpiryMonth","Guest/User tries to complete payment with missing Expiry Month");
		   StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CHMethods.afNavigateToCreditCardPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoExpiryMonth(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC144_SignUpp_CreditCard_NoExpiryMonth : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC145_SignUpp_CreditCard_NoExpiryYear: Execution Started");
		   Exttest = Extreport.startTest("TC145_SignUpp_CreditCard_NoExpiryYear","Guest/User tries to complete payment with missing Expiry Year");
		   StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CHMethods.afNavigateToCreditCardPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoExpiryYear(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC145_SignUpp_CreditCard_NoExpiryYear : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC146_SignUpp_CreditCard_NoCreditCardCVV: Execution Started");
		   Exttest = Extreport.startTest("TC146_SignUpp_CreditCard_NoCreditCardCVV","Guest/User tries to complete payment with missing Credit Card CVV");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoCVV(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC146_SignUpp_CreditCard_NoCreditCardCVV : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC147_SignUpp_CreditCard_WrongCreditCardFormat: Execution Started");
		   Exttest = Extreport.startTest("TC147_SignUpp_CreditCard_WrongCreditCardFormat","Guest/User tries to complete pament with invalid Credit Card Number");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_WrongCreditCardFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC147_SignUpp_CreditCard_WrongCreditCardFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC148_SignUpp_CreditCard_InvalidCreditCardExpiryDate: Execution Started");
		   Exttest = Extreport.startTest("TC148_SignUpp_CreditCard_InvalidCreditCardExpiryDate","Guest/User tries to complete payment with invalid Expiration Date");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidExpiryDate(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC148_SignUpp_CreditCard_InvalidCreditCardExpiryDate : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC149_SignUpp_CreditCard_InvalidCVVFormat: Execution Started");
		   Exttest = Extreport.startTest("TC149_SignUpp_CreditCard_InvalidCVVFormat","Guest/User tries to complete payment with invalid CVV format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidCVV(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC149_SignUpp_CreditCard_InvalidCVVFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC150_SignUpp_CreditCard_InvalidFirstNameFormat: Execution Started");
		   Exttest = Extreport.startTest("TC150_SignUpp_CreditCard_InvalidFirstNameFormat","Guest/User tries to complete payment with invalid First Name format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidFirstNameFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC150_SignUpp_CreditCard_InvalidFirstNameFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC151_SignUpp_CreditCard_InvalidLastNameFormat: Execution Started");
		   Exttest = Extreport.startTest("TC151_SignUpp_CreditCard_InvalidLastNameFormat","Guest/User tries to complete payment with invalid Last Name format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidLastNameFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC151_SignUpp_CreditCard_InvalidLastNameFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		  
		  Driver.close();
	}
	
	public void TC152_TC159_Landing_To_SignUp_Facebook_CreditCard_Happy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC152_Landing_To_SignUp_with_Facebook: Execution Started");
		  Exttest = Extreport.startTest("TC152_Landing_To_SignUp_with_Facebook","Guest signs up with Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserNameForHappySignUP", "OneTimeData", "TC152_Landing_To_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPasswordForHappySignUP", "OneTimeData", "TC152_Landing_To_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC152_Landing_To_SignUp_with_Facebook : Execution Finished  "); 
		   
		   System.out.println("TC153_Landing_To_SignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC153_Landing_To_SignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC153_Landing_To_SignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC154_Landing_To_SignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC154_Landing_To_SignUppp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC154_Landing_To_SignUppp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC155_Landing_To_SignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC155_Landing_To_SignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC155_Landing_To_SignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC156_Landing_To_SignUp_PaymentPage_MoPCreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC156_Landing_To_SignUp_PaymentPage_MoPCreditCard","Guest chooses Credit Card as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoPCreditCard_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC156_Landing_To_SignUp_PaymentPage_MoPCreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC157_Landing_To_SignUp_CreditCard_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC157_Landing_To_SignUp_CreditCard_ToArbic","Guest at Credit Card payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC157_Landing_To_SignUp_CreditCard_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC158_Landing_To_SignU_CreditCard_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC158_Landing_To_SignU_CreditCard_ToFrench","Guest at Credit Card Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC158_Landing_To_SignU_CreditCard_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC159_Landing_To_SignU_With_CreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC159_Landing_To_SignU_With_CreditCard","Guest enters valid credit card details: First name, Last Name, Card Number, Exp Month, Exp Year, CVV");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC159_Landing_To_SignU_With_CreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC160_TC164_Landng_To_SignUP_Facebook_Voucher_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		int StepNum = 1;
		System.out.println("TC160_Landing_SignUp_with_Facebook: Execution Started");
		  Exttest = Extreport.startTest("TC160_Landing_SignUp_with_Facebook","Guest signs up with Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserName", "LandingPageTest", "TC160_Landing_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPassword", "LandingPageTest", "TC160_Landing_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC160_Landing_SignUp_with_Facebook : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC161_SignUp_User_All_MOPs_verifcation","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC161_SignUp_User_All_MOPs_verifcation : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		   System.out.println("TC162_GuestAffiliateSignUp_MoPVoucher: Execution Started");
		   Exttest = Extreport.startTest("TC162_GuestAffiliateSignUp_MoPVoucher","Guest chooses STARZPLAY Voucher as MoP at Payment page");
		   StepNum = CMoPMethods.afMoPVoucher_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC162_GuestAffiliateSignUp_MoPVoucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC163_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode: Execution Started");
		   Exttest = Extreport.startTest("TC163_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode","Guest tries to complete process without entering Voucher Code");
		   StepNum = CMoPMethods.afSignUPWithVoucher_NoVoucherCode(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC163_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC164_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode: Execution Started");
		   Exttest = Extreport.startTest("TC164_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode","Guest tries to complete process with an invalid Voucher");
		   StepNum = CMoPMethods.afSignUPWithVoucher_InvalidVoucherCode(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC164_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
	  
	  Driver.close();
}
	
	public void TC165_TC172_Landing_To_SignUp_Facebook_Voucher_Happy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap,String VoucherCode) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC165_Landing_To_SignUp_with_Facebook_MoPVoucher: Execution Started");
		  Exttest = Extreport.startTest("TC165_Landing_To_SignUp_with_Facebook_MoPVoucher","Guest signs up with Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserNameForHappySignUP", "OneTimeData", "TC165_Landing_To_SignUp_with_Facebook_MoPVoucher", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPasswordForHappySignUP", "OneTimeData", "TC165_Landing_To_SignUp_with_Facebook_MoPVoucher", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC165_Landing_To_SignUp_with_Facebook_MoPVoucher : Execution Finished  "); 
		   
		   System.out.println("TC166_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC166_SignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC166_SignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC167_SignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC167_SignUp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC167_SignUp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC168_SignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC168_SignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC168_SignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC169_SignUp_PaymentPage_MoPVoucher: Execution Started");
		   Exttest = Extreport.startTest("TC169_SignUp_PaymentPage_MoPVoucher","Guest chooses STARZPLAY Voucher as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoPVoucher_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC169_SignUp_PaymentPage_MoPVoucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC170_SignUp_Voucher_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC170_SignUp_Voucher_ToArbic","Guest at STARZPLAY Voucher payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC170_SignUp_Voucher_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC171_SignUp_Voucher_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC171_SignUp_Voucher_ToFrench","Guest at STARZPLAY Voucher Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC171_SignUp_Voucher_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC172_SignUp_With_Voucher: Execution Started");
		   Exttest = Extreport.startTest("TC172_SignUp_With_Voucher","Guest enters valid STARZPLAY Voucher details");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithVoucher(Extreport, Exttest, Driver, StepNum, ResultLocation,VoucherCode);
		   System.out.println("TC172_SignUp_With_Voucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	public void TC081_User_Landing_to_Login_ActiveUSer(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC081_User_Landing_to_Login: Execution Started");
		  Exttest = Extreport.startTest("TC081_User_Landing_to_Login","User is at Landing page and proceeds to Log In page");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  System.out.println("TC081_User_Landing_to_Login : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	
	
	public void TC082_User_Landing_to_Login_NoUsrName_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC082_User_Landing_to_Login_NoUsrName_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC082_User_Landing_to_Login_NoUsrName_NoPwd","User tries to Log In with no username and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_NoUsrName_NoPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC082_User_Landing_to_Login_NoUsrName_NoPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC083_User_Landing_to_Login_WrongEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC083_User_Landing_to_Login_WrongEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC083_User_Landing_to_Login_WrongEmailFormat_NoPwd","User tries to Log In with wrong email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_NoPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC083_User_Landing_to_Login_WrongEmailFormat_NoPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC084_User_Landing_to_Login_WrongEmailFormat_NoPwd_DeleteEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC084_User_Landing_to_Login_WrongEmailFormat_NoPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC084_User_Landing_to_Login_WrongEmailFormat_NoPwd_DeleteEmail","User tries to Log In with wrong email format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_NoPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC084_User_Landing_to_Login_WrongEmailFormat_NoPwd_DeleteEmail : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC085_User_Landing_to_Login_WrongMSISDN_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC085_User_Landing_to_Login_WrongMSISDN_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC085_User_Landing_to_Login_WrongMSISDN_NoPwd","User tries to Log In with wrong MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_WrongMSISDNFormat_NoPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC085_User_Landing_to_Login_WrongMSISDN_NoPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC086_User_Landing_to_Login_WrongMSISDN_NoPwd_DeleteMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC086_User_Landing_to_Login_WrongMSISDN_NoPwd_DeleteMSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC086_User_Landing_to_Login_WrongMSISDN_NoPwd_DeleteMSISDN","User tries to Log In with wrong MSISDN format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_WrongMSISDNFormat_NoPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC086_User_Landing_to_Login_WrongMSISDN_NoPwd_DeleteMSISDN : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC087_User_Landing_to_Login_CorrectEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC087_User_Landing_to_Login_CorrectEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC087_User_Landing_to_Login_CorrectEmailFormat_NoPwd","User tries to Log In with correct email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_CorrectEmailFormat_NoPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC087_User_Landing_to_Login_CorrectEmailFormat_NoPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC088_User_Landing_to_Login_CorrectEmailFormat_NoPwd_DeleteEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC088_User_Landing_to_Login_CorrectEmailFormat_NoPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC088_User_Landing_to_Login_CorrectEmailFormat_NoPwd_DeleteEmail","User tries to Log In with correct email format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_CorrectEmailFormat_NoPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC088_User_Landing_to_Login_CorrectEmailFormat_NoPwd_DeleteEmail : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC089_User_Landing_to_Login_CorrectMSISDN_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC089_User_Landing_to_Login_CorrectMSISDN_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC089_User_Landing_to_Login_CorrectMSISDN_NoPwd","User tries to Log In with correct MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_CorrectMSISDNFormat_NoPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC089_User_Landing_to_Login_CorrectMSISDN_NoPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC090_User_Landing_to_Login_CorrectMSISDN_NoPwd_DeleteMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC090_User_Landing_to_Login_CorrectMSISDN_NoPwd_DeleteMSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC090_User_Landing_to_Login_CorrectMSISDN_NoPwd_DeleteMSISDN","User tries to Log In with correct MSISDN format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_CorrectMSISDNFormat_NoPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC090_User_Landing_to_Login_CorrectMSISDN_NoPwd_DeleteMSISDN : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC091_User_Landing_to_Login_NoUserName_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC091_User_Landing_to_Login_NoUserName_AnyPwd: Execution Started");
		  Exttest = Extreport.startTest("TC091_User_Landing_to_Login_NoUserName_AnyPwd","User tries to Log In with no username and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_NoUsrName_AnyPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC091_User_Landing_to_Login_NoUserName_AnyPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC092_User_Landing_to_Login_WrongEmailFormat_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC092_User_Landing_to_Login_WrongEmailFormat_AnyPwd: Execution Started");
		  Exttest = Extreport.startTest("TC092_User_Landing_to_Login_WrongEmailFormat_AnyPwd","User tries to Log In with wrong email format and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC092_User_Landing_to_Login_WrongEmailFormat_AnyPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC093_User_Landing_to_Login_WrongEmailFormat_AnyPwd_DeleteEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC093_User_Landing_to_Login_WrongEmailFormat_AnyPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC093_User_Landing_to_Login_WrongEmailFormat_AnyPwd_DeleteEmail","User tries to Log In with wrong email format and any password and then deletes username");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC093_User_Landing_to_Login_WrongEmailFormat_AnyPwd_DeleteEmail : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC094_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC094_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd: Execution Started");
		  Exttest = Extreport.startTest("TC094_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd","User tries to Log In with wrong MSISDN format and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_WrongMSISDNFormat_AnyPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC094_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC095_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd_DeleteMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC095_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd_DeleteMSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC095_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd_DeleteMSISDN","User tries to Log In with wrong MSISDN format and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_WrongMSISDNFormat_AnyPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC095_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd_DeleteMSISDN : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC096_User_Landing_to_Login_ValidEmail_InvalidPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC096_User_Landing_to_Login_ValidEmail_InvalidPwd: Execution Started");
		  Exttest = Extreport.startTest("TC096_User_Landing_to_Login_ValidEmail_InvalidPwd","User tries to Log In with correct email format and any non-matching password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmail_InvalidPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC096_User_Landing_to_Login_ValidEmail_InvalidPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC097_User_Landing_to_Login_ValidMSISDN_InvalidPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC097_User_Landing_to_Login_ValidMSISDN_InvalidPwd: Execution Started");
		  Exttest = Extreport.startTest("TC097_User_Landing_to_Login_ValidMSISDN_InvalidPwd","User tries to Log In with correct MSISDN format and any non-matching password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidMSISDN_InvalidPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC097_User_Landing_to_Login_ValidMSISDN_InvalidPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC098_User_Landing_to_Login_ValidEmail_InvalidPwd_DeleteBoth(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC098_User_Landing_to_Login_ValidEmail_InvalidPwd_DeleteBoth: Execution Started");
		  Exttest = Extreport.startTest("TC098_User_Landing_to_Login_ValidEmail_InvalidPwd_DeleteBoth","User tries to Log In with correct email format and any non-matching password and then deletes username/password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmail_InvalidPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeletePassword_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC098_User_Landing_to_Login_ValidEmail_InvalidPwd_DeleteBoth : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC099_User_Landing_to_Login_ValidMSISDN_InvalidPwd_DeleteBoth(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC099_User_Landing_to_Login_ValidMSISDN_InvalidPwd_DeleteBoth: Execution Started");
		  Exttest = Extreport.startTest("TC099_User_Landing_to_Login_ValidMSISDN_InvalidPwd_DeleteBoth","User tries to Log In with correct MSISDN format and any non-matching password and then deletes username/password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidMSISDN_InvalidPwd( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum  = CLoginMethods.afDeletePassword_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC099_User_Landing_to_Login_ValidMSISDN_InvalidPwd_DeleteBoth : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC100_User_Landing_to_Login_Facebook_Cancel(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC100_User_Landing_to_Login_Facebook_Cancel: Execution Started");
		  Exttest = Extreport.startTest("TC100_User_Landing_to_Login_Facebook_Cancel","User tries to Log In with Facebook and cancelling the Facebook Log In");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook_Cancel( Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC100_User_Landing_to_Login_Facebook_Cancel : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC101_User_Landing_to_Login_ValidEmail_ValidPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC101_User_Landing_to_Login_ValidEmail_ValidPwd: Execution Started");
		  Exttest = Extreport.startTest("TC101_User_Landing_to_Login_ValidEmail_ValidPwd","User tries to Log In with Facebook and cancelling the Facebook Log In");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  System.out.println("TC101_User_Landing_to_Login_ValidEmail_ValidPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC102_User_Landing_to_Login_ValidMSISDN_ValidPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String MSISDN,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC102_User_Landing_to_Login_ValidMSISDN_ValidPwd: Execution Started");
		  Exttest = Extreport.startTest("TC102_User_Landing_to_Login_ValidMSISDN_ValidPwd","User tries to Log In with Facebook and cancelling the Facebook Log In");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidMSISDNAndPwd( Extreport, Exttest, Driver, StepNum, MSISDN,ResultLocation);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  System.out.println("TC102_User_Landing_to_Login_ValidMSISDN_ValidPwd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC103_User_Landing_to_Login_Facebook_ValidCredentials(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum=1;
		System.out.println("TC103_User_Landing_to_Login_Facebook_ValidCredentials: Execution Started");
		  Exttest = Extreport.startTest("TC103_User_Landing_to_Login_Facebook_ValidCredentials","User tries to Log In with Facebook and cancelling the Facebook Log In");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String fbemail = Futil.fgetData("Facebook Account - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("Facebook Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook_ValidCredentials( Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  System.out.println("TC103_User_Landing_to_Login_Facebook_ValidCredentials : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC173_Landing_To_LoginWith_Facebook_UnRegisteredAccount(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC173_Landing_To_LoginWith_Facebook_UnRegisteredAccount: Execution Started");
		  Exttest = Extreport.startTest("TC173_Landing_To_LoginWith_Facebook_UnRegisteredAccount","User tries to Log In with an unregistered Facebook account");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String fbemail = Futil.fgetData("Facebook Account - Unregistered", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("Facebook Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  StepNum = CLoginMethods.afVerifyUnRegistered_Facebook_Error(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC173_Landing_To_LoginWith_Facebook_UnRegisteredAccount : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC174_Landing_To_LoginWith_ValidEmailAndPwd_ProspectUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC174_Landing_To_LoginWith_ValidEmailAndPwd_ProspectUser: Execution Started");
		  Exttest = Extreport.startTest("TC174_Landing_To_LoginWith_ValidEmailAndPwd_ProspectUser","User logs in with correct prospect email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Prospect", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  System.out.println("TC174_Landing_To_LoginWith_ValidEmailAndPwd_ProspectUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC175_Landing_To_LoginWith_Facebook_ProspectUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC175_Landing_To_LoginWith_Facebook_ProspectUser: Execution Started");
		  Exttest = Extreport.startTest("TC175_Landing_To_LoginWith_Facebook_ProspectUser","User logs in with correct prospect Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Facebook Account - Prospect", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Facebook Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password);
		  StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  System.out.println("TC175_Landing_To_LoginWith_Facebook_ProspectUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC176_Landing_To_LoginWith_ValidEmailAndPwd_DisconnectedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC176_Landing_To_LoginWith_ValidEmailAndPwd_DisconnectedUser: Execution Started");
		  Exttest = Extreport.startTest("TC176_Landing_To_LoginWith_ValidEmailAndPwd_DisconnectedUser","User logs in with correct Disconnected email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Disconnected", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Disconnected User", "Your subscription is currently deactivated. To reactivate your subscription, please enter valid payment details to your account. ACTIVATE");
		  System.out.println("TC176_Landing_To_LoginWith_ValidEmailAndPwd_DisconnectedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC177_Landing_To_LoginWith_Facebook_DisconnectedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC177_Landing_To_LoginWith_Facebook_DisconnectedUser: Execution Started");
		  Exttest = Extreport.startTest("TC177_Landing_To_LoginWith_Facebook_DisconnectedUser","User logs in with correct Disconnected facebook email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Facebook Account - Disconnected", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Disconnected User", "Your subscription is currently deactivated. To reactivate your subscription, please enter valid payment details to your account. ACTIVATE");
		  System.out.println("TC177_Landing_To_LoginWith_Facebook_DisconnectedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC178_Landing_To_LoginWith_ValidEmailAndPwd_DeactivatedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC178_Landing_To_LoginWith_ValidEmailAndPwd_DeactivatedUser: Execution Started");
		  Exttest = Extreport.startTest("TC178_Landing_To_LoginWith_ValidEmailAndPwd_DeactivatedUser","User logs in with correct Deactivated email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Deactivated", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC178_Landing_To_LoginWith_ValidEmailAndPwd_DeactivatedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC179_Landing_To_LoginWith_Facebook_DeactivatedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC179_Landing_To_LoginWith_Facebook_DeactivatedUser: Execution Started");
		  Exttest = Extreport.startTest("TC179_Landing_To_LoginWith_Facebook_DeactivatedUser","User logs in with correct Deactivated facebook email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Facebook Account - Deactivated", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC179_Landing_To_LoginWith_Facebook_DeactivatedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC180_188_Landing_To_SignUp_WithEmail_MoP_Du_Weekly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC180_Landing_To_SignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC180_Landing_To_SignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC180_Landing_To_SignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC181_Landing_To_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC181_Landing_To_SignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC181_Landing_To_SignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC182_Landing_To_SignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC182_Landing_To_SignUp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC182_Landing_To_SignUp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC183_Landing_To_SignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC183_Landing_To_SignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC183_Landing_To_SignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC184_Landing_To_SignUp_PaymentPage_MoP_DU: Execution Started");
		   Exttest = Extreport.startTest("TC184_Landing_To_SignUp_PaymentPage_MoP_DU","Guest chooses DU as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_DU_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC184_Landing_To_SignUp_PaymentPage_MoP_DU : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC185_Landing_To_SignUp_DU_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC185_Landing_To_SignUp_DU_ToArbic","Guest at DU page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC185_Landing_To_SignUp_DU_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC186_Landing_To_SignUp_DU_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC186_Landing_To_SignUp_DU_ToFrench","Guest at DU Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC186_Landing_To_SignUp_DU_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC187_Landing_To_SignUp_DU_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC187_Landing_To_SignUp_DU_Billing_Weekly","Guest select billing frequency as weekly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a weekly subscription payment of AED 10 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC187_Landing_To_SignUp_DU_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC188_Landing_To_SignUpp_With_Active_DU_Registration: Execution Started");
		   Exttest = Extreport.startTest("TC188_Landing_To_SignUpp_With_Active_DU_Registration","Guest enters a valid du number and requests activation code OTP");
		   String DuNumber = Futil.fgetData("Du - Mbile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValidDU_Registration(DuNumber,Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC188_Landing_To_SignUpp_With_Active_DU_Registration : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC189_197_Landing_To_SignUp_WithEmail_MoP_Du_Monthly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC189_Landing_To_SignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC189_Landing_To_SignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC189_Landing_To_SignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC190_Landing_To_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC190_Landing_To_SignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC190_Landing_To_SignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC191_Landing_To_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC191_Landing_To_SignUP_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC191_Landing_To_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC192_Landing_To_SignUP_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC192_Landing_To_SignUP_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC192_Landing_To_SignUP_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC193_Landing_To_SignUP_PaymentPage_MoP_DU: Execution Started");
		   Exttest = Extreport.startTest("TC193_Landing_To_SignUP_PaymentPage_MoP_DU","Guest chooses DU as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_DU_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC193_Landing_To_SignUP_PaymentPage_MoP_DU : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC194_Landing_To_SignUP_DU_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC194_Landing_To_SignUP_DU_ToArbic","Guest at DU payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC194_Landing_To_SignUP_DU_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC195_Landing_To_SignUp_DU_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC195_Landing_To_SignUp_DU_ToFrench","Guest at DU Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC195_Landing_To_SignUp_DU_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC196_Landing_To_SignUp_DU_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC196_Landing_To_SignUp_DU_Billing_Monthly","Guest select billing frequency as Monthly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a monthly subscription payment of AED 30 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC196_Landing_To_SignUp_DU_Billing_Monthly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC197_Landing_To_SignUp_With_Active_DU_Registration: Execution Started");
		   Exttest = Extreport.startTest("TC197_Landing_To_SignUp_With_Active_DU_Registration","Guest enters a valid du number and requests activation code OTP");
		   String DuNumber = Futil.fgetData("Du - Mbile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValidDU_Registration(DuNumber,Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC197_Landing_To_SignUp_With_Active_DU_Registration : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC200_TC218_Landing_to_SignUP_DU_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  String DuNumber = Futil.fgetData("Du - Mbile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  int StepNum = 1;
		  System.out.println("TC200_Landing_To_SignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC200_Landing_To_SignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC200_Landing_To_SignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC1201_Landing_To_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC1201_Landing_To_SignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC1201_Landing_To_SignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC202_GuestAffiliateSignUpp_PaymentPage_MoP_DU: Execution Started");
		   Exttest = Extreport.startTest("TC202_GuestAffiliateSignUpp_PaymentPage_MoP_DU","Guest chooses DU as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_DU_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC202_GuestAffiliateSignUpp_PaymentPage_MoP_DU : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC203_GuestAffiliateSignUpp_DU_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC203_GuestAffiliateSignUpp_DU_Billing_Weekly","Guest select billing frequency as weekly");
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a weekly subscription payment of AED 10 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC203_GuestAffiliateSignUpp_DU_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC204_GuestAffiliateSignUp_MoP_DU_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC204_GuestAffiliateSignUp_MoP_DU_EmptyNumber","Select Weekly Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyDuNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC204_GuestAffiliateSignUp_MoP_DU_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC205_GuestAffiliateSignUp_MoP_DU_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC205_GuestAffiliateSignUp_MoP_DU_ShortNumber","Select Weekly Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithDu_ShortDuNumber(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC205_GuestAffiliateSignUp_MoP_DU_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC206_GuestAffiliateSignUp_MoP_DU_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC206_GuestAffiliateSignUp_MoP_DU_LongNumber","Select Weekly Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithDu_LongDuNumber(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC206_GuestAffiliateSignUp_MoP_DU_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC207_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC207_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber","Select Weekly Payment And enter wrong format du number");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongDuNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC207_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC208_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC208_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode","Select Weekly Payment And enter correct du number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC208_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC209_GuestAffiliateSignUp_MoP_DU_WrongActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC209_GuestAffiliateSignUp_MoP_DU_WrongActivationCode","Select Weekly Payment And enter correct du number, then provide wrong activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC209_GuestAffiliateSignUp_MoP_DU_WrongActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC210_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC210_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode","Select Weekly Payment And enter correct du number, then provide expired activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_ExpiredActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC210_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC211_GuestAffiliateSignUpp_DU_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC211_GuestAffiliateSignUpp_DU_Billing_Monthly","Guest select billing frequency as Monthly");
		   WebElement BtnChangePlan= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-form-back-button']");
		   StepNum = Genf.fClickOnObject(BtnChangePlan, "Click On ChangePlan", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg2, "Disclaimer Text", "By proceeding you agree that a monthly subscription payment of AED 30 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC211_GuestAffiliateSignUpp_DU_Billing_Monthly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC212_GuestAffiliateSignUp_MoP_DU_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC212_GuestAffiliateSignUp_MoP_DU_EmptyNumber","Select Monthly Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyDuNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC212_GuestAffiliateSignUp_MoP_DU_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC213_GuestAffiliateSignUp_MoP_DU_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC213_GuestAffiliateSignUp_MoP_DU_ShortNumber","Select Monthly Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithDu_ShortDuNumber(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC213_GuestAffiliateSignUp_MoP_DU_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC214_GuestAffiliateSignUp_MoP_DU_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC214_GuestAffiliateSignUp_MoP_DU_LongNumber","Select Monthly Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithDu_LongDuNumber(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC214_GuestAffiliateSignUp_MoP_DU_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC215_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC215_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber","Select Monthly Payment And enter wrong format du number");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongDuNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC215_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC216_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC216_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode","Select Monthly Payment And enter correct du number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC216_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC217_GuestAffiliateSignUp_MoP_DU_WrongActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC217_GuestAffiliateSignUp_MoP_DU_WrongActivationCode","Select Monthly Payment And enter correct du number, then provide wrong activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC217_GuestAffiliateSignUp_MoP_DU_WrongActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC218_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC218_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode","Select Monthly Payment And enter correct du number, then provide expired activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_ExpiredActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC218_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
	  
	  Driver.close();
		   
		   
	}
	
	public void TC219_227_Landing_to_SignUp_WithEmail_MoP_Etisalat_Daily_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  
		  int StepNum = 1;
		  System.out.println("TC219_Landing_To_SignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC219_Landing_To_SignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC219_Landing_To_SignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC220_Landing_To_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC220_Landing_To_SignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC220_Landing_To_SignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC221_Landing_To_SignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC221_Landing_To_SignUp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC221_Landing_To_SignUp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC222_Landing_To_SignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC222_Landing_To_SignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC222_Landing_To_SignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC223_Landing_To_SignUp_PaymentPage_MoP_Etisalat: Execution Started");
		   Exttest = Extreport.startTest("TC223_Landing_To_SignUp_PaymentPage_MoP_Etisalat","Guest chooses Etislat as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Etisalat_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC223_Landing_To_SignUp_PaymentPage_MoP_Etisalat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC224_Landing_To_SignUp_Etisalat_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC224_Landing_To_SignUp_Etisalat_ToArbic","Guest at Etislat payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC224_Landing_To_SignUp_Etisalat_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC225_Landing_To_SignUp_Etisalat_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC225_Landing_To_SignUp_Etisalat_ToFrench","Guest at Etisalat Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC225_Landing_To_SignUp_Etisalat_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC226_Landing_To_SignUp_Etisalat_Billing_Daily: Execution Started");
		   Exttest = Extreport.startTest("TC226_Landing_To_SignUp_Etisalat_Billing_Daily","Guest select billing frequency as Daily");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Daily(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a daily subscription payment of AED 3 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC226_Landing_To_SignUp_Etisalat_Billing_Daily : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC227_Landing_To_SignUp_With_Active_Etisalat_Registration_Daily_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC227_Landing_To_SignUp_With_Active_Etisalat_Registration_Daily_Subscription","Guest enters a valid Etisalat number, provide access(OTP) code and register ");
		   String EtisalatNumber = Futil.fgetData("Etisalat - Mobile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_Etisalat_Registration(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC227_Landing_To_SignUp_With_Active_Etisalat_Registration_Daily_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC228_236_Landing_to_SignUp_WithEmail_MoP_Etisalat_Weekly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  
		  int StepNum = 1;
		  System.out.println("TC228_Landing_To_SignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC228_Landing_To_SignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC228_Landing_To_SignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC229_Landing_To_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC229_Landing_To_SignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC229_Landing_To_SignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC230_Landing_To_SignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC230_Landing_To_SignUp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC230_Landing_To_SignUp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC231_Landing_To_SignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC231_Landing_To_SignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC231_Landing_To_SignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC232_Landing_To_SignUp_PaymentPage_MoP_Etisalat: Execution Started");
		   Exttest = Extreport.startTest("TC232_Landing_To_SignUp_PaymentPage_MoP_Etisalat","Guest chooses Etislat as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Etisalat_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC232_Landing_To_SignUp_PaymentPage_MoP_Etisalat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC233_Landing_To_SignUp_Etisalat_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC233_Landing_To_SignUp_Etisalat_ToArbic","Guest at Etislat payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC233_Landing_To_SignUp_Etisalat_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC234_Landing_To_SignUp_Etisalat_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC234_Landing_To_SignUp_Etisalat_ToFrench","Guest at Etisalat Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC234_Landing_To_SignUp_Etisalat_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC235_Landing_To_SignUp_Etisalat_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC235_Landing_To_SignUp_Etisalat_Billing_Weekly","Guest select billing frequency as Daily");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a weekly subscription payment of AED 10 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC235_Landing_To_SignUp_Etisalat_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC236_Landing_To_SignUp_With_Active_Etisalat_Registration_Weekly_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC236_Landing_To_SignUp_With_Active_Etisalat_Registration_Daily_Subscription","Guest enters a valid Etisalat number, provide access(OTP) code and register ");
		   String EtisalatNumber = Futil.fgetData("Etisalat - Mobile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_Etisalat_Registration(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC236_Landing_To_SignUp_With_Active_Etisalat_Registration_Daily_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC237_245_Landing_to_SignUp_WithEmail_MoP_Etisalat_Monthly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  
		  int StepNum = 1;
		  System.out.println("TC237_Landing_To_SignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC237_Landing_To_SignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC237_Landing_To_SignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC238_Landing_To_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC238_Landing_To_SignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC238_Landing_To_SignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC239_Landing_To_SignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC239_Landing_To_SignUp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC239_Landing_To_SignUp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC240_Landing_To_SignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC240_Landing_To_SignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC240_Landing_To_SignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC241_Landing_To_SignUp_PaymentPage_MoP_Etisalat: Execution Started");
		   Exttest = Extreport.startTest("TC241_Landing_To_SignUp_PaymentPage_MoP_Etisalat","Guest chooses Etislat as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Etisalat_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC241_Landing_To_SignUp_PaymentPage_MoP_Etisalat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC242_Landing_To_SignUp_Etisalat_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC242_Landing_To_SignUp_Etisalat_ToArbic","Guest at Etislat payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC242_Landing_To_SignUp_Etisalat_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC243_Landing_To_SignUp_Etisalat_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC243_Landing_To_SignUp_Etisalat_ToFrench","Guest at Etisalat Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC243_Landing_To_SignUp_Etisalat_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC244_Landing_To_SignUp_Etisalat_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC244_Landing_To_SignUp_Etisalat_Billing_Monthly","Guest select billing frequency as Daily");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a monthly subscription payment of AED 30 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC244_Landing_To_SignUp_Etisalat_Billing_Monthly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC245_Landing_To_SignUp_With_Active_Etisalat_Registration_Monthly_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC245_Landing_To_SignUp_With_Active_Etisalat_Registration_Monthly_Subscription","Guest enters a valid Etisalat number, provide access(OTP) code and register ");
		   String EtisalatNumber = Futil.fgetData("Etisalat - Mobile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_Etisalat_Registration(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC245_Landing_To_SignUp_With_Active_Etisalat_Registration_Monthly_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC246_TC266_Landing_to_SignUP_WithEmail_Etisalat_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  String EtisalatNumber = Futil.fgetData("Etisalat - Mobile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  int StepNum = 1;
		  System.out.println("TC246_Landing_To_SignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC246_Landing_To_SignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC246_Landing_To_SignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC1247_Landing_To_SignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC1247_Landing_To_SignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC1247_Landing_To_SignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC248_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat: Execution Started");
		   Exttest = Extreport.startTest("TC248_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat","Guest chooses Etisalat as MoP at Payment page");
		   StepNum = CMoPMethods.afMoP_Etisalat_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC248_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC249_GuestAffiliateSignUpp_Etisalat_Billing_Daily: Execution Started");
		   Exttest = Extreport.startTest("TC249_GuestAffiliateSignUpp_Etisalat_Billing_Daily","Guest select billing frequency as Daily");
		   StepNum = CMoPMethods.afMoP_Billing_Daily(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a daily subscription payment of AED 3 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC249_GuestAffiliateSignUpp_Etisalat_Billing_Daily : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC250_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC250_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber","Select daily Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithEtislata_EmptyEtisalatNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC250_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC251_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC251_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber","Select daily Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_ShortEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC251_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC252_GuestAffiliateSignUp_MoP_Etisalat_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC252_GuestAffiliateSignUp_MoP_Etisalat_LongNumber","Select daily Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_LongEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC252_GuestAffiliateSignUp_MoP_Etisalat_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC253_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC253_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber","Select daily Payment And enter wrong format Etisalat number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_WrongEtisalatNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC253_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC254_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC254_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode","Select daily Payment And enter correct Etisalat number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_EmptyActivationCode(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC254_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC255_GuestAffiliateSignUpp_Etisalat_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC255_GuestAffiliateSignUpp_Etisalat_Billing_Weekly","Guest select billing frequency as weekly");
		   WebElement BtnChangePlan= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-form-back-button']");
		   StepNum = Genf.fClickOnObject(BtnChangePlan, "Click On ChangePlan", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsgDaily= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsgDaily, "Disclaimer Text", "By proceeding you agree that a weekly subscription payment of AED 10 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC255_GuestAffiliateSignUpp_Etisalat_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC256_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC256_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber","Select weekly Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithEtislata_EmptyEtisalatNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC256_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC257_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC257_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber","Select weekly Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_ShortEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC257_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC258_GuestAffiliateSignUp_MoP_Etisalat_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC258_GuestAffiliateSignUp_MoP_Etisalat_LongNumber","Select weekly Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_LongEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC258_GuestAffiliateSignUp_MoP_Etisalat_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC259_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC259_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber","Select weekly Payment And enter wrong format Etisalat number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_WrongEtisalatNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC259_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC260_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC260_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode","Select weekly Payment And enter correct Etisalat number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_EmptyActivationCode(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC260_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   
		   System.out.println("TC261_GuestAffiliateSignUpp_Etisalat_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC261_GuestAffiliateSignUpp_Etisalat_Billing_Monthly","Guest select billing frequency as Monthly");
		   StepNum = Genf.fClickOnObject(BtnChangePlan, "Click On ChangePlan", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg2, "Disclaimer Text", "By proceeding you agree that a monthly subscription payment of AED 30 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC261_GuestAffiliateSignUpp_Etisalat_Billing_Monthly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC262_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC262_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber","Select Monthly Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithEtislata_EmptyEtisalatNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC262_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC263_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC263_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber","Select Monthly Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_ShortEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC263_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC264_GuestAffiliateSignUp_MoP_Etisalat_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC264_GuestAffiliateSignUp_MoP_Etisalat_LongNumber","Select Monthly Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_LongEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC264_GuestAffiliateSignUp_MoP_Etisalat_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC265_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC265_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber","Select Monthly Payment And enter wrong format Etisalat number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_WrongEtisalatNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC265_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC266_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC266_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode","Select Monthly Payment And enter correct Etisalat number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_EmptyActivationCode(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC266_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode : Execution Finished");
		   StepNum = Genf.gfDeleteGigyaTraces(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Extreport.flush();
		   Extreport.endTest(Exttest);
	       Driver.close();
		   
		   
	}
	
	public void TC269_277_Guest_landing_To_SignUp_WithEmail_MoP_MarocTelecom_Weekly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC269_Landing_To_SignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC269_Landing_To_SignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC269_Landing_To_SignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC270_Landing_To_SignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC270_Landing_To_SignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC270_Landing_To_SignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC271_Landing_To_SignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC271_Landing_To_SignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC271_Landing_To_SignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC272_Landing_To_SignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC272_Landing_To_SignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC272_Landing_To_SignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC273_Landing_To_SignUpp_PaymentPage_MoP_MarocTelecom: Execution Started");
		   Exttest = Extreport.startTest("TC273_Landing_To_SignUpp_PaymentPage_MoP_MarocTelecom","Guest chooses Maroc Telecom as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_MarocTelecom_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC273_Landing_To_SignUpp_PaymentPage_MoP_MarocTelecom : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC274_Landing_To_SignUpp_MarocTelecom_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC274_Landing_To_SignUpp_MarocTelecom_ToArbic","Guest at Maroc Telecom payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC274_Landing_To_SignUpp_MarocTelecom_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC275_Landing_To_SignUpp_MarocTelecom_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC275_Landing_To_SignUpp_MarocTelecom_ToFrench","Guest at Maroc telecom Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC275_Landing_To_SignUpp_MarocTelecom_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC276_Landing_To_SignUpp_MarocTelecom_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC276_Landing_To_SignUpp_MarocTelecom_Billing_Weekly","Guest select billing frequency as Weekly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Genf.gfSleep(3);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By clicking continue you agree to be charged 15 DH TTC per week after your trial ends. It will be deducted from your account balance or your monthly bill if you are post paid.");
		   System.out.println("TC276_Landing_To_SignUpp_MarocTelecom_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC277_Landing_To_SignUpp_With_Active_MarocTelecom_Registration_Weekly_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC277_Landing_To_SignUpp_With_Active_MarocTelecom_Registration_Weekly_Subscription","Guest enters a valid Maroc Telecom number, provide access(OTP) code and register ");
		   String MarocNumber = Futil.fgetData("Maroc Telecom Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_MarocTelecom_Registration(MarocNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC277_Landing_To_SignUpp_With_Active_MarocTelecom_Registration_Weekly_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC278_286_Guest_landing_To_SignUp_WithEmail_MoP_MarocTelecom_Monthly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC278_Landing_To_SignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC278_Landing_To_SignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyLandingPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC278_Landing_To_SignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC279_Landing_To_SignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC279_Landing_To_SignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC279_Landing_To_SignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC280_Landing_To_SignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC280_Landing_To_SignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC280_Landing_To_SignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC281_Landing_To_SignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC281_Landing_To_SignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC281_Landing_To_SignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC282_Landing_To_SignUpp_PaymentPage_MoP_MarocTelecom: Execution Started");
		   Exttest = Extreport.startTest("TC282_Landing_To_SignUpp_PaymentPage_MoP_MarocTelecom","Guest chooses Maroc Telecom as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_MarocTelecom_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC282_Landing_To_SignUpp_PaymentPage_MoP_MarocTelecom : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC283_Landing_To_SignUpp_MarocTelecom_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC283_Landing_To_SignUpp_MarocTelecom_ToArbic","Guest at Maroc Telecom payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC283_Landing_To_SignUpp_MarocTelecom_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC284_Landing_To_SignUpp_MarocTelecom_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC284_Landing_To_SignUpp_MarocTelecom_ToFrench","Guest at Maroc telecom Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC284_Landing_To_SignUpp_MarocTelecom_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC285_Landing_To_SignUpp_MarocTelecom_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC285_Landing_To_SignUpp_MarocTelecom_Billing_Monthly","Guest select billing frequency as Monthly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Genf.gfSleep(3);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By clicking continue you agree to be charged 50 DH TTC per month after your trial ends. It will be deducted from your account balance or your monthly bill if you are post paid.");
		   System.out.println("TC285_Landing_To_SignUpp_MarocTelecom_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC286_Landing_To_SignUpp_With_Active_MarocTelecom_Registration_Monthly_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC286_Landing_To_SignUpp_With_Active_MarocTelecom_Registration_Monthly_Subscription","Guest enters a valid Maroc Telecom number, provide access(OTP) code and register ");
		   String MarocNumber = Futil.fgetData("Maroc Telecom Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_MarocTelecom_Registration(MarocNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC286_Landing_To_SignUpp_With_Active_MarocTelecom_Registration_Monthly_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}

	public int afVerifyLandingPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String ResultLocation,int StepNum)
	{
		String strCurrURL = null;
		try{
			Futil = new FrameWorkUtility();
			  Genf = new GeneralMethods();
			  //CHMethods = new CommonHeaderMethods();
			  //CSignUpMethods = new CommonSignUpMethods();
			   strCurrURL = Driver.getCurrentUrl();
			  if(strCurrURL.contains("landing")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify landing Page", "Successfully verified the landing page With URL :"+strCurrURL, "No");
				  System.out.println("Successfully verified the landing page With URL :"+strCurrURL);  
				  StepNum = StepNum+1;
			  }else if(strCurrURL.equals("about:blank"))
			  {
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify landing Page", "Landing page verification failed And The URL was"+strCurrURL, "No");
				  System.out.println("Landing page verification failed And The URL was"+strCurrURL); 
				  StepNum = StepNum+1;
				  Driver.close();
				  Assert.fail("Landing page verification failed And The URL was"+strCurrURL);
			  }else if(strCurrURL.contains("error"))
			  {
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify landing Page", "Landing page verification failed And The URL was"+strCurrURL, "No");
				  System.out.println("Landing page verification failed And The URL was"+strCurrURL); 
				  StepNum = StepNum+1;
				  Driver.close();
				  Assert.fail("Landing page verification failed And The URL was"+strCurrURL);
			  }
			  else
			  {
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Warning", "Step "+StepNum+". Verify landing Page", "Landing page verification failed And The URL was"+strCurrURL, "Yes");
				  System.out.println("Landing page verification failed And The URL was"+strCurrURL); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify landing Page", "Landing page verification failed And The URL was"+strCurrURL, "Yes");
			  System.out.println("Landing page verification failed And The URL was"+strCurrURL); 
			  StepNum = StepNum+1;
		}
		
		   return StepNum;
	}
	
	
	/*public void TC051_User_Landing_to_Login_fromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  int StepNum = 1;
		  System.out.println("TC051_User_Landing_to_Login_fromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC051_User_Landing_to_Login_fromHeader","User is at Landing page and proceeds to Log In page");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  if(AppTitle.equals(Driver.getTitle())){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
			  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
			  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
			  StepNum = StepNum+1;
		  }
		  
		   StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC051_User_Landing_to_Login_fromHeader : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}
	
	public void TC052_User_Landing_to_Login_NoUsrNameNoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CLoginMethods = new CommonLoginMethods();
		  int StepNum = 1;
		  System.out.println("TC052_User_Landing_to_Login_NoUsrNameNoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC052_User_Landing_to_Login_NoUsrNameNoPwd","User tries to Log In with no username and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, "Firefox", AppURL,TestType,NodeURL,Cap);
		  if(AppTitle.equals(Driver.getTitle())){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Launch Starz Play Test Environment landing Page", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
			  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
			  System.out.println("Failed To Launch And The title was"+Driver.getTitle()); 
			  StepNum = StepNum+1;
		  }
		  
		   StepNum = CHMethods.afLandingToLogin(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   
		   System.out.println("TC052_User_Landing_to_Login_NoUsrNameNoPwd : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
		   
	}*/
	
	/*public int afVerifyGooglePlayStore_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		try{
			WebElement imgDwnldOnPlayStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='Get it on Google play']");
			  if(imgDwnldOnPlayStore!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", imgDwnldOnPlayStore);
				  //btnRegisterYourFree.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store From Footer", "Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer", "No");
				  System.out.println("Successfully Clicked on the button GET IT ON GOOGLE PLAY From Footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On GET IT ON GOOGLE PLAY Store From Footer", "Failed To Click On  the button GET IT ON GOOGLE PLAY From Footer", "No");   
				  System.out.println("Failed To Click On  the button Download on the App Store From Footer"); 
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("com.parsifal.starz"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the play store page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the play store page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed to verify the play store page for STARZ PLAY ARABIA the URL is "+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The play store for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for play store", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for play store");
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Google Play Store page", "Verification failed for The Google Play Store page", "No");
			  System.out.println("Verification failed for The Page Google Play Store");
			  StepNum = StepNum+1;	
		}
		 	 return  StepNum;
	}
	
	
	public int afVerifyAppStore_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		 try{
			 WebElement imgDwnldOnAppStore= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer//img[@alt='Download on the App Store']");
			  if(imgDwnldOnAppStore!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", imgDwnldOnAppStore);
				  //btnRegisterYourFree.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Download on the App Store from Footer", "Successfully Clicked on the button Download on the App Store from Footer", "No");
				  System.out.println("Successfully Clicked on the button Download on the App Store from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Download on the App Store from footer", "Failed To Click On  the button Download on the App Store from footer", "No");   
				  System.out.println("Failed To Click On  the button Download on the App Store from footer"); 
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("itunes.apple.com/ae/app/starzplay-arabia"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the Appstore page for STARZ PLAY ARABIA, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed to verify the Appstore page for STARZ PLAY ARABIA  with URL :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Appstore for Starzplay Arabia Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Appstore", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Appstore");
				  StepNum = StepNum+1;
				  
			  }
			 
		 }catch(Exception e){
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The AppStore page", "Verification failed for The AppStore page", "No");
			  System.out.println("Verification failed for The Page AppStore");
			  StepNum = StepNum+1;
			 
		 }
		  
		return  StepNum; 
		  
	}
	
	public int afVerifyYouTube_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			WebElement ImgYoutube= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialYoutube']/span");
			  if(ImgYoutube!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", ImgYoutube);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Youtube icon from footer", "Successfully Clicked on the Youtube icon from footer", "No");
				  System.out.println("Successfully Clicked on the Youtube icon from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Youtube icon from footer", "Failed To Click On  the Youtube icon from footer", "No");   
				  System.out.println("Failed To Click On  the Youtube icon from footer"); 
				  StepNum = StepNum+1;
				  //Assert.fail("Failed To Click On  the Instagram icon from footer");
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("youtube"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Youtube Page Has opened", "Successfully verified the Youtube page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Youtube page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Youtube Page Has opened", "Failed To Verfiy the Youtube page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Youtube page, And the URL is :"+Driver.getCurrentUrl());
				 //Assert.fail("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Youtube Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Youtube", "No");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Youtube");
				  //Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The YouTube page", "Verification failed for The YouTube page", "No");
			  System.out.println("Verification failed for The Page YouTube");
			  StepNum = StepNum+1;
			
		}
		 
		return   StepNum;
		  
	}
	
	public int afVerifyInstagram_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		try{
			WebElement ImgInstagram= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialInstagram']/span");
			  if(ImgInstagram!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", ImgInstagram);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Instagram icon from footer", "Successfully Clicked on the Instagram icon from footer", "No");
				  System.out.println("Successfully Clicked on the Instagram icon from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Instagram icon from footer", "Failed To Click On  the Instagram icon from footer", "No");   
				  System.out.println("Failed To Click On  the Instagram icon from footer"); 
				  Assert.fail("Failed To Click On  the Instagram icon from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("instagram.com/starzplayarabia/"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Instagram Page Has opened", "Successfully verified the Instagram page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Instagram page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Instagram Page Has opened", "Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Instagram page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Instagram Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram", "No");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Instagram");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Instagram page", "Verification failed for The Instagram page", "No");
			  System.out.println("Verification failed for The Page Instagram");
			  StepNum = StepNum+1;
		}
		 
		return   StepNum;
		 
	}
	
	public int afVerifyTwitter_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		try{
			WebElement ImgTwitter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialTwitter']/span");
			  if(ImgTwitter!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", ImgTwitter);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Twitter icon from footer", "Successfully Clicked on the Twitter icon from footer", "No");
				  System.out.println("Successfully Clicked on the Twitter icon from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Twitter icon from footer", "Failed To Click On  the Twitter icon from footer", "No");   
				  System.out.println("Failed To Click On  the Twitter icon from footer"); 
				  Assert.fail("Successfully Clicked on the Twitter icon from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("twitter.com/starzplayarabia"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Twitter Page Has opened", "Successfully verified the Twitter page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Twitter page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Twitter Page Has opened", "Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Twitter page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Twitter Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter", "No");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter");
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Twitter");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Twitter page", "Verification failed for The Twitter page", "No");
			  System.out.println("Verification failed for The Page Twitter");
			  StepNum = StepNum+1;
		}
		 
		return StepNum;
		  
	}
	
	public int afverifyFaceBook_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		try{
			WebElement ImgFacebook= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='socialFacebook']/span");
			  if(ImgFacebook!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", ImgFacebook);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Facebook icon from footer", "Successfully Clicked on the Facebook icon from footer", "No");
				  System.out.println("Successfully Clicked on the Facebook icon from footer"); 
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Facebook icon from footer", "Failed To Click On  the Facebook icon from footer", "No");   
				  System.out.println("Failed To Click On  the Facebook icon from footer"); 
				  Assert.fail("Failed To Click On  the Facebook icon from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("facebook.com/starzplayarabia"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "No");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook page", "Verification failed for The Facebook page", "No");
			  System.out.println("Verification failed for The Page Facebook");
			  StepNum = StepNum+1;
			
		}
		 
		return StepNum;  
		  
	}
	
	
	public int afVerifyCareers_Footer(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		try{
			WebElement lnCareers= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Careers']");
			  if(lnCareers!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnCareers);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Careers from footer", "Successfully Clicked on the link Careers from footer", "No");
				  System.out.println("Successfully Clicked on the link Careers from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Careers from footer", "Failed To Click On  the link Careers from footer", "No");   
				  System.out.println("Failed To Click On the link Careers from footer"); 
				  Assert.fail("Failed To Click On the link Careers from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(5000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("starzplayarabia.bamboohr"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Career Page Has opened", "Successfully verified the Career page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Career page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Career Page Has opened", "Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Career page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Career Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Career", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Career");
				  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Career");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy page", "Verification failed for The Privacy Policy page", "No");
			  System.out.println("Verification failed for The Page Privacy Policy");
			  StepNum = StepNum+1;
			
		}
		 
		return StepNum; 	  
		  
	}
	
	public int afVerifyBlog_fromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{

		 try{
			 WebElement lnBlog= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Blog']");
			  if(lnBlog!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnBlog);
				  //btnRegisterYourFree.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Blog from footer", "Successfully Clicked on the link Blog from footer", "No");
				  System.out.println("Successfully Clicked on the Blog Policy from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Blog from footer", "Failed To Click On  the link Blog from footer", "No");   
				  System.out.println("Failed To Click On  the link Blog from footer"); 
				  Assert.fail("Failed To Click On  the link Blog from footer");
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  if((Driver.getCurrentUrl().contains("blog.starzplay"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Blog Page Has opened", "Successfully verified the Blog page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Blog page with URL :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Blog Page Has opened", "Failed To Verfiy the Blog page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Blog page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Blog page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			 
		 }catch(Exception e){
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy page", "Verification failed for The Privacy Policy page", "No");
			  System.out.println("Verification failed for The Page Privacy Policy");
			  StepNum = StepNum+1;
		 }
		 return StepNum;    
		  
	}
	
	public int afVerifyPrivacyPolicy_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		try{
			
			WebElement lnPrivacyPolicy= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Privacy Policy']");
			  if(lnPrivacyPolicy!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnPrivacyPolicy);
				  //btnRegisterYourFree.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Privacy Policy from footer", "Successfully Clicked on the link Privacy Policy from footer", "No");
				  System.out.println("Successfully Clicked on the link Privacy Policy from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Privacy Policy from footer", "Failed To Click On  the link Privacy Policy from footer", "No");   
				  System.out.println("Failed To Click On  the link Privacy Policy from footer"); 
				  Assert.fail("Failed To Click On  the link Privacy Policy from footer");
				  StepNum = StepNum+1;
				   }
			  
			  try{
				  Thread.sleep(10000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("/en/privacypolicy"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Privacy Policy Page Has opened", "Successfully verified the Privacy Policy page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Privacy Policy page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy Page Has opened", "Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Verfiy the Privacy Policy page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Privacy Policy", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Privacy Policy");
				  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Privacy Policy");
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Privacy Policy page", "Verification failed for The Privacy Policy page", "No");
			  System.out.println("Verification failed for The Page Privacy Policy");
			  StepNum = StepNum+1;
		}
		 
		return StepNum;  
		  
	}
	
	public int afVerifyTermsAndAconditions_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		  try{
			  
			  WebElement lnTermsNCond= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Terms & Conditions']");
			  if(lnTermsNCond!=null){
				  //lnTermsNCond.click();
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnTermsNCond);
				  
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Terms & Conditions from footer", "Successfully Clicked on Terms & Conditions from footer", "No");
				  System.out.println("Successfully Clicked on Terms & Conditions from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Terms & Conditions from footer", "Failed To Click Terms & Conditions from footer", "No");   
				  System.out.println("Failed To Click Terms & Conditions from footer"); 
				  Assert.fail("Failed To Click Terms & Conditions from footer");
				  StepNum = StepNum+1;
				   }
			  Genf.gfSleep(5);
			  
			  try{
				  Thread.sleep(10000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  WebElement lblTermsAndCond= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//h1[text()='Terms & Conditions']");
			  if((Driver.getCurrentUrl().contains("/en/termsandconditions"))&& (lblTermsAndCond!=null)){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to Terms & Conditions", "Successfully Loaded the Terms & Conditions page with URL :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Successfully Loaded the Terms & Conditions page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Terms & Conditions", "Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Load the Terms & Conditions page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Terms & Conditions", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Terms & Conditions", "No");
				  System.out.println("Failed To Verfiy the Window Opened Or There is no new Window Opened for Terms & Conditions");
				  Assert.fail("Failed To Verfiy the Window Opened Or There is no new Window Opened for Terms & Conditions");
				  StepNum = StepNum+1;
				  
			  }
			  
		  }catch(Exception e){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Terms & Conditions page", "Verification failed for The Terms & Conditions page", "No");
			  System.out.println("Verification failed for The Page Terms & Conditions");
			  StepNum = StepNum+1;
			  
		  }
		  
		return StepNum;  
		  
	}
	
	public int afVerifyCompanyPage_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		try{
			
			WebElement lnCompany= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Company']");
			  if(lnCompany!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnCompany);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Company from footer", "Successfully Clicked on Company from footer", "No");
				  System.out.println("Successfully Clicked on Company from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Company from footer", "Failed To Click Company from footer", "No");   
				  System.out.println("Failed To Click On Company from footer"); 
				  Assert.fail("Failed To Click On Company from footer");
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  
			  if((Driver.getCurrentUrl().contains("/en/aboutus"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to company aboutus", "Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to company aboutus", "Failed To Load the aboutus page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the aboutus page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Successfully Loaded the aboutus page with URL :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Company page", "Verification failed for The Company page", "No");
			  System.out.println("Verification failed for The Page Company");
			  StepNum = StepNum+1;
			
		}
		 
		return StepNum;  
		  
		  
		  
	}
	
	public int afVerifyPartnerWithUs_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		 try{
			 
			 WebElement lnPartnerWithUs= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Partner with us']");
			  if(lnPartnerWithUs!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", lnPartnerWithUs);
				  //btnLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Partner with us from footer", "Successfully Clicked on Partner with us from footer", "No");
				  System.out.println("Successfully Clicked on Partner with us from footer");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Partner with us from footer", "Failed To Click Partner with us from footer", "No");   
				  System.out.println("Failed To Click On Partner with us from footer"); 
				  Assert.fail("Failed To Click On Partner with us from footer");
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  
			  if((Driver.getCurrentUrl().contains("/en/affiliatesubscription"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to Partner with us", "Successfully Loaded the Partner with us page with URL :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Successfully Loaded the Partner with us page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to Partner with us", "Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl());
				  Assert.fail("Failed To Load the Partner with us page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			 
		 }catch(Exception e){
			 
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Partner with us page", "Verification failed for The Partner with us page", "No");
			  System.out.println("Verification failed for The Page Partner with us");
			  StepNum = StepNum+1;
			 
		 }
		  
		  return StepNum;
		  
	}
	
	public int afVerifyHelpCenter_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		
		try{
			WebElement btnHelpCenter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Help Center']");
			  if(btnHelpCenter!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnHelpCenter);
				  //btnLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Help Center from footer", "Successfully Clicked on Help Center from footer", "No");
				  System.out.println("Successfully Clicked on Help Center from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Help Center from footer", "Failed To Click On Clicked on Help Center from footer", "No");   
				  System.out.println("Failed To Click On Clicked on Help Center from footer"); 
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  
			  WebElement lblHelpCenter= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//h1[text()='Help Center']");
			  if(lblHelpCenter!=null){
				  
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The HELP CENTER Heading Label", "Successfully Verified the HELP CENTER Heading label", "Yes");
				  System.out.println("Successfully Verified the HELP CENTER Heading label");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The HELP CENTER Heading Label", "Failed To Verified the HELP CENTER Heading label", "No");   
				  System.out.println("Failed To Verified the HELP CENTER Heading label"); 
				  StepNum = StepNum+1;
			  }
			  
			  if((Driver.getCurrentUrl().contains("faq"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to faq", "Successfully Loaded the faq page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Loaded the Help Center page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to faq", "Failed To Load the faq page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the Why Help Center page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Help Center Page", "Verification failed for The Page Help Center", "No");
			  System.out.println("Verification failed for The Page Help Center");
			  StepNum = StepNum+1;
			}  
		return StepNum;
	}
	
	public int afVerifyWhyStarzPlayFromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		try{
			WebElement btnWhyStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//footer[@class='footer']//a[@title='Why STARZ PLAY?']");
			  if(btnWhyStarzPlay!=null){
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnWhyStarzPlay);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Why STARZ PLAY? from footer", "Successfully Clicked on Why STARZ PLAY? from footer", "No");
				  System.out.println("Successfully Clicked on Why STARZ PLAY? from footer");   
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Why STARZ PLAY? from footer", "Failed To Click On Clicked on Why STARZ PLAY? from footer", "No");   
				  System.out.println("Failed To Click On Clicked on Why STARZ PLAY? from footer"); 
				  StepNum = StepNum+1;
				   }
			  Genf.gfSleep(5);
			  
			  WebElement lblWhyStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[text()='WHY STARZ PLAY?']");
			  if(lblWhyStarzPlay!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The WHY STARZ PLAY? Label", "Successfully Verified the WHY STARZ PLAY? label", "Yes");
				  System.out.println("Successfully Verified the Why Starz Play Page");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The WHY STARZ PLAY? Label", "Failed To Verified the WHY STARZ PLAY? label", "No");   
				  System.out.println("Failed To Verified the WHY STARZ PLAY? label");  
				  StepNum = StepNum+1;
			  }
			  
			  if((Driver.getCurrentUrl().contains("whyStarzPlay"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to whyStarzPlay", "Successfully Loaded the whyStarzPlay page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Loaded the whyStarzPlay page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to whyStarzPlay", "Failed To Load the Why STARZ PLAY page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the Why STARZ PLAY page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  
			  
			
		}catch(Exception e)
		{
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Wahy Starz play? Page", "Verfication Failed, Please refer report", "Yes"); 
			 System.out.println("WHY STARZ Play? Page verification failed");  
			 Assert.fail("WHY STARZ Play? Page verification failed");
			 StepNum = StepNum+1; 		
		}	
		
		return StepNum;
		  
	}*/
	
	public int afLoginToPortal(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String ScreenShot)
	{
		  
		 try{
			 
			 
			 WebElement btnLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//a[@id='login-button']");
			  if(btnLogin!=null){
				  btnLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login Button", "Successfully Clicked on the button Login", "No");
				  System.out.println("Successfully Clicked on the button Login");  
				  StepNum = StepNum+1;
				   }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login Button", "Failed To Click On  Clicked on the button Login", ScreenShot);   
				  System.out.println("Failed To Click On  Clicked on the button Login");  
				  Assert.fail("Failed To Click On  Clicked on the button Login");
				  StepNum = StepNum+1;
				   }
			  Thread.sleep(5000);
			  WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  //String EmailId = "ibrahim.poovakkattil@starzplayarabia.com";
				  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
				  EdtUserName.sendKeys(EmailId);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Email Address", "Successfully Entered The Email id :"+EmailId, "No");
				  System.out.println("Successfully Entered The Email id :"+EmailId);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Email Address", "Failed To Enter Email ID", ScreenShot);   
				  System.out.println("Failed To Enter Email ID"); 
				  Assert.fail("Failed To Enter Email ID");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  //String Password = "123456";
				  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", ScreenShot);   
			      System.out.println("Failed To Enter Password");  
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin!=null){
				  btnSubmitLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login Submit BUtton", "Successfully Clicked on the button Login", "No");
				  System.out.println("Successfully Clicked on the button Login");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login Submit Button", "Failed To Click On Clicked on the button Login", ScreenShot);   
				  System.out.println("Failed To Click On Clicked on the button Login"); 
				  Assert.fail("Failed To Click On Clicked on the button Login");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(5000);
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  //WebElement imgSettings = Driver.findElement(By.xpath(".//*[@id='dropSettings']"));
				  WebElement imgSettings= Genf.setObjectInHtmlUnit(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
				  if(imgSettings!=null){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The user is logged in", "Successfully Verified The User is logged in", ScreenShot);
					  System.out.println("Successfully Verified The User is logged in");  
					  StepNum = StepNum+1;   
				  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "Failed To Verify the User is logged in", ScreenShot);   
					  System.out.println("Failed To Verify the user is logged in"); 
					  //Assert.fail("Failed To Verify the Landing page, page The URL is :"+Driver.getCurrentUrl());
					  //Driver.close();
					  StepNum = StepNum+1;   
				  }
			  }
		 else{
	          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds", ScreenShot);
			  System.out.println("The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
			  StepNum = StepNum+1;
		 } 
			  
			 
		 }catch(Exception e){
			 
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Login To Application", "Login Failed, Please refer the report", ScreenShot); 
			 System.out.println("Login Failed");  
			 Assert.fail("Login Failed");
			 StepNum = StepNum+1;  
		 }
		  
		  return StepNum;
	  }
	
public void fManageAlert(WebDriver Driver){
	Genf = new GeneralMethods();
	boolean isAlertExist = Genf.isAlertPresent(Driver);
	if(isAlertExist){
		 Alert alert=Driver.switchTo().alert();
		 System.out.println(alert.getText());
		 alert.authenticateUsing(new UserAndPassword("starz","milkyway"));
		
	}

	
}

public void samplef(){
	
	System.out.println("sample AF");
	
}
	
}

