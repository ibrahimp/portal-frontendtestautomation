package portalfrontend;

import java.awt.Toolkit;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;


public class GeneralMethods {
	
	//public FrameWorkUtility Futil;
	
	
	public boolean isAlertPresent(WebDriver Driver){
	    boolean foundAlert = false;
	    WebDriverWait wait = new WebDriverWait(Driver, 10);
	    try {
	        wait.until(ExpectedConditions.alertIsPresent());
	        foundAlert = true;
	    } catch (Exception eTO) {
	        foundAlert = false;
	    }
	    return foundAlert;
	}
	
	public WebDriver gnLaunchBrowser(WebDriver Driver,String Browser,String AppURL,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		FrameWorkUtility Futil = new FrameWorkUtility();
		GeneralMethods Genf = new GeneralMethods();
		if(TestType.equals("Normal"))
		{
		switch(Browser.toLowerCase()){
		case "phantomjs":
			DesiredCapabilities caps =  DesiredCapabilities.phantomjs();
		    ArrayList<String> cliArgsCap = new ArrayList<String>();
		    cliArgsCap.add("--web-security=no");
		    cliArgsCap.add("--ssl-protocol=any");
		    cliArgsCap.add("--ignore-ssl-errors=yes");
		    caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cliArgsCap);
		    caps.setCapability(PhantomJSDriverService.PHANTOMJS_GHOSTDRIVER_CLI_ARGS,new String[] { "--logLevel=2" });
			//caps.setJavascriptEnabled(true);
	        //caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes","--ignore-ssl-errors=true","--ssl-protocol=TLSv1"});
	        //caps.setJavascriptEnabled(true);
	        //caps.setCapability("--ssl-protocol", "tlsv1");
	        //caps.setCapability("--ignore-ssl-errors", "yes");
	        //caps.setCapability("--web-security", "false");
	        //caps.setCapability("webSecurityEnabled", "false");
	        //caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--ssl-protocol=any","--ignore-ssl-errors=yes","--web-security=no"});
	        //caps.setCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.26 Safari/537.36");
	      
		  Driver = new PhantomJSDriver(caps); 
		  Driver.manage().window().setSize(new Dimension(1920, 1080));
		  break;
		case "firefox":
			Driver = new FirefoxDriver(); 
			Driver.manage().window().setSize(new Dimension(1920, 1080));
		  break;
		case "chrome":
			ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-infobars");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			prefs.put("browser.cache.disk.enable", false);
			prefs.put("browser.cache.memory.enable", false);
			prefs.put("browser.cache.offline.enable", false);
			prefs.put("network.http.use-cache", false);
			options.setExperimentalOption("prefs", prefs);
			File curDir = new File(".");
			String chromeDriverPath = Futil.getfilePath(curDir,"chromedriver");
			System.setProperty("webdriver.chrome.driver", chromeDriverPath);
			//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/Resources/chromedriver");
			Driver = new ChromeDriver(options); 
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Dimension screenResolution = new Dimension((int) 
			toolkit.getScreenSize().getWidth(), (int) 
			toolkit.getScreenSize().getHeight());
			Driver.manage().window().setSize(screenResolution);
			Driver.manage().window().maximize();
		  break;
		case "safari":
			DesiredCapabilities safaricaps =  DesiredCapabilities.safari();
			safaricaps.setCapability("safariIgnoreFraudWarning", "True");
			Driver = new SafariDriver(safaricaps); 
			Driver.manage().window().setSize(new Dimension(1920, 1080));
		  break;
		case "htmlunit":
			Driver = new HtmlUnitDriver(true); 
			Driver.manage().window().setSize(new Dimension(1920, 1080));
		  break;
		}
		}
		else if(TestType.equals("Grid"))
		{
			//Grid Driver Initialization
			Driver = new RemoteWebDriver(new URL(NodeURL),Cap);
		}
		//Driver.manage().window().maximize();
		//Driver.manage().window().setSize(new Dimension(1920, 1080));
		Driver.get(AppURL);
		Genf.gfSleep(2);
		Driver.navigate().to(AppURL.replace("starz:milkyway", ""));
		return Driver;
	}
	
	public void gfSleep(int seconds){
		try {
			Thread.sleep(seconds*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public WebElement setObject(int intMaxSec,WebDriver Driver,ExtentReports Extreport,ExtentTest Exttest,String ResultLocation,String How,String value)
	 {
		WebElement myDynamicElement = null;
		FrameWorkUtility Futil = new FrameWorkUtility();
		 try
		 {
			 switch(How)
			 {
			 case "id":
			 myDynamicElement = (new WebDriverWait(Driver, intMaxSec)).until(ExpectedConditions.presenceOfElementLocated(By.id(value)));
			 break;
			 case "xpath":
			 myDynamicElement = (new WebDriverWait(Driver, intMaxSec)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(value)));
			 break;
			 case "name":
			 myDynamicElement = (new WebDriverWait(Driver, intMaxSec)).until(ExpectedConditions.presenceOfElementLocated(By.name(value)));
			 break;
			 } 
		 }
		 catch(Exception e)
		 {
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Object With "+How+"="+value+"Creation Failed", e.toString(), "Yes");
			 return myDynamicElement;
		 }
		 return myDynamicElement;
	}
	
	public WebElement setObjectInHtmlUnit(int intMaxSec,WebDriver Driver,ExtentReports Extreport,ExtentTest Exttest,String ResultLocation,String How,String value)
	 {
		WebElement myDynamicElement = null;
		FrameWorkUtility Futil = new FrameWorkUtility();
		 try
		 {
			 switch(How)
			 {
			 case "id":
			 myDynamicElement = (new WebDriverWait(Driver, intMaxSec)).until(ExpectedConditions.visibilityOfElementLocated(By.id(value)));
			 break;
			 case "xpath":
			 myDynamicElement = (new WebDriverWait(Driver, intMaxSec)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value)));
			 break;
			 case "name":
			 myDynamicElement = (new WebDriverWait(Driver, intMaxSec)).until(ExpectedConditions.visibilityOfElementLocated(By.name(value)));
			 break;
			 } 
		 }
		 catch(Exception e)
		 {
			 Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Object With "+How+"="+value+"Creation Failed", e.toString(), "Yes");
			 return myDynamicElement;
		 }
		 return myDynamicElement;
	}
	
	public String GenrateEmailId()
	{
		
		  Calendar calendar = Calendar.getInstance();
			calendar.setTime(calendar.getTime());
			int Year = Calendar.YEAR;
			int month = Calendar.MONTH;
			int day = Calendar.DAY_OF_MONTH;
			int hours = calendar.get(Calendar.HOUR_OF_DAY);
			int minutes = calendar.get(Calendar.MINUTE);
			int seconds = calendar.get(Calendar.SECOND);
			String Rand = Integer.toString(Year) +Integer.toString(month) + Integer.toString(day) + Integer.toString(hours)
			+ Integer.toString(minutes) + Integer.toString(seconds);
		  //ran = 100 + (int)(Math.random() * ((10000 - 100) + 1));
		  String EmailId = "SignUpEmail."+Rand+"@test.com";
		  return EmailId;
		
	}
	
	public String GenrateRandomName()
	{
		
		  Calendar calendar = Calendar.getInstance();
			calendar.setTime(calendar.getTime()); 
			int Year = Calendar.YEAR;
			int month = Calendar.MONTH;
			int day = Calendar.DAY_OF_MONTH;
			int hours = calendar.get(Calendar.HOUR_OF_DAY);
			int minutes = calendar.get(Calendar.MINUTE);
			int seconds = calendar.get(Calendar.SECOND);
			String Rand = Integer.toString(Year)+Integer.toString(month) + Integer.toString(day) + Integer.toString(hours)
			+ Integer.toString(minutes) + Integer.toString(seconds);
		  //ran = 100 + (int)(Math.random() * ((10000 - 100) + 1));
		  String EmailId = "Name"+Rand;
		  return EmailId;
		
	}
	
	public int gfVerifyErrorMessage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,WebElement errObj,String errParentObjectName,String errExpErrorMsg)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			
			String actErrorMessage = errObj.getText().trim();
			if(actErrorMessage.equals(errExpErrorMsg.trim())){
				Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Error Message for :"+errParentObjectName, "Successfully Verified The error message '"+actErrorMessage+"' for the field :"+errParentObjectName, "Yes");
				System.out.println("Successfully Verified The error message '"+actErrorMessage+"' for the field :"+errParentObjectName);  
				StepNum = StepNum+1; 
			}else{
				Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Error Message for :"+errParentObjectName, "Failed to Verify The error message '"+errExpErrorMsg+"' for the field :"+errParentObjectName+" , the actual error message in application was "+actErrorMessage, "Yes");
				System.out.println("Failed to Verify The error message '"+errExpErrorMsg+"' for the field :"+errParentObjectName+" , the actual error message in application was "+actErrorMessage);  
				StepNum = StepNum+1; 
			}
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Error Message for :"+errParentObjectName, "Failed to Verify The error message '"+errExpErrorMsg+"' for the field :"+errParentObjectName, "Yes");
			System.out.println("Failed to Verify The error message "+errExpErrorMsg+" for the field"+errParentObjectName);  
			StepNum = StepNum+1; 
		}
		return StepNum;
	}
	
	public int gfVerifyAlertMessage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,WebElement errObj,String alertParentObjectName,String ExpAlertMsg)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			
			String actAlertMessage = errObj.getText().trim();
			actAlertMessage = actAlertMessage.replace("\n", " ").replace("\r", " ");
			if(actAlertMessage.equals(ExpAlertMsg.trim())){
				Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Alert Message for :"+alertParentObjectName, "Successfully Verified The Alert message '"+actAlertMessage+"' for the field :"+alertParentObjectName, "No");
				System.out.println("Successfully Verified The Alert message '"+actAlertMessage+"' for the field :"+alertParentObjectName);  
				StepNum = StepNum+1; 
			}else{
				Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Alert Message for :"+alertParentObjectName, "Failed to Verify The Alert message '"+ExpAlertMsg+"' for the field :"+alertParentObjectName+" , the actual error message in application was "+actAlertMessage, "Yes");
				System.out.println("Failed to Verify The Alert message '"+ExpAlertMsg+"' for the field :"+alertParentObjectName+" , the actual Alert message in application was "+actAlertMessage);  
				StepNum = StepNum+1; 
			}
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Alert Message for :"+alertParentObjectName, "Failed to Verify The Alert message '"+ExpAlertMsg+"' for the field :"+alertParentObjectName, "Yes");
			System.out.println("Failed to Verify The Alert message "+ExpAlertMsg+" for the field"+alertParentObjectName);  
			StepNum = StepNum+1; 
		}
		return StepNum;
	}
	
	public int gfVerifyText(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,WebElement errObj,String alertParentObjectName,String ExpText)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			
			String actAlertMessage = errObj.getText().trim();
			actAlertMessage = actAlertMessage.replace("\n", " ").replace("\r", " ");
			if(actAlertMessage.equals(ExpText.trim())){
				Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Text for :"+alertParentObjectName, "Successfully Verified The Alert message '"+actAlertMessage+"' for the field :"+alertParentObjectName, "No");
				System.out.println("Successfully Verified The Text '"+actAlertMessage+"' for the field :"+alertParentObjectName);  
				StepNum = StepNum+1; 
			}else{
				Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Text for :"+alertParentObjectName, "Failed to Verify The text '"+ExpText+"' for the field :"+alertParentObjectName+" , the actual error message in application was "+actAlertMessage, "Yes");
				System.out.println("Failed to Verify The Alert message '"+ExpText+"' for the field :"+alertParentObjectName+" , the actual Alert message in application was "+actAlertMessage);  
				StepNum = StepNum+1; 
			}
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify text Message for :"+alertParentObjectName, "Failed to Verify The text message '"+ExpText+"' for the field :"+alertParentObjectName, "Yes");
			System.out.println("Failed to Verify The text message "+ExpText+" for the field"+alertParentObjectName);  
			StepNum = StepNum+1; 
		}
		return StepNum;
	}
	
	public boolean WaitUnitlObjectDisappear(WebDriver Driver,String How,String value,int Seconds)
	 {
		 boolean Status = false;
		 try{			 
		
			 switch(How)
			 {
			 case "id":
			 Status = (new WebDriverWait(Driver, Seconds)).until(ExpectedConditions.invisibilityOfElementLocated(By.id(value)));
			 break;
			 case "xpath":
			 Status = (new WebDriverWait(Driver, Seconds)).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(value)));
			 break;
			 case "name":
			 Status = (new WebDriverWait(Driver, Seconds)).until(ExpectedConditions.invisibilityOfElementLocated(By.name(value)));
			 break;
			 }				 
			return Status;
		 }
		 catch(NoSuchElementException e)
		 {
			 return true;
		 }
		 catch (StaleElementReferenceException e) 
		 {
			 return true;
		 }
		 catch (Exception e) 
		 {
			 return false;
		 }
		 
	 }
	
	public void TypeInField(WebElement element, String value){
	    
		String val = value; 
	    element.clear();
	    for (int i = 0; i < val.length()/2; i++){
	        char c = val.charAt(i);
	        String s = new StringBuilder().append(c).toString();
	        element.sendKeys(s);
	    } 
	    gfSleep(1);
	    for (int i = (val.length()/2); i < val.length(); i++){
	        char c = val.charAt(i);
	        String s = new StringBuilder().append(c).toString();
	        element.sendKeys(s);
	    } 
	}
	
	public int fEnterDataToEditBox(WebElement edtObj,String Data,String StepDescription,String Critical,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			if(edtObj!=null){
				  
				  edtObj.clear();
				  TypeInField(edtObj, Data);
				  edtObj.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". "+StepDescription, "Successfully Entered :"+Data, "No");
				  System.out.println(StepDescription+"==> Successfully Entered :"+Data);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To Enter", "Yes");   
				  System.out.println(StepDescription+"==> Failed To Enter"); 
				  StepNum = StepNum+1; 
				  if(Critical.equals("Critical")){
					  Driver.close();
					  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
				  } 
			  	  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To Enter", "Yes");   
			  System.out.println(StepDescription+"==> Failed To Enter"); 
			  StepNum = StepNum+1; 
			  if(Critical.equals("Critical")){
				  Driver.close();
				  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
			  } 
		}
		  
		return StepNum;
	}
	
	public int fClearDataFromEditBox(WebElement edtObj,String StepDescription,String Critical,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			if(edtObj!=null){
				  edtObj.clear();
				  edtObj.sendKeys(Keys.BACK_SPACE);
				  edtObj.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". "+StepDescription, "Successfully cleared ", "No");
				  System.out.println(StepDescription+"==> Successfully cleared");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To clear", "Yes");   
				  System.out.println(StepDescription+"==> Failed To clear"); 
				  StepNum = StepNum+1; 
				  if(Critical.equals("Critical")){
					  Driver.close();
					  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
				  } 
			  	  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To clear", "Yes");   
			  System.out.println(StepDescription+"==> Failed To clear"); 
			  StepNum = StepNum+1; 
			  if(Critical.equals("Critical")){
				  Driver.close();
				  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
			  } 
		}
		  
		return StepNum;
	}
	
	public int fVerifyObjectExistance(WebElement Obj,String StepDescription,String Critical,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			if(Obj!=null){
				  
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". "+StepDescription, "Successfully Verified :", "No");
				  System.out.println(StepDescription+"==> Successfully Verified the object :"+StepDescription.replace("Verify", ""));  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To Verify", "Yes");   
				  System.out.println(StepDescription+"==> Failed To verify :"+StepDescription.replace("Verify", "")); 
				  StepNum = StepNum+1; 
				  if(Critical.equals("Critical")){
					  Driver.close();
					  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
				  } 
			  	  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To Verify", "Yes");   
			  System.out.println(StepDescription+"==> Failed To verify :"+StepDescription.replace("Verify", "")); 
			  StepNum = StepNum+1; 
			  if(Critical.equals("Critical")){
				  Driver.close();
				  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
			  } 
		}
		  
		return StepNum;
	}
	
	public int fVerifyValueFromObject(WebElement edtObj,String ExpData,String StepDescription,String Critical,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			if(edtObj!=null){
				  String actData = edtObj.getAttribute("value");
				  if(actData.equals(ExpData)){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". "+StepDescription, "Successfully verified :"+ExpData, "No");
					  System.out.println(StepDescription+"==> Successfully verified :"+ExpData);  
					  StepNum = StepNum+1;  
				  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To verify and the actual value is :"+actData, "Yes");   
					  System.out.println(StepDescription+"==> Failed To verify and the actual value is :"+actData); 
					  StepNum = StepNum+1; 
					  if(Critical.equals("Critical")){
						  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
					  }   
				  }
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To verify", "Yes");   
				  System.out.println(StepDescription+"==> Failed To verify"); 
				  StepNum = StepNum+1; 
				  if(Critical.equals("Critical")){
					  Driver.close();
					  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
				  } 
			  	  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To Enter", "Yes");   
			  System.out.println(StepDescription+"==> Failed To Enter"); 
			  StepNum = StepNum+1; 
			  if(Critical.equals("Critical")){
				  Driver.close();
				  Assert.fail("Test critical Step:'"+StepDescription+"'Failed, hence moving to next test case");
			  } 
		}
		  
		return StepNum;
	}
	
	public int fClickOnObject(WebElement edtObj,String StepDescription,String Critical,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String ScreenShot)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			if(edtObj!=null){
				  edtObj.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". "+StepDescription, "Successfully Clicked", ScreenShot);
				  System.out.println(StepDescription+"==> Successfully clicked ");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To click", ScreenShot);   
				  System.out.println(StepDescription+"==> Failed To click"); 
				  StepNum = StepNum+1; 
				  if(Critical.equals("Critical")){
					  Driver.close();
					  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
				  } 
			  	  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To click", "Yes");   
			  System.out.println(StepDescription+"==> Failed To Click"); 
			  StepNum = StepNum+1; 
			  if(Critical.equals("Critical")){
				  Driver.close();
				  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
			  } 
		}
		  
		return StepNum;
	}
	
	public int fClickOnObjectWithAction(WebElement edtObj,String StepDescription,String Critical,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String ScreenShot)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			if(edtObj!=null){
				Actions Act = new Actions(Driver);
				Act.moveToElement(edtObj).click().perform();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". "+StepDescription, "Successfully Clicked", ScreenShot);
				  System.out.println(StepDescription+"==> Successfully clicked ");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To click", "Yes");   
				  System.out.println(StepDescription+"==> Failed To click"); 
				  StepNum = StepNum+1; 
				  if(Critical.equals("Critical")){
					  Driver.close();
					  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
				  } 
			  	  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To click", "Yes");   
			  System.out.println(StepDescription+"==> Failed To Click"); 
			  StepNum = StepNum+1; 
			  if(Critical.equals("Critical")){
				  Driver.close();
				  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
			  } 
		}
		  
		return StepNum;
	}
	
	public int fJSClickOnObject(WebElement edtObj,String StepDescription,String Critical,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String ScreenShot)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			if(edtObj!=null){
				JavascriptExecutor executor = (JavascriptExecutor)Driver;
				executor.executeScript("arguments[0].click();", edtObj);

				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". "+StepDescription, "Successfully Clicked", ScreenShot);
				  System.out.println(StepDescription+"==> Successfully clicked ");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To click", "Yes");   
				  System.out.println(StepDescription+"==> Failed To click"); 
				  StepNum = StepNum+1; 
				  if(Critical.equals("Critical")){
					  Driver.close();
					  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
				  } 
			  	  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To click", "Yes");   
			  System.out.println(StepDescription+"==> Failed To Enter"); 
			  StepNum = StepNum+1; 
			  if(Critical.equals("Critical")){
				  Driver.close();
				  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
			  } 
		}
		  
		return StepNum;
	}
	
	public int fSelectFromDropDown(WebElement edtObj,String Data,String StepDescription,String Critical,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			if(edtObj!=null){
				  edtObj.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". "+StepDescription, "Successfully Clicked :", "No");
				  System.out.println(StepDescription+"==> Successfully clicked ");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To click", "Yes");   
				  System.out.println(StepDescription+"==> Failed To click"); 
				  StepNum = StepNum+1; 
				  if(Critical.equals("Critical")){
					  Driver.close();
					  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
				  } 
			  	  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". "+StepDescription, "Failed To click", "Yes");   
			  System.out.println(StepDescription+"==> Failed To Enter"); 
			  StepNum = StepNum+1; 
			  if(Critical.equals("Critical")){
				  Assert.fail("Test critical Step :'"+StepDescription+"'Failed, hence moving to next test case");
			  } 
		}
		  
		return StepNum;
	}
	public int gfDeleteGigyaTraces(String MSISDN,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation){
		FrameWorkUtility Futil = new FrameWorkUtility();
		try{
			String strSQLQuerry = "select user_id from sms_verification_codes where phone_number = '"+MSISDN+"' order by id DESC";
			ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			rs.next();
			String USERID = rs.getString(1);
			String endpoint1 = "https://accounts.eu1.gigya.com/accounts.setAccountInfo?format=json&UID="+USERID+"&apiKey=3_dFt8EnQzDmeB6DwcuF2lf2E6JIsbjiwSZolwwwCw58kBDmouvah9cyMxFxOOEtAl&secret=rRiE4LPpZ8FJZzWoBUsPFAH5TF2vo6Vjdt5PtULWR/c=&username="+GenrateRandomName();
			doGet(endpoint1,Extreport, Exttest, Driver, StepNum, ResultLocation);
			String endpoint2 = "https://socialize.eu1.gigya.com/accounts.setAccountInfo?UID="+USERID+"&profile=%7B%22phones.number%22:null%7D&apiKey=3_dFt8EnQzDmeB6DwcuF2lf2E6JIsbjiwSZolwwwCw58kBDmouvah9cyMxFxOOEtAl%20&secret=rRiE4LPpZ8FJZzWoBUsPFAH5TF2vo6Vjdt5PtULWR%2Fc%3D";
			doGet(endpoint2,Extreport, Exttest, Driver, StepNum+1, ResultLocation);
			String spxModifyBillingPhone = "https://test-api.aws.playco.com/api/v0.2/userAccounts/" + USERID;
			String AccessTocken = getAccessToken(USERID, Extreport, Exttest, Driver, StepNum,ResultLocation );
			doPut(spxModifyBillingPhone, USERID, updateUserAccount(), AccessTocken);
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". User Deletion from Gigya", "Failed Due to Exception" + e, "No");
		}
		return StepNum;
	}
	public Response doGet(String endpoint,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) {
		Response response = null;
		try{
			FrameWorkUtility Futil = new FrameWorkUtility();
		    response = RestAssured.given().urlEncodingEnabled(false).when().log().all().get(endpoint);
			if (response.getStatusCode() != 200) {
				Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Do Get", "Do get failed!\n" + response.asString(), "No");
				System.out.println("Do get failed!\n" + response.asString());
				StepNum = StepNum+1;
			}else{
				Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Do Get", "Response:\n" + response.asString(), "No");
				System.out.println("Response:\n" + response.asString());
				StepNum = StepNum+1;
			}
			return response;
		}catch(Exception e){
			System.out.println(e);
			return response;
		}
	}
	
	public Response doPut(String endpoint, String userId, String body, String accessToken) {
		Header header = new Header("Authorization", "Bearer " + accessToken);
		Response response = RestAssured.given().urlEncodingEnabled(false).header(header).body(body).with().contentType("application/json").when().log().all()
				.put(endpoint);
		if (response.getStatusCode() != 200) {
			//System.out.println("Do Put failed!\n" + response.asString() + response.getStatusCode());
		}
		else{
			System.out.println("Do Put Passed!\n" + response.asString() + response.getStatusCode());
		}
		return response;
	}
	
	public String updateUserAccount() {
		JSONObject settings = new JSONObject();
		JSONObject json = new JSONObject();
		try {
			json.put("billingPhone", "");
			settings.put("settings", json);

		} catch (JSONException e) {
			//e.printStackTrace();
			System.out.println(e);
		}
		return settings.toString();
	}
	
	public String getAccessToken(String userId,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) {
		String gigyaEndpoint = "https://socialize.eu1.gigya.com/socialize.getToken?secret=rRiE4LPpZ8FJZzWoBUsPFAH5TF2vo6Vjdt5PtULWR/c=&format=json&apiKey=3_dFt8EnQzDmeB6DwcuF2lf2E6JIsbjiwSZolwwwCw58kBDmouvah9cyMxFxOOEtAl&x_siteUID="
				+ userId + "&grant_type=none";
		Response response = doGet(gigyaEndpoint, Extreport, Exttest, Driver, StepNum, ResultLocation);
		return response.getBody().jsonPath().get("access_token").toString();
	}
	
	public void gnWaitUntilPageLoad(WebDriver driver)
	{
		WebDriverWait wait = new WebDriverWait(driver, 30);

	    wait.until(new ExpectedCondition<Boolean>() {
	        public Boolean apply(WebDriver wdriver) {
	            return ((JavascriptExecutor) driver).executeScript(
	                "return document.readyState"
	            ).equals("complete");
	        }
	    });
	}
}
