package portalfrontend;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CommonMoPMethods {
	
	public GeneralMethods Genf;
	public FrameWorkUtility Futil; 
	
	public int afMoPCreditCard_Selection(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement lblCreditCard= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='creditCardLogo']");
			  if(lblCreditCard!=null){
				  lblCreditCard.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on Credit Card", "Successfully clicked on Payment methode credit card", "No");
				  System.out.println("Successfully clicked on Payment methode credit card");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on Credit Card", "Failed To clicked on Payment methode credit card", "Yes");   
				  System.out.println("Failed To clicked on Payment methode credit card"); 
				  Assert.fail("Failed To clicked on Payment methode credit card");
				  StepNum = StepNum+1; 
			  	  }
			  
			  
			  StepNum = afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation) ;
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Credit casrd as MoP", "Failed to slect Credit Card as MoP", "No");
			  System.out.println("Failed to slect Credit Card as MoP");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afMoPVoucher_Selection(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement lblStarzVoucher= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='voucherLogo']");
			  if(lblStarzVoucher!=null){
				  lblStarzVoucher.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on Starz Play Voucher", "Successfully clicked on Payment methode Starz Play Voucher", "No");
				  System.out.println("Successfully clicked on Payment methode Starz Play Voucher");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on Starz Play Voucher", "Failed To clicked on Payment methode Starz Play Voucher", "Yes");   
				  System.out.println("Failed To clicked on Payment methode Starz Play Voucher"); 
				  Assert.fail("Failed To clicked on Payment methode Starz Play Voucher");
				  StepNum = StepNum+1; 
			  	  }
			  
			  
			  StepNum = afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation) ;
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Starz Play Voucher as MoP", "Failed to slect Starz Play Voucher as MoP", "No");
			  System.out.println("Failed to slect Starz Play Voucher as MoP");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afMoP_DU_Selection(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement lblDu= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='duLogo']");
			StepNum = Genf.fClickOnObject(lblDu, "Select DU", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			StepNum = afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation) ;
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select DU as MoP", "Failed to slect DU as MoP", "No");
			  System.out.println("Failed to slect DU as MoP");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afMoP_Etisalat_Selection(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement lblDu= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalatLogo']");
			StepNum = Genf.fClickOnObject(lblDu, "Select Etisalat", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			StepNum = afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation) ;
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Etisalat as MoP", "Failed to slect Etisalat as MoP", "No");
			  System.out.println("Failed to slect Etisalat as MoP");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afMoP_MarocTelecom_Selection(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement lblDu= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecomLogo']");
			StepNum = Genf.fClickOnObject(lblDu, "Select Maroc Telecom", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			StepNum = afVerifyMarocTelcomPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation) ;
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Maroc Telecom as MoP", "Failed to slect Maroc telecom as MoP", "No");
			  System.out.println("Failed to slect Maroc telecom as MoP");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	public int afMoP_Billing_Daily(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement lblDu= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@data-period-keyword='DAY']//parent::div");
			//String stringcolor = lblDu.getCssValue("color");  =>> Need to work on this to get the color of object
			StepNum = Genf.fClickOnObjectWithAction(lblDu, "Select Daily Payment", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Daily Payment", "Failed to slect Daily Payment", "No");
			  System.out.println("Failed to Select Daily Payment");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	public int afMoP_Billing_Weekly(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement lblDu= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@data-period-keyword='WEEK']//parent::div");
			//String stringcolor = lblDu.getCssValue("color");  =>> Need to work on this to get the color of object
			StepNum = Genf.fClickOnObjectWithAction(lblDu, "Select Weekly Payment", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Weekly Payment", "Failed to slect Weekly Payment", "No");
			  System.out.println("Failed to Select Weekly Payment");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afMoP_Billing_Monthly(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			Genf.gfSleep(4);
			WebElement lblDu= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@data-period-keyword='MONTH']//parent::div");
			//String stringcolor = lblDu.getCssValue("color");  =>> Need to work on this to get the color of object
			StepNum = Genf.fClickOnObjectWithAction(lblDu, "Select Monthly Payment", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Weekly Payment", "Failed to slect Monthly Payment", "No");
			  System.out.println("Failed to Select Monthly Payment");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyCreditCardPageURL(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			if(Driver.getCurrentUrl().contains("payment/creditCard")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Credit Card Page URL", "Successfully verifed the Credit Card page URL, it is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verifed the Credit Card page URL, it is :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Credit Card Page URL", "Failed the verification of Credit Card page, it is "+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed the verification of Credit Card page, it is "+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Credit Card Page URL", "Failed the verification of Credit Card page", "Yes");
			  System.out.println("Failed the verification of Credit Card page");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyVoucherPageURL(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			if(Driver.getCurrentUrl().contains("/payment/voucher")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Voucher Page URL", "Successfully verifed the Voucher page URL, it is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verifed the Voucher page URL, it is :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Voucher Page URL", "Failed the verification of Voucher page, it is "+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed the verification of Voucher page, it is "+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Voucher Page URL", "Failed the verification of Voucher page", "Yes");
			  System.out.println("Failed the verification of Voucher page");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afVerifyDuPageURL(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			if(Driver.getCurrentUrl().contains("/payment/du")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify DU Page URL", "Successfully verifed the DU page URL, it is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verifed the DU page URL, it is :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify DU Page URL", "Failed the verification of DU page, it is "+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed the verification of DU page, it is "+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify DU Page URL", "Failed the verification of DU page", "Yes");
			  System.out.println("Failed the verification of DU page");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afVerifyEtisalatPageURL(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			if(Driver.getCurrentUrl().contains("/payment/etisalat")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Etisalat Page URL", "Successfully verifed the Etisalat page URL, it is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verifed the Etisalat page URL, it is :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Etisalat Page URL", "Failed the verification of Etisalat page, it is "+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed the verification of Etisalat page, it is "+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Etisalat Page URL", "Failed the verification of Etisalat page", "Yes");
			  System.out.println("Failed the verification of Etisalat page");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afVerifyMarocTelcomPageURL(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			if(Driver.getCurrentUrl().contains("payment/maroctelecom")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Maroc Telecom Page URL", "Successfully verifed the Maroc Telecom page URL, it is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verifed the Maroc Telecom page URL, it is :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Maroc Telecom Page URL", "Failed the verification of Maroc Telecom page, it is "+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed the verification of Maroc Telecom page, it is "+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Maroc Telecom Page URL", "Failed the verification of Maroc Telecom page", "Yes");
			  System.out.println("Failed the verification of Maroc Telecom page");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithCreditCard(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  String CCNumber = "4111111111111111";
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, 4, 4) ;
			  Genf.gfSleep(1);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
				if(EditCCCvv!=null){
					String strCvv = "123";
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			   
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("/registration/payment/creditCard/thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Credit Card Thank you page", "Successfully verified the Credit Card Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the Credit Card Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Credit Card Thank you page", "Failed To verify The credit card thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The credit card thank you page, the url is "+Driver.getCurrentUrl()); 
					  Assert.fail("Failed To verify The credit card thank you page, the url is "+Driver.getCurrentUrl());
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Credit Card Thank you page", "The credit card thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The credit card thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Credit casrd as MoP", "Failed to slect Credit Card as MoP", "No");
			  System.out.println("Failed to slect Credit Card as MoP");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	
	public int afMoP_WithValidDU_Registration(String DuNumber,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditDuMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, DuNumber.substring(3), "Enter Valid DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			rs.next();
			String OTP = rs.getString(1);
			WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }
				  
			StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select du as MoP", "Failed to slect du as MoP", "No");
			  System.out.println("Failed to slect du as MoP");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afMoP_WithValid_Etisalat_Registration(String EtisalatNumber,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditEtislatMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditEtislatMobileNUmber, EtisalatNumber.substring(3), "Enter Valid Etisalat Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+EtisalatNumber+"' order by id DESC";
			ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			rs.next();
			String OTP = rs.getString(1);
			WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-sms-code']");
			StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Etisalat Thank you page", "Successfully verified the Etisalat Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the Etisalat Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Etisalat Thank you page", "Failed To verify The Etisalat thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The Etisalat thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Etisalat Thank you page", "The Etisalat thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The Etisalat thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }
				  
			StepNum = Genf.gfDeleteGigyaTraces(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Etisalat as MoP", "Failed to slect Etisalat as MoP", "No");
			  System.out.println("Failed to slect Etisalat as MoP");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afMoP_WithValid_MarocTelecom_Registration(String Number,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditMarocTelcMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditMarocTelcMobileNUmber, Number.substring(3), "Enter Valid Maroc Telecom Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+Number+"' order by id DESC";
			ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			rs.next();
			String OTP = rs.getString(1);
			WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-sms-code']");
			StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Maroc Telecom Thank you page", "Successfully verified the Etisalat Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the Maroc Telecom Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Maroc Telecom Thank you page", "Failed To verify The Maroc Telecom thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The Maroc Telecom thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Maroc Telecom Thank you page", "The Maroc Telecom thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The Maroc Telecom thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }
				  
			StepNum = Genf.gfDeleteGigyaTraces(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Register Maroc Telecom as MoP", "Failed to register Maroc Telecom as MoP", "No");
			  System.out.println("Failed to register Maroc Telecom as MoP");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWith_EmptyMarocTelcomNumber(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			//WebElement EditMarocTelcMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-mobile-number']");
			//StepNum = Genf.fEnterDataToEditBox(EditMarocTelcMobileNUmber, Number.substring(3), "Enter Valid Maroc Telecom Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-invalid-telephone-error']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			String errorMsg = Futil.fgetData("Wrong Maroc Telecom Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "Empty Number", errorMsg);
			//Genf.gfSleep(3);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+Number+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			//Genf.gfSleep(3);
			//WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-continue-button']");
			//StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				/*boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Maroc Telecom Thank you page", "Successfully verified the Etisalat Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the Maroc Telecom Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Maroc Telecom Thank you page", "Failed To verify The Maroc Telecom thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The Maroc Telecom thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Maroc Telecom Thank you page", "The Maroc Telecom thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The Maroc Telecom thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Maroc Telecom as MoP", "Failed due to exception"+e, "No");
			  System.out.println("Failed to register Maroc Telecom as MoP");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	
	
	public int afSignUPWithCreditCard_NoDetails(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
				
				Genf.gfSleep(2);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
				  
				  WebElement objerrFirstName= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname-error']");
				  WebElement objerrLastName= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname-error']");
				  WebElement objerrCardNum= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard-error']");
				  WebElement objerrExpMonth= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cexpirationdatemonth-error']");
				  WebElement objerrExpYear= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cexpirationdateyear-error']");
				  WebElement objerrCVV= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode-error']");
				  
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrFirstName, "First Name", "This field is required.");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrLastName, "Last Name", "This field is required.");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrCardNum, "Card Number", "This field is required.");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrExpMonth, "Exp. Month", "This field is required.");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrExpYear, "Exp. Year", "This field is required.");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrCVV, "CVV", "Please enter a valid CVV number.");
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching without providing any details", "No");
			  System.out.println("Failed to verify error messages while click on start watching without providing any details");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithCreditCard_NoFirstName(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  EditCCFirstName.clear();  
			  	  }
			
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  EditCCLastName.clear();
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  EditCCNumber.clear();
				  String CCNumber = "4111111111111111";
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, 4, 4) ;
			  Genf.gfSleep(2);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
				if(EditCCCvv!=null){
					EditCCCvv.clear();
					String strCvv = "123";
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			  
				  WebElement objerrFirstName= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname-error']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrFirstName, "First Name", "This field is required.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching without providing First Name", "No");
			  System.out.println("Failed to verify error messages while click on start watching without providing First Name");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithCreditCard_InvalidFirstNameFormat(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  EditCCFirstName.clear();
				  String FirtsName = "123@#$";
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  EditCCLastName.clear();
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  EditCCNumber.clear();
				  String CCNumber = "4111111111111111";
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, 4, 4) ;
			  Genf.gfSleep(1);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
				if(EditCCCvv!=null){
					EditCCCvv.clear();
					String strCvv = "123";
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			   
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  WebElement objPaymenterr= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='paymentErrorMessage']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objPaymenterr, "Payment Error Due to Invalid First Name", "We were unable to process your payment. Please contact customer service to resolve this issue.");
				  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching by providing Invalid First Name Format", "No");
					  System.out.println("Failed to verify error messages while click on start watching providing Invalid First Name Format");
							  StepNum = StepNum+1;
				  }
			  
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Credit casrd as MoP", "Failed to slect Credit Card as MoP", "No");
			  System.out.println("Failed to slect Credit Card as MoP");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithCreditCard_InvalidLastNameFormat(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  EditCCFirstName.clear();
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  EditCCLastName.clear();
				  String LastName = "123!@#$";
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  EditCCNumber.clear();
				  String CCNumber = "4111111111111111";
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, 4, 4) ;
			  Genf.gfSleep(1);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
				if(EditCCCvv!=null){
					String strCvv = "123";
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			   
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  WebElement objPaymenterr= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='paymentErrorMessage']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objPaymenterr, "Payment Error Due to Invalid Last Name", "We were unable to process your payment. Please contact customer service to resolve this issue.");
				  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching by providing Invalid Last Name Format", "No");
					  System.out.println("Failed to verify error messages while click on start watching providing Invalid Last Name Format");
							  StepNum = StepNum+1;
				  }
			  
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Credit casrd as MoP", "Failed to slect Credit Card as MoP", "No");
			  System.out.println("Failed to slect Credit Card as MoP");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	public int afSignUPWithCreditCard_NoLastName(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  EditCCFirstName.clear();
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  EditCCLastName.clear();  
			  	  }
			
			  Genf.gfSleep(1);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  String CCNumber = "4111111111111111";
				  EditCCNumber.clear();
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(2);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, 4, 4) ;
			  Genf.gfSleep(2);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
				if(EditCCCvv!=null){
					String strCvv = "123";
					EditCCCvv.clear();
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(2);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			  
				  WebElement objerrLastName= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname-error']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrLastName, "Last Name", "This field is required.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching without providing Last Name", "No");
			  System.out.println("Failed to verify error messages while click on start watching without providing Last Name");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithCreditCard_NoCreditCardNumber(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.clear();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.clear();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			
			  Genf.gfSleep(2);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  EditCCNumber.clear();
				  EditCCNumber.sendKeys(Keys.TAB);
			  	  }
			  Genf.gfSleep(2);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, 4, 4) ;
			  Genf.gfSleep(2);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
				if(EditCCCvv!=null){
					String strCvv = "123";
					EditCCCvv.clear();
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(2);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			  
				  WebElement objerrCardNum= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard-error']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrCardNum, "CREDIT CARD", "This field is required.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching without providing Credit Card Number", "No");
			  System.out.println("Failed to verify error messages while click on start watching without providing Credit Card Number");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithCreditCard_NoExpiryMonth(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.clear();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.clear();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			
			  Genf.gfSleep(2);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  String CCNumber = "4111111111111111";
				  EditCCNumber.clear();
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(2);
			  StepNum = afEnterCCExpiryYear(Extreport, Exttest, Driver, StepNum, ResultLocation, 4);
			  Genf.gfSleep(2);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
				if(EditCCCvv!=null){
					String strCvv = "123";
					EditCCCvv.clear();
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			  
				  WebElement objerrExpMonth= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cexpirationdatemonth-error']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrExpMonth, "Expiration Month", "This field is required.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching without providing Expiry Month", "No");
			  System.out.println("Failed to verify error messages while click on start watching without providing Credit Card Expiry Month");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithCreditCard_NoExpiryYear(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.clear();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.clear();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			
			  Genf.gfSleep(2);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  String CCNumber = "4111111111111111";
				  EditCCNumber.clear();
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(2);
			  StepNum = afEnterCCExpiryMonth(Extreport, Exttest, Driver, StepNum, ResultLocation, 4);
			  Genf.gfSleep(2);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
				if(EditCCCvv!=null){
					String strCvv = "123";
					EditCCCvv.clear();
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			  
				  WebElement objerrExpYear= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cexpirationdateyear-error']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrExpYear, "Expiration Year", "This field is required.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching without providing Credit Card Expiry Year", "No");
			  System.out.println("Failed to verify error messages while click on start watching without providing Credit Card Expiry Year");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	
	public int afSignUPWithCreditCard_NoCVV(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  EditCCFirstName.clear();
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  EditCCLastName.clear();
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  EditCCNumber.clear();
				  String CCNumber = "4111111111111111";
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, 4, 4) ;
			  Genf.gfSleep(1);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
				if(EditCCCvv!=null){
					EditCCCvv.clear();
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
				  
				  WebElement objerrCVV= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode-error']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrCVV, "CVV", "Please enter a valid CVV number.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching without providing Credit Card CVV", "No");
			  System.out.println("Failed to verify error messages while click on start watching without providing Credit Card CVV");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithCreditCard_WrongCreditCardFormat(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  EditCCFirstName.clear();
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  EditCCLastName.clear();
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  EditCCNumber.clear();
				  String CCNumber = "asc.234";
				  EditCCNumber.sendKeys(CCNumber);
				  EditCCNumber.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(4);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, 4, 4) ;
			  Genf.gfSleep(1);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
			  if(EditCCCvv!=null){
					String strCvv = "123";
					EditCCCvv.clear();
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
				  
				  WebElement objerrCardNum= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard-error']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrCardNum, "CREDIT CARD", "Please enter a valid credit card number.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching providing Wrong format Credit Card Number", "No");
			  System.out.println("Failed to verify error messages while click on start watching providing Wrong format Credit Card Number");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithCreditCard_InvalidExpiryDate(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  EditCCFirstName.clear();
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  EditCCLastName.clear();
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  EditCCNumber.clear();
				  String CCNumber = "4111111111111111";
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  java.util.Date date= new Date();
			  Calendar cal = Calendar.getInstance();
			  cal.setTime(date);
			  int month = cal.get(Calendar.MONTH);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, month-1, 0) ;
			  Genf.gfSleep(1);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
			  if(EditCCCvv!=null){
					String strCvv = "123";
					EditCCCvv.clear();
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
				  
				  WebElement objerrExpYear= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='paymentErrorMessage']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrExpYear, "Expiration Year", "Please enter a valid expiration date.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching providing Invalid expiry date", "No");
			  System.out.println("Failed to verify error messages while click on start watching providing Invalid expiry date");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithCreditCard_InvalidCVV(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditCCFirstName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='cfirstname']");
			  if(EditCCFirstName!=null){
				  EditCCFirstName.clear();
				  String FirtsName = Genf.GenrateRandomName();
				  EditCCFirstName.sendKeys(FirtsName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter First Name", "Successfully Entered First Name"+FirtsName, "No");
				  System.out.println("Successfully Entered First Name"+FirtsName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter First Name", "Failed To Enter First Name", "Yes");   
				  System.out.println("Failed To Enter First Name"); 
				  Assert.fail("Failed To Enter First Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCLastName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='clastname']");
			  if(EditCCLastName!=null){
				  EditCCLastName.clear();
				  String LastName = Genf.GenrateRandomName();
				  EditCCLastName.sendKeys(LastName);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Last Name", "Successfully Entered Last Name"+LastName, "No");
				  System.out.println("Successfully Entered Last Name"+LastName);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Last Name", "Failed To Enter Last Name", "Yes");   
				  System.out.println("Failed To Enter Last Name"); 
				  Assert.fail("Failed To Enter Last Name");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  WebElement EditCCNumber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ccreditcard']");
			  if(EditCCNumber!=null){
				  EditCCNumber.clear();
				  String CCNumber = "4111111111111111";
				  EditCCNumber.sendKeys(CCNumber);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Credit Card Number", "Successfully Entered Credit Card Number"+CCNumber, "No");
				  System.out.println("Successfully Entered Credit Card Number"+CCNumber);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Number", "Failed To Enter Credit Card Number", "Yes");   
				  System.out.println("Failed To Enter Credit Card Number"); 
				  Assert.fail("Failed To Enter Credit Card Number");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(1);
			  StepNum = afEnterCCExpiryDate( Extreport, Exttest,  Driver, StepNum, ResultLocation, 4, 4) ;
			  Genf.gfSleep(1);
			  WebElement EditCCCvv= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode']");
			  if(EditCCCvv!=null){
					String strCvv = "12";
					EditCCCvv.clear();
					EditCCCvv.sendKeys(strCvv);
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Cvv", "Successfully Entered CVV " +strCvv, "No");
					  System.out.println("Successfully Entered CVV " +strCvv);  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Cvv", "Failed To Enter CVV", "Yes");   
					  System.out.println("Failed To Enter CVV"); 
					  Assert.fail("Failed To Enter CVV");
					  StepNum = StepNum+1; 
				  	  }
				
				Genf.gfSleep(1);
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnSubmitPayment']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
				  
				  WebElement objerrCVV= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='csecuritycode-error']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrCVV, "CVV", "Please enter a valid CVV number.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching providing Invalid CVV", "No");
			  System.out.println("Failed to verify error messages while click on start watching providing Invalid CVV");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	public int afEnterCCExpiryDate(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,int Month,int YearFromNow) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditCCExpiryMonth= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='contentMonth']/button");
			if(EditCCExpiryMonth!=null){
				 
				EditCCExpiryMonth.click();
				Genf.gfSleep(2);
				WebElement lnkCCExpiryMonth= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='monthCreditCard']/li["+Month+"]/a");
				lnkCCExpiryMonth.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Select Expiry Month", "Successfully Selected Expiry Month as "+Month, "No");
				  System.out.println("Successfully Selected Expiry Month as "+Month);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Expiry Month", "Failed To select expiry month", "Yes");   
				  System.out.println("Failed To select expiry month"); 
				  Assert.fail("Failed To select expiry month");
				  StepNum = StepNum+1; 
			  	  }
			Genf.gfSleep(2);
			WebElement EditCCExpiryYear= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='contentYear']/button");
			if(EditCCExpiryYear!=null){
				 
				EditCCExpiryYear.click();
				Genf.gfSleep(2);
				YearFromNow = YearFromNow+1;
				WebElement lnkCCExpiryYear= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='yearCreditCard']/li["+YearFromNow+"]/a");
				YearFromNow = YearFromNow-1;
				lnkCCExpiryYear.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Select Expiry Year", "Successfully Selected Expiry Year as "+YearFromNow +" Years From today", "No");
				  System.out.println("Successfully Selected Expiry Year as "+YearFromNow +" Years From today");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Expiry Year", "Failed To select expiry Year", "Yes");   
				  System.out.println("Failed To select expiry month"); 
				  Assert.fail("Failed To select expiry month");
				  StepNum = StepNum+1; 
			  	  }
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Expiry Date", "Failed To Enter", "Yes");
			  System.out.println("Failed to enter the Credit card Expiry Date");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afEnterCCExpiryMonth(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,int Month) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditCCExpiryMonth= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='contentMonth']/button");
			if(EditCCExpiryMonth!=null){
				 
				EditCCExpiryMonth.click();
				WebElement lnkCCExpiryMonth= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='monthCreditCard']/li["+Month+"]/a");
				lnkCCExpiryMonth.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Select Expiry Month", "Successfully Selected Expiry Month as "+Month, "No");
				  System.out.println("Successfully Selected Expiry Month as "+Month);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Expiry Month", "Failed To select expiry month", "Yes");   
				  System.out.println("Failed To select expiry month"); 
				  Assert.fail("Failed To select expiry month");
				  StepNum = StepNum+1; 
			  	  }
			Genf.gfSleep(2);
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Expiry Month", "Failed To Enter", "Yes");
			  System.out.println("Failed to enter the Credit card Expiry month");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afEnterCCExpiryYear(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,int YearFromNow) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditCCExpiryYear= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='contentYear']/button");
			if(EditCCExpiryYear!=null){
				 
				EditCCExpiryYear.click();
				WebElement lnkCCExpiryYear= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='yearCreditCard']/li["+YearFromNow+"]/a");
				lnkCCExpiryYear.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Select Expiry Year", "Successfully Selected Expiry Year as "+YearFromNow +" Years From today", "No");
				  System.out.println("Successfully Selected Expiry Year as "+YearFromNow +" Years From today");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Select Expiry Year", "Failed To select expiry Year", "Yes");   
				  System.out.println("Failed To select expiry month"); 
				  Assert.fail("Failed To select expiry month");
				  StepNum = StepNum+1; 
			  	  }
			Genf.gfSleep(2);
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Credit Card Expiry Month", "Failed To Enter", "Yes");
			  System.out.println("Failed to enter the Credit card Expiry month");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithVoucher(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String VoucherCode) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EditVoucherCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='voucherCode']");
			  if(EditVoucherCode!=null){
				  EditVoucherCode.sendKeys(VoucherCode);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Voucher Code", "Successfully Entered Voucher Code"+VoucherCode, "No");
				  System.out.println("Successfully Entered Voucher Code"+VoucherCode);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Voucher Code", "Failed To Enter Voucher Code", "Yes");   
				  System.out.println("Failed To Enter Voucher Code"); 
				  Assert.fail("Failed To Enter Voucher Code");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(2);
			  
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnContinueVoucher']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			   
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
					  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("/registration/payment/voucher/thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Voucher Thank you page", "Successfully verified the Voucher Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the Voucher Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Voucher Thank you page", "Failed To verify The Voucher thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The Voucher thank you page, the url is "+Driver.getCurrentUrl()); 
					  Assert.fail("Failed To verify The Voucher thank you page, the url is "+Driver.getCurrentUrl());
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Voucher Thank you page", "The Voucher thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The Voucher thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }
			  
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Sign Up With Starz Play Voucher", "Failed to Sign Up With Voucher", "No");
			  System.out.println("Failed to Sign Up With Voucher");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithVoucher_NoVoucherCode(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditVoucherCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='voucherCode']");
			  if(EditVoucherCode!=null){
				  EditVoucherCode.clear();
			  	  }
			  Genf.gfSleep(2);
			  
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnContinueVoucher']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
			   
				  WebElement objerrVoucher= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='missing-voucher-error']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrVoucher, "Voucher Code", "This field is required");
		
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching without providing Voucher code", "No");
			  System.out.println("Failed to verify error messages while click on start watching without providing Voucher code");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithVoucher_InvalidVoucherCode(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditVoucherCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='voucherCode']");
			String InvalidVoucherCode = "xxxxxxx";  
			if(EditVoucherCode!=null){
				  
				  EditVoucherCode.sendKeys(InvalidVoucherCode);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Voucher Code", "Successfully Entered Voucher Code"+InvalidVoucherCode, "No");
				  System.out.println("Successfully Entered Voucher Code"+InvalidVoucherCode);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Voucher Code", "Failed To Enter Voucher Code", "Yes");   
				  System.out.println("Failed To Enter Voucher Code"); 
				  Assert.fail("Failed To Enter Voucher Code");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(2);
			  Genf.gfSleep(2);
			  
				WebElement lblStartWatching= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='btnContinueVoucher']");
				  if(lblStartWatching!=null){
					  lblStartWatching.click();
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click on START WATCHING", "Successfully clicked on START WATCHING", "No");
					  System.out.println("Successfully clicked on START WATCHING");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click on START WATCHING", "Failed To clicked on START WATCHING", "Yes");   
					  System.out.println("Failed To clicked on START WATCHING"); 
					  Assert.fail("Failed To clicked on START WATCHING");
					  StepNum = StepNum+1; 
				  	  }
				  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
					  Genf.gfSleep(3);
					  WebElement objgeneralerrVoucher= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='general-voucher-error']");
					  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objgeneralerrVoucher, "Invalid Vouched code", "We’re unable to validate your voucher code. Please check you have entered the details correctly.");
				  
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Voucher Thank you page", "The Voucher thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The Voucher thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }
			   
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify error messages", "Failed to verify error messages while click on start watching providing Invalid Voucher code", "No");
			  System.out.println("Failed to verify error messages while click on start watching invalid providing Voucher code");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUPWithDu_EmptyDuNumber(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditDuMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-mobile-number']");
			EditDuMobileNUmber.clear();
			//StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, DuNumber.substring(3), "Enter Valid DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "Empty Number", errorMsg);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with du as MoP", "Failed", "No");
			  System.out.println("Failed");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithEtislata_EmptyEtisalatNumber(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditEtisalatMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-mobile-number']");
			EditEtisalatMobileNUmber.clear();
			//StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, DuNumber.substring(3), "Enter Valid DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Etisalat Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "Empty Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Etisalat as MoP", "Failed", "No");
			  System.out.println("Failed");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithDu_ShortDuNumber(String DuNumber, ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditDuMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, DuNumber.substring(5), "Enter short DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "short Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with du as MoP", "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with DU");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithEtisalat_ShortEtisalatNumber(String DuNumber, ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditEtisalatMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditEtisalatMobileNUmber, DuNumber.substring(5), "Enter short Etisalat Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Etisalat Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "short Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Etisalat as MoP", "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with Etisalat");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWith_ShortMarocTelecomNumber(String DuNumber, ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditMarocTelcMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditMarocTelcMobileNUmber, DuNumber.substring(5), "Enter short Maroc Telecom Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Maroc Telecom Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "short Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Maroc telecom as MoP", "Failed with exception :"+e, "No");
			  System.out.println("Failed Unhappy scenario with Maroc Telecom");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	public int afSignUPWithDu_LongDuNumber(String DuNumber, ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditDuMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, DuNumber, "Enter long DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "long Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with du as MoP", "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with DU");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithEtisalat_LongEtisalatNumber(String DuNumber, ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditEtisalatMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditEtisalatMobileNUmber, DuNumber, "Enter long Etisalat Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Etisalat Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "long Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Etisalat as MoP", "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with Etisalat");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWith_LongMarocTelecomNumber(String Number, ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement MarocTelcMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(MarocTelcMobileNUmber, Number, "Enter long Maroc Telecom Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Maroc Telecom Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "long Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Maroc Telecom as MoP", "Failed with exceptio :"+e, "No");
			  System.out.println("Failed Unhappy scenario with Maroc telecom");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	public int afSignUPWithDu_WrongDuNumberFormat(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditDuMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, "12!@$##!", "Enter Wrong format DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "wrong format Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with du as MoP", "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with DU");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithEtisalat_WrongEtisalatNumberFormat(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditEtisalatMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditEtisalatMobileNUmber, "12!@$##!", "Enter Wrong format Etisalat Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Etisalat Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-invalid-telephone-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "wrong format Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Etisalat as MoP", "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with Etisalat");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWith_WrongMarocTelecomNumberFormat(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditMorocTelcomMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditMorocTelcomMobileNUmber, "12!@$##!", "Enter Wrong format DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String errorMsg = Futil.fgetData("Wrong Maroc Telecom Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-invalid-telephone-error’]");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "wrong format Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+DuNumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			//WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			/*Genf.gfSleep(3);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			
				boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Maroc telecom as MoP", "Failed with exception :"+e, "No");
			  System.out.println("Failed Unhappy scenario with Maroc telecom");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	public int afSignUPWithDu_EmptyActivationCode(String Dunumber,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditDuMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, Dunumber.substring(3), "Enter DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			//String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			//WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-invalid-telephone-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "wrong format Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+Dunumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			EditActivationCode.clear();
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
		    Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			//String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidActivationCode= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-empty-code-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidActivationCode, "Empty Activation code", "Please type the code you received by SMS");
			
			
				/*boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with du as MoP : Exception ="+e, "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with DU");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithMarocTelecom_EmptyActivationCode(String Dunumber,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditDuMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, Dunumber.substring(3), "Enter DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			//String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			//WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-invalid-telephone-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "wrong format Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+Dunumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-sms-code']");
			EditActivationCode.clear();
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
		    Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			//String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidActivationCode= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecom-empty-code-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidActivationCode, "Empty Activation code", "Please type the code you received by SMS");
			
			
				/*boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Maroc telecom as MoP : Exception ="+e, "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with maroc telecom");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithEtisalat_EmptyActivationCode(String Dunumber,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditEtisalatMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditEtisalatMobileNUmber, Dunumber.substring(3), "Enter Etisalat Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			//String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			//WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-invalid-telephone-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "wrong format Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+Dunumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-sms-code']");
			EditActivationCode.clear();
			//StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
		    Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			//String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidActivationCode= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-empty-code-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidActivationCode, "Empty Activation code", "Please type the code you received by SMS");
			
			
				/*boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with Etisalat as MoP , Exception "+e, "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with Etisalat");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithDu_WrongActivationCode(String Dunumber,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditDuMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, Dunumber.substring(3), "Enter DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			//String errorMsg = Futil.fgetData("Wrong Du Number Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			//WebElement objerrInvalidNumber= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-invalid-telephone-error']");
			//StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidNumber, "wrong format Number", errorMsg);
			//String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+Dunumber+"' order by id DESC";
			//ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			//rs.next();
			//String OTP = rs.getString(1);
			WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			StepNum = Genf.fEnterDataToEditBox(EditActivationCode, "xxxxx", "Enter The OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
		    Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			String errorMsg = Futil.fgetData("Invalid Activation Code Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidActivationCode= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-wrong-validation-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidActivationCode, "Wrong Activation code", errorMsg);
			
			/*boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with du as MoP", "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with DU");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
	
	public int afSignUPWithDu_ExpiredActivationCode(String Dunumber,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EditDuMobileNUmber= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-mobile-number']");
			StepNum = Genf.fEnterDataToEditBox(EditDuMobileNUmber, Dunumber.substring(3), "Enter DU Number", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			Genf.gfSleep(1);
			WebElement BtnSENDCODE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-resend-sms']");
			StepNum = Genf.fClickOnObject(BtnSENDCODE, "Click On SEND CODE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(3);
			String strSQLQuerry = "select verification_code from sms_verification_codes where phone_number = '"+Dunumber+"' order by id DESC";
			ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			rs.next();
			String OTP = rs.getString(1);
			Genf.gfSleep(300);//wait 5 minutes
			WebElement EditActivationCode= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-sms-code']");
			StepNum = Genf.fEnterDataToEditBox(EditActivationCode, OTP, "Enter The expired OTP", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
		    Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			WebElement BtnCONTINUE= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-continue-button']");
			StepNum = Genf.fClickOnObject(BtnCONTINUE, "Click On CONTINUE", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			Genf.gfSleep(2);
			Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			String errorMsg = Futil.fgetData("Invalid Activation Code Error Message", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			WebElement objerrInvalidActivationCode= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-wrong-validation-error']");
			StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrInvalidActivationCode, "Wrong Activation code", errorMsg);
			
			/*boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
				  if(Ready){
				  Genf.gfSleep(3);
				  if(Driver.getCurrentUrl().contains("thankyou")){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify du Thank you page", "Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl(), "Yes");
					  System.out.println("Successfully verified the du Thank you page by checking The URL "+Driver.getCurrentUrl());  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl(), "Yes");   
					  System.out.println("Failed To verify The du thank you page, the url is "+Driver.getCurrentUrl()); 
					  StepNum = StepNum+1; 
				  	  }
				  }else{
					          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify du Thank you page", "The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
							  System.out.println("The du thank you page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
							  StepNum = StepNum+1;
				  }*/
				  
			//StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
			
		}catch(Exception e){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Unhappy scenario with du as MoP", "Failed ", "No");
			  System.out.println("Failed Unhappy scenario with DU");
			  StepNum = StepNum+1;
		}
		return   StepNum;
	}
}

