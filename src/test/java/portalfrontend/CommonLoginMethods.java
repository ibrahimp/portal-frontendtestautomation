package portalfrontend;

import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CommonLoginMethods {
	
	public GeneralMethods Genf;
	public FrameWorkUtility Futil;
	
	public int afLoginWith_ValidEmailAndPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String EmailId,String Password) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  EdtUserName.sendKeys(EmailId);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Email Address", "Successfully Entered The Email id :"+EmailId, "No");
				  System.out.println("Successfully Entered The Email id :"+EmailId);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Email Address", "Failed To Enter Email ID", "Yes");   
				  System.out.println("Failed To Enter Email ID"); 
				  Driver.close();
				  Assert.fail("Failed To Enter Email ID");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin!=null){
				  btnSubmitLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login ", "Successfully Clicked on the button Login", "No");
				  System.out.println("Successfully Clicked on the button Login");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login ", "Failed To Click on the button Login", "Yes");   
				  System.out.println("Failed To Click On the button Login"); 
				  Driver.close();
				  Assert.fail("Failed To Click On the button Login");
				  StepNum = StepNum+1; 
			  	  }
			  
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  Genf.gfSleep(5);
				  /*WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The user is logged in", "Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl(), "Yes");   
				  System.out.println("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl()); 
				  Assert.fail("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl());
				  StepNum = StepNum+1; 
			  	  }*/
			  }else{
				          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
						  System.out.println("The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
						  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login functionality", "Verification failed for The login functionality", "No");
			  System.out.println("Verification failed for The login functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyLoginAction(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The user is logged in", "Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl(), "Yes");   
				  System.out.println("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl()); 
				  Driver.close();
				  Assert.fail("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl());
				  StepNum = StepNum+1; 
			  	  } 
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login functionality", "Verification failed for The login functionality", "No");
			  System.out.println("Verification failed for The login functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_ValidMSISDNAndPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String MSISDN,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  
				  EdtUserName.sendKeys(MSISDN);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter MSISDN ", "Successfully Entered The MSISDN  :"+MSISDN, "No");
				  System.out.println("Successfully Entered The MSISDN :"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter MSISDN ", "Failed To Enter MSISDN", "Yes");   
				  System.out.println("Failed To Enter MSISDN"); 
				  Driver.close();
				  Assert.fail("Failed To Enter MSISDN");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123456";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin!=null){
				  btnSubmitLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login ", "Successfully Clicked on the button Login", "No");
				  System.out.println("Successfully Clicked on the button Login");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login ", "Failed To Click on the button Login", "Yes");   
				  System.out.println("Failed To Click On the button Login"); 
				  Driver.close();
				  Assert.fail("Failed To Click On the button Login");
				  StepNum = StepNum+1; 
			  	  }
			  
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  Genf.gfSleep(5);
				  /*WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The user is logged in", "Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl(), "Yes");   
				  System.out.println("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl()); 
				  Assert.fail("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl());
				  StepNum = StepNum+1; 
			  	  }*/
			  }else{
				          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
						  System.out.println("The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
						  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login functionality", "Verification failed for The login functionality", "No");
			  System.out.println("Verification failed for The login functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_NoUsrName_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()!=true){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is disabled", "Login Button is disabled", "No");
				  System.out.println("Login Button is disabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is disabled", "Failed To verify the disabled Login button", "Yes");   
				  System.out.println("Failed To verify the disabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the disabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login button disabled if the username and password is empty", "Verification failed for the disabled login button", "Yes");
			  System.out.println("Verification failed for the disabled login button");
			  StepNum = StepNum+1;
		}
		return   StepNum;	  
	}
	
	public int afLoginWith_WrongEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String EmailId = "sdhhhdbbbcggggdhh.dd";
				  Genf.TypeInField(EdtUserName, EmailId);
				  EdtUserName.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Wrong Email Format", "Successfully Entered The Wrong Email Format :"+EmailId, "No");
				  System.out.println("Successfully Entered The Wrong Email Format:"+EmailId);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Wrong Email Format", "Failed To Enter The Wrong Email Format", "Yes");   
				  System.out.println("Failed To Enter The Wrong Email Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Wrong Email Format");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(3);
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='emailLogin-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Email Address", "Please enter a valid email address e.g. XXX.XXX@gmail.com");
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()!=true){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is disabled", "Login Button is disabled", "No");
				  System.out.println("Login Button is disabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is disabled", "Failed To verify the disabled Login button", "Yes");   
				  System.out.println("Failed To verify the disabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the disabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login Unhappy scenario functionality", "Verification failed for The login Unhappy scenario functionality", "No");
			  System.out.println("Verification failed for The login functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_CorrectEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String EmailId = "Abc@def.dd";
				  Genf.TypeInField(EdtUserName, EmailId);
				  EdtUserName.sendKeys(EmailId);
				  EdtUserName.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter correct Email Format", "Successfully Entered The correct Email Format :"+EmailId, "No");
				  System.out.println("Successfully Entered The correct Email Format:"+EmailId);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter correct Email Format", "Failed To Enter The correct Email Format", "Yes");   
				  System.out.println("Failed To Enter The correct Email Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The correct Email Format");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(3);
			  
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()!=true){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is disabled", "Login Button is disabled", "No");
				  System.out.println("Login Button is disabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is disabled", "Failed To verify the disabled Login button", "Yes");   
				  System.out.println("Failed To verify the disabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the disabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login Unhappy scenario functionality", "Verification failed for The login Unhappy scenario functionality", "No");
			  System.out.println("Verification failed for The login functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyLoginPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  Genf.gfSleep(3);
			  if(Driver.getCurrentUrl().contains("login")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The login page", "Successfully Verified the Login Page", "Yes");
				  System.out.println("Successfully Verified the Login Page");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login page", "Failed To Verify the Login Page, and the loaded URL is :"+Driver.getCurrentUrl(), "Yes");   
				  System.out.println("Failed To Verify the Login Page");   
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Login Page", "Verification failed for The Login Page, the loaded URL is :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Verification failed for The Login Page, The loaded URL is :"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_WrongMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String MSISDN = "111122.333";
				  Genf.TypeInField(EdtUserName, MSISDN);
				  EdtUserName.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Wrong MSISDN Format", "Successfully Entered The Wrong MSISDN Format :"+MSISDN, "No");
				  System.out.println("Successfully Entered The Wrong MSISDN Format:"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Wrong MSISDN Format", "Failed To Enter The Wrong MSISDN Format", "Yes");   
				  System.out.println("Failed To Enter The Wrong MSISDN Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Wrong MSISDN Format");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(3);
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='emailLogin-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Mobile Number", "Please enter your mobile number in the correct format, e.g. 971XXXXXXXXX.");
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()!=true){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is disabled", "Login Button is disabled", "No");
				  System.out.println("Login Button is disabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is disabled", "Failed To verify the disabled Login button", "Yes");   
				  System.out.println("Failed To verify the disabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the disabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login Unhappy scenario functionality", "Verification failed for The login Unhappy scenario functionality", "No");
			  System.out.println("Verification failed for The login Unhappy scenario functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_WrongMSISDNFormat_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String MSISDN = "111122.333";
				  Genf.TypeInField(EdtUserName, MSISDN);
				  EdtUserName.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Wrong MSISDN Format", "Successfully Entered The Wrong MSISDN Format :"+MSISDN, "No");
				  System.out.println("Successfully Entered The Wrong MSISDN Format:"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Wrong MSISDN Format", "Failed To Enter The Wrong MSISDN Format", "Yes");   
				  System.out.println("Failed To Enter The Wrong MSISDN Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Wrong MSISDN Format");
				  StepNum = StepNum+1; 
			  	  }
			  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123456";
				  Genf.TypeInField(EdtPassword, Password);
				  EdtPassword.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Genf.gfSleep(3);
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='emailLogin-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Mobile Number", "Please enter your mobile number in the correct format, e.g. 971XXXXXXXXX.");
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()==true){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is Enabled", "Login Button is Enabled", "No");
				  System.out.println("Login Button is Enabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is Enabled", "Failed To verify the Enabled Login button", "Yes");   
				  System.out.println("Failed To verify the Enabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the Enabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
			  
			  btnSubmitLogin.click();
			  if(EdtUserName!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". verify click Action on LOG IN button", "Successfully verified there is no action associated with button LOG IN", "No");
				  System.out.println("Successfully verified there is no action associated with button LOG IN");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify click Action on LOG IN button", "There is some action performed by click on LOG IN", "Yes");   
				  System.out.println("There is some action performed by click on LOG IN"); 
				  Driver.close();
				  Assert.fail("There is some action performed by click on LOG IN");
				  StepNum = StepNum+1; 
			  	  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login Unhappy scenario functionality", "Verification failed for The login Unhappy scenario functionality", "No");
			  System.out.println("Verification failed for The login Unhappy scenario functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_CorrectMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String MSISDN = "971581111111";
				  Genf.TypeInField(EdtUserName, MSISDN);
				  //EdtUserName.sendKeys(MSISDN);
				  EdtUserName.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Correct MSISDN Format", "Successfully Entered The Correct MSISDN Format :"+MSISDN, "No");
				  System.out.println("Successfully Entered The Correct MSISDN Format:"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Correct MSISDN Format", "Failed To Enter The Correct MSISDN Format", "Yes");   
				  System.out.println("Failed To Enter The Correct MSISDN Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Correct MSISDN Format");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(3);
			  
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()!=true){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is disabled", "Login Button is disabled", "No");
				  System.out.println("Login Button is disabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is disabled", "Failed To verify the disabled Login button", "Yes");   
				  System.out.println("Failed To verify the disabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the disabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login Unhappy scenario functionality", "Verification failed for The login Unhappy scenario functionality", "No");
			  System.out.println("Verification failed for The login Unhappy scenario functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afDeleteUserName_VerifyErrorMessage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  EdtUserName.sendKeys(Keys.BACK_SPACE);
				  EdtUserName.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Clear user id", "Successfully cleared user", "No");
				  System.out.println("Successfully cleared user");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Clear user id", "Failed To clear user id", "Yes");   
				  System.out.println("Failed To clear user id"); 
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(3);
			  WebElement objerrEmailID= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='emailLogin-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Email Address Or Mobile number", "This field is required.");
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()!=true){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is disabled", "Login Button is disabled", "No");
				  System.out.println("Login Button is disabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is disabled", "Failed To verify the disabled Login button", "Yes");   
				  System.out.println("Failed To verify the disabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the disabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login Unhappy scenario functionality", "Verification failed for The login Unhappy scenario functionality", "No");
			  System.out.println("Verification failed for The login Unhappy scenario functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afDeletePassword_VerifyErrorMessage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  EdtPassword.sendKeys(Keys.BACK_SPACE);
				  EdtPassword.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Clear The Password", "Successfully Cleared The Password :", "No");
				  System.out.println("Successfully Cleared the password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Clear The Password", "Failed To Clear Password", "Yes");   
			      System.out.println("Failed To Clear Password"); 
			      Driver.close();
			      Assert.fail("Failed To Clear Password");
			  	  StepNum = StepNum+1;
				  }
			  Genf.gfSleep(3);
			  WebElement objerrPassword= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "This field is required.");
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()!=true){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is disabled", "Login Button is disabled", "No");
				  System.out.println("Login Button is disabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is disabled", "Failed To verify the disabled Login button", "Yes");   
				  System.out.println("Failed To verify the disabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the disabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login Unhappy scenario functionality", "Verification failed for The login Unhappy scenario functionality", "No");
			  System.out.println("Verification failed for The login Unhappy scenario functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_NoUsrName_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123456";
				  Genf.TypeInField(EdtPassword, Password);
				  EdtPassword.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()!=true){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is disabled", "Login Button is disabled", "No");
				  System.out.println("Login Button is disabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is disabled", "Failed To verify the disabled Login button", "Yes");   
				  System.out.println("Failed To verify the disabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the disabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login functionality", "Verification failed for The login functionality", "No");
			  System.out.println("Verification failed for The login functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_WrongEmailFormat_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String EmailId = "sdhhhdbbbcggggdhh.dd";
				  EdtUserName.sendKeys(EmailId);
				  EdtUserName.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Wrong Email Format", "Successfully Entered The Wrong Email Format :"+EmailId, "No");
				  System.out.println("Successfully Entered The Wrong Email Format:"+EmailId);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Wrong Email Format", "Failed To Enter The Wrong Email Format", "Yes");   
				  System.out.println("Failed To Enter The Wrong Email Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Wrong Email Format");
				  StepNum = StepNum+1; 
			  	  }
			  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123456";
				  EdtPassword.sendKeys(Password);
				  EdtPassword.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='emailLogin-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Email Address", "Please enter a valid email address e.g. XXX.XXX@gmail.com");
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin.isEnabled()==true){
				  
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login Button is Enabled", "Login Button is Enabled", "No");
				  System.out.println("Login Button is disabled");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login Button is Enabled", "Failed To verify the Enabled Login button", "Yes");   
				  System.out.println("Failed To verify the disabled Login button"); 
				  Driver.close();
				  Assert.fail("Failed To verify the disabled Login button");
				  StepNum = StepNum+1; 
			  	  } 
			  btnSubmitLogin.click();
			  if(EdtUserName!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". verify click Action on LOG IN button", "Successfully verified there is no action associated with button LOG IN", "No");
				  System.out.println("Successfully verified there is no action associated with button LOG IN");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify click Action on LOG IN button", "There is some action performed by click on LOG IN", "Yes");   
				  System.out.println("There is some action performed by click on LOG IN"); 
				  Driver.close();
				  Assert.fail("There is some action performed by click on LOG IN");
				  StepNum = StepNum+1; 
			  	  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login functionality", "Verification failed for The login functionality", "No");
			  System.out.println("Verification failed for The login functinality");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_ValidEmail_InvalidPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String EmailId = "ibrahim.poovakkattil@starzplayarabia.com";
				  Genf.TypeInField(EdtUserName, EmailId);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Email Address", "Successfully Entered The Email id :"+EmailId, "No");
				  System.out.println("Successfully Entered The Email id :"+EmailId);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Email Address", "Failed To Enter Email ID", "Yes");   
				  System.out.println("Failed To Enter Email ID"); 
				  Driver.close();
				  Assert.fail("Failed To Enter Email ID");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "567890";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The wrong Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the wrong password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The wrong Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin!=null){
				  btnSubmitLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login Login BUtton ", "Successfully Clicked on the button Login", "No");
				  System.out.println("Successfully Clicked on the button Login");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login Submit BUtton", "Failed To Click On the button Login", "Yes");   
				  System.out.println("Failed To Clicked on the button Login"); 
				  Driver.close();
				  Assert.fail("Failed To Clicked on the button Login");
				  StepNum = StepNum+1; 
			  	  }
			  
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordIncorrect']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Wrong Password", "Please enter a valid username and/or password.");
			  }else{
				          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The error message for wrong password", "The error message verification failed. The wait message lasted for more than 50 seconds", "Yes");
						  System.out.println("The error message verification failed. The wait message lasted for more than 50 seconds");
						  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message for wrong password", "verification failed for The error message", "No");
			  System.out.println("verification failed for The error message");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_ValidMSISDN_InvalidPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String MSISDN = "971581111111";
				  Genf.TypeInField(EdtUserName, MSISDN);
				  EdtUserName.sendKeys(Keys.TAB);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Correct MSISDN", "Successfully Entered The Correct MSISDN :"+MSISDN, "No");
				  System.out.println("Successfully Entered The Correct MSISDN:"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Correct MSISDN", "Failed To Enter The Correct MSISDN", "Yes");   
				  System.out.println("Failed To Enter The Correct MSISDN"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Correct MSISDN");
				  StepNum = StepNum+1; 
			  	  }
			  Genf.gfSleep(3);
			  
			  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "567890";
				  Genf.TypeInField(EdtPassword, Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The wrong Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the wrong password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The wrong Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
			  if(btnSubmitLogin!=null){
				  btnSubmitLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login BUtton ", "Successfully Clicked on the button Login", "No");
				  System.out.println("Successfully Clicked on the button Login");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login BUtton", "Failed To Click On the button Login", "Yes");   
				  System.out.println("Failed To Clicked on the button Login"); 
				  Driver.close();
				  Assert.fail("Failed To Clicked on the button Login");
				  StepNum = StepNum+1; 
			  	  }
			  
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='passwordIncorrect']");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Wrong Password", "Please enter a valid username and/or password.");
			  }else{
				          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The error message for wrong password", "The error message verification failed. The wait message lasted for more than 50 seconds", "Yes");
						  System.out.println("The error message verification failed. The wait message lasted for more than 50 seconds");
						  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error message for wrong password", "verification failed for The error message", "No");
			  System.out.println("verification failed for The error message");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_facebook_Cancel(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			  WebElement btnFacebookLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  if(btnFacebookLogin!=null){
				  //btnFacebookSignUp.click();
				  Thread.sleep(5000);
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnFacebookLogin);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On LOG IN WITH FACEBOOK Button ", "Successfully Clicked on the LOG IN WITH FACEBOOK Button", "Yes");
				  System.out.println("Successfully Clicked on the LOG IN WITH FACEBOOK Button");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On LOG IN WITH FACEBOOK Button", "Failed To Click On LOG IN WITH FACEBOOK Button", "Yes");   
				  System.out.println("Failed To Click On LOG IN WITH FACEBOOK Button"); 
				  Driver.close();
				  Assert.fail("Failed To Click On LOG IN WITH FACEBOOK Button");
				  StepNum = StepNum+1; 
			  	  }
			  
			  try{
				  Thread.sleep(3000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("facebook.com/login.php"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  Driver.close();
				  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "Yes");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  Driver.close();
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  StepNum = StepNum+1;
			  }
			  
			  WebElement btnFacebookLogIinAfterFBClose= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  if(btnFacebookLogIinAfterFBClose!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify appliaction is is on the same page ", "Successfully verifed", "Yes");
				  System.out.println("Successfully verifed the application has returned to the Login page");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify appliaction is is on the same page", "Application not returned to the same page as the LOG IN WITH FACEBOOK is not present ", "Yes");   
				  System.out.println("Application not returned to the same page as the LOG IN WITH FACEBOOK is not present"); 
				  Driver.close();
				  Assert.fail("Application not returned to the same page as the LOG IN WITH FACEBOOK is not present");
				  StepNum = StepNum+1; 
			  	  }
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". UN Happy scenarion with Facebook LOG IN", "UnHappy scenarion with Facebook Sign UP failed", "No");
			  System.out.println("UN HAppy scenarion with Facebook Sign UP failed");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_facebook_ValidCredentials(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String fbemail, String fbPass  ) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			  WebElement btnFacebookLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  StepNum = Genf.fJSClickOnObject(btnFacebookLogin, "Click On SIGN UP WITH FACEBOOK Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  
			  try{
				  Thread.sleep(3000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("facebook.com/login.php"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  Driver.close();
				  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  WebElement EdtFacebookEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  StepNum = Genf.fEnterDataToEditBox(EdtFacebookEmail, fbemail, "Enter Facebook email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			  
			  WebElement EdtFacebookPass= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='pass']");
			  StepNum = Genf.fEnterDataToEditBox(EdtFacebookPass, fbPass, "Enter Facebook password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			  
			  WebElement BtnFacebookLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='u_0_2' and @name='login']");
			  StepNum = Genf.fClickOnObject(BtnFacebookLogin, "Click On Facebook Login Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  
			  Driver.switchTo().window(tabs2.get(0)); 
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  Genf.gfSleep(5);
				  /*WebElement btnSettings= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
			  if(btnSettings!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The user is logged in", "Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the verified the user is logged in by checking the settings Icon, An the URL is"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl(), "Yes");   
				  System.out.println("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl()); 
				  Assert.fail("Failed To verify the User is logged in, the url is "+Driver.getCurrentUrl());
				  StepNum = StepNum+1; 
			  	  }*/
			  }else{
				          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
						  System.out.println("The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
						  StepNum = StepNum+1;
			  }
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "Yes");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  Driver.close();
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  StepNum = StepNum+1;
			  }
			  
			  
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". UN Happy scenarion with Facebook LOG IN", "UnHappy scenarion with Facebook Sign UP failed", "No");
			  System.out.println("UN HAppy scenarion with Facebook Sign UP failed");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afLoginWith_facebook(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String fbemail, String fbPass  ) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			  WebElement btnFacebookLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  StepNum = Genf.fJSClickOnObject(btnFacebookLogin, "Click On LOGIN WITH FACEBOOK Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  
			  try{
				  Thread.sleep(3000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("facebook.com/login.php"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  Driver.close();
				  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  WebElement EdtFacebookEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  StepNum = Genf.fEnterDataToEditBox(EdtFacebookEmail, fbemail, "Enter Facebook email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			  
			  WebElement EdtFacebookPass= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='pass']");
			  StepNum = Genf.fEnterDataToEditBox(EdtFacebookPass, fbPass, "Enter Facebook password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			  
			  WebElement BtnFacebookLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='u_0_2' and @name='login']");
			  StepNum = Genf.fClickOnObject(BtnFacebookLogin, "Click On Facebook Login Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  Genf.gfSleep(2);
			  if(Driver.getWindowHandles().size()>1){
				  
				  WebElement BtnCountinue= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@name='__CONFIRM__']");
				  StepNum = Genf.fClickOnObject(BtnCountinue, "Click On Countinue", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
			  }
			  Driver.switchTo().window(tabs2.get(0)); 
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  Genf.gfSleep(5);
			  }else{
				          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The user is logged in", "The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
						  System.out.println("The user login verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
						  StepNum = StepNum+1;
			  }
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "Yes");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  StepNum = StepNum+1;
			  }
			  
			  
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+".scenarion with Facebook LOG IN", "scenarion with Facebook Sign UP failed", "No");
			  System.out.println("UN HAppy scenarion with Facebook Sign UP failed");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyUnRegistered_Facebook_Error(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation  ) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			  WebElement objErrorUnRegisteredFBUser= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-account-not-found-modal']//p");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objErrorUnRegisteredFBUser, "Un Registered Facebook User", "This Facebook account does not exist in our system. Please go to signup screen and register.");
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+".scenarion with Facebook LOG IN", "scenarion with Facebook Sign UP failed", "No");
			  System.out.println("UN HAppy scenarion with Facebook Sign UP failed");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	
}
