package portalfrontend;

import java.io.File;
import java.net.MalformedURLException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class LandingPageTest {
	public ExtentReports Extreport;
	public ExtentTest Exttest;
	public WebDriver Driver;
	public HtmlUnitDriver Hdriver; 
	public String ResultLocation;
	public FrameWorkUtility Futil;
	public GeneralMethods Genf;
	public DesiredCapabilities Cap;
	public LandingPageMethods LPMethods;
	public File PhantomJsSrc;
	
	
	@Parameters({"Browser"})
	@BeforeTest(alwaysRun = true)
	 public void startTest(String BrowserName,final ITestContext testContext) {
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		LPMethods = new LandingPageMethods();
	    ResultLocation = System.getProperty("user.dir")+"/Custom_Report/"+testContext.getName();
	    Extreport = new ExtentReports(ResultLocation+"/"+testContext.getName()+"-"+Futil.fGetRandomNumUsingTime()+".html", true);
	      Cap = DesiredCapabilities.chrome();
		  Cap.setBrowserName(BrowserName);
		  Cap.setCapability("ignoreZoomSetting", true);
		  Cap.setPlatform(Platform.MAC);
		  Cap.setCapability("nativeEvents","false");
		  PhantomJsSrc = new File(System.getProperty("user.dir")+"/Resources/phantomjs/bin/phantomjs");
	}
	
	  @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")//WORKING END TO END IN JENKIN
	  public void TC001_GuestLandingHome_GoToHome(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC001_GuestLandingHome_GoToHome(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")//WORKING END TO END IN JENKIN
	  public void TC002_GuestLandingHome_GotoExplore(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC002_GuestLandingHome_GotoExplore(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")//WORKING END TO END IN JENKIN
	  public void TC003_Guest_Landing_to_Login(String AppURL,String AppTitle, String Browser,String TestType,String NodeURL) throws MalformedURLException 
	 {
		LPMethods.TC003_Guest_Landing_to_Login(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);	
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC004_Guest_Landing_to_StartFreePeriod(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		//LPMethods.TC004_Guest_Landing_to_StartFreePeriod(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC005_Guest_Landing_to_AppStore(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC005_Guest_Landing_to_AppStore(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC006_Guest_Landing_to_GooglePlayStore(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC006_Guest_Landing_to_GooglePlayStore(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC007_Guest_LanguageTo_Arabic(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	 {
		LPMethods.TC007_Guest_LanguageTo_Arabic(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC008_Guest_LanguageTo_French(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC008_Guest_LanguageTo_French(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
  
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC009_Guest_Landing_to_WhyStarzPlay_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC009_Guest_Landing_to_WhyStarzPlay(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC010_Guest_Landing_to_Help_Center_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC010_Guest_Landing_to_Help_Center(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC011_Guest_Landing_to_PartnerWithUs_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC011_Guest_Landing_to_PartnerWithUs(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC012_Guest_Landing_to_Company_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC012_Guest_Landing_to_Company(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC013_Guest_Landing_to_TermsAndAconditions_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC013_Guest_Landing_to_TermsAndAconditions(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC014_Guest_Landing_to_PrivacyPolicy_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC014_Guest_Landing_to_PrivacyPolicy(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC015_Guest_Landing_to_Blog_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC015_Guest_Landing_to_Blog(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC016_Guest_Landing_to_Careers_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC016_Guest_Landing_to_Careers(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC017_Guest_Landing_to_FaceBook_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC017_Guest_Landing_to_FaceBook(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC018_Guest_Landing_to_Twitter_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC018_Guest_Landing_to_Twitter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC019_Guest_Landing_to_Instagram_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC019_Guest_Landing_to_Instagram(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC020_Guest_Landing_to_YouTube_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC020_Guest_Landing_to_YouTube(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC021_Guest_Landing_to_AppStore_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC021_Guest_Landing_to_AppStore_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC022_Guest_Landing_to_GooglePlayStore_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC022_Guest_Landing_to_GooglePlayStore_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC023_User_Landing_to_WhyStarzPlay_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC023_User_Landing_to_WhyStarzPlay_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC024_User_Landing_to_Help_Center_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC024_User_Landing_to_Help_Center_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC025_User_Landing_to_Partener_WithUS_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC025_User_Landing_to_Partener_WithUS_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC026_User_Landing_to_Company_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC026_User_Landing_to_Company_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC027_User_Landing_to_TermsAndConditions_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC027_User_Landing_to_TermsAndConditions_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC028_User_Landing_to_PrivacyPolicy_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC028_User_Landing_to_PrivacyPolicy_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC029_User_Landing_to_Blog_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC029_User_Landing_to_Blog_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC030_User_Landing_to_Careers_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC030_User_Landing_to_Careers_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC031_User_Landing_to_Facebook_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC031_User_Landing_to_Facebook_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC032_User_Landing_to_Twitter_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC032_User_Landing_to_Twitter_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC033_User_Landing_to_Instagram_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC033_User_Landing_to_Instagram_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC034_User_Landing_to_YouTube_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC034_User_Landing_to_YouTube_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC035_User_Landing_to_AppStore_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC035_User_Landing_to_AppStore_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC036_User_Landing_to_GooglePlayStore_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC036_User_Landing_to_GooglePlayStore_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC037_User_Landing_to_AppStore_FromBody(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC037_User_Landing_to_AppStore_FromBody(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC038_User_Landing_to_GooglePlayStore_FromBody(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC038_User_Landing_to_GooglePlayStore_FromBody(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC039_User_Landing_to_HomePage_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC039_User_Landing_to_HomePage_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC040_User_Landing_to_Movies_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC040_User_Landing_to_Movies_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC041_User_Landing_to_Series_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC041_User_Landing_to_Series_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC042_User_Landing_to_ARABIC_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC042_User_Landing_to_ARABIC_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC043_User_Landing_to_KIDS_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC043_User_Landing_to_KIDS_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC044_User_Landing_OpensSearchBar_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC044_User_Landing_OpensSearchBar_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC045_User_Landing_OpensSettings_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC045_User_Landing_OpensSettings_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC046_User_Landing_GoToSettingsPage_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC046_User_Landing_GoToSettingsPage_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC047_User_Landing_GoToMyLibraryPage_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC047_User_Landing_GoToMyLibraryPage_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC048_User_Landing_ChangeLanguageTo_Arabic_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC048_User_Landing_ChangeLanguageTo_Arabic_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC049_User_Landing_ChangeLanguageTo_French_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC049_User_Landing_ChangeLanguageTo_French_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC050_User_Landing_Logout_fromHeader(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC050_User_Landing_Logout_fromHeader(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC051_Guest_Landing_to_SignUp_NoUsrNameNoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC051_Guest_Landing_to_SignUp_NoUsrNameNoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC052_Guest_Landing_to_SignUp_WrongEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC052_Guest_Landing_to_SignUp_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC053_Guest_Landing_to_SignUp_WrongMSISDN_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC053_Guest_Landing_to_SignUp_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC054_Guest_Landing_to_SignUp_ValidEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC054_Guest_Landing_to_SignUp_ValidEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC055_Guest_Landing_to_SignUp_ValidMSISDNFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC055_Guest_Landing_to_SignUp_ValidMSISDNFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC056_Guest_Landing_to_SignUp_NoUserName_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC056_Guest_Landing_to_SignUp_NoUserName_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC057_Guest_Landing_to_SignUp_WrongEmailFormat_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC057_Guest_Landing_to_SignUp_WrongEmailFormat_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC058_Guest_Landing_to_SignUp_WrongMSISDNFormat_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC058_Guest_Landing_to_SignUp_WrongMSISDNFormat_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC059_Guest_Landing_to_SignUp_ValidEmailFormat_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC059_Guest_Landing_to_SignUp_ValidEmailFormat_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC060_Guest_Landing_to_SignUp_ValidMSISDNFormat_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC060_Guest_Landing_to_SignUp_ValidMSISDNFormat_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC061_Guest_Landing_to_SignUp_AlreadyReg_Email(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC061_Guest_Landing_to_SignUp_AlreadyReg_Email(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC062_Guest_Landing_to_SignUp_AlreadyReg_MSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC062_Guest_Landing_to_SignUp_AlreadyReg_MSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC063_Guest_Landing_to_SignUp_FaceBook_Cancel(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC063_Guest_Landing_to_SignUp_FaceBook_Cancel(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC064_Guest_Landing_to_SignUp_FaceBook_NotApprove(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC064_Guest_Landing_to_SignUp_FaceBook_NotApprove(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC065_072_Guest_Landing_to_SignUp_Email_CreditCard(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC065_072_Guest_Landing_to_SignUp_Email_CreditCard(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL","StarzPlayVoucherCode"})
	  @Test(enabled=false,groups="Guest")
	  public void TC073_080_Guest_Landing_to_SignUp_Email_Voucher(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL,String StarzPlayVoucherCode) throws MalformedURLException 
	  {
		LPMethods.TC073_080_Guest_Landing_to_SignUp_Email_Voucher(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap,StarzPlayVoucherCode);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC081_User_Landing_to_Login_ActiveUSer(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC081_User_Landing_to_Login_ActiveUSer(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC082_User_Landing_to_Login_NoUsrName_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC082_User_Landing_to_Login_NoUsrName_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC083_User_Landing_to_Login_WrongEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC083_User_Landing_to_Login_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC084_User_Landing_to_Login_WrongEmailFormat_NoPwd_DeleteEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC084_User_Landing_to_Login_WrongEmailFormat_NoPwd_DeleteEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC085_User_Landing_to_Login_WrongMSISDN_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC085_User_Landing_to_Login_WrongMSISDN_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC086_User_Landing_to_Login_WrongMSISDN_NoPwd_DeleteMSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC086_User_Landing_to_Login_WrongMSISDN_NoPwd_DeleteMSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC087_User_Landing_to_Login_CorrectEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC087_User_Landing_to_Login_CorrectEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC088_User_Landing_to_Login_CorrectEmailFormat_NoPwd_DeleteEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC088_User_Landing_to_Login_CorrectEmailFormat_NoPwd_DeleteEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC089_User_Landing_to_Login_CorrectMSISDN_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC089_User_Landing_to_Login_CorrectMSISDN_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC090_User_Landing_to_Login_CorrectMSISDN_NoPwd_DeleteMSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC090_User_Landing_to_Login_CorrectMSISDN_NoPwd_DeleteMSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC091_User_Landing_to_Login_NoUserName_AnyPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC091_User_Landing_to_Login_NoUserName_AnyPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC092_User_Landing_to_Login_WrongEmailFormat_AnyPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC092_User_Landing_to_Login_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC093_User_Landing_to_Login_WrongEmailFormat_AnyPwd_DeleteEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC093_User_Landing_to_Login_WrongEmailFormat_AnyPwd_DeleteEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC094_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC094_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC095_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd_DeleteMSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC095_User_Landing_to_Login_WrongMSISDNFormat_AnyPwd_DeleteMSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC096_User_Landing_to_Login_ValidEmail_InvalidPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC096_User_Landing_to_Login_ValidEmail_InvalidPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC097_User_Landing_to_Login_ValidMSISDN_InvalidPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC097_User_Landing_to_Login_ValidMSISDN_InvalidPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC098_User_Landing_to_Login_ValidEmail_InvalidPwd_DeleteBoth(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC098_User_Landing_to_Login_ValidEmail_InvalidPwd_DeleteBoth(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC099_User_Landing_to_Login_ValidMSISDN_InvalidPwd_DeleteBoth(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		//LPMethods.TC099_User_Landing_to_Login_ValidMSISDN_InvalidPwd_DeleteBoth(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC100_User_Landing_to_Login_Facebook_Cancel(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC100_User_Landing_to_Login_Facebook_Cancel(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC101_User_Landing_to_Login_ValidEmail_ValidPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC101_User_Landing_to_Login_ValidEmail_ValidPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL","ValidMSISDN"})
	  @Test(enabled=false,groups="User")
	  public void TC102_User_Landing_to_Login_ValidMSISDN_ValidPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL,String ValidMSISDN) throws MalformedURLException 
	  {
		LPMethods.TC102_User_Landing_to_Login_ValidMSISDN_ValidPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,ValidMSISDN,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC103_User_Landing_to_Login_Facebook_ValidCredentials(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC103_User_Landing_to_Login_Facebook_ValidCredentials(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC104_Guest_Landing_to_SignUp_Facebook_NotApprove_AccessToEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC104_Guest_Landing_to_SignUp_Facebook_NotApprove_AccessToEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","ProspectUser","EndToEnd","Email"})
	  public void TC105_TC107_Landing_to_SignUP_ProspectUser_Email_EndToEnd_Happy(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC105_TC107_Landing_to_SignUP_ProspectUser_Email_EndToEnd_Happy(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","ProspectUser","EndToEnd","Facebook"})
	  public void TC108_TC110_Landing_to_SignUP_ProspectUser_Facebook_EndToEnd_Happy(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC108_TC110_Landing_to_SignUP_ProspectUser_Facebook_EndToEnd_Happy(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Email","EndToEnd","CreditCard"})
	  public void TC111_TC128_Landing_to_SignUP_Email_CreditCard_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC111_TC128_Landing_to_SignUP_Email_CreditCard_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Email","EndToEnd","Voucher"})
	  public void TC129_TC133_Landing_To_SignUP_Email_Voucher_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC129_TC133_Landing_To_SignUP_Email_Voucher_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Facebook","EndToEnd","CreditCard"})
	  public void TC134_TC151_SignUP_Facebook_CreditCard_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC134_TC151_SignUP_Facebook_CreditCard_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Facebook","EndToEnd","CreditCard"})
	  public void TC152_TC159_Landing_To_SignUp_Facebook_CreditCard_Happy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC152_TC159_Landing_To_SignUp_Facebook_CreditCard_Happy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Facebook","EndToEnd","Voucher"})
	  public void TC160_TC164_Landng_To_SignUP_Facebook_Voucher_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC160_TC164_Landng_To_SignUP_Facebook_Voucher_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL","StarzPlayVoucherCode"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Facebook","EndToEnd","Voucher"})
	  public void TC160_TC167_SignUp_Facebook_Voucher_Happy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL,String StarzPlayVoucherCode) throws MalformedURLException 
	  {
		LPMethods.TC165_TC172_Landing_To_SignUp_Facebook_Voucher_Happy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap,StarzPlayVoucherCode);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","UnHappy","Login","Facebook","EndToEnd"})
	  public void TC173_Landing_To_LoginWith_Facebook_UnRegisteredAccount(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC173_Landing_To_LoginWith_Facebook_UnRegisteredAccount(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Email","EndToEnd"})
	  public void TC174_Landing_To_LoginWith_ValidEmailAndPwd_ProspectUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC174_Landing_To_LoginWith_ValidEmailAndPwd_ProspectUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	@Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd"})
	  public void TC175_Landing_To_LoginWith_Facebook_ProspectUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC175_Landing_To_LoginWith_Facebook_ProspectUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Email","EndToEnd"})
	  public void TC176_Landing_To_LoginWith_ValidEmailAndPwd_DisconnectedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC176_Landing_To_LoginWith_ValidEmailAndPwd_DisconnectedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd"})
	  public void TC177_Landing_To_LoginWith_Facebook_DisconnectedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC177_Landing_To_LoginWith_Facebook_DisconnectedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Email","EndToEnd"})
	  public void TC178_Landing_To_LoginWith_ValidEmailAndPwd_DeactivatedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC178_Landing_To_LoginWith_ValidEmailAndPwd_DeactivatedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd"})
	  public void TC179_Landing_To_LoginWith_Facebook_DeactivatedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC179_Landing_To_LoginWith_Facebook_DeactivatedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Du","EndToEnd"})
	  public void TC180_188_Landing_To_SignUp_WithEmail_MoP_Du_Weekly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC180_188_Landing_To_SignUp_WithEmail_MoP_Du_Weekly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Du","EndToEnd"})
	  public void TC189_197_GuestAffiliateSignUp_WithEmail_MoP_Du_Monthly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC189_197_Landing_To_SignUp_WithEmail_MoP_Du_Monthly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC198_Guest_Landing_to_MuleSoft_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC198_Guest_Landing_to_MuleSoft_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC199_User_Landing_to_MuleSoft_fromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC199_User_Landing_to_MuleSoft_fromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Email","EndToEnd","DU"})
	  public void TC200_TC218_Landing_to_SignUP_DU_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		LPMethods.TC200_TC218_Landing_to_SignUP_DU_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Du","EndToEnd"})
	  public void TC219_228_Landing_to_SignUp_WithEmail_MoP_Etisalat_Daily_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC219_227_Landing_to_SignUp_WithEmail_MoP_Etisalat_Daily_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Du","EndToEnd"})
	  public void TC228_236_Landing_to_SignUp_WithEmail_MoP_Etisalat_Weekly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC228_236_Landing_to_SignUp_WithEmail_MoP_Etisalat_Weekly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Du","EndToEnd"})
	  public void TC237_236_Landing_to_SignUp_WithEmail_MoP_Etisalat_Monthly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC237_245_Landing_to_SignUp_WithEmail_MoP_Etisalat_Monthly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Email","EndToEnd","Etisalat"})
	  public void TC246_TC266_Landing_to_SignUP_WithEmail_Etisalat_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC246_TC266_Landing_to_SignUP_WithEmail_Etisalat_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC267_Guest_Landing_to_SignUp_fromGreatEntertainment(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC267_Guest_Landing_to_SignUp_fromGreatEntertainment(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="Guest")
	  public void TC268_Guest_Landing_to_SignUp_fromKidsSection(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL)throws MalformedURLException 
	 {
		LPMethods.TC268_Guest_Landing_to_SignUp_fromKidsSection(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	 }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"Guest","Happy","SignUp","Email","MarocTelecom","EndToEnd"})
	  public void TC269_277_Guest_landing_To_SignUp_WithEmail_MoP_MarocTelecom_Weekly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC269_277_Guest_landing_To_SignUp_WithEmail_MoP_MarocTelecom_Weekly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"Guest","Happy","SignUp","Email","MarocTelecom","EndToEnd"})
	  public void TC278_286_Guest_landing_To_SignUp_WithEmail_MoP_MarocTelecom_Monthly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 LPMethods.TC278_286_Guest_landing_To_SignUp_WithEmail_MoP_MarocTelecom_Monthly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
}
