package portalfrontend;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class SettingsPageMethods {
	public GeneralMethods Genf;
	public FrameWorkUtility Futil;
	public CommonFooterMethods CFMethods;
	public CommonHeaderMethods CHMethods;
	public CommonLoginMethods CLoginMethods;
	public CommonSignUpMethods CSignUpMethods;
	public CommonMoPMethods CMoPMethods;
	public CommonSettingsMethods CSettingsMethods;
	public void TC001_UserSettingPage_ValidateFirstName(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC001_UserSettingPage_ValidateFirstName: Execution Started");
		  Exttest = Extreport.startTest("TC001_UserSettingPage_ValidateFirstName","User is at Setting Page and Verify First Name field and functionality");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afVerifyFirstNameFieldFunctions(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC001_UserSettingPage_ValidateFirstName : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC002_UserSettingPage_ValidateSecondName(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC002_UserSettingPage_ValidateSecondName: Execution Started");
		  Exttest = Extreport.startTest("TC002_UserSettingPage_ValidateSecondName","User is at Setting Page and Verify last Name field and functionality");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afVerifyFirstNameFieldFunctions(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC002_UserSettingPage_ValidateSecondName : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC003_UserSettingPage_EditEmail_AllEmpty(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC003_UserSettingPage_EditEmail_AllEmpty: Execution Started");
		  Exttest = Extreport.startTest("TC003_UserSettingPage_EditEmail_AllEmpty","User at Setting page edit Email address and provide all empty and verify error messages");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditEmail_AllEmpty(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC003_UserSettingPage_EditEmail_AllEmpty : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC004_UserSettingPage_EditEmail_Incorrect_oldEmail_Empty_Email1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC004_UserSettingPage_EditEmail_Incorrect_oldEmail_Empty_Email1AndEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC004_UserSettingPage_EditEmail_Incorrect_oldEmail_Empty_Email1AndEmail2","User at Setting page edit Email address and provide incorrect old email id and empty new email,Re-enter New Email ");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_Incorrect_oldEmail_Empty_Email1AndEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC004_UserSettingPage_EditEmail_Incorrect_oldEmail_Empty_Email1AndEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC005_UserSettingPage_EditEmail_Incorrect_oldEmailAndEmail1_Empty_Email2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC005_UserSettingPage_EditEmail_Incorrect_oldEmailAndEmail1_Empty_Email2: Execution Started");
		  Exttest = Extreport.startTest("TC005_UserSettingPage_EditEmail_Incorrect_oldEmailAndEmail1_Empty_Email2","User at Setting page edit Email address and provide incorrect old email id and incorrect new email, Empty Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_Incorrect_oldEmailAndEmail1_Empty_Email2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC005_UserSettingPage_EditEmail_Incorrect_oldEmailAndEmail1_Empty_Email2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC006_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC006_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_IncorrectEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC006_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_IncorrectEmail2","User at Setting page edit Email address and provide incorrect old email id and empty new email, incorrect Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_IncorrectoldEmail_EmptyEmail1_IncorrectEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC006_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_IncorrectEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC007_UserSettingPage_EditEmail_IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC007_UserSettingPage_EditEmail_IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC007_UserSettingPage_EditEmail_IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2","User at Setting page edit Email address and provide incorrect old email id, new email, and Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC007_UserSettingPage_EditEmail_IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC008_UserSettingPage_EditEmail_IncorrectoldEmail_CorrectEmail1_EmptyEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC008_UserSettingPage_EditEmail_IncorrectoldEmail_CorrectEmail1_EmptyEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC008_UserSettingPage_EditEmail_IncorrectoldEmail_CorrectEmail1_EmptyEmail2","User at Setting page edit Email address and provide incorrect old email id, correct new email, and Empty Re-enter New Email ");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_IncorrectoldEmail_CorrectEmail1_EmptyEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC008_UserSettingPage_EditEmail_IncorrectoldEmail_CorrectEmail1_EmptyEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC009_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_CorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC009_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_CorrectEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC009_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_CorrectEmail2","User at Setting page edit Email address and provide incorrect old email id, empty new email, and correct Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_IncorrectoldEmail_EmptyEmail1_correctEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC009_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_CorrectEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC010_UserSettingPage_EditEmail_IncorrectoldEmail_MisMatchingEmail1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC010_UserSettingPage_EditEmail_IncorrectoldEmail_MisMatchingEmail1AndEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC010_UserSettingPage_EditEmail_IncorrectoldEmail_MisMatchingEmail1AndEmail2","User at Setting page edit Email address and provide incorrect old email id and mismatching new email, Re-enter New Email ");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_IncorrectoldEmail_MisMatchingEmail1AndEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC010_UserSettingPage_EditEmail_IncorrectoldEmail_MisMatchingEmail1AndEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC011_UserSettingPage_EditEmail_IncorrectoldEmail_MatchingEmail1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC011_UserSettingPage_EditEmail_IncorrectoldEmail_MatchingEmail1AndEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC011_UserSettingPage_EditEmail_IncorrectoldEmail_MatchingEmail1AndEmail2","User at Setting page edit Email address and provide incorrect old email id and matching new email, Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_IncorrectoldEmail_MatchingEmail1AndEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC011_UserSettingPage_EditEmail_IncorrectoldEmail_MatchingEmail1AndEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC012_UserSettingPage_EditEmail_Correct_oldEmail_Empty_Email1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC012_UserSettingPage_EditEmail_Correct_oldEmail_Empty_Email1AndEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC012_UserSettingPage_EditEmail_Correct_oldEmail_Empty_Email1AndEmail2","User at Setting page edit Email address and provide correct old email id and empty new email,Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_Correct_oldEmail_Empty_Email1AndEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC012_UserSettingPage_EditEmail_Correct_oldEmail_Empty_Email1AndEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC013_UserSettingPage_EditEmail_CorrectOldEmail_IncorrectEmail1_EmptyEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC013_UserSettingPage_EditEmail_CorrectOldEmail_IncorrectEmail1_EmptyEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC013_UserSettingPage_EditEmail_CorrectOldEmail_IncorrectEmail1_EmptyEmail2","User at Setting page edit Email address and provide correct old email id and incorrect new email, Empty Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_CorrectoldEmail_IncorrectEmail1_Empty_Email2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC013_UserSettingPage_EditEmail_CorrectOldEmail_IncorrectEmail1_EmptyEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC014_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC014_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_IncorrectEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC014_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_IncorrectEmail2","User at Setting page edit Email address and provide correct old email id and empty new email, incorrect Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_CorrectoldEmail_EmptyEmail1_IncorrectEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC014_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_IncorrectEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC015_UserSettingPage_EditEmail_CorrectoldEmail_IncorrectEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC015_UserSettingPage_EditEmail_CorrectoldEmail_IncorrectEmail1_IncorrectEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC015_UserSettingPage_EditEmail_CorrectoldEmail_IncorrectEmail1_IncorrectEmail2","User at Setting page edit Email address and provide correct old email id, incorrect new email, and Re-enter New Email ");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_CorrectoldEmail_IncorrectEmail1_IncorrectEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC015_UserSettingPage_EditEmail_CorrectoldEmail_IncorrectEmail1_IncorrectEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC016_UserSettingPage_EditEmail_CorrectoldEmail_CorrectEmail1_EmptyEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC016_UserSettingPage_EditEmail_CorrectoldEmail_CorrectEmail1_EmptyEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC016_UserSettingPage_EditEmail_CorrectoldEmail_CorrectEmail1_EmptyEmail2","User at Setting page edit Email address and provide correct old email id, correct new email, and Empty Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_CorrectoldEmail_CorrectEmail1_EmptyEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC016_UserSettingPage_EditEmail_CorrectoldEmail_CorrectEmail1_EmptyEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC017_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_CorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC017_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_CorrectEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC017_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_CorrectEmail2","User at Setting page edit Email address and provide correct old email id, empty new email, and correct Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_CorrectoldEmail_EmptyEmail1_correctEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC017_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_CorrectEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC018_UserSettingPage_EditEmail_CorrectoldEmail_MisMatchingEmail1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC018_UserSettingPage_EditEmail_CorrectoldEmail_MisMatchingEmail1AndEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC018_UserSettingPage_EditEmail_CorrectoldEmail_MisMatchingEmail1AndEmail2","User at Setting page edit Email address and provide correct old email id and mismatching new email, Re-enter New Email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_CorrectoldEmail_MisMatchingEmail1AndEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC018_UserSettingPage_EditEmail_CorrectoldEmail_MisMatchingEmail1AndEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC019_UserSettingPage_EditEmail_CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC019_UserSettingPage_EditEmail_CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC019_UserSettingPage_EditEmail_CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2","User at Setting page edit Email address and provide correct old email id and already registered new email, Re-enter New Email ");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC019_UserSettingPage_EditEmail_CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC020_UserSettingPage_EditEmail_Happy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC020_UserSettingPage_EditEmail_Happy_EndToEnd: Execution Started");
		  Exttest = Extreport.startTest("TC020_UserSettingPage_EditEmail_Happy_EndToEnd","User at Setting page edit Email address and provide correct old email id and already registered new email, Re-enter New Email ");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_EndToEnd(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, "NewEmail@test.com");
		  Genf.gfSleep(3);
		  StepNum = CSettingsMethods.EditEmail_EndToEnd(Extreport, Exttest, Driver, StepNum, ResultLocation, "NewEmail@test.com", EmailId);
		  System.out.println("TC020_UserSettingPage_EditEmail_Happy_EndToEnd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC065_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC065_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC065_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2","User at Setting page edit Email address and provide empty old , empty new email and Incorrect Re-enter email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC065_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC066_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC066_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC066_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2","User at Setting page edit Email address and provide empty old , empty new email and correct Re-enter email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC066_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC067_UserSettingPage_EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC067_UserSettingPage_EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC067_UserSettingPage_EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2","User at Setting page edit Email address and provide empty old , incorrect new email and empty Re-enter email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC067_UserSettingPage_EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC068_UserSettingPage_EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC068_UserSettingPage_EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2: Execution Started");
		  Exttest = Extreport.startTest("TC068_UserSettingPage_EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2","User at Setting page edit Email address and provide empty old , correct new email and empty Re-enter email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC068_UserSettingPage_EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC021_UserSettingPage_EditPassword_AllEmpty(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC021_UserSettingPage_EditPassword_AllEmpty: Execution Started");
		  Exttest = Extreport.startTest("TC021_UserSettingPage_EditPassword_AllEmpty","User at Setting page edit Account password and provide all empty and verify error messages");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_AllEmpty(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC021_UserSettingPage_EditPassword_AllEmpty : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC022_UserSettingPage_EditPassword_EmptyOld_ShortNew_EmptyReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC022_UserSettingPage_EditPassword_EmptyOld_ShortNew_EmptyReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC022_UserSettingPage_EditPassword_EmptyOld_ShortNew_EmptyReEnter","User at Setting page edit Account password and provide empty old password,short New Password and empty Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_EmptyOld_ShortNew_EmptyRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC022_UserSettingPage_EditPassword_EmptyOld_ShortNew_EmptyReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC023_UserSettingPage_EditPassword_EmptyOld_EmptyNew_ShortReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC023_UserSettingPage_EditPassword_EmptyOld_EmptyNew_ShortReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC023_UserSettingPage_EditPassword_EmptyOld_EmptyNew_ShortReEnter","User at Setting page edit Account password and provide empty old password,empty New Password and short Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_EmptyOdl_EmptyNew_ShortRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC023_UserSettingPage_EditPassword_EmptyOld_EmptyNew_ShortReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC024_UserSettingPage_EditPassword_EmptyOld_ShortNew_ShortReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC024_UserSettingPage_EditPassword_EmptyOld_ShortNew_ShortReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC024_UserSettingPage_EditPassword_EmptyOld_ShortNew_ShortReEnter","User at Setting page edit Account password and provide empty old password,short New Password and short Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_EmptyOdl_ShortNew_ShortRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC024_UserSettingPage_EditPassword_EmptyOld_ShortNew_ShortReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC025_UserSettingPage_EditPassword_EmptyOld_MismatchinNewAndReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC025_UserSettingPage_EditPassword_EmptyOld_MismatchinNewAndReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC025_UserSettingPage_EditPassword_EmptyOld_MismatchinNewAndReEnter","User at Setting page edit Account password and provide empty old password,mismatching New Password and Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_EmptyOdl_MismatchingNewandRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC025_UserSettingPage_EditPassword_EmptyOld_MismatchinNewAndReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC026_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_EmptyReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC026_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_EmptyReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC026_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_EmptyReEnter","User at Setting page edit Account password and provide incorrect old password,empty New Password and empty Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_IncorrectOdl_EmptyNew_EmptyRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC026_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_EmptyReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC027_UserSettingPage_EditPassword_IncorrectOld_ShortNew_EmptyReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC027_UserSettingPage_EditPassword_IncorrectOld_ShortNew_EmptyReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC027_UserSettingPage_EditPassword_IncorrectOld_ShortNew_EmptyReEnter","User at Setting page edit Account password and provide incorrect old password,short New Password and empty Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_IncorrectOdl_ShortNew_EmptyRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC027_UserSettingPage_EditPassword_IncorrectOld_ShortNew_EmptyReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC028_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_ShortReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC028_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_ShortReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC028_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_ShortReEnter","User at Setting page edit Account password and provide incorrect old password,empty New Password and short Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_IncorrectOdl_EmptyNew_ShortRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC028_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_ShortReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC029_UserSettingPage_EditPassword_IncorrectOld_ShortNew_ShortReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC029_UserSettingPage_EditPassword_IncorrectOld_ShortNew_ShortReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC029_UserSettingPage_EditPassword_IncorrectOld_ShortNew_ShortReEnter","User at Setting page edit Account password and provide incorrect old password,short New Password and short Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_IncorrectOdl_ShortNew_ShortRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC029_UserSettingPage_EditPassword_IncorrectOld_ShortNew_ShortReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC030_UserSettingPage_EditPassword_IncorrectOld_MismatchingNewAndReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC030_UserSettingPage_EditPassword_IncorrectOld_MismatchingNewAndReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC030_UserSettingPage_EditPassword_IncorrectOld_MismatchingNewAndReEnter","User at Setting page edit Account password and provide incorrect old password,mismatching New Password and Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_IncorrectOdl_MismatchingNewAndRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC030_UserSettingPage_EditPassword_IncorrectOld_MismatchingNewAndReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC031_UserSettingPage_EditPassword_CorrectOld_EmptyNew_EmptyReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC031_UserSettingPage_EditPassword_CorrectOld_EmptyNew_EmptyReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC031_UserSettingPage_EditPassword_CorrectOld_EmptyNew_EmptyReEnter","User at Setting page edit Account password and provide correct old password,empty New Password and empty Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_CorrectOdl_EmptyNew_EmptyRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC031_UserSettingPage_EditPassword_CorrectOld_EmptyNew_EmptyReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC032_UserSettingPage_EditPassword_CorrectOld_ShortNew_EmptyReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC032_UserSettingPage_EditPassword_CorrectOld_ShortNew_EmptyReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC032_UserSettingPage_EditPassword_CorrectOld_ShortNew_EmptyReEnter","User at Setting page edit Account password and provide correct old password,short New Password and empty Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_IncorrectOdl_ShortNew_EmptyRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC032_UserSettingPage_EditPassword_CorrectOld_ShortNew_EmptyReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC033_UserSettingPage_EditPassword_CorrectOld_EmptyNew_ShortReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC033_UserSettingPage_EditPassword_CorrectOld_EmptyNew_ShortReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC033_UserSettingPage_EditPassword_CorrectOld_EmptyNew_ShortReEnter","User at Setting page edit Account password and provide correct old password,empty New Password and short Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_CorrectOdl_EmptyNew_ShortRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC033_UserSettingPage_EditPassword_CorrectOld_EmptyNew_ShortReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC034_UserSettingPage_EditPassword_CorrectOld_ShortNew_ShortReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC034_UserSettingPage_EditPassword_CorrectOld_ShortNew_ShortReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC034_UserSettingPage_EditPassword_CorrectOld_ShortNew_ShortReEnter","User at Setting page edit Account password and provide correct old password,short New Password and short Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_CorrectOdl_ShortNew_ShortRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC034_UserSettingPage_EditPassword_CorrectOld_ShortNew_ShortReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC035_UserSettingPage_EditPassword_CorrectOld_MismatchingNewAndReEnter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC035_UserSettingPage_EditPassword_CorrectOld_MismatchingNewAndReEnter: Execution Started");
		  Exttest = Extreport.startTest("TC035_UserSettingPage_EditPassword_CorrectOld_MismatchingNewAndReEnter","User at Setting page edit Account password and provide correct old password,mismatching New Password and Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_CorrectOdl_MismatchingNewAndRetype(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC035_UserSettingPage_EditPassword_CorrectOld_MismatchingNewAndReEnter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC036_UserSettingPage_EditPassword_CorrectOld_NewAndReEnter_SameAsOLD(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC036_UserSettingPage_EditPassword_CorrectOld_NewAndReEnter_SameAsOLD: Execution Started");
		  Exttest = Extreport.startTest("TC036_UserSettingPage_EditPassword_CorrectOld_NewAndReEnter_SameAsOLD","User at Setting page edit Account password and provide correct old password,same old password for  New Password and Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_CorrectOdl_NewAndRetype_SameAsOLD(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC036_UserSettingPage_EditPassword_CorrectOld_NewAndReEnter_SameAsOLD : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC037_UserSettingPage_EditPassword_Happy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC037_UserSettingPage_EditPassword_Happy_EndToEnd: Execution Started");
		  Exttest = Extreport.startTest("TC037_UserSettingPage_EditPassword_Happy_EndToEnd","User at Setting page edit Account password and provide correct old password,mismatching New Password and Re-enter New Password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_EndToEnd(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password, "Newpassword");
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CSettingsMethods.afEditPassword_EndToEnd(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, "Newpassword",Password );
		  System.out.println("TC037_UserSettingPage_EditPassword_Happy_EndToEnd : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC038_UserSettingPage_GoTo_Home_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC038_UserSettingPage_GoTo_Home_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC038_UserSettingPage_GoTo_Home_FromHeader","User goes to the Home page by clicking STARZ PLAY logo(from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC038_UserSettingPage_GoTo_Home_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC039_UserSettingPage_GoTo_Movies_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC039_UserSettingPage_GoTo_Movies_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC039_UserSettingPage_GoTo_Movies_FromHeader","User goes to the Movies page by clicking Movies(from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyMoviesPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC039_UserSettingPage_GoTo_Movies_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC040_UserSettingPage_GoTo_Series_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC040_UserSettingPage_GoTo_Series_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC040_UserSettingPage_GoTo_Series_FromHeader","User goes to the Series page by clicking Series(from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifySeriesPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC040_UserSettingPage_GoTo_Series_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC041_UserSettingPage_GoTo_Arabic_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC041_UserSettingPage_GoTo_Arabic_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC041_UserSettingPage_GoTo_Arabic_FromHeader","User goes to the Arabic page by clicking Arabic(from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyArabicPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC041_UserSettingPage_GoTo_Arabic_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC042_UserSettingPage_GoTo_Kids_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC042_UserSettingPage_GoTo_Kids_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC042_UserSettingPage_GoTo_Kids_FromHeader","User goes to the Kids Page by clicking Kids(from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyKidsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC042_UserSettingPage_GoTo_Kids_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC043_UserSettingPage_OpensSearchBar_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC043_UserSettingPage_OpensSearchBar_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC043_UserSettingPage_OpensSearchBar_FromHeader","User opens the Search bar");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifySearchFields(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC043_UserSettingPage_OpensSearchBar_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC044_UserSettingPage_OpensSettings_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC044_UserSettingPage_OpensSettings_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC044_UserSettingPage_OpensSettings_FromHeader","User opens the Settings options");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifySettingsFields(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC044_UserSettingPage_OpensSettings_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC045_UserSettingPage_GoTo_Settings_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC045_UserSettingPage_GoTo_Settings_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC045_UserSettingPage_GoTo_Settings_FromHeader","User goes to the Settings page");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC045_UserSettingPage_GoTo_Settings_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC046_UserSettingPage_GoTo_MyLibrary_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC046_UserSettingPage_GoTo_MyLibrary_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC046_UserSettingPage_GoTo_MyLibrary_FromHeader","User goes to the My Libray page");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyMyLibraryPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC046_UserSettingPage_GoTo_MyLibrary_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC047_UserSettingPage_LanguageTo_Arabic_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC047_UserSettingPage_LanguageTo_Arabic_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC047_UserSettingPage_LanguageTo_Arabic_FromHeader","User changes language to Arabic(from the Settings drop down options in the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afChangeLanguageToArabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC047_UserSettingPage_LanguageTo_Arabic_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC048_UserSettingPage_LanguageTo_French_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC048_UserSettingPage_LanguageTo_French_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC048_UserSettingPage_LanguageTo_French_FromHeader","User changes language to French(from the Settings drop down options in the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC048_UserSettingPage_LanguageTo_French_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC049_UserSettingPage_Logout_FromHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC049_UserSettingPage_Logout_FromHeader: Execution Started");
		  Exttest = Extreport.startTest("TC049_UserSettingPage_Logout_FromHeader","User logs out from STARZ PLAY from settings page");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afLogout(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC049_UserSettingPage_Logout_FromHeader : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC050_UserSettingPage_GoTo_WhyStarzPlay_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		CSettingsMethods = new CommonSettingsMethods();
		int StepNum=1;
		System.out.println("TC050_UserSettingPage_GoTo_WhyStarzPlay_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC050_UserSettingPage_GoTo_WhyStarzPlay_FromFooter","User goes to the Why STARZ PLAY? page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyWhyStarzPlayFromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC050_UserSettingPage_GoTo_WhyStarzPlay_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC051_UserSettingPage_GoTo_HelpCenter_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC051_UserSettingPage_GoTo_HelpCenter_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC051_UserSettingPage_GoTo_HelpCenter_FromFooter","User goes to the Help Center page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyHelpCenter_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC051_UserSettingPage_GoTo_HelpCenter_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC052_UserSettingPage_GoTo_PartnerWithUs_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC052_UserSettingPage_GoTo_PartnerWithUs_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC052_UserSettingPage_GoTo_PartnerWithUs_FromFooter","User goes to the Partner with us page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyPartnerWithUs_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC052_UserSettingPage_GoTo_PartnerWithUs_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC053_UserSettingPage_GoTo_CompanyPage_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC053_UserSettingPage_GoTo_CompanyPage_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC053_UserSettingPage_GoTo_CompanyPage_FromFooter","User goes to the Company page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyCompanyPage_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC053_UserSettingPage_GoTo_CompanyPage_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC054_UserSettingPage_GoTo_TermAndConditions_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC054_UserSettingPage_GoTo_TermAndConditions_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC054_UserSettingPage_GoTo_TermAndConditions_FromFooter","User goes to the Terms & Conditions page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, "HtmlUnit", AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyTermsAndAconditions_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC054_UserSettingPage_GoTo_TermAndConditions_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC055_UserSettingPage_GoTo_PrivacyPolicy_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC055_UserSettingPage_GoTo_PrivacyPolicy_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC055_UserSettingPage_GoTo_PrivacyPolicy_FromFooter","User goes to the Privacy Policy page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, "HtmlUnit", AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyPrivacyPolicy_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC055_UserSettingPage_GoTo_PrivacyPolicy_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC056_UserSettingPage_GoTo_Blog_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC056_UserSettingPage_GoTo_Blog_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC056_UserSettingPage_GoTo_Blog_FromFooter","User goes to the Blog page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyBlog_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC056_UserSettingPage_GoTo_Blog_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC057_UserSettingPage_GoTo_Careers_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC057_UserSettingPage_GoTo_Careers_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC057_UserSettingPage_GoTo_Careers_FromFooter","User goes to the Careers page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyCareers_Footer(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC057_UserSettingPage_GoTo_Careers_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC058_UserSettingPage_GoTo_Facebook_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC058_UserSettingPage_GoTo_Facebook_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC058_UserSettingPage_GoTo_Facebook_FromFooter","User goes to the STARZ PLAY Facebook page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afverifyFaceBook_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC058_UserSettingPage_GoTo_Facebook_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC059_UserSettingPage_GoTo_Twitter_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC059_UserSettingPage_GoTo_Twitter_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC059_UserSettingPage_GoTo_Twitter_FromFooter","User goes to the STARZ PLAY Twitter page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyTwitter_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC059_UserSettingPage_GoTo_Twitter_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC060_UserSettingPage_GoTo_Instagram_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC060_UserSettingPage_GoTo_Instagram_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC060_UserSettingPage_GoTo_Instagram_FromFooter","User goes to the STARZ PLAY Instagram page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyInstagram_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC060_UserSettingPage_GoTo_Instagram_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC061_UserSettingPage_GoTo_YouTube_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC061_UserSettingPage_GoTo_YouTube_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC061_UserSettingPage_GoTo_YouTube_FromFooter","User goes to the STARZ PLAY Youtube page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyYouTube_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC061_UserSettingPage_GoTo_YouTube_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	public void TC062_UserSettingPage_GoTo_Appstore_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC062_UserSettingPage_GoTo_Appstore_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC062_UserSettingPage_GoTo_Appstore_FromFooter","User goes to the STARZ PLAY Appstore page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyAppStore_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC062_UserSettingPage_GoTo_Appstore_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC063_UserSettingPage_GoTo_PlayStore_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC063_UserSettingPage_GoTo_PlayStore_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC063_UserSettingPage_GoTo_PlayStore_FromFooter","User goes to the STARZ PLAY Google Playstore page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyAppStore_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC063_UserSettingPage_GoTo_PlayStore_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC064_UserSettingPage_GoTo_MuleSoft_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CLoginMethods = new CommonLoginMethods();
		CSettingsMethods = new CommonSettingsMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum=1;
		System.out.println("TC064_UserSettingPage_GoTo_MuleSoft_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC064_UserSettingPage_GoTo_MuleSoft_FromFooter","User goes to the MuleSoft page(from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = CLoginMethods.afVerifyLoginPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd( Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afNavigateToSettingsPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CFMethods.afVerifyMuleSoft_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC064_UserSettingPage_GoTo_MuleSoft_FromFooter : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
}
