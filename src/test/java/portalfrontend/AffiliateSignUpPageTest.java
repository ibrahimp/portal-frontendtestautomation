package portalfrontend;

import java.io.File;
import java.net.MalformedURLException;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class AffiliateSignUpPageTest {
	
	public ExtentReports Extreport;
	public ExtentTest Exttest;
	public WebDriver Driver;
	public HtmlUnitDriver Hdriver; 
	public String ResultLocation;
	public FrameWorkUtility Futil;
	public GeneralMethods Genf;
	public DesiredCapabilities Cap;
	public AffiliateSignUpPageMethods ASPMethods;
	public File PhantomJsSrc;
	
	@Parameters({"Browser"})
	@BeforeTest(alwaysRun = true)
	 public void startTest(String BrowserName,final ITestContext testContext) {
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		ASPMethods = new AffiliateSignUpPageMethods();
	    ResultLocation = System.getProperty("user.dir")+"/Custom_Report/"+testContext.getName();
	    Extreport = new ExtentReports(ResultLocation+"/"+testContext.getName()+"-"+Futil.fGetRandomNumUsingTime()+".html", true);
	      Cap = DesiredCapabilities.chrome();
		  Cap.setBrowserName(BrowserName);
		  Cap.setCapability("ignoreZoomSetting", true);
		  Cap.setPlatform(Platform.MAC);
		  Cap.setCapability("nativeEvents","false");
		  PhantomJsSrc = new File(System.getProperty("user.dir")+"/Resources/phantomjs/bin/phantomjs");
	}
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Header"})
	  public void TC001_GuestAffiliateSignUp_GoToHome(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC001_GuestAffiliateSignUp_GoToHome(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Header"})
	  public void TC002_GuestAffiliateSignUp_GotoAboutUs(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC002_GuestAffiliateSignUp_GotoAboutUs(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Header"})
	  public void TC003_GuestAffiliateSignUp_GotoContactUs(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC003_GuestAffiliateSignUp_GotoContactUs(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Header"})
	  public void TC004_GuestAffiliateSignUp_GotoLogin(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC004_GuestAffiliateSignUp_GotoLogin(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation"})
	  public void TC005_GuestAffiliateSignUp_GotoLoginFromSighnUp(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC005_GuestAffiliateSignUp_GotoLoginFromSighnUp(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Header"})
	  public void TC006_GuestAffiliateSignUp_LanguageToArabic(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC006_GuestAffiliateSignUp_LanguageToArabic(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Header"})
	  public void TC007_GuestAffiliateSignUp_LanguageToFrench(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC007_GuestAffiliateSignUp_LanguageToFrench(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Header"})
	  public void TC008_GuestAffiliateSignUp_WhyStarzPlay_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC008_GuestAffiliateSignUp_WhyStarzPlay_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"Guest","Navigation","Footer"})
	  public void TC009_GuestAffiliateSignUp_HelpCenter_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC009_GuestAffiliateSignUp_HelpCenter_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC010_GuestAffiliateSignUp_PartnerWithUS_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC010_GuestAffiliateSignUp_PartnerWithUS_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }

	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC011_GuestAffiliateSignUp_Company_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC011_GuestAffiliateSignUp_Company_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC012_GuestAffiliateSignUp_TermsAndCond_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC012_GuestAffiliateSignUp_TermsAndCond_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC013_GuestAffiliateSignUp_PrivacyPolicy_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC013_GuestAffiliateSignUp_PrivacyPolicy_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC014_GuestAffiliateSignUp_Blog_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC014_GuestAffiliateSignUp_Blog_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC015_GuestAffiliateSignUp_Careers_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC015_GuestAffiliateSignUp_Careers_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC016_GuestAffiliateSignUp_Facebook_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC016_GuestAffiliateSignUp_Facebook_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC017_GuestAffiliateSignUp_Twitter_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC017_GuestAffiliateSignUp_Twitter_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC018_GuestAffiliateSignUp_Instagram_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC018_GuestAffiliateSignUp_Instagram_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC019_GuestAffiliateSignUp_Youtube_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC019_GuestAffiliateSignUp_Youtube_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC020_GuestAffiliateSignUp_Appstore_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC020_GuestAffiliateSignUp_Appstore_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC021_GuestAffiliateSignUp_PlayStore_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC021_GuestAffiliateSignUp_PlayStore_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC022_GuestAffiliateSignUp_NoUserName_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC022_GuestAffiliateSignUp_NoUserName_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC023_GuestAffiliateSignUp_WrongEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC023_GuestAffiliateSignUp_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC024_GuestAffiliateSignUp_WrongMSISDNFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC024_GuestAffiliateSignUp_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC025_GuestAffiliateSignUp_CorrectEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC025_GuestAffiliateSignUp_CorrectEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC026_GuestAffiliateSignUp_CorrectMSISDNFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC026_GuestAffiliateSignUp_CorrectMSISDNFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC027_GuestAffiliateSignUp_NoUserName_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC027_GuestAffiliateSignUp_NoUserName_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC028_GuestAffiliateSignUp_WrongEmailFormat_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC028_GuestAffiliateSignUp_WrongEmailFormat_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC029_GuestAffiliateSignUp_WrongMSISDNFormat_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC029_GuestAffiliateSignUp_WrongMSISDNFormat_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC030_GuestAffiliateSignUp_CorrectEmailFormat_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC030_GuestAffiliateSignUp_CorrectEmailFormat_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC031_GuestAffiliateSignUp_CorrectMSISDNFormat_ShortPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC031_GuestAffiliateSignUp_CorrectMSISDNFormat_ShortPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC032_GuestAffiliateSignUp_AlreadyRegisterd_Email(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC032_GuestAffiliateSignUp_AlreadyRegisterd_Email(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC033_GuestAffiliateSignUp_AlreadyRegisterd_MSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC033_GuestAffiliateSignUp_AlreadyRegisterd_MSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC034_GuestAffiliateSignUp_Facebook_Cancel(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC034_GuestAffiliateSignUp_Facebook_Cancel(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","SignUp","UnHappy"})
	  public void TC035_GuestAffiliateSignUp_Facebook_NotApprove(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC035_GuestAffiliateSignUp_Facebook_NotApprove(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","EndToEnd","CreditCard"})
	  public void TC036_043_Guest_GuestAffiliateSignUp_With_Email_CreditCard(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC036_043_GuestAffiliateSignUp_With_Email_CreditCard(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL","StarzPlayVoucherCode"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","EndToEnd","Voucher"})
	  public void TC044_051_GuestAffiliateSignUp_Email_Voucher(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL,String StarzPlayVoucherCode) throws MalformedURLException 
	  {
		 ASPMethods.TC044_051_GuestAffiliateSignUp_Email_Voucher(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap,StarzPlayVoucherCode);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"}) 
	  @Test(enabled=false,groups={"User","Navigation"})
	  public void TC052_UserAffiliateSignUp_To_Login(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC052_UserAffiliateSignUp_To_Login(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC053_UserAffiliateSignUp_LoginWith_NoUserName_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC053_UserAffiliateSignUp_LoginWith_NoUserName_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC054_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC054_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC055_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd_DeleteEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC055_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd_DeleteEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC056_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC056_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC057_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd_DeleteMSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC057_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd_DeleteMSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC058_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC058_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC059_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd_DeleteEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC059_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd_DeleteEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC060_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC060_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC061_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd_DeleteMSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC061_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd_DeleteMSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC062_UserAffiliateSignUp_LoginWith_NoUserName_AnyPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC062_UserAffiliateSignUp_LoginWith_NoUserName_AnyPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC063_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC063_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC064_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd_DeleteEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC064_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd_DeleteEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC065_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC065_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC066_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd_DeleteMSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC066_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd_DeleteMSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC067_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC067_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC068_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC068_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC069_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd_DeleteBoth(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC069_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd_DeleteBoth(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC070_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd_DeleteBoth(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC070_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd_DeleteBoth(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Login","UnHappy"})
	  public void TC071_UserAffiliateSignUp_LoginWith_Facebook_Cancel(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC071_UserAffiliateSignUp_LoginWith_Facebook_Cancel(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Email","EndToEnd"})
	  public void TC072_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_Active(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC072_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_Active(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL","ValidMSISDN"})
	  @Test(enabled=false,groups="User")
	  public void TC073_UserAffiliateSignUp_LoginWith_ValidMSISDNAndPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL,String ValidMSISDN) throws MalformedURLException 
	  {
		 ASPMethods.TC073_UserAffiliateSignUp_LoginWith_ValidMSISDNAndPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap,ValidMSISDN); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd"})
	  public void TC074_UserAffiliateSignUp_LoginWith_Facebook_ValidCred_Active(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC074_UserAffiliateSignUp_LoginWith_Facebook_ValidCred_Active(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC075_UserAffiliateSignUp_RedirectToSignUPPage(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC075_UserAffiliateSignUp_RedirectToSignUPPage(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC076_UserAffiliateSignUp_ToLoginPage_FromSignUpBox(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC076_UserAffiliateSignUp_ToLoginPage_FromSignUpBox(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC077_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC077_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC078_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC078_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC079_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd_DeleteEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC079_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd_DeleteEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC080_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC080_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC081_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd_DeleteMSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC081_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd_DeleteMSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC082_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC082_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC083_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd_DeleteEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC083_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd_DeleteEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC084_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC084_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC085_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd_DeleteMSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC085_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd_DeleteMSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC086_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_AnyPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC086_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_AnyPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC087_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC087_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC088_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd_DeleteEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC088_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd_DeleteEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC089_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC089_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC090_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd_DeleteMSISDN(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC090_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd_DeleteMSISDN(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC091_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC091_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC092_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC092_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC093_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd_DeleteBoth(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC093_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd_DeleteBoth(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC094_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd_DeleteBoth(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC094_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd_DeleteBoth(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC095_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_Cancel(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC095_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_Cancel(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups="User")
	  public void TC096_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_Active(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC096_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_Active(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL","ValidMSISDN"})
	  @Test(enabled=false,groups="User")
	  public void TC097_UserAffiliateSignUp_LoginFromSignUpBox_ValidMSISDNAndPwd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL,String ValidMSISDN) throws MalformedURLException 
	  {
		 //ASPMethods.TC097_UserAffiliateSignUp_LoginFromSignUpBox_ValidMSISDNAndPwd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap,ValidMSISDN); 
	  }
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Facebook","ActiveUser"})
	  public void TC098_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_ValidCred_Active(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC098_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_ValidCred_Active(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Facebook"})
	  public void TC099_GuestAffiliateSignUp_Facebook_NotApprove_AccessToEmail(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC099_GuestAffiliateSignUp_Facebook_NotApprove_AccessToEmail(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","ProspectUser","EndToEnd","Email"})
	  public void TC100_TC102_SignUP_ProspectUser_Email_EndToEnd_Happy(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC100_TC102_SignUP_ProspectUser_Email_EndToEnd_Happy(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","ProspectUser","EndToEnd","Facebook"})
	  public void TC103_TC105_SignUP_ProspectUser_Facebook_EndToEnd_Happy(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC103_TC105_SignUP_ProspectUser_Facebook_EndToEnd_Happy(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Email","EndToEnd","CreditCard"})
	  public void TC106_TC123_SignUP_Email_CreditCard_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC106_TC123_SignUP_Email_CreditCard_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Email","EndToEnd","Voucher"})
	  public void TC124_TC128_SignUP_Email_Voucher_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC124_TC128_SignUP_Email_Voucher_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Facebook","EndToEnd","CreditCard"})
	  public void TC129_TC146_SignUP_Facebook_CreditCard_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC129_TC146_SignUP_Facebook_CreditCard_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Facebook","EndToEnd","CreditCard"})
	  public void TC147_TC154_SignUp_Facebook_CreditCard_Happy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC147_TC154_SignUp_Facebook_CreditCard_Happy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Facebook","EndToEnd","Voucher"})
	  public void TC155_TC159_SignUP_Facebook_Voucher_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC155_TC159_SignUP_Facebook_Voucher_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL","StarzPlayVoucherCode"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Facebook","EndToEnd","Voucher"})
	  public void TC160_TC167_SignUp_Facebook_Voucher_Happy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL,String StarzPlayVoucherCode) throws MalformedURLException 
	  {
		 ASPMethods.TC160_TC167_SignUp_Facebook_Voucher_Happy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap,StarzPlayVoucherCode);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","UnHappy","Login","Facebook","EndToEnd"})
	  public void TC168_UserAffiliateSignUp_LoginWith_Facebook_UnRegisteredAccount(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC168_UserAffiliateSignUp_LoginWith_Facebook_UnRegisteredAccount(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Email","EndToEnd"})
	  public void TC169_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_ProspectUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC169_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_ProspectUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd"})
	  public void TC170_UserAffiliateSignUp_LoginWith_Facebook_ProspectUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC170_UserAffiliateSignUp_LoginWith_Facebook_ProspectUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Email","EndToEnd"})
	  public void TC171_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DisconnectedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC171_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DisconnectedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd"})
	  public void TC172_UserAffiliateSignUp_LoginWith_Facebook_DisconnectedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC172_UserAffiliateSignUp_LoginWith_Facebook_DisconnectedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Email","EndToEnd"})
	  public void TC173_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DeactivatedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC173_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DeactivatedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd"})
	  public void TC174_UserAffiliateSignUp_LoginWith_Facebook_DeactivatedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC174_UserAffiliateSignUp_LoginWith_Facebook_DeactivatedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd"})
	  public void TC175_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_UnRegisteredAccount(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC175_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_UnRegisteredAccount(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Email","EndToEnd","ProspectUser"})
	  public void TC176_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_ProspectUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC176_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_ProspectUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd","ProspectUser"})
	  public void TC177_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_ProspectUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC177_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_ProspectUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Email","EndToEnd","DisconnectedUser"})
	  public void TC178_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_DisconnectedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC178_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_DisconnectedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd","DisconnectedUser"})
	  public void TC179_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_DisconnectedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC179_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_DisconnectedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Email","EndToEnd","DeactivatedUser"})
	  public void TC180_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_DeactivatedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC180_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_DeactivatedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"User","Happy","Login","Facebook","EndToEnd","DeactivatedUser"})
	  public void TC181_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_DeactivatedUser(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 //ASPMethods.TC181_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_DeactivatedUser(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Du","EndToEnd"})
	  public void TC182_190_GuestAffiliateSignUp_WithEmail_MoP_Du_Weekly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC182_190_GuestAffiliateSignUp_WithEmail_MoP_Du_Weekly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Du","EndToEnd"})
	  public void TC191_199_GuestAffiliateSignUp_WithEmail_MoP_Du_Monthly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC191_199_GuestAffiliateSignUp_WithEmail_MoP_Du_Monthly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation","Footer"})
	  public void TC200_GuestAffiliateSignUp_MuleSoft_FromFooter(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC200_GuestAffiliateSignUp_MuleSoft_FromFooter(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Email","EndToEnd","Du"})
	  public void TC201_TC220_SignUP_Email_DU_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC201_TC219_SignUP_Email_DU_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Etisalat","EndToEnd"})
	  public void TC220_228_GuestAffiliateSignUp_WithEmail_MoP_Etisalat_Daily_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC220_228_GuestAffiliateSignUp_WithEmail_MoP_Etisalat_Daily_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Etisalat","EndToEnd"})
	  public void TC229_237_GuestAffiliateSignUp_WithEmail_MoP_Etisalat_Weekly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC229_237_GuestAffiliateSignUp_WithEmail_MoP_Etisalat_Weekly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Happy","SignUp","Email","Etisalat","EndToEnd"})
	  public void TC238_246_GuestAffiliateSignUp_WithEmail_MoP_Etisalat_Monthly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC238_246_GuestAffiliateSignUp_WithEmail_MoP_Etisalat_Monthly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","UnHappy","SignUp","Email","EndToEnd","Etisalat"})
	  public void TC247_TC267_SignUP_Email_Etisalat_UnHappy_EndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC247_TC267_SignUP_Email_Etisalat_UnHappy_EndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation"})
	  public void TC268_GuestAffiliateSignUp_GoToSignUP_FromGreatEntertainment(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC268_GuestAffiliateSignUp_GoToSignUP_FromGreatEntertainment(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=false,groups={"Guest","Navigation"})
	  public void TC269_GuestAffiliateSignUp_GoToSignUP_FromKidsSection(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC269_GuestAffiliateSignUp_GoToSignUP_FromKidsSection(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"Guest","Happy","SignUp","Email","MarocTelecom","EndToEnd"})
	  public void TC270_278_GuestAffiliateSignUp_WithEmail_MoP_MarocTelecom_Weekly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC270_278_GuestAffiliateSignUp_WithEmail_MoP_MarocTelecom_Weekly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"Guest","Happy","SignUp","Email","MarocTelecom","EndToEnd"})
	  public void TC279_287_GuestAffiliateSignUp_WithEmail_MoP_MarocTelecom_Monthly_HappyEndToEnd(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.TC279_287_GuestAffiliateSignUp_WithEmail_MoP_MarocTelecom_Monthly_HappyEndToEnd(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"Database"})
	  public void DataBaseTest(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.DataBaseTest(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
	 @Parameters({"ApplnURL","AppTitle","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"gigyaDeletion"})
	  public void GuigyaTrace(String AppURL,String AppTitle,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		 ASPMethods.GuigyaTrace(Extreport, Exttest, Driver, AppURL, AppTitle, ResultLocation,Browser,TestType,NodeURL,Cap);
	  }
	 
}
