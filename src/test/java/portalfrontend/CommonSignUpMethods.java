package portalfrontend;

import java.net.MalformedURLException;
import java.util.ArrayList;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CommonSignUpMethods {
	
	public GeneralMethods Genf;
	public FrameWorkUtility Futil;
	
	public int afSignUpWith_NoUsrName_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSubmitLogin!=null){
				  btnSubmitLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button Without UserName And Password", "Successfully Clicked on the button SIGN UP", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP Without UserName And Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button Without UserName And Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email-error']");
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Email Address", "This field is required.");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "This field is required.");
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for Empty Email Id and Password While Sign Up", "Error message verification failed for Empty email id and password", "No");
			  System.out.println("Error message verification failed for Empty email id and password");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_WrongEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String EmailId = "sdhhhdbbbcggggdhh.dd";
				  EdtUserName.sendKeys(EmailId);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Wrong Email Format", "Successfully Entered The Wrong Email Format :"+EmailId, "No");
				  System.out.println("Successfully Entered The Wrong Email Format :"+EmailId);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Wrong Email Format", "Failed To Enter The Wrong Email Format", "Yes");   
				  System.out.println("Failed To Enter The Wrong Email Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Wrong Email Format");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSubmitLogin!=null){
				  btnSubmitLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with wrong email format and no Password", "Successfully Clicked on the button SIGN UP With Wrong Email Format And No Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP With Wrong Email Format And No Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with wrong email format and no Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email-error']");
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Email Address", "Please enter a valid email address e.g. XXX.XXX@gmail.com");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "This field is required.");
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for wrong Email format and empty Password While Sign Up", "Error message verification failed for wrong format email id and empty password", "No");
			  System.out.println("Error message verification failed for wrong format email id and empty password");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_WrongMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String MSISDN = "23233232.43545456";
				  EdtUserName.sendKeys(MSISDN);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Wrong MSISDN Format", "Successfully Entered The Wrong MSISDN Format :"+MSISDN, "No");
				  System.out.println("Successfully Entered The Wrong MSISDN Format :"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Wrong MSISDN Format", "Failed To Enter The Wrong MSISDN Format", "Yes");   
				  System.out.println("Failed To Enter The Wrong MSISDN Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Wrong MSISDN Format");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSubmitLogin!=null){
				  btnSubmitLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with wrong MSISDN format and no Password", "Successfully Clicked on the button SIGN UP With Wrong MSISDN Format And No Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP With Wrong MSISDN Format And No Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with wrong MSISDN format and no Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email-error']");
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Mobile Number	", "Please enter your mobile number in the correct format, e.g. 971XXXXXXXXX.");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "This field is required.");
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for wrong MSISDN format and empty Password While Sign Up", "Error message verification failed for wrong format MSISDN id and empty password", "No");
			  System.out.println("Error message verification failed for wrong format MSISDN id and empty password");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_ValidEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String email = "starzTest@test.com";
				  EdtUserName.sendKeys(email);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Valid email Format", "Successfully Entered The valid email Format :"+email, "No");
				  System.out.println("Successfully Entered The valid email Format :"+email);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter valid email Format", "Failed To Enter The valid email Format", "Yes");   
				  System.out.println("Failed To Enter The valid Email Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The valid email Format");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with valid email format and no Password", "Successfully Clicked on the button SIGN UP With valid email Format And No Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP With valid email Format And No Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with valid email format and no Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			  
	
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "This field is required.");
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for valid email format and empty Password While Sign Up", "Error message verification failed for valid email format and empty password", "No");
			  System.out.println("Error message verification failed for valid email format and empty password");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_ValidMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String MSISDN = "971581111111";
				  EdtUserName.sendKeys(MSISDN);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Valid MSISDN Format", "Successfully Entered The valid MSISDN Format :"+MSISDN, "No");
				  System.out.println("Successfully Entered The valid MSISDN Format :"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter valid MSISDN Format", "Failed To Enter The valid MSISDN Format", "Yes");   
				  System.out.println("Failed To Enter The valid MSISDN Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The valid MSISDN Format");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with valid MSISDN format and no Password", "Successfully Clicked on the button SIGN UP With valid MSISDN Format And No Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP With valid MSISDN Format And No Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with valid MSISDN format and no Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			 
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "This field is required.");
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for valid MSISDN format and empty Password While Sign Up", "Error message verification failed for valid MSISDN format and empty password", "No");
			  System.out.println("Error message verification failed for valid MSISDN format and empty password");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_NoUserName_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			  
			WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The short Password", "Successfully Entered The short Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :"+Password);  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The short Password", "Failed To Enter short Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with empty username and short Password", "Successfully Clicked on the button SIGN UP with empty username and short Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP with empty username and short Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with empty username and short Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			 
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email-error']");
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Mobile Number	", "This field is required.");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "Please enter at least 6 characters.");
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for empty user name and short Password While Sign Up", "Error message verification failed for empty user name and short password while sign up", "No");
			  System.out.println("Error message verification failed for empty user name and short password while sign up");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_WrongEmailFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String EmailId = "sdhhhdbbbcggggdhh.dd";
				  EdtUserName.sendKeys(EmailId);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Wrong Email Format", "Successfully Entered The Wrong Email Format :"+EmailId, "No");
				  System.out.println("Successfully Entered The Wrong Email Format :"+EmailId);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Wrong Email Format", "Failed To Enter The Wrong Email Format", "Yes");   
				  System.out.println("Failed To Enter The Wrong Email Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Wrong Email Format");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  
			WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The short Password", "Successfully Entered The short Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :"+Password);  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The short Password", "Failed To Enter short Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with wrong email format and short Password", "Successfully Clicked on the button SIGN UP with wrong email format and short Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP with wrong email format and short Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with wrong email format and short Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			 
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email-error']");
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Email Address", "Please enter a valid email address e.g. XXX.XXX@gmail.com");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "Please enter at least 6 characters.");
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for wrong format email id and short Password While Sign Up", "Error message verification failed for wrong format email id and short password while sign up", "No");
			  System.out.println("Error message verification failed for wrong format email id and short password while sign up");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_WrongMSISDNFFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String MSISDN = "23233232.43545456";
				  EdtUserName.sendKeys(MSISDN);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Wrong MSISDN Format", "Successfully Entered The Wrong MSISDN Format :"+MSISDN, "No");
				  System.out.println("Successfully Entered The Wrong MSISDN Format :"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Wrong MSISDN Format", "Failed To Enter The Wrong MSISDN Format", "Yes");   
				  System.out.println("Failed To Enter The Wrong MSISDN Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The Wrong MSISDN Format");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  
			WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The short Password", "Successfully Entered The short Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :"+Password);  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The short Password", "Failed To Enter short Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with wrong MSISDNF format and short Password", "Successfully Clicked on the button SIGN UP with wrong MSISDNF format and short Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP with wrong MSISDNF format and short Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with wrong MSISDNF format and short Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			 
			  WebElement objerrEmailID= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email-error']");
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrEmailID, "Mobile Number", "Please enter your mobile number in the correct format, e.g. 971XXXXXXXXX.");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "Please enter at least 6 characters.");
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for wrong format email id and short Password While Sign Up", "Error message verification failed for wrong format email id and short password while sign up", "No");
			  System.out.println("Error message verification failed for wrong format email id and short password while sign up");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_ValidEmailFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String email = "starzTest@test.com";
				  EdtUserName.sendKeys(email);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Valid email Format", "Successfully Entered The valid email Format :"+email, "No");
				  System.out.println("Successfully Entered The valid email Format :"+email);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter valid email Format", "Failed To Enter The valid email Format", "Yes");   
				  System.out.println("Failed To Enter The valid Email Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The valid email Format");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  
			WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The short Password", "Successfully Entered The short Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :"+Password);  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The short Password", "Failed To Enter short Password", "Yes");   
			      System.out.println("Failed To Enter Password"); 
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with Valid email format and short Password", "Successfully Clicked on the button SIGN UP with Valid email format and short Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP with Valid email format and short Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with Valid email format and short Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "Please enter at least 6 characters.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for wrong format email id and short Password While Sign Up", "Error message verification failed for wrong format email id and short password while sign up", "No");
			  System.out.println("Error message verification failed for wrong format email id and short password while sign up");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_ValidMSISDNFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String MSISDN = "971581111111";
				  EdtUserName.sendKeys(MSISDN);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Valid MSISDN Format", "Successfully Entered The valid MSISDN Format :"+MSISDN, "No");
				  System.out.println("Successfully Entered The valid MSISDN Format :"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter valid MSISDN Format", "Failed To Enter The valid MSISDN Format", "Yes");   
				  System.out.println("Failed To Enter The valid MSISDN Format"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The valid MSISDN Format");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  
			WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The short Password", "Successfully Entered The short Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :"+Password);  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The short Password", "Failed To Enter short Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with Valid MSISDN format and short Password", "Successfully Clicked on the button SIGN UP with Valid MSISDN format and short Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP with Valid MSISDN format and short Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with Valid MSISDN format and short Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			  WebElement objerrPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrPassword, "Password", "Please enter at least 6 characters.");
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for valid format MSISDN  and short Password While Sign Up", "Error message verification failed for valid format MSISDN id and short password while sign up", "No");
			  System.out.println("Error message verification failed for valid format MSISDN id and short password while sign up");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_AlreadyRegEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String email = "pue@playco.com";
				  EdtUserName.sendKeys(email);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Already Registered email", "Successfully Entered The Already registered email :"+email, "No");
				  System.out.println("Successfully Entered The already registered email :"+email);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Already Registered email", "Failed To Enter The Already registered email", "Yes");   
				  System.out.println("Failed To Enter The Email"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The already registered email");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  
			WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123456";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password"); 
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with already registered email id", "Successfully Clicked on the button SIGN UP with with already registered email id", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP with already registered email id");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with already registered email id", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  WebElement objerrAlredyRegistered= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='emailAlreadyExists']/p");
				  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrAlredyRegistered, "Sign Up", "An account with this email address already exists. Please click here to log in.");
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for valid format MSISDN  and short Password While Sign Up", "Error message verification failed for valid format MSISDN id and short password while sign up, the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
				  System.out.println("Error message verification failed for valid format MSISDN id and short password while sign up, the system is very slow. The wait message lasted for more than 50 seconds");
				  StepNum = StepNum+1;
			  }
			  
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for valid format MSISDN  and short Password While Sign Up", "Error message verification failed for valid format MSISDN id and short password while sign up", "No");
			  System.out.println("Error message verification failed for valid format MSISDN id and short password while sign up");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_AlreadyRegMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String MSISDN = "97333000111";
				  EdtUserName.sendKeys(MSISDN);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Already Registered MSISDN", "Successfully Entered The Already registered MSISDN :"+MSISDN, "No");
				  System.out.println("Successfully Entered The already registered MSISDN :"+MSISDN);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Already Registered MSISDN", "Failed To Enter The Already registered MSISDN", "Yes");   
				  System.out.println("Failed To Enter The MSISDN"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The already registered MSISDN");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  
			WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123456";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :");  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with already registered MSISDN id", "Successfully Clicked on the button SIGN UP with with already registered MSISDN id", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP with already registered MSISDN id");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with already registered MSISDN id", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			  
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
			  WebElement objerrAlredyRegistered= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='phone-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objerrAlredyRegistered, "Sign Up", "An account with this mobile number already exists. Please click here to log in.");
			  }else{
				          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for already registered MSISDN While Sign Up", "Error message verification failed for valid format MSISDN id and short password while sign up,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
						  System.out.println("Error message verification failed for already registered MSISDN id while sign up,the system is very slow. The wait message lasted for more than 50 seconds");
						  StepNum = StepNum+1;
			  }
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for already registered MSISDN While Sign Up", "Error message verification failed for valid format MSISDN id and short password while sign up", "No");
			  System.out.println("Error message verification failed for already registered MSISDN id while sign up");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_Facebook_Cancel(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			  WebElement btnFacebookSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  if(btnFacebookSignUp!=null){
				  //btnFacebookSignUp.click();
				  Thread.sleep(5000);
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnFacebookSignUp);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP WITH FACEBOOK Button ", "Successfully Clicked on the SIGN UP WITH FACEBOOK Button", "Yes");
				  System.out.println("Successfully Clicked on the SIGN UP WITH FACEBOOK Button");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP WITH FACEBOOK Button", "Failed To Click On SIGN UP WITH FACEBOOK Button", "Yes");   
				  System.out.println("Failed To Click On SIGN UP WITH FACEBOOK Button"); 
				  Driver.close();
				  Assert.fail("Failed To Click On SIGN UP WITH FACEBOOK Button");
				  StepNum = StepNum+1; 
			  	  }
			  
			  try{
				  Thread.sleep(3000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("facebook.com/login.php"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  Driver.close();
				  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  Driver.close();
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "Yes");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  Driver.close();
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  StepNum = StepNum+1;
			  }
			  
			  WebElement btnFacebookSignUpAfterFBClose= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  if(btnFacebookSignUpAfterFBClose!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify appliaction is is on the same page ", "Successfully verifed", "Yes");
				  System.out.println("Successfully verifed the application has returned to the SignUp page");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify appliaction is is on the same page", "Application not returned to the same page as the SIGN UP WITH FACEBOOK is not present ", "Yes");   
				  System.out.println("Application not returned to the same page as the SIGN UP WITH FACEBOOK is not present"); 
				  Driver.close();
				  Assert.fail("Application not returned to the same page as the SIGN UP WITH FACEBOOK is not present");
				  StepNum = StepNum+1; 
			  	  }
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". UN HAppy scenarion with Facebook Sign UP", "UN HAppy scenarion with Facebook Sign UP failed", "No");
			  System.out.println("Unhappy scenarion with Facebook Sign UP failed");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_Facebook_NotApprove(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			  WebElement btnFacebookSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  if(btnFacebookSignUp!=null){
				  //btnFacebookSignUp.click();
				  Thread.sleep(5000);
				  JavascriptExecutor js = (JavascriptExecutor)Driver;
				  js.executeScript("arguments[0].click();", btnFacebookSignUp);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP WITH FACEBOOK Button ", "Successfully Clicked on the SIGN UP WITH FACEBOOK Button", "Yes");
				  System.out.println("Successfully Clicked on the SIGN UP WITH FACEBOOK Button");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP WITH FACEBOOK Button", "Failed To Click On SIGN UP WITH FACEBOOK Button", "Yes");   
				  System.out.println("Failed To Click On SIGN UP WITH FACEBOOK Button"); 
				  Driver.close();
				  Assert.fail("Failed To Click On SIGN UP WITH FACEBOOK Button");
				  StepNum = StepNum+1; 
			  	  }
			  
			  try{
				  Thread.sleep(3000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("facebook.com/login.php"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  Driver.close();
				  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  
			  WebElement EdtFacebookEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtFacebookEmail!=null){
				  Thread.sleep(2000);
				  String fbemail = "taeksyuyai_1500554945@tfbnw.net";
				  EdtFacebookEmail.sendKeys(fbemail);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Facebook email id ", "Successfully entered facebook email id"+fbemail, "Yes");
				  System.out.println("Successfully entered facebook email id"+fbemail);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Facebook email id", "Failed To enter facebook email id", "Yes");   
				  System.out.println("Failed To enter facebook email id"); 
				  Driver.close();
				  Assert.fail("Failed To enter facebook email id");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement EdtFacebookPass= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='pass']");
			  if(EdtFacebookEmail!=null){
				  Thread.sleep(1000);
				  String fbPass= "tester01.";
				  EdtFacebookPass.sendKeys(fbPass);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Facebook password ", "Successfully entered facebook password"+fbPass, "Yes");
				  System.out.println("Successfully entered facebook password"+fbPass);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter Facebook password", "Failed To enter facebook password", "Yes");   
				  System.out.println("Failed To enter facebook password"); 
				  Driver.close();
				  Assert.fail("Failed To enter facebook password");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement BtnFacebookLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='u_0_2' and @name='login']");
			  if(BtnFacebookLogin!=null){
				  BtnFacebookLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Facebook Login Button ", "Successfully Clicked on Facebook Login Button", "Yes");
				  System.out.println("Successfully Clicked on Facebook Login Button");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Facebook Login Button", "Failed To Click On Facebook Login Button", "Yes");   
				  System.out.println("Failed To Click On Facebook Login Button"); 
				  Driver.close();
				  Assert.fail("Failed To Click On Facebook Login Button");
				  StepNum = StepNum+1; 
			  	  }
			  
			  WebElement BtnFacebookCancel= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[text()='Cancel']");
			  if(BtnFacebookCancel!=null){
				  BtnFacebookCancel.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Facebook Cancel Button ", "Successfully Clicked on Facebook Cancel Button", "Yes");
				  System.out.println("Successfully Clicked on Facebook Cancel Button");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Facebook Cancel Button", "Failed To Click On Facebook Cancel Button", "Yes");   
				  System.out.println("Failed To Click On Facebook Cancel Button"); 
				  Driver.close();
				  Assert.fail("Failed To Click On Facebook Cancel Button");
				  StepNum = StepNum+1; 
			  	  }
			  
			  Genf.gfSleep(3);
			  Driver.switchTo().window(tabs2.get(0)); 
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "Yes");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  Driver.close();
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  StepNum = StepNum+1;
			  }
			  
			  WebElement btnFacebookSignUpAfterFBClose= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  if(btnFacebookSignUpAfterFBClose!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify appliaction is is on the same page ", "Successfully verifed", "Yes");
				  System.out.println("Successfully verifed the application has returned to the SignUp page");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify appliaction is is on the same page", "Application not returned to the same page as the SIGN UP WITH FACEBOOK is not present ", "Yes");   
				  System.out.println("Application not returned to the same page as the SIGN UP WITH FACEBOOK is not present"); 
				  Driver.close();
				  Assert.fail("Application not returned to the same page as the SIGN UP WITH FACEBOOK is not present");
				  StepNum = StepNum+1; 
			  	  }
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for already registered MSISDN While Sign Up", "Error message verification failed for valid format MSISDN id and short password while sign up", "No");
			  System.out.println("UN Happy scenarion with Facebook Sign UP failed");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_Facebook_NotApprove_AccessToEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			String fbemail = Futil.fgetData("UnRegisteredFBAccountUName", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			String fbPass = Futil.fgetData("UnRegisteredFBAccountPwd", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
			Genf.gfSleep(4);
			  WebElement btnFacebookSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  //StepNum = Genf.fClickOnObject(btnFacebookSignUp, "Click On SIGN UP WITH FACEBOOK Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  StepNum = Genf.fClickOnObjectWithAction(btnFacebookSignUp, "Click On SIGN UP WITH FACEBOOK Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  try{
				  Thread.sleep(3000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("facebook.com/login.php"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  Driver.close();
				  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  
			  WebElement EdtFacebookEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  StepNum = Genf.fEnterDataToEditBox(EdtFacebookEmail, fbemail, "Enter Facebook email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			  
			  WebElement EdtFacebookPass= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='pass']");
			  StepNum = Genf.fEnterDataToEditBox(EdtFacebookPass, fbPass, "Enter Facebook password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			  
			  WebElement BtnFacebookLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='u_0_2' and @name='login']");
			  StepNum = Genf.fClickOnObject(BtnFacebookLogin, "Click On Facebook Login Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  Genf.gfSleep(3);
			  if(Driver.getWindowHandles().size()>1){
				  WebElement BtnFacebookEdit= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='u_0_5']");
				  StepNum = Genf.fClickOnObject(BtnFacebookEdit, "Click On Facebook Edit This", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
				  
				  WebElement BtnFacebookEmailChkbx= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='u_0_7']//label[2]//input[@type='checkbox']");
				  StepNum = Genf.fClickOnObject(BtnFacebookEmailChkbx, "Uncheck Email ID", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
				  
				  WebElement BtnCountinue= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@name='__CONFIRM__']");
				  StepNum = Genf.fClickOnObject(BtnCountinue, "Click On Countinue", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
			  }
			  
			  Driver.switchTo().window(tabs2.get(0)); 
			  Genf.gfSleep(3);
			  
			  WebElement EdtEmailRequirdForm= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='provide-email-form']");
			  if(EdtEmailRequirdForm!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Popup for Missing Email ", "Successfully verifed", "Yes");
				  System.out.println("Successfully verifed the popup for missing email");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Popup for Missing Email", "Failed to verify", "Yes");   
				  System.out.println("Failed to verify the the popup for missing email"); 
				  Driver.close();
				  Assert.fail("Failed to verify the the popup for missing email");
				  StepNum = StepNum+1; 
			  	  }
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "Yes");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  Driver.close();
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  StepNum = StepNum+1;
			  }
			  
			  WebElement BtnSavChanges= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='provide-email-button']");
			  StepNum = Genf.fClickOnObject(BtnSavChanges, "Click On Save Changes Without Email", "NotCritical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  
			  WebElement EmailErrorMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='missing-email-input-error']");
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailErrorMsg, "Email Address", "This field is required.");
			  
			  WebElement EdtEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='missing-email-input']");
			  StepNum = Genf.fEnterDataToEditBox(EdtEmail, "ssss.ssss", "Enter Wrong Format email", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			  StepNum = Genf.gfVerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailErrorMsg, "Email Address", "Please enter a valid email address e.g. XXX.XXX@gmail.com");
			  
			  WebElement BtnClose= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='close-email-missing-modal']");
			  StepNum = Genf.fClickOnObject(BtnClose, "Click On Close", "NotCritical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  
			  WebElement btnFacebookSignUpAfterFBClose= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  if(btnFacebookSignUpAfterFBClose!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify appliaction is is on the same page ", "Successfully verifed", "Yes");
				  System.out.println("Successfully verifed the application has returned to the SignUp page");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify appliaction is is on the same page", "Application not returned to the same page as the SIGN UP WITH FACEBOOK is not present ", "Yes");   
				  System.out.println("Application not returned to the same page as the SIGN UP WITH FACEBOOK is not present");
				  Driver.close();
				  Assert.fail("Application not returned to the same page as the SIGN UP WITH FACEBOOK is not present");
				  StepNum = StepNum+1; 
			  	  }
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Error Message for already registered MSISDN While Sign Up", "Error message verification failed for valid format MSISDN id and short password while sign up", "No");
			  System.out.println("UN Happy scenarion with Facebook Sign UP failed");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_Facebook_ProspectUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String fbemail,String fbPass) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			/*String fbemail = Futil.fgetData("FBUserNameForProspectUserSignUP", "OneTimeData", "OneTimeData", Driver, Extreport, Exttest, ResultLocation);
			String fbPass = Futil.fgetData("FBPwdForProspectUserSignUP", "OneTimeData", "OneTimeData", Driver, Extreport, Exttest, ResultLocation);*/
			StepNum=afSignUpWith_Facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, fbemail, fbPass);
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Signup with facebook", "SignUp with facebook failed", "No");
			  System.out.println("SignUp with facebook failed");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afSignUpWith_Facebook(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation,String fbemail,String fbPass) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			Genf.gfSleep(5);
			  WebElement btnFacebookSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='facebook-login']");
			  //StepNum = Genf.fJSClickOnObject(btnFacebookSignUp, "Click On SIGN UP WITH FACEBOOK Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  StepNum = Genf.fClickOnObjectWithAction(btnFacebookSignUp, "Click On SIGN UP WITH FACEBOOK Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  try{
				  Thread.sleep(8000);
			  ArrayList<String> tabs2 = new ArrayList<String> (Driver.getWindowHandles());
			  Driver.switchTo().window(tabs2.get(1));
			  if((Driver.getCurrentUrl().contains("facebook"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verified the Facebook page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Login Page Has opened", "Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  Driver.close();
				  Assert.fail("Failed To Verfiy the Facebook page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  
			  WebElement EdtFacebookEmail= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  StepNum = Genf.fEnterDataToEditBox(EdtFacebookEmail, fbemail, "Enter Facebook email id", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			  
			  WebElement EdtFacebookPass= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='pass']");
			  StepNum = Genf.fEnterDataToEditBox(EdtFacebookPass, fbPass, "Enter Facebook password", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation);
			  
			  WebElement BtnFacebookLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='u_0_2' and @name='login']");
			  StepNum = Genf.fClickOnObject(BtnFacebookLogin, "Click On Facebook Login Button", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"No");
			  Genf.gfSleep(3);
			  if(Driver.getWindowHandles().size()>1){
				  
				  WebElement BtnCountinue= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@name='__CONFIRM__']");
				  StepNum = Genf.fClickOnObject(BtnCountinue, "Click On Countinue", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation,"Yes");
			  }
			  
			  Driver.switchTo().window(tabs2.get(0)); 
			  Genf.gfSleep(3);
			  }catch(Exception e){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Facebook Page Has opened", "Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook", "Yes");
				  System.out.println("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  Driver.close();
				  Assert.fail("Failed To Verfiy the new Window Opened Or There is no new Window Opened for Facebook");
				  StepNum = StepNum+1;
			  }
			 
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  Genf.gfSleep(4);
				  WebElement objCreditCard= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='creditCardLogo']");
				  if(objCreditCard!=null){
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payments page", "Successfully verified the payment page by checking credit card logo", "Yes");
					  System.out.println("Successfully verified the payment page by checking credit card logo");  
					  StepNum = StepNum+1;   
				  	  }else{
					  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify payments page", "Failed To verify payments page", "Yes");   
					  System.out.println("Failed To verify payments page"); 
					  Driver.close();
					  Assert.fail("Failed To verify payments page");
					  StepNum = StepNum+1; 
				  	  }
			  }else{
		          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Payment page", "The payment page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
				  System.out.println("The payment page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
				  StepNum = StepNum+1;
	  }
			  
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". SignUp With facebook", "SignUp with facebook failed", "No");
			  System.out.println("SignUp with facebook failed");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	
	public int afSignUpWith_Correct_Email_ValidPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='email']");
			  if(EdtUserName!=null){
				  EdtUserName.clear();
				  String email = Genf.GenrateEmailId();
				  EdtUserName.sendKeys(email);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter Valid email id", "Successfully Entered The valid email :"+email, "No");
				  System.out.println("Successfully Entered The valid email :"+email);  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter valid email id", "Failed To Enter The valid email id", "Yes");   
				  System.out.println("Failed To Enter The valid Email id"); 
				  Driver.close();
				  Assert.fail("Failed To Enter The valid email id");
				  StepNum = StepNum+1; 
			  	  }
			  Thread.sleep(2000);
			  
			WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='password']");
			  if(EdtPassword!=null){
				  EdtPassword.clear();
				  String Password = "123456";
				  EdtPassword.sendKeys(Password);
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Enter The Password", "Successfully Entered The Password :"+Password, "No");
				  System.out.println("Successfully Entered the password :"+Password);  
				  StepNum = StepNum+1; 
			  	  }else{
			  	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Enter The Password", "Failed To Enter Password", "Yes");   
			      System.out.println("Failed To Enter Password");  
			      Driver.close();
			      Assert.fail("Failed To Enter Password");
			  	  StepNum = StepNum+1;
				  }
			  Thread.sleep(2000);
			  WebElement btnSignUp= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonRegisterScreen']");
			  if(btnSignUp!=null){
				  btnSignUp.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On SIGN UP Button with Valid email id and Password", "Successfully Clicked on the button SIGN UP with Valid email id and Password", "Yes");
				  System.out.println("Successfully Clicked on the button SIGN UP with Valid email id and Password");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On SIGN UP Button with Valid email id and Password", "Failed To Click On SIGN UP", "Yes");   
				  System.out.println("Failed To Click On button SIGN UP"); 
				  Driver.close();
				  Assert.fail("Failed To Click On button SIGN UP");
				  StepNum = StepNum+1; 
			  	  }
			  
			  boolean Ready = Genf.WaitUnitlObjectDisappear(Driver, "xpath", "//div[@class='Blockscreen-msg']",50);
			  if(Ready){
				  Genf.gfSleep(4);
			  WebElement objCreditCard= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='creditCardLogo']");
			  
			  if(objCreditCard!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payments page", "Successfully verified the payment page by checking credit card logo", "Yes");
				  System.out.println("Successfully verified the payment page by checking credit card logo");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify payments page", "Failed To verify payments page", "Yes");   
				  System.out.println("Failed To verify payments page"); 
				  Driver.close();
				  Assert.fail("Failed To verify payments page");
				  StepNum = StepNum+1; 
			  	  }
			  }else{
				          Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The Payment page", "The payment page verification failed,the system is very slow. The wait message lasted for more than 50 seconds", "Yes");
						  System.out.println("The payment page verification failed,the system is very slow. The wait message lasted for more than 50 seconds");
						  StepNum = StepNum+1;
			  }
			  
			  Thread.sleep(2000);
			  
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". SignUp with valid email id and Password ", "Signup failed for valid email id and password", "No");
			  System.out.println("Signup failed for valid email id and password");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyPaymentPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			
			WebElement lblCreditCard= Genf.setObject(10, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='creditCardLogo']");
			  if(lblCreditCard!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Credit Card", "Successfully Verified Payment methode credit card", "No");
				  System.out.println("Successfully Verified Payment methode credit card");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Credit Card", "Failed To verify Payment methode credit card", "Yes");   
				  System.out.println("Failed To verify Payment methode credit card"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblEtisalat= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalatLogo']");
			  if(lblEtisalat!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Etisalat", "Successfully Verified Payment methode Etisalat", "No");
				  System.out.println("Successfully Verified Payment methode Etisalat");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Etisalat", "Failed To verify Payment methode Etisalat", "Yes");   
				  System.out.println("Failed To verify Payment methode Etisalat"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lbldu= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='duLogo']");
			  if(lbldu!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method du", "Successfully Verified Payment methode du", "No");
				  System.out.println("Successfully Verified Payment methode du");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method du", "Failed To verify Payment methode du", "Yes");   
				  System.out.println("Failed To verify Payment methode du"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblMorocoTelecom= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='maroctelecomLogo']");
			  if(lblMorocoTelecom!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Moroco Telecom", "Successfully Verified Payment methode Moroco Telecom", "No");
				  System.out.println("Successfully Verified Payment methode Moroco Telecom");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Moroco Telecom", "Failed To verify Payment methode Moroco Telecom", "Yes");   
				  System.out.println("Failed To verify Payment methode Moroco Telecom"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblOrangeJordan= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='orangejordanLogo']");
			  if(lblOrangeJordan!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Orange Jordan", "Successfully Verified Payment methode Orange Jordan", "No");
				  System.out.println("Successfully Verified Payment methode Orange Jordan");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Orange Jordan", "Failed To verify Payment methode Orange Jordan", "Yes");   
				  System.out.println("Failed To verify Payment methode Orange Jordan"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblOrangeMoroco= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='orangemarocLogo']");
			  if(lblOrangeMoroco!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Orange Moroco", "Successfully Verified Payment methode Orange Moroco", "No");
				  System.out.println("Successfully Verified Payment methode Orange Moroco");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Orange Moroco", "Failed To verify Payment methode Orange Moroco", "Yes");   
				  System.out.println("Failed To verify Payment methode Orange Moroco"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblOoredoTunisia= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ooredootunisiaLogo']");
			  if(lblOoredoTunisia!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Ooredo Tunisia", "Successfully Verified Payment methode Ooredo Tunisia", "No");
				  System.out.println("Successfully Verified Payment methode Ooredo Tunisia");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Ooredo Tunisia", "Failed To verify Payment methode Ooredo Tunisia", "Yes");   
				  System.out.println("Failed To verify Payment methode Ooredo Tunisia"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblOoredoKuwait= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ooredookuwaitLogo']");
			  if(lblOoredoKuwait!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Ooredo Kuwait", "Successfully Verified Payment methode Ooredo Kuwait", "No");
				  System.out.println("Successfully Verified Payment methode Ooredo Kuwait");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Ooredo Kuwait", "Failed To verify Payment methode Ooredo Kuwait", "Yes");   
				  System.out.println("Failed To verify Payment methode Ooredo Kuwait"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblSTC= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='stcsaudiLogo']");
			  if(lblSTC!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method STC", "Successfully Verified Payment methode STC", "No");
				  System.out.println("Successfully Verified Payment methode STC");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method STCt", "Failed To verify Payment methode STC", "Yes");   
				  System.out.println("Failed To verify Payment methode STC"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblVodafoneEygypt= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='vodafoneegyptLogo']");
			  if(lblVodafoneEygypt!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Vodafone Eygypt", "Successfully Verified Payment methode Vodafone Eygypt", "No");
				  System.out.println("Successfully Verified Payment methode Vodafone Eygypt");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Vodafone Eygypt", "Failed To verify Payment methode Vodafone Eygypt", "Yes");   
				  System.out.println("Failed To verify Payment methode Vodafone Eygypt"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblViva= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='vivaLogo']");
			  if(lblViva!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Viva", "Successfully Verified Payment methode Viva", "No");
				  System.out.println("Successfully Verified Payment methode Viva");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Viva", "Failed To verify Payment methode Viva", "Yes");   
				  System.out.println("Failed To verify Payment methode Viva"); 
				  StepNum = StepNum+1; 
			  	  }
			  WebElement lblStarzVoucher= Genf.setObject(3, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='voucherLogo']");
			  if(lblStarzVoucher!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment method Starz Play Voucher", "Successfully Verified Payment methode Starz Play Voucher", "No");
				  System.out.println("Successfully Verified Payment methode Starz Play Voucher");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment method Starz Play Voucher", "Failed To verify Payment methode Starz Play Voucher", "Yes");   
				  System.out.println("Failed To verify Payment methode Starz Play Voucher"); 
				  StepNum = StepNum+1; 
			  	  }
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment Page - Payment Methods", "Verification failed for Payment Page - Payment Methods", "No");
			  System.out.println("Verification failed for Payment Page - Payment Methods");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifyRegistrationPaymentPageURL(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement objCreditCard= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='creditCardLogo']");
			  if(objCreditCard!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payments page", "Successfully verified the payment page by checking credit card logo", "Yes");
				  System.out.println("Successfully verified the payment page by checking credit card logo");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify payments page", "Failed To verify payments page", "Yes");   
				  System.out.println("Failed To verify payments page"); 
				  Driver.close();
				  Assert.fail("Failed To verify payments page");
				  StepNum = StepNum+1; 
			  	  }
			if(Driver.getCurrentUrl().contains("registration/payment")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment Page URL", "Successfully verifed the Payment page URL, it is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verifed the Payment page URL, it is :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment Page URL", "Failed the verification of Payment page, it is "+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed the verification of Payment page, it is "+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment Page URL", "Failed the verification of Payment page", "Yes");
			  System.out.println("Failed the verification of Payment page");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
		  
	}
	
	public int afVerifySettingsPaymentPageURL(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			WebElement objCreditCard= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='creditCardLogo']");
			  if(objCreditCard!=null){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payments page", "Successfully verified the payment page by checking credit card logo", "Yes");
				  System.out.println("Successfully verified the payment page by checking credit card logo");  
				  StepNum = StepNum+1;   
			  	  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". verify payments page", "Failed To verify payments page", "Yes");   
				  System.out.println("Failed To verify payments page"); 
				  Driver.close();
				  Assert.fail("Failed To verify payments page");
				  StepNum = StepNum+1; 
			  	  }
			if(Driver.getCurrentUrl().contains("settings/payment")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Payment Page URL", "Successfully verifed the Payment page URL, it is :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully verifed the Payment page URL, it is :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment Page URL", "Failed the verification of Payment page, it is "+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed the verification of Payment page, it is "+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
		
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Payment Page URL", "Failed the verification of Payment page", "Yes");
			  System.out.println("Failed the verification of Payment page");
			  StepNum = StepNum+1;
		}
		
		return   StepNum;
	}
	
	public int afVerifySignUpPage_FromGreatEntertainment(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			Genf.gfSleep(3);
			WebElement btnStartYourFreeTrial= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='great-content']//a[@class='Btn--primary']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnStartYourFreeTrial, "Click on START YOUR FREE TRIAL from Great Entertainment ", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			  
			  Genf.gnWaitUntilPageLoad(Driver);
			  
			  if((Driver.getCurrentUrl().contains("registration/userinfo"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to SignUP", "Successfully Loaded the SignUP page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Loaded the SignUP page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to SignUP", "Failed To Load the SignUP page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the SignUP page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The SignUP Page", "Verification failed for The Page SignUP", "No");
			  System.out.println("Verification failed for The Page SignUP");
			  StepNum = StepNum+1;
			}  
		return StepNum;
	}
	
	public int afVerifySignUpPage_FromKids(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation) throws MalformedURLException
	{
		
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			Genf.gfSleep(3);
			WebElement btnStartYourFreeTrial= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@class='landingkids-content']//a[@class='Btn--primary']");
			Genf.gfSleep(3);
			StepNum = Genf.fClickOnObject(btnStartYourFreeTrial, "Click on START YOUR FREE TRIAL from Kids section ", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
			  
			  Genf.gnWaitUntilPageLoad(Driver);
			  
			  if((Driver.getCurrentUrl().contains("registration/userinfo"))){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify The URL Changed to SignUP", "Successfully Loaded the SignUP page with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Successfully Loaded the SignUP page with URL :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The URL Changed to SignUP", "Failed To Load the SignUP page, And the URL is :"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Load the SignUP page, And the URL is :"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
		}catch(Exception e){
			
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The SignUP Page", "Verification failed for The Page SignUP", "No");
			  System.out.println("Verification failed for The Page SignUP");
			  StepNum = StepNum+1;
			}  
		return StepNum;
	}
	
}
