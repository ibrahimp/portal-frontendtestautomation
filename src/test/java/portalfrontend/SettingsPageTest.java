package portalfrontend;

import java.io.File;
import java.net.MalformedURLException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class SettingsPageTest {
	public ExtentReports Extreport;
	public ExtentTest Exttest;
	public WebDriver Driver;
	public HtmlUnitDriver Hdriver; 
	public String ResultLocation;
	public FrameWorkUtility Futil;
	public GeneralMethods Genf;
	public DesiredCapabilities Cap;
	public SettingsPageMethods SPMethods;
	public File PhantomJsSrc;
	
	
	@Parameters({"Browser"})
	@BeforeTest(alwaysRun = true)
	 public void startTest(String BrowserName,final ITestContext testContext) {
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		SPMethods = new SettingsPageMethods();
	    ResultLocation = System.getProperty("user.dir")+"/Custom_Report/"+testContext.getName();
	    Extreport = new ExtentReports(ResultLocation+"/"+testContext.getName()+"-"+Futil.fGetRandomNumUsingTime()+".html", true);
	      Cap = DesiredCapabilities.chrome();
		  Cap.setBrowserName(BrowserName);
		  Cap.setCapability("ignoreZoomSetting", true);
		  Cap.setPlatform(Platform.MAC);
		  Cap.setCapability("nativeEvents","false");
		  //PhantomJsSrc = new File(System.getProperty("user.dir")+"/Resources/phantomjs/bin/phantomjs");
	}
	
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups="User")
	  public void TC001_UserSettingPage_ValidateFirstName(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC001_UserSettingPage_ValidateFirstName(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups="User")
	  public void TC002_UserSettingPage_ValidateSecondName(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC002_UserSettingPage_ValidateSecondName(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC003_UserSettingPage_EditEmail_AllEmpty(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC003_UserSettingPage_EditEmail_AllEmpty(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC004_UserSettingPage_EditEmail_Incorrect_oldEmail_Empty_Email1AndEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC004_UserSettingPage_EditEmail_Incorrect_oldEmail_Empty_Email1AndEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC005_UserSettingPage_EditEmail_Incorrect_oldEmailAndEmail1_Empty_Email2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC005_UserSettingPage_EditEmail_Incorrect_oldEmailAndEmail1_Empty_Email2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC006_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_IncorrectEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC006_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_IncorrectEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC007_UserSettingPage_EditEmail_IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC007_UserSettingPage_EditEmail_IncorrectoldEmail_IncorrectEmail1_IncorrectEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC008_UserSettingPage_EditEmail_IncorrectoldEmail_CorrectEmail1_EmptyEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC008_UserSettingPage_EditEmail_IncorrectoldEmail_CorrectEmail1_EmptyEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC009_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_CorrectEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC009_UserSettingPage_EditEmail_IncorrectoldEmail_EmptyEmail1_CorrectEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC010_UserSettingPage_EditEmail_IncorrectoldEmail_MisMatchingEmail1AndEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC010_UserSettingPage_EditEmail_IncorrectoldEmail_MisMatchingEmail1AndEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC011_UserSettingPage_EditEmail_IncorrectoldEmail_MatchingEmail1AndEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC011_UserSettingPage_EditEmail_IncorrectoldEmail_MatchingEmail1AndEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC012_UserSettingPage_EditEmail_Correct_oldEmail_Empty_Email1AndEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC012_UserSettingPage_EditEmail_Correct_oldEmail_Empty_Email1AndEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC013_UserSettingPage_EditEmail_CorrectOldEmail_IncorrectEmail1_EmptyEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC013_UserSettingPage_EditEmail_CorrectOldEmail_IncorrectEmail1_EmptyEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC014_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_IncorrectEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC014_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_IncorrectEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC015_UserSettingPage_EditEmail_CorrectoldEmail_IncorrectEmail1_IncorrectEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC015_UserSettingPage_EditEmail_CorrectoldEmail_IncorrectEmail1_IncorrectEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC016_UserSettingPage_EditEmail_CorrectoldEmail_CorrectEmail1_EmptyEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC016_UserSettingPage_EditEmail_CorrectoldEmail_CorrectEmail1_EmptyEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC017_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_CorrectEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC017_UserSettingPage_EditEmail_CorrectoldEmail_EmptyEmail1_CorrectEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC018_UserSettingPage_EditEmail_CorrectoldEmail_MisMatchingEmail1AndEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC018_UserSettingPage_EditEmail_CorrectoldEmail_MisMatchingEmail1AndEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC019_UserSettingPage_EditEmail_CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC019_UserSettingPage_EditEmail_CorrectoldEmail_AlreadyRegisteredEmail1AndEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Happy","EndToEnd"})
	  public void TC020_UserSettingPage_EditEmail_Happy_EndToEnd(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC020_UserSettingPage_EditEmail_Happy_EndToEnd(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC021_UserSettingPage_EditPassword_AllEmpty(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC021_UserSettingPage_EditPassword_AllEmpty(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC022_UserSettingPage_EditPassword_EmptyOld_ShortNew_EmptyReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC022_UserSettingPage_EditPassword_EmptyOld_ShortNew_EmptyReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC023_UserSettingPage_EditPassword_EmptyOld_EmptyNew_ShortReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC023_UserSettingPage_EditPassword_EmptyOld_EmptyNew_ShortReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC024_UserSettingPage_EditPassword_EmptyOld_ShortNew_ShortReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC024_UserSettingPage_EditPassword_EmptyOld_ShortNew_ShortReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC025_UserSettingPage_EditPassword_EmptyOld_MismatchinNewAndReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC025_UserSettingPage_EditPassword_EmptyOld_MismatchinNewAndReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC026_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_EmptyReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC026_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_EmptyReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC027_UserSettingPage_EditPassword_IncorrectOld_ShortNew_EmptyReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC027_UserSettingPage_EditPassword_IncorrectOld_ShortNew_EmptyReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC028_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_ShortReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC028_UserSettingPage_EditPassword_IncorrectOld_EmptyNew_ShortReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC029_UserSettingPage_EditPassword_IncorrectOld_ShortNew_ShortReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC029_UserSettingPage_EditPassword_IncorrectOld_ShortNew_ShortReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC030_UserSettingPage_EditPassword_IncorrectOld_MismatchingNewAndReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC030_UserSettingPage_EditPassword_IncorrectOld_MismatchingNewAndReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC031_UserSettingPage_EditPassword_CorrectOld_EmptyNew_EmptyReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC031_UserSettingPage_EditPassword_CorrectOld_EmptyNew_EmptyReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC032_UserSettingPage_EditPassword_CorrectOld_ShortNew_EmptyReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC032_UserSettingPage_EditPassword_CorrectOld_ShortNew_EmptyReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC033_UserSettingPage_EditPassword_CorrectOld_EmptyNew_ShortReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC033_UserSettingPage_EditPassword_CorrectOld_EmptyNew_ShortReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC034_UserSettingPage_EditPassword_CorrectOld_ShortNew_ShortReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC034_UserSettingPage_EditPassword_CorrectOld_ShortNew_ShortReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC035_UserSettingPage_EditPassword_CorrectOld_MismatchingNewAndReEnter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC035_UserSettingPage_EditPassword_CorrectOld_MismatchingNewAndReEnter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC036_UserSettingPage_EditPassword_CorrectOld_NewAndReEnter_SameAsOLD(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC036_UserSettingPage_EditPassword_CorrectOld_NewAndReEnter_SameAsOLD(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC037_UserSettingPage_EditPassword_Happy_EndToEnd(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC037_UserSettingPage_EditPassword_Happy_EndToEnd(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC038_UserSettingPage_GoTo_Home_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC038_UserSettingPage_GoTo_Home_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC039_UserSettingPage_GoTo_Movies_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC039_UserSettingPage_GoTo_Movies_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC040_UserSettingPage_GoTo_Series_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC040_UserSettingPage_GoTo_Series_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC041_UserSettingPage_GoTo_Arabic_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC041_UserSettingPage_GoTo_Arabic_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC042_UserSettingPage_GoTo_Kids_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC042_UserSettingPage_GoTo_Kids_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC043_UserSettingPage_OpensSearchBar_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC043_UserSettingPage_OpensSearchBar_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC044_UserSettingPage_OpensSettings_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC044_UserSettingPage_OpensSettings_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC045_UserSettingPage_GoTo_Settings_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC045_UserSettingPage_GoTo_Settings_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC046_UserSettingPage_GoTo_MyLibrary_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC046_UserSettingPage_GoTo_MyLibrary_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC047_UserSettingPage_LanguageTo_Arabic_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC047_UserSettingPage_LanguageTo_Arabic_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC048_UserSettingPage_LanguageTo_French_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC048_UserSettingPage_LanguageTo_French_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC049_UserSettingPage_Logout_FromHeader(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC049_UserSettingPage_Logout_FromHeader(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC050_UserSettingPage_GoTo_WhyStarzPlay_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC050_UserSettingPage_GoTo_WhyStarzPlay_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC051_UserSettingPage_GoTo_HelpCenter_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC051_UserSettingPage_GoTo_HelpCenter_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC052_UserSettingPage_GoTo_PartnerWithUs_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC052_UserSettingPage_GoTo_PartnerWithUs_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC053_UserSettingPage_GoTo_CompanyPage_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC053_UserSettingPage_GoTo_CompanyPage_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC054_UserSettingPage_GoTo_TermAndConditions_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC054_UserSettingPage_GoTo_TermAndConditions_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC055_UserSettingPage_GoTo_PrivacyPolicy_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC055_UserSettingPage_GoTo_PrivacyPolicy_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC056_UserSettingPage_GoTo_Blog_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC056_UserSettingPage_GoTo_Blog_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC057_UserSettingPage_GoTo_Careers_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC057_UserSettingPage_GoTo_Careers_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC058_UserSettingPage_GoTo_Facebook_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC058_UserSettingPage_GoTo_Facebook_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC059_UserSettingPage_GoTo_Twitter_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC059_UserSettingPage_GoTo_Twitter_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC060_UserSettingPage_GoTo_Instagram_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC060_UserSettingPage_GoTo_Instagram_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC061_UserSettingPage_GoTo_YouTube_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC061_UserSettingPage_GoTo_YouTube_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC062_UserSettingPage_GoTo_Appstore_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC062_UserSettingPage_GoTo_Appstore_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC063_UserSettingPage_GoTo_PlayStore_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC063_UserSettingPage_GoTo_PlayStore_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","Navigation"})
	  public void TC064_UserSettingPage_GoTo_MuleSoft_FromFooter(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC064_UserSettingPage_GoTo_MuleSoft_FromFooter(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC065_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC065_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_IncorrectEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC066_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC066_UserSettingPage_EditEmail_EmptyoldEmail_EmptyEmail1_CorrectEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC067_UserSettingPage_EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC067_UserSettingPage_EditEmail_EmptyoldEmail_IncorrectEmail1_EmptyEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
	  
	  @Parameters({"ApplnURL","Browser","TestType","NodeURL"})
	  @Test(enabled=true,groups={"User","UnHappy"})
	  public void TC068_UserSettingPage_EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2(String AppURL,String Browser,String TestType,String NodeURL) throws MalformedURLException 
	  {
		  SPMethods.TC068_UserSettingPage_EditEmail_EmptyoldEmail_CorrectEmail1_EmptyEmail2(Extreport, Exttest, Driver, AppURL, ResultLocation,Browser,TestType,NodeURL,Cap); 
	  }
}

