package portalfrontend;

import java.net.MalformedURLException;
import java.sql.ResultSet;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class AffiliateSignUpPageMethods {
	
	public GeneralMethods Genf;
	public FrameWorkUtility Futil;
	public CommonFooterMethods CFMethods;
	public CommonHeaderMethods CHMethods;
	public CommonLoginMethods CLoginMethods;
	public CommonSignUpMethods CSignUpMethods;
	public CommonMoPMethods CMoPMethods;
	
	public void TC001_GuestAffiliateSignUp_GoToHome(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		CHMethods = new CommonHeaderMethods();
		System.out.println("TC001_GuestAffiliateSignUp_GoToHome: Execution Started");
		  Exttest = Extreport.startTest("TC001_GuestAffiliateSignUp_GoToHome","A guest at the Affiliate Sign Up page goes to the Home Page (from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  WebElement lblStarzPlay= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='headerLogo']");
		  if(lblStarzPlay!=null){
			  lblStarzPlay.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Starz Play logo", "Successfully Clicked on Starz Play logo", "No");
			  System.out.println("Successfully Clicked on Starz Play Logo"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Starz Play logo", "Failed To Clicked on Starz Play logo", "No");   
			  System.out.println("Failed To Clicked on Starz Play logo"); 
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		  if(Driver.getCurrentUrl().contains("en/start")){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Starz Play Home page", "Successfully Verified the Home Page, The home Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Verified the Home Page With URL :"+Driver.getCurrentUrl()); 
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Starz Play Home page", "Failed To Verify home page And The URL was"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Failed To Verify home page And The URL was"+Driver.getCurrentUrl()); 
			  StepNum = StepNum+1;
		  }
		  System.out.println("TC001_GuestAffiliateSignUp_GoToHome : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC002_GuestAffiliateSignUp_GotoAboutUs(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC002_GuestAffiliateSignUp_GotoAboutUs: Execution Started");
		  Exttest = Extreport.startTest("TC002_GuestAffiliateSignUp_GotoAboutUs","A guest at the Affiliate Sign Up page goes to the About Us page (from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  WebElement lnkAboutUs= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='headerAbout']");
		  if(lnkAboutUs!=null){
			  lnkAboutUs.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On About US", "Successfully Clicked on About US", "No");
			  System.out.println("Successfully Clicked on About US");    
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On About US", "Failed To Click On  Clicked on About US", "No");   
			  System.out.println("Failed To Click On  Clicked on About US"); 
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		  String strHomeUrl = Driver.getCurrentUrl();
		  if(strHomeUrl.contains("en/aboutus")&&(strHomeUrl.contains("error")!=true)){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify About Us page", "Successfully Verified the About Us, The About Us for guest loaded with Title :"+Driver.getTitle(), "Yes");
			  System.out.println("Verified the About Us Page URL is :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify About Us page", "Failed To Launch And The title was"+Driver.getTitle(), "No");
			  System.out.println("Failed To Launch And The URL was"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC002_GuestAffiliateSignUp_GotoAboutUs : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC003_GuestAffiliateSignUp_GotoContactUs(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC003_GuestAffiliateSignUp_GotoContactUs: Execution Started");
		  Exttest = Extreport.startTest("TC003_GuestAffiliateSignUp_GotoContactUs","A guest at the Affiliate Sign Up page goes to the Contact Us page (from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  WebElement lnkContactUs= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='headerContact']");
		  if(lnkContactUs!=null){
			  lnkContactUs.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Contact US", "Successfully Clicked on Contact US", "No");
			  System.out.println("Successfully Clicked on Contact US");    
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Contact US", "Failed To Click On  Clicked on Contact US", "No");   
			  System.out.println("Failed To Click On  Clicked on Contact US");  
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		  String strHomeUrl = Driver.getCurrentUrl();
		  if(strHomeUrl.contains("en/contact")&&(strHomeUrl.contains("error")!=true)){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Contact Us page", "Successfully Verified the Contact Us, The Contact Us for guest loaded with Title :"+Driver.getTitle(), "Yes");
			  System.out.println("Verified the Contact Us Page URL is :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Contact Us page", "Failed To Launch And The title was"+Driver.getTitle(), "No");
			  System.out.println("Failed To Launch Contact Us page And The URL was"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC003_GuestAffiliateSignUp_GotoContactUs : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC004_GuestAffiliateSignUp_GotoLogin(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC004_GuestAffiliateSignUp_GotoLogin: Execution Started");
		  Exttest = Extreport.startTest("TC004_GuestAffiliateSignUp_GotoLogin","A guest at the Affiliate Sign Up page goes to the Log In page (from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  
		  WebElement lnkLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='login-button']");
		  if(lnkLogin!=null){
			  lnkLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login", "Successfully Clicked on Login from Header", "No");
			  System.out.println("Successfully Clicked on Login from header");  
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login from Header", "Failed To Click On  Login", "No");   
			  System.out.println("Failed To Click On Login from header");  
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		  String strHomeUrl = Driver.getCurrentUrl();
		  if(strHomeUrl.contains("/login")&&(strHomeUrl.contains("error")!=true)){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login page", "Successfully Verified the Login, The Login page for guest loaded with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Verified the Login Page URL is :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login page", "Failed To Launch login Page And The URL was"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed To Launch Login page And The URL was"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC004_GuestAffiliateSignUp_GotoLogin : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC005_GuestAffiliateSignUp_GotoLoginFromSighnUp(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		int StepNum = 1;
		System.out.println("TC005_GuestAffiliateSignUp_GotoLoginFromSighnUp: Execution Started");
		  Exttest = Extreport.startTest("TC005_GuestAffiliateSignUp_GotoLoginFromSighnUp","A guest at the Affiliate Sign Up page goes to the Log In page (from the sign up box)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  WebElement lnkLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='signup-link']");
		  if(lnkLogin!=null){
			  lnkLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login from SignUpBox", "Successfully Clicked on Login from SignUpBox", "No");
			  System.out.println("Successfully Clicked on Login from SignUpBox"); 
			  StepNum = StepNum+1;
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login from SignUpBox", "Failed To Click On  Login from SignUpBox", "No");   
			  System.out.println("Failed To Click On Login from SignUpBox"); 
			  StepNum = StepNum+1;
			   }
		  
		  Genf.gfSleep(5);
		  String strHomeUrl = Driver.getCurrentUrl();
		  if(strHomeUrl.contains("/login")&&(strHomeUrl.contains("error")!=true)){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login page", "Successfully Verified the Login, The Login page for guest loaded with URL :"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Verified the Login Page URL is :"+Driver.getCurrentUrl());  
			  StepNum = StepNum+1;
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login page", "Failed To Launch login Page And The URL was"+Driver.getCurrentUrl(), "No");
			  System.out.println("Failed To Launch Login page And The URL was"+Driver.getCurrentUrl());
			  StepNum = StepNum+1;
		  }
		  
		  System.out.println("TC005_GuestAffiliateSignUp_GotoLoginFromSighnUp : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC006_GuestAffiliateSignUp_LanguageToArabic(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC006_GuestAffiliateSignUp_LanguageToArabic: Execution Started");
		  Exttest = Extreport.startTest("TC006_GuestAffiliateSignUp_LanguageToArabic","Guest at the Landing page change language to Arabic");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  System.out.println("TC006_GuestAffiliateSignUp_LanguageToArabic : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC007_GuestAffiliateSignUp_LanguageToFrench(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC007_GuestAffiliateSignUp_LanguageToFrench: Execution Started");
		  Exttest = Extreport.startTest("TC007_GuestAffiliateSignUp_LanguageToFrench","Guest at the Landing page change language to French");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  System.out.println("TC007_GuestAffiliateSignUp_LanguageToFrench : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC008_GuestAffiliateSignUp_WhyStarzPlay_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC008_GuestAffiliateSignUp_WhyStarzPlay_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC008_GuestAffiliateSignUp_WhyStarzPlay_FromFooter","Guest goes to the Why STARZ PLAY? Page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyWhyStarzPlayFromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC008_GuestAffiliateSignUp_WhyStarzPlay_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC009_GuestAffiliateSignUp_HelpCenter_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC009_GuestAffiliateSignUp_HelpCenter_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC009_GuestAffiliateSignUp_HelpCenter_FromFooter","Guest goes to the Help Center page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyHelpCenter_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC009_GuestAffiliateSignUp_HelpCenter_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC010_GuestAffiliateSignUp_PartnerWithUS_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC010_GuestAffiliateSignUp_PartnerWithUS_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC010_GuestAffiliateSignUp_PartnerWithUS_FromFooter","Guest goes to the Partner with us page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyPartnerWithUs_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC010_GuestAffiliateSignUp_PartnerWithUS_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC011_GuestAffiliateSignUp_Company_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC011_GuestAffiliateSignUp_Company_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC011_GuestAffiliateSignUp_Company_FromFooter","Guest goes to the Company page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyCompanyPage_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC011_GuestAffiliateSignUp_Company_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC012_GuestAffiliateSignUp_TermsAndCond_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC012_GuestAffiliateSignUp_TermsAndCond_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC012_GuestAffiliateSignUp_TermsAndCond_FromFooter","Guest goes to the Terms & Conditions page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, "HtmlUnit", AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyTermsAndAconditions_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC012_GuestAffiliateSignUp_TermsAndCond_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC013_GuestAffiliateSignUp_PrivacyPolicy_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC013_GuestAffiliateSignUp_PrivacyPolicy_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC013_GuestAffiliateSignUp_PrivacyPolicy_FromFooter","Guest goes to the Privacy Policy page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, "HtmlUnit", AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyPrivacyPolicy_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC013_GuestAffiliateSignUp_PrivacyPolicy_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC014_GuestAffiliateSignUp_Blog_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC014_GuestAffiliateSignUp_Blog_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC014_GuestAffiliateSignUp_Blog_FromFooter","Guest goes to the Blog page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyBlog_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC014_GuestAffiliateSignUp_Blog_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC015_GuestAffiliateSignUp_Careers_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC015_GuestAffiliateSignUp_Careers_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC015_GuestAffiliateSignUp_Careers_FromFooter","Guest goes to the Careers page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyCareers_Footer(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC015_GuestAffiliateSignUp_Careers_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC016_GuestAffiliateSignUp_Facebook_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC016_GuestAffiliateSignUp_Facebook_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC016_GuestAffiliateSignUp_Facebook_FromFooter","Guest goes to the STARZ PLAY Facebook page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afverifyFaceBook_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC016_GuestAffiliateSignUp_Facebook_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC017_GuestAffiliateSignUp_Twitter_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC017_GuestAffiliateSignUp_Twitter_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC017_GuestAffiliateSignUp_Twitter_FromFooter","Guest goes to the STARZ PLAY Twitter page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyTwitter_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC017_GuestAffiliateSignUp_Twitter_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC018_GuestAffiliateSignUp_Instagram_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC018_GuestAffiliateSignUp_Instagram_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC018_GuestAffiliateSignUp_Instagram_FromFooter","Guest goes to the STARZ PLAY Instagram page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyInstagram_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC018_GuestAffiliateSignUp_Instagram_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC019_GuestAffiliateSignUp_Youtube_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC019_GuestAffiliateSignUp_Youtube_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC019_GuestAffiliateSignUp_Youtube_FromFooter","Guest goes to the STARZ PLAY YouTube page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyYouTube_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC019_GuestAffiliateSignUp_Youtube_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC020_GuestAffiliateSignUp_Appstore_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC020_GuestAffiliateSignUp_Appstore_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC020_GuestAffiliateSignUp_Appstore_FromFooter","Guest goes to the STARZ PLAY Appstore page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyAppStore_FromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC020_GuestAffiliateSignUp_Appstore_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC021_GuestAffiliateSignUp_PlayStore_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		int StepNum = 1;
		System.out.println("TC021_GuestAffiliateSignUp_PlayStore_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC021_GuestAffiliateSignUp_PlayStore_FromFooter","Guest goes to the STARZ PLAY Google Play Store page (from the footer)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyGooglePlayStore_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC021_GuestAffiliateSignUp_PlayStore_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC200_GuestAffiliateSignUp_MuleSoft_FromFooter(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		int StepNum = 1;
		System.out.println("TC200_GuestAffiliateSignUp_MuleSoft_FromFooter: Execution Started");
		  Exttest = Extreport.startTest("TC200_GuestAffiliateSignUp_MuleSoft_FromFooter","Guest goes to the MuleSoft Page from footer");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CFMethods.afVerifyMuleSoft_fromFooter(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC200_GuestAffiliateSignUp_MuleSoft_FromFooter : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC022_GuestAffiliateSignUp_NoUserName_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC022_GuestAffiliateSignUp_NoUserName_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC022_GuestAffiliateSignUp_NoUserName_NoPwd","Guest tries to sign up with no username and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_NoUsrName_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC022_GuestAffiliateSignUp_NoUserName_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC023_GuestAffiliateSignUp_WrongEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC023_GuestAffiliateSignUp_WrongEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC023_GuestAffiliateSignUp_WrongEmailFormat_NoPwd","Guest tries to sign up with wrong email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC023_GuestAffiliateSignUp_WrongEmailFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC024_GuestAffiliateSignUp_WrongMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC024_GuestAffiliateSignUp_WrongMSISDNFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC024_GuestAffiliateSignUp_WrongMSISDNFormat_NoPwd","Guest tries to sign up with wrong MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC024_GuestAffiliateSignUp_WrongMSISDNFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC025_GuestAffiliateSignUp_CorrectEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC025_GuestAffiliateSignUp_CorrectEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC025_GuestAffiliateSignUp_CorrectEmailFormat_NoPwd","Guest tries to sign up with correct email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_ValidEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC025_GuestAffiliateSignUp_CorrectEmailFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC026_GuestAffiliateSignUp_CorrectMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC026_GuestAffiliateSignUp_CorrectMSISDNFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC026_GuestAffiliateSignUp_CorrectMSISDNFormat_NoPwd","Guest tries to sign up with correct MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_ValidMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC026_GuestAffiliateSignUp_CorrectMSISDNFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC027_GuestAffiliateSignUp_NoUserName_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC027_GuestAffiliateSignUp_NoUserName_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC027_GuestAffiliateSignUp_NoUserName_ShortPwd","Guest tries to sign up with no username and short password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_NoUserName_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC027_GuestAffiliateSignUp_NoUserName_ShortPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC028_GuestAffiliateSignUp_WrongEmailFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC028_GuestAffiliateSignUp_WrongEmailFormat_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC028_GuestAffiliateSignUp_WrongEmailFormat_ShortPwd","Guest tries to sign up with wrong email format and short password (less than 6 characters)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_WrongEmailFormat_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC028_GuestAffiliateSignUp_WrongEmailFormat_ShortPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC029_GuestAffiliateSignUp_WrongMSISDNFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC029_GuestAffiliateSignUp_WrongMSISDNFormat_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC029_GuestAffiliateSignUp_WrongMSISDNFormat_ShortPwd","Guest tries to sign up with wrong MSISDN format and short password (less than 6 characters)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_WrongEmailFormat_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC029_GuestAffiliateSignUp_WrongMSISDNFormat_ShortPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC030_GuestAffiliateSignUp_CorrectEmailFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC030_GuestAffiliateSignUp_CorrectEmailFormat_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC030_GuestAffiliateSignUp_CorrectEmailFormat_ShortPwd","Guest tries to sign up with correct email format and short password (less than 6 characters)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_ValidEmailFormat_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC030_GuestAffiliateSignUp_CorrectEmailFormat_ShortPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC031_GuestAffiliateSignUp_CorrectMSISDNFormat_ShortPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC031_GuestAffiliateSignUp_CorrectMSISDNFormat_ShortPwd: Execution Started");
		  Exttest = Extreport.startTest("TC031_GuestAffiliateSignUp_CorrectMSISDNFormat_ShortPwd","Guest tries to sign up with correct MSISDN format and short password (less than 6 characters)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_ValidMSISDNFormat_ShortPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC031_GuestAffiliateSignUp_CorrectMSISDNFormat_ShortPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC032_GuestAffiliateSignUp_AlreadyRegisterd_Email(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC032_GuestAffiliateSignUp_AlreadyRegisterd_Email: Execution Started");
		  Exttest = Extreport.startTest("TC032_GuestAffiliateSignUp_AlreadyRegisterd_Email","Guest tries to sign up with an already registered email");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_AlreadyRegEmail(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC032_GuestAffiliateSignUp_AlreadyRegisterd_Email : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC033_GuestAffiliateSignUp_AlreadyRegisterd_MSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC033_GuestAffiliateSignUp_AlreadyRegisterd_MSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC033_GuestAffiliateSignUp_AlreadyRegisterd_MSISDN","Guest tries to sign up with an already registered MSISDN");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_AlreadyRegMSISDN(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC033_GuestAffiliateSignUp_AlreadyRegisterd_MSISDN : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC034_GuestAffiliateSignUp_Facebook_Cancel(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC034_GuestAffiliateSignUp_Facebook_Cancel: Execution Started");
		  Exttest = Extreport.startTest("TC034_GuestAffiliateSignUp_Facebook_Cancel","Guest tries to sign up with Facebook and cancelling the Facebook log in");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook_Cancel(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC034_GuestAffiliateSignUp_Facebook_Cancel : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC035_GuestAffiliateSignUp_Facebook_NotApprove(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC035_GuestAffiliateSignUp_Facebook_NotApprove: Execution Started");
		  Exttest = Extreport.startTest("TC035_GuestAffiliateSignUp_Facebook_NotApprove","Guest tries to sign up with Facebook and not approving STARZ PLAY application");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook_NotApprove(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC035_GuestAffiliateSignUp_Facebook_NotApprove : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC036_043_GuestAffiliateSignUp_With_Email_CreditCard(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC036_GuestAffiliateSignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC036_Guest_GuestAffiliateSignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC036_Guest_GuestAffiliateSignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC037_GuestAffiliateSignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC037_GuestAffiliateSignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC037_GuestAffiliateSignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC038_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC038_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC038_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC039_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC039_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC039_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC040_GuestAffiliateSignUpp_PaymentPage_MoPCreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC040_GuestAffiliateSignUpp_PaymentPage_MoPCreditCard","Guest chooses Credit Card as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoPCreditCard_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC040_GuestAffiliateSignUpp_PaymentPage_MoPCreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC041_GuestAffiliateSignUpp_CreditCard_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC041_GuestAffiliateSignUpp_CreditCard_ToArbic","Guest at Credit Card payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC041_GuestAffiliateSignUpp_CreditCard_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC042_GuestAffiliateSignUpp_CreditCard_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC042_GuestAffiliateSignUpp_CreditCard_ToFrench","Guest at Credit Card Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC042_GuestAffiliateSignUpp_CreditCard_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC043_GuestAffiliateSignUpp_With_CreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC043_GuestAffiliateSignUpp_With_CreditCard","Guest enters valid credit card details: First name, Last Name, Card Number, Exp Month, Exp Year, CVV");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC043_GuestAffiliateSignUpp_With_CreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC044_051_GuestAffiliateSignUp_Email_Voucher(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap,String VoucherCode) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC044_GuestAffiliateSignUp_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC044_GuestAffiliateSignUp_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC044_GuestAffiliateSignUp_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC045_GuestAffiliateSignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC045_GuestAffiliateSignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC045_GuestAffiliateSignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC046_GuestAffiliateSignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC046_GuestAffiliateSignUp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC046_GuestAffiliateSignUp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC047_GuestAffiliateSignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC047_GuestAffiliateSignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC047_GuestAffiliateSignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC048_GuestAffiliateSignUp_PaymentPage_MoPVoucher: Execution Started");
		   Exttest = Extreport.startTest("TC048_GuestAffiliateSignUp_PaymentPage_MoPVoucher","Guest chooses STARZPLAY Voucher as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoPVoucher_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC048_GuestAffiliateSignUp_PaymentPage_MoPVoucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC049_GuestAffiliateSignUp_Voucher_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC049_GuestAffiliateSignUp_Voucher_ToArbic","Guest at STARZPLAY Voucher payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC049_GuestAffiliateSignUp_Voucher_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC050_GuestAffiliateSignUp_Voucher_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC050_GuestAffiliateSignUp_Voucher_ToFrench","Guest at STARZPLAY Voucher Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC050_GuestAffiliateSignUp_Voucher_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC051_GuestAffiliateSignUp_With_Voucher: Execution Started");
		   Exttest = Extreport.startTest("TC051_GuestAffiliateSignUp_With_Voucher","Guest enters valid STARZPLAY Voucher details");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithVoucher(Extreport, Exttest, Driver, StepNum, ResultLocation,VoucherCode);
		   System.out.println("TC051_GuestAffiliateSignUp_With_Voucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC052_UserAffiliateSignUp_To_Login(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC052_UserAffiliateSignUp_To_Login: Execution Started");
		  Exttest = Extreport.startTest("TC052_UserAffiliateSignUp_To_Login","User at the Affiliate Sign Up page goes to the Log In page (from the header)");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  System.out.println("TC052_UserAffiliateSignUp_To_Login : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC053_UserAffiliateSignUp_LoginWith_NoUserName_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC053_UserAffiliateSignUp_LoginWith_NoUserName_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC053_UserAffiliateSignUp_LoginWith_NoUserName_NoPwd","User tries to Log In with no username and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_NoUsrName_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC053_UserAffiliateSignUp_LoginWith_NoUserName_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC054_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC054_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC054_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd","User tries to Log In with wrong email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC054_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC055_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd_DeleteEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC055_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC055_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd_DeleteEmail","User tries to Log In with wrong email format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC055_UserAffiliateSignUp_LoginWith_WrongEmailFormat_NoPwd_DeleteEmail : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC056_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC056_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC056_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd_DeleteEmail","User tries to Log In with wrong MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC056_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd_DeleteEmail : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC057_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd_DeleteMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC057_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd_DeleteMSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC057_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd_DeleteMSISDN","User tries to Log In with wrong MSISDN format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC057_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_NoPwd_DeleteMSISDN : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC058_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC058_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC058_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd","User tries to Log In with correct email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_CorrectEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC058_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC059_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd_DeleteEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC059_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC059_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd_DeleteEmail","User tries to Log In with correct email format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_CorrectEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC059_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_NoPwd_DeleteEmail : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC060_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC060_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC060_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd","User tries to Log In with correct MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_CorrectMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC060_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC061_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd_DeleteMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC061_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd_DeleteMSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC061_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd_DeleteMSISDN","User tries to Log In with correct MSISDN format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_CorrectMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC061_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_NoPwd_DeleteMSISDN : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC062_UserAffiliateSignUp_LoginWith_NoUserName_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC062_UserAffiliateSignUp_LoginWith_NoUserName_AnyPwd: Execution Started");
		  Exttest = Extreport.startTest("TC062_UserAffiliateSignUp_LoginWith_NoUserName_AnyPwd","User tries to Log In with no username and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_NoUsrName_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC062_UserAffiliateSignUp_LoginWith_NoUserName_AnyPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC063_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC063_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd: Execution Started");
		  Exttest = Extreport.startTest("TC063_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd","User tries to Log In with wrong email format and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC063_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC064_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd_DeleteEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC064_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC064_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd_DeleteEmail","User tries to Log In with wrong email format and any password and then deletes username");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC064_UserAffiliateSignUp_LoginWith_WrongEmailFormat_AnyPwd_DeleteEmail : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC065_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC065_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd: Execution Started");
		  Exttest = Extreport.startTest("TC065_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd","User tries to Log In with wrong MSISDN format and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC065_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC066_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd_DeleteMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC066_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd_DeleteMSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC066_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd_DeleteMSISDN","User tries to Log In with wrong MSISDN format and any password and then deletes username");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC066_UserAffiliateSignUp_LoginWith_WrongMSISDNFormat_AnyPwd_DeleteMSISDN : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC067_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC067_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd: Execution Started");
		  Exttest = Extreport.startTest("TC067_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd","User tries to Log In with correct email format and any non-matching password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidEmail_InvalidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC067_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC068_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC068_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd: Execution Started");
		  Exttest = Extreport.startTest("TC068_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd","User tries to Log In with correct MSISDN format and any non-matching password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidMSISDN_InvalidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC068_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC069_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd_DeleteBoth(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC069_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd_DeleteBoth: Execution Started");
		  Exttest = Extreport.startTest("TC069_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd_DeleteBoth","User tries to Log In with correct email format and any non-matching password and then deletes username/password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidEmail_InvalidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeletePassword_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC069_UserAffiliateSignUp_LoginWith_CorrectEmailFormat_WrongPAwd_DeleteBoth : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC070_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd_DeleteBoth(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC070_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd_DeleteBoth: Execution Started");
		  Exttest = Extreport.startTest("TC070_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd_DeleteBoth","User tries to Log In with correct MSISDN format and any non-matching password and then deletes username/password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidEmail_InvalidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeletePassword_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC070_UserAffiliateSignUp_LoginWith_CorrectMSISDNFormat_WrongPAwd_DeleteBoth : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC071_UserAffiliateSignUp_LoginWith_Facebook_Cancel(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC071_UserAffiliateSignUp_LoginWith_Facebook_Cancel: Execution Started");
		  Exttest = Extreport.startTest("TC071_UserAffiliateSignUp_LoginWith_Facebook_Cancel","User tries to Log In with Facebook and cancelling the Facebook Log In");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_facebook_Cancel(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC071_UserAffiliateSignUp_LoginWith_Facebook_Cancel : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC072_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_Active(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC072_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_Active: Execution Started");
		  Exttest = Extreport.startTest("TC072_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_Active","Active User logs in with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC072_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_Active : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC073_UserAffiliateSignUp_LoginWith_ValidMSISDNAndPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap,String ValidMSISDN) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC073_UserAffiliateSignUp_LoginWith_ValidMSISDNAndPwd: Execution Started");
		  Exttest = Extreport.startTest("TC073_UserAffiliateSignUp_LoginWith_ValidMSISDNAndPwd","User logs in with correct MSISDN and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidMSISDNAndPwd(Extreport, Exttest, Driver, StepNum, ValidMSISDN, ResultLocation);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC073_UserAffiliateSignUp_LoginWith_ValidMSISDNAndPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC074_UserAffiliateSignUp_LoginWith_Facebook_ValidCred_Active(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC074_UserAffiliateSignUp_LoginWith_Facebook_ValidCred_Active: Execution Started");
		  Exttest = Extreport.startTest("TC074_UserAffiliateSignUp_LoginWith_Facebook_ValidCred_Active","User logs in with correct registered Facebook active account");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("Facebook Account - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("Facebook Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook_ValidCredentials(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC074_UserAffiliateSignUp_LoginWith_Facebook_ValidCred_Active : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC075_UserAffiliateSignUp_RedirectToSignUPPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC075_UserAffiliateSignUp_RedirectToSignUPPage: Execution Started");
		  Exttest = Extreport.startTest("TC075_UserAffiliateSignUp_RedirectToSignUPPage","User tries to reach the Affiliate Sign Up page");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  String NewURL = Futil.fgetData("RedirectionURL", "AffiliateSignUpPageTest", "TC075_UserAffiliateSignUp_RedirectToSignUPPage", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = RedirectToSignNUPafterLogin( NewURL, Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC075_UserAffiliateSignUp_RedirectToSignUPPage : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC076_UserAffiliateSignUp_ToLoginPage_FromSignUpBox(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC076_UserAffiliateSignUp_ToLoginPage_FromSignUpBox: Execution Started");
		  Exttest = Extreport.startTest("TC076_UserAffiliateSignUp_ToLoginPage_FromSignUpBox","User logs in with correct MSISDN and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  System.out.println("TC076_UserAffiliateSignUp_ToLoginPage_FromSignUpBox : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC077_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC077_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC077_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_NoPwd","User tries to Log In with no username and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_NoUsrName_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC077_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC078_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC078_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC078_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd","User tries to Log In with wrong email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC078_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC079_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd_DeleteEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC079_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC079_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd_DeleteEmail","User tries to Log In with wrong email format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC079_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_NoPwd_DeleteEmail : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC080_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC080_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC080_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd","User tries to Log In with wrong MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC080_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC081_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd_DeleteMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC081_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd_DeleteMSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC081_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd_DeleteMSISDN","User tries to Log In with wrong MSISDN format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC081_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_NoPwd_DeleteMSISDN : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC082_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC082_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC082_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd","User tries to Log In with correct email format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_CorrectEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC082_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC083_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd_DeleteEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC083_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC083_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd_DeleteEmail","User tries to Log In with correct email format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_CorrectEmailFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC083_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_NoPwd_DeleteEmail : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	
	public void TC084_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC084_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd: Execution Started");
		  Exttest = Extreport.startTest("TC084_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd","User tries to Log In with correct MSISDN format and no password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_CorrectMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC084_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC085_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd_DeleteMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC085_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd_DeleteMSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC085_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd_DeleteMSISDN","User tries to Log In with correct MSISDN format and no password and then deletes username field");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_CorrectMSISDNFormat_NoPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC085_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_NoPwd_DeleteMSISDN : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC086_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC086_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_AnyPwd: Execution Started");
		  Exttest = Extreport.startTest("TC086_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_AnyPwd","User tries to Log In with no username and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_NoUsrName_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC086_UserAffiliateSignUp_LoginFromSignUpBox_NoUserName_AnyPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC087_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC087_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd: Execution Started");
		  Exttest = Extreport.startTest("TC087_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd","User tries to Log In with wrong email format and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC087_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC088_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd_DeleteEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC088_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd_DeleteEmail: Execution Started");
		  Exttest = Extreport.startTest("TC088_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd_DeleteEmail","User tries to Log In with wrong email format and any password and then deletes username");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC088_UserAffiliateSignUp_LoginFromSignUpBox_WrongEmailFormat_AnyPwd_DeleteEmail : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC089_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC089_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd: Execution Started");
		  Exttest = Extreport.startTest("TC089_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd","User tries to Log In with wrong MSISDN format and any password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC089_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC090_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd_DeleteMSISDN(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC090_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd_DeleteMSISDN: Execution Started");
		  Exttest = Extreport.startTest("TC090_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd_DeleteMSISDN","User tries to Log In with wrong MSISDN format and any password and then deletes username");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_WrongEmailFormat_AnyPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC090_UserAffiliateSignUp_LoginFromSignUpBox_WrongMSISDNFormat_AnyPwd_DeleteMSISDN : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC091_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC091_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd: Execution Started");
		  Exttest = Extreport.startTest("TC091_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd","User tries to Log In with correct email format and any non-matching password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidEmail_InvalidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC091_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC092_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC092_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd: Execution Started");
		  Exttest = Extreport.startTest("TC092_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd","User tries to Log In with correct MSISDN format and any non-matching password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidMSISDN_InvalidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC092_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC093_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd_DeleteBoth(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC093_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd_DeleteBoth: Execution Started");
		  Exttest = Extreport.startTest("TC093_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd_DeleteBoth","User tries to Log In with correct email format and any non-matching password and then deletes username/password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidEmail_InvalidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeletePassword_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC093_UserAffiliateSignUp_LoginFromSignUpBox_CorrectEmailFormat_WrongPAwd_DeleteBoth : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC094_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd_DeleteBoth(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC094_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd_DeleteBoth: Execution Started");
		  Exttest = Extreport.startTest("TC094_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd_DeleteBoth","User tries to Log In with correct MSISDN format and any non-matching password and then deletes username/password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidMSISDN_InvalidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeleteUserName_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CLoginMethods.afDeletePassword_VerifyErrorMessage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC094_UserAffiliateSignUp_LoginFromSignUpBox_CorrectMSISDNFormat_WrongPAwd_DeleteBoth : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC095_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_Cancel(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC095_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_Cancel: Execution Started");
		  Exttest = Extreport.startTest("TC095_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_Cancel","User tries to Log In with Facebook and cancelling the Facebook Log In");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_facebook_Cancel(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC095_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_Cancel : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC096_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_Active(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC096_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_Active: Execution Started");
		  Exttest = Extreport.startTest("TC096_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_Active","User logs in with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Email - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC096_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_Active : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC097_UserAffiliateSignUp_LoginFromSignUpBox_ValidMSISDNAndPwd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap,String MSISDN) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC097_UserAffiliateSignUp_LoginFromSignUpBox_ValidMSISDNAndPwd: Execution Started");
		  Exttest = Extreport.startTest("TC097_UserAffiliateSignUp_LoginFromSignUpBox_ValidMSISDNAndPwd","User logs in with correct MSISDN and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CLoginMethods.afLoginWith_ValidMSISDNAndPwd(Extreport, Exttest, Driver, StepNum, MSISDN, ResultLocation);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC097_UserAffiliateSignUp_LoginFromSignUpBox_ValidMSISDNAndPwd : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC098_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_ValidCred_Active(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC098_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_ValidCred_Active: Execution Started");
		  Exttest = Extreport.startTest("TC098_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_ValidCred_Active","User logs in with correct registered Facebook account");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("Facebook Account - Active", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("Facebook Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook_ValidCredentials(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  StepNum = CLoginMethods.afVerifyLoginAction(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC098_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_ValidCred_Active : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC099_GuestAffiliateSignUp_Facebook_NotApprove_AccessToEmail(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC099_GuestAffiliateSignUp_Facebook_NotApprove_AccessToEmail: Execution Started");
		  Exttest = Extreport.startTest("TC099_GuestAffiliateSignUp_Facebook_NotApprove_AccessToEmail","Guest tries to sign up with Facebook and not approving access to email address");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook_NotApprove_AccessToEmail(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC099_GuestAffiliateSignUp_Facebook_NotApprove_AccessToEmail : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC100_TC102_SignUP_ProspectUser_Email_EndToEnd_Happy(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC100_ProspectUser_SignUp_with_Email: Execution Started");
		  Exttest = Extreport.startTest("TC100_ProspectUser_SignUp_with_Email","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC100_ProspectUser_SignUp_with_Email : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC101_ProspectUser_All_MOPs","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC101_ProspectUser_AllType_MOPs : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC102_ProspectUser_Navigate_to_Home","Guest goes to the Home page by clicking STARZ PLAY(from the header)");
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  Extreport.endTest(Exttest);
		  System.out.println("TC102_ProspectUser_Naviagete_to_Home : Execution Finished  "); 
		  
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC103_TC105_SignUP_ProspectUser_Facebook_EndToEnd_Happy(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		
		int StepNum = 1;
		System.out.println("TC103_ProspectUser_SignUp_with_Facebook: Execution Started");
		  Exttest = Extreport.startTest("TC103_ProspectUser_SignUp_with_Facebook","Guest signs up with valid facebook credential");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserNameForProspectUserSignUP", "OneTimeData", "TC103_ProspectUser_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPwdForProspectUserSignUP", "OneTimeData", "TC103_ProspectUser_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook_ProspectUser(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC103_ProspectUser_SignUp_with_Facebook : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC104_ProspectUser_All_MOPs","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC104_ProspectUser_All_MOPs : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC105_ProspectUser_Naviagete_to_Home","Guest goes to the Home page by clicking STARZ PLAY(from the header)");
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  Extreport.endTest(Exttest);
		  System.out.println("TC105_ProspectUser_Naviagete_to_Home : Execution Finished  "); 
		  
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC106_TC123_SignUP_Email_CreditCard_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		int StepNum = 1;
		System.out.println("TC106_SignUp_with_Email: Execution Started");
		  Exttest = Extreport.startTest("TC106_SignUp_with_Email","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC106_SignUp_with_Email : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC107_SignUp_User_All_MOPs","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC107_SignUp_User_All_MOPs : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		   System.out.println("TC108_GuestAffiliateSignUp_MoPCreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC108_GuestAffiliateSignUp_MoPCreditCard","Guest chooses Credit Card as MoP at Payment page");
		   StepNum = CMoPMethods.afMoPCreditCard_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC108_GuestAffiliateSignUp_MoPCreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC109_GuestAffiliateSignUp_VerifyCreditCardPage: Execution Started");
		   Exttest = Extreport.startTest("TC109_GuestAffiliateSignUp_VerifyCreditCardPage","Guest should be directed to Credit Card Payment page");
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC109_GuestAffiliateSignUp_VerifyCreditCardPage : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC110_GuestAffiliateSignUpp_CreditCard_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC110_GuestAffiliateSignUpp_CreditCard_ToArbic","Guest at Credit Card payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC110_GuestAffiliateSignUpp_CreditCard_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC111_GuestAffiliateSignUpp_CreditCard_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC111_GuestAffiliateSignUpp_CreditCard_ToFrench","Guest at Credit Card Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC111_GuestAffiliateSignUpp_CreditCard_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC112_SignUpp_CreditCard_NoDetails: Execution Started");
		   Exttest = Extreport.startTest("TC112_SignUpp_CreditCard_NoDetails","Guest/User tries to complete payment with no details");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoDetails(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC112_SignUpp_CreditCard_NoDetails : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC113_SignUpp_CreditCard_NoFirstName: Execution Started");
		   Exttest = Extreport.startTest("TC113_SignUpp_CreditCard_NoFirstName","Guest/User tries to complete payment with missing First Name");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoFirstName(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC113_SignUpp_CreditCard_NoFirstName : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC114_SignUpp_CreditCard_NoLastName: Execution Started");
		   Exttest = Extreport.startTest("TC114_SignUpp_CreditCard_NoLastName","Guest/User tries to complete payment with missing last Name");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoLastName(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC114_SignUpp_CreditCard_NoLastName : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC115_SignUpp_CreditCard_NoCreditCardNumber: Execution Started");
		   Exttest = Extreport.startTest("TC115_SignUpp_CreditCard_NoCreditCardNumber","Guest/User tries to complete payment with missing Credit Card Number");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoCreditCardNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC115_SignUpp_CreditCard_NoCreditCardNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC116_SignUpp_CreditCard_NoExpiryMonth: Execution Started");
		   Exttest = Extreport.startTest("TC116_SignUpp_CreditCard_NoExpiryMonth","Guest/User tries to complete payment with missing Expiry Month");
		   StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CHMethods.afNavigateToCreditCardPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoExpiryMonth(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC116_SignUpp_CreditCard_NoExpiryMonth : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC117_SignUpp_CreditCard_NoExpiryYear: Execution Started");
		   Exttest = Extreport.startTest("TC117_SignUpp_CreditCard_NoExpiryYear","Guest/User tries to complete payment with missing Expiry Year");
		   StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CHMethods.afNavigateToCreditCardPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoExpiryYear(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC117_SignUpp_CreditCard_NoExpiryYear : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC118_SignUpp_CreditCard_NoCreditCardCVV: Execution Started");
		   Exttest = Extreport.startTest("TC118_SignUpp_CreditCard_NoCreditCardCVV","Guest/User tries to complete payment with missing Credit Card CVV");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoCVV(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC118_SignUpp_CreditCard_NoCreditCardCVV : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC119_SignUpp_CreditCard_WrongCreditCardFormat: Execution Started");
		   Exttest = Extreport.startTest("TC119_SignUpp_CreditCard_WrongCreditCardFormat","Guest/User tries to complete pament with invalid Credit Card Number");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_WrongCreditCardFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC119_SignUpp_CreditCard_WrongCreditCardFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC120_SignUpp_CreditCard_InvalidCreditCardExpiryDate: Execution Started");
		   Exttest = Extreport.startTest("TC120_SignUpp_CreditCard_InvalidCreditCardExpiryDate","Guest/User tries to complete payment with invalid Expiration Date");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidExpiryDate(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC120_SignUpp_CreditCard_InvalidCreditCardExpiryDate : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC121_SignUpp_CreditCard_InvalidCVVFormat: Execution Started");
		   Exttest = Extreport.startTest("TC121_SignUpp_CreditCard_InvalidCVVFormat","Guest/User tries to complete payment with invalid CVV format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidCVV(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC121_SignUpp_CreditCard_InvalidCVVFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC122_SignUpp_CreditCard_InvalidFirstNameFormat: Execution Started");
		   Exttest = Extreport.startTest("TC122_SignUpp_CreditCard_InvalidFirstNameFormat","Guest/User tries to complete payment with invalid First Name format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidFirstNameFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC122_SignUpp_CreditCard_InvalidFirstNameFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC123_SignUpp_CreditCard_InvalidLastNameFormat: Execution Started");
		   Exttest = Extreport.startTest("TC123_SignUpp_CreditCard_InvalidLastNameFormat","Guest/User tries to complete payment with invalid Last Name format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidLastNameFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC123_SignUpp_CreditCard_InvalidLastNameFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		  
		  Driver.close();
	}
	
	public void TC124_TC128_SignUP_Email_Voucher_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			CSignUpMethods = new CommonSignUpMethods();
			CHMethods = new CommonHeaderMethods();
			CMoPMethods = new CommonMoPMethods();
			int StepNum = 1;
			System.out.println("TC124_SignUp_with_Email: Execution Started");
			  Exttest = Extreport.startTest("TC124_SignUp_with_Email","Guest signs up with correct email and password");
			  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
			  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
			  StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
			  Extreport.endTest(Exttest);
			  System.out.println("TC124_SignUp_with_Email : Execution Finished  ");
			  Extreport.flush();
			  Extreport.endTest(Exttest);
			  
			  Exttest = Extreport.startTest("TC125_SignUp_User_All_MOPs_verifcation","Guest at Payment page should see correct MoPs as per country of guest");
			  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
			  Extreport.endTest(Exttest);
			  System.out.println("TC125_SignUp_User_All_MOPs_verifcation : Execution Finished  "); 
			  Extreport.flush();
			  Extreport.endTest(Exttest);
			  
			   System.out.println("TC126_GuestAffiliateSignUp_MoPVoucher: Execution Started");
			   Exttest = Extreport.startTest("TC126_GuestAffiliateSignUp_MoPVoucher","Guest chooses STARZPLAY Voucher as MoP at Payment page");
			   StepNum = CMoPMethods.afMoPVoucher_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
			   System.out.println("TC126_GuestAffiliateSignUp_MoPVoucher : Execution Finished");
			   Extreport.flush();
			   Extreport.endTest(Exttest);
			   
			   System.out.println("TC127_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode: Execution Started");
			   Exttest = Extreport.startTest("TC127_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode","Guest tries to complete process without entering Voucher Code");
			   StepNum = CMoPMethods.afSignUPWithVoucher_NoVoucherCode(Extreport, Exttest, Driver, StepNum, ResultLocation);
			   System.out.println("TC127_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode : Execution Finished");
			   Extreport.flush();
			   Extreport.endTest(Exttest);
			   
			   System.out.println("TC128_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode: Execution Started");
			   Exttest = Extreport.startTest("TC128_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode","Guest tries to complete process with an invalid Voucher");
			   StepNum = CMoPMethods.afSignUPWithVoucher_InvalidVoucherCode(Extreport, Exttest, Driver, StepNum, ResultLocation);
			   System.out.println("TC128_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode : Execution Finished");
			   Extreport.flush();
			   Extreport.endTest(Exttest);
		  
		  Driver.close();
	}
	
	public void TC129_TC146_SignUP_Facebook_CreditCard_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		int StepNum = 1;
		System.out.println("TC129_SignUp_with_Facebook: Execution Started");
		  Exttest = Extreport.startTest("TC129_SignUp_with_Facebook","Guest signs up with Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserName", "AffiliateSignUpPageTest", "TC129_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPassword", "AffiliateSignUpPageTest", "TC129_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC129_SignUp_with_Facebook : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC130_SignUp_User_All_MOPs","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC130_SignUp_User_All_MOPs : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		   System.out.println("TC131_GuestAffiliateSignUp_MoPCreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC131_GuestAffiliateSignUp_MoPCreditCard","Guest chooses Credit Card as MoP at Payment page");
		   StepNum = CMoPMethods.afMoPCreditCard_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC131_GuestAffiliateSignUp_MoPCreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC132_GuestAffiliateSignUp_VerifyCreditCardPage: Execution Started");
		   Exttest = Extreport.startTest("TC132_GuestAffiliateSignUp_VerifyCreditCardPage","Guest should be directed to Credit Card Payment page");
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC132_GuestAffiliateSignUp_VerifyCreditCardPage : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC133_GuestAffiliateSignUpp_CreditCard_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC133_GuestAffiliateSignUpp_CreditCard_ToArbic","Guest at Credit Card payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC133_GuestAffiliateSignUpp_CreditCard_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC134_GuestAffiliateSignUpp_CreditCard_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC134_GuestAffiliateSignUpp_CreditCard_ToFrench","Guest at Credit Card Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC134_GuestAffiliateSignUpp_CreditCard_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC135_SignUpp_CreditCard_NoDetails: Execution Started");
		   Exttest = Extreport.startTest("TC135_SignUpp_CreditCard_NoDetails","Guest/User tries to complete payment with no details");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoDetails(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC135_SignUpp_CreditCard_NoDetails : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC136_SignUpp_CreditCard_NoFirstName: Execution Started");
		   Exttest = Extreport.startTest("TC136_SignUpp_CreditCard_NoFirstName","Guest/User tries to complete payment with missing First Name");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoFirstName(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC136_SignUpp_CreditCard_NoFirstName : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC137_SignUpp_CreditCard_NoLastName: Execution Started");
		   Exttest = Extreport.startTest("TC137_SignUpp_CreditCard_NoLastName","Guest/User tries to complete payment with missing last Name");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoLastName(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC137_SignUpp_CreditCard_NoLastName : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC138_SignUpp_CreditCard_NoCreditCardNumber: Execution Started");
		   Exttest = Extreport.startTest("TC138_SignUpp_CreditCard_NoCreditCardNumber","Guest/User tries to complete payment with missing Credit Card Number");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoCreditCardNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC138_SignUpp_CreditCard_NoCreditCardNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC139_SignUpp_CreditCard_NoExpiryMonth: Execution Started");
		   Exttest = Extreport.startTest("TC139_SignUpp_CreditCard_NoExpiryMonth","Guest/User tries to complete payment with missing Expiry Month");
		   StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CHMethods.afNavigateToCreditCardPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoExpiryMonth(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC139_SignUpp_CreditCard_NoExpiryMonth : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC140_SignUpp_CreditCard_NoExpiryYear: Execution Started");
		   Exttest = Extreport.startTest("TC140_SignUpp_CreditCard_NoExpiryYear","Guest/User tries to complete payment with missing Expiry Year");
		   StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CHMethods.afNavigateToCreditCardPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoExpiryYear(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC140_SignUpp_CreditCard_NoExpiryYear : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC141_SignUpp_CreditCard_NoCreditCardCVV: Execution Started");
		   Exttest = Extreport.startTest("TC141_SignUpp_CreditCard_NoCreditCardCVV","Guest/User tries to complete payment with missing Credit Card CVV");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_NoCVV(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC141_SignUpp_CreditCard_NoCreditCardCVV : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC142_SignUpp_CreditCard_WrongCreditCardFormat: Execution Started");
		   Exttest = Extreport.startTest("TC142_SignUpp_CreditCard_WrongCreditCardFormat","Guest/User tries to complete pament with invalid Credit Card Number");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_WrongCreditCardFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC142_SignUpp_CreditCard_WrongCreditCardFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC143_SignUpp_CreditCard_InvalidCreditCardExpiryDate: Execution Started");
		   Exttest = Extreport.startTest("TC143_SignUpp_CreditCard_InvalidCreditCardExpiryDate","Guest/User tries to complete payment with invalid Expiration Date");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidExpiryDate(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC143_SignUpp_CreditCard_InvalidCreditCardExpiryDate : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC144_SignUpp_CreditCard_InvalidCVVFormat: Execution Started");
		   Exttest = Extreport.startTest("TC144_SignUpp_CreditCard_InvalidCVVFormat","Guest/User tries to complete payment with invalid CVV format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidCVV(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC144_SignUpp_CreditCard_InvalidCVVFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC145_SignUpp_CreditCard_InvalidFirstNameFormat: Execution Started");
		   Exttest = Extreport.startTest("TC145_SignUpp_CreditCard_InvalidFirstNameFormat","Guest/User tries to complete payment with invalid First Name format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidFirstNameFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC145_SignUpp_CreditCard_InvalidFirstNameFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC146_SignUpp_CreditCard_InvalidLastNameFormat: Execution Started");
		   Exttest = Extreport.startTest("TC146_SignUpp_CreditCard_InvalidLastNameFormat","Guest/User tries to complete payment with invalid Last Name format");
		   StepNum = CMoPMethods.afSignUPWithCreditCard_InvalidLastNameFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC146_SignUpp_CreditCard_InvalidLastNameFormat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		  
		  Driver.close();
	}
	
	public void TC147_TC154_SignUp_Facebook_CreditCard_Happy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC147_SignUp_with_Facebook: Execution Started");
		  Exttest = Extreport.startTest("TC147_SignUp_with_Facebook_MoPCreditCard","Guest signs up with Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserNameForHappySignUP", "OneTimeData", "TC147_SignUp_with_Facebook_MoPCreditCard", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPasswordForHappySignUP", "OneTimeData", "TC147_SignUp_with_Facebook_MoPCreditCard", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC147_SignUp_with_Facebook_MoPCreditCard : Execution Finished  "); 
		   
		   System.out.println("TC148_GuestAffiliateSignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC148_GuestAffiliateSignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC148_GuestAffiliateSignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC149_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC149_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC149_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC150_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC150_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC150_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC151_GuestAffiliateSignUpp_PaymentPage_MoPCreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC151_GuestAffiliateSignUpp_PaymentPage_MoPCreditCard","Guest chooses Credit Card as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoPCreditCard_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC151_GuestAffiliateSignUpp_PaymentPage_MoPCreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC152_GuestAffiliateSignUpp_CreditCard_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC152_GuestAffiliateSignUpp_CreditCard_ToArbic","Guest at Credit Card payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC152_GuestAffiliateSignUpp_CreditCard_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC153_GuestAffiliateSignUpp_CreditCard_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC153_GuestAffiliateSignUpp_CreditCard_ToFrench","Guest at Credit Card Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC153_GuestAffiliateSignUpp_CreditCard_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC154_GuestAffiliateSignUpp_With_CreditCard: Execution Started");
		   Exttest = Extreport.startTest("TC154_GuestAffiliateSignUpp_With_CreditCard","Guest enters valid credit card details: First name, Last Name, Card Number, Exp Month, Exp Year, CVV");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyCreditCardPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithCreditCard(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC154_GuestAffiliateSignUpp_With_CreditCard : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC155_TC159_SignUP_Facebook_Voucher_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		int StepNum = 1;
		System.out.println("TC155_SignUp_with_Facebook: Execution Started");
		  Exttest = Extreport.startTest("TC155_SignUp_with_Facebook","Guest signs up with Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserName", "AffiliateSignUpPageTest", "TC155_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPassword", "AffiliateSignUpPageTest", "TC155_SignUp_with_Facebook", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC155_SignUp_with_Facebook : Execution Finished  "); 
		  
		  Exttest = Extreport.startTest("TC156_SignUp_User_All_MOPs_verifcation","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC156_SignUp_User_All_MOPs_verifcation : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		   System.out.println("TC157_GuestAffiliateSignUp_MoPVoucher: Execution Started");
		   Exttest = Extreport.startTest("TC157_GuestAffiliateSignUp_MoPVoucher","Guest chooses STARZPLAY Voucher as MoP at Payment page");
		   StepNum = CMoPMethods.afMoPVoucher_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC157_GuestAffiliateSignUp_MoPVoucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC158_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode: Execution Started");
		   Exttest = Extreport.startTest("TC158_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode","Guest tries to complete process without entering Voucher Code");
		   StepNum = CMoPMethods.afSignUPWithVoucher_NoVoucherCode(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC158_GuestAffiliateSignUp_MoPVoucher_NoVoucherCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC159_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode: Execution Started");
		   Exttest = Extreport.startTest("TC159_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode","Guest tries to complete process with an invalid Voucher");
		   StepNum = CMoPMethods.afSignUPWithVoucher_InvalidVoucherCode(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC159_GuestAffiliateSignUp_MoPVoucher_InvalidVoucherCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
	  
	  Driver.close();
}
	
	public void TC160_TC167_SignUp_Facebook_Voucher_Happy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap,String VoucherCode) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC160_SignUp_with_Facebook_MoPVoucher: Execution Started");
		  Exttest = Extreport.startTest("TC160_SignUp_with_Facebook_MoPVoucher","Guest signs up with Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("FBUserNameForHappySignUP", "OneTimeData", "TC160_SignUp_with_Facebook_MoPVoucher", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("FBPasswordForHappySignUP", "OneTimeData", "TC160_SignUp_with_Facebook_MoPVoucher", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CSignUpMethods.afSignUpWith_Facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  Extreport.endTest(Exttest);
		  System.out.println("TC160_SignUp_with_Facebook_MoPVoucher : Execution Finished  "); 
		   
		   System.out.println("TC161_GuestAffiliateSignUp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC161_GuestAffiliateSignUp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC161_GuestAffiliateSignUp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC162_GuestAffiliateSignUp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC162_GuestAffiliateSignUp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC162_GuestAffiliateSignUp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC163_GuestAffiliateSignUp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC163_GuestAffiliateSignUp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC163_GuestAffiliateSignUp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC164_GuestAffiliateSignUp_PaymentPage_MoPVoucher: Execution Started");
		   Exttest = Extreport.startTest("TC164_GuestAffiliateSignUp_PaymentPage_MoPVoucher","Guest chooses STARZPLAY Voucher as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoPVoucher_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC164_GuestAffiliateSignUp_PaymentPage_MoPVoucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC165_GuestAffiliateSignUp_Voucher_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC165_GuestAffiliateSignUp_Voucher_ToArbic","Guest at STARZPLAY Voucher payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC165_GuestAffiliateSignUp_Voucher_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC166_GuestAffiliateSignUp_Voucher_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC166_GuestAffiliateSignUp_Voucher_ToFrench","Guest at STARZPLAY Voucher Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC166_GuestAffiliateSignUp_Voucher_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC167_GuestAffiliateSignUp_With_Voucher: Execution Started");
		   Exttest = Extreport.startTest("TC167_GuestAffiliateSignUp_With_Voucher","Guest enters valid STARZPLAY Voucher details");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyVoucherPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afSignUPWithVoucher(Extreport, Exttest, Driver, StepNum, ResultLocation,VoucherCode);
		   System.out.println("TC167_GuestAffiliateSignUp_With_Voucher : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC168_UserAffiliateSignUp_LoginWith_Facebook_UnRegisteredAccount(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC168_UserAffiliateSignUp_LoginWith_Facebook_UnRegisteredAccount: Execution Started");
		  Exttest = Extreport.startTest("TC168_UserAffiliateSignUp_LoginWith_Facebook_UnRegisteredAccount","User tries to Log In with an unregistered Facebook account");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("Facebook Account - Unregistered", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("Facebook Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  StepNum = CLoginMethods.afVerifyUnRegistered_Facebook_Error(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC168_UserAffiliateSignUp_LoginWith_Facebook_UnRegisteredAccount : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC169_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_ProspectUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC169_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_ProspectUser: Execution Started");
		  Exttest = Extreport.startTest("TC169_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_ProspectUser","User logs in with correct prospect email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Email - Prospect", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  System.out.println("TC169_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_ProspectUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC170_UserAffiliateSignUp_LoginWith_Facebook_ProspectUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC170_UserAffiliateSignUp_LoginWith_Facebook_ProspectUser: Execution Started");
		  Exttest = Extreport.startTest("TC170_UserAffiliateSignUp_LoginWith_Facebook_ProspectUser","User logs in with correct prospect Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Facebook Account - Prospect", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Facebook Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password);
		  StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  System.out.println("TC170_UserAffiliateSignUp_LoginWith_Facebook_ProspectUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC171_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DisconnectedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC171_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DisconnectedUser: Execution Started");
		  Exttest = Extreport.startTest("TC171_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DisconnectedUser","User logs in with correct Disconnected email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Email - Disconnected", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Disconnected User", "Your subscription is currently deactivated. To reactivate your subscription, please enter valid payment details to your account. ACTIVATE");
		  System.out.println("TC171_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DisconnectedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC172_UserAffiliateSignUp_LoginWith_Facebook_DisconnectedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC172_UserAffiliateSignUp_LoginWith_Facebook_DisconnectedUser: Execution Started");
		  Exttest = Extreport.startTest("TC172_UserAffiliateSignUp_LoginWith_Facebook_DisconnectedUser","User logs in with correct Disconnected facebook email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Facebook Account - Disconnected", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Disconnected User", "Your subscription is currently deactivated. To reactivate your subscription, please enter valid payment details to your account. ACTIVATE");
		  System.out.println("TC172_UserAffiliateSignUp_LoginWith_Facebook_DisconnectedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC173_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DeactivatedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC173_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DeactivatedUser: Execution Started");
		  Exttest = Extreport.startTest("TC173_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DeactivatedUser","User logs in with correct Deactivated email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Email - Deactivated", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC173_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DeactivatedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC174_UserAffiliateSignUp_LoginWith_Facebook_DeactivatedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC174_UserAffiliateSignUp_LoginWith_Facebook_DeactivatedUser: Execution Started");
		  Exttest = Extreport.startTest("TC174_UserAffiliateSignUp_LoginWith_Facebook_DeactivatedUser","User logs in with correct Deactivated facebook email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNaviagteToLoginFromAffilatePageHeader(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Facebook Account - Deactivated", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC174_UserAffiliateSignUp_LoginWith_Facebook_DeactivatedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC175_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_UnRegisteredAccount(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CLoginMethods = new CommonLoginMethods();
		int StepNum = 1;
		System.out.println("TC175_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_UnRegisteredAccount: Execution Started");
		  Exttest = Extreport.startTest("TC175_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_UnRegisteredAccount","User tries to Log In with an unregistered Facebook account");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String fbemail = Futil.fgetData("Facebook Account - Unregistered", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String fbPass = Futil.fgetData("Facebook Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation,fbemail,fbPass);
		  StepNum = CLoginMethods.afVerifyUnRegistered_Facebook_Error(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC175_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_UnRegisteredAccount : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC176_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_ProspectUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC176_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_ProspectUser: Execution Started");
		  Exttest = Extreport.startTest("TC176_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_ProspectUser","User logs in with correct prospect email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Email - Prospect", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  System.out.println("TC176_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_ProspectUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC177_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_ProspectUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC177_UserAffiliateSignUp_LoginWith_Facebook_ProspectUser: Execution Started");
		  Exttest = Extreport.startTest("TC177_UserAffiliateSignUp_LoginWith_Facebook_ProspectUser","User logs in with correct prospect Facebook");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Facebook Account - Prospect", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Facebook Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password);
		  StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Prospect User", "Welcome back! click here to start your FREE TRIAL");
		  System.out.println("TC177_UserAffiliateSignUp_LoginWith_Facebook_ProspectUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC178_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_DisconnectedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC178_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DisconnectedUser: Execution Started");
		  Exttest = Extreport.startTest("TC178_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DisconnectedUser","User logs in with correct Disconnected email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Email - Disconnected", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Disconnected User", "Your subscription is currently deactivated. To reactivate your subscription, please enter valid payment details to your account. ACTIVATE");
		  System.out.println("TC178_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DisconnectedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC179_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_DisconnectedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC179_UserAffiliateSignUp_LoginWith_Facebook_DisconnectedUser: Execution Started");
		  Exttest = Extreport.startTest("TC179_UserAffiliateSignUp_LoginWith_Facebook_DisconnectedUser","User logs in with correct Disconnected facebook email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Facebook Account - Disconnected", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  StepNum = CHMethods.afVerifyGoToHomeByLogo(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  WebElement objAlert= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='alert']/div");
		  StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objAlert, "Disconnected User", "Your subscription is currently deactivated. To reactivate your subscription, please enter valid payment details to your account. ACTIVATE");
		  System.out.println("TC179_UserAffiliateSignUp_LoginWith_Facebook_DisconnectedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC180_UserAffiliateSignUp_LoginFromSignUpBox_ValidEmailAndPwd_DeactivatedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC180_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DeactivatedUser: Execution Started");
		  Exttest = Extreport.startTest("TC180_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DeactivatedUser","User logs in with correct Deactivated email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Email - Deactivated", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_ValidEmailAndPwd(Extreport, Exttest, Driver, StepNum, ResultLocation,EmailId,Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC180_UserAffiliateSignUp_LoginWith_ValidEmailAndPwd_DeactivatedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC181_UserAffiliateSignUp_LoginFromSignUpBox_Facebook_DeactivatedUser(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CLoginMethods = new CommonLoginMethods();
		CHMethods = new CommonHeaderMethods();
		int StepNum = 1;
		System.out.println("TC181_UserAffiliateSignUp_LoginWith_Facebook_DeactivatedUser: Execution Started");
		  Exttest = Extreport.startTest("TC181_UserAffiliateSignUp_LoginWith_Facebook_DeactivatedUser","User logs in with correct Deactivated facebook email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = afNavigateToLoginFromAffilatePageSignUpBox(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  String EmailId = Futil.fgetData("Facebook Account - Deactivated", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  String Password = Futil.fgetData("Email Common Password", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		  StepNum = CLoginMethods.afLoginWith_facebook(Extreport, Exttest, Driver, StepNum, ResultLocation, EmailId, Password);
		  StepNum = CSignUpMethods.afVerifySettingsPaymentPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC181_UserAffiliateSignUp_LoginWith_Facebook_DeactivatedUser : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC182_190_GuestAffiliateSignUp_WithEmail_MoP_Du_Weekly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC182_GuestAffiliateSignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC182_GuestAffiliateSignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC182_GuestAffiliateSignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC183_GuestAffiliateSignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC183_GuestAffiliateSignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC183_GuestAffiliateSignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC184_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC184_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC184_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC185_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC185_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC185_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC186_GuestAffiliateSignUpp_PaymentPage_MoP_DU: Execution Started");
		   Exttest = Extreport.startTest("TC186_GuestAffiliateSignUpp_PaymentPage_MoP_DU","Guest chooses DU as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_DU_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC186_GuestAffiliateSignUpp_PaymentPage_MoP_DU : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC187_GuestAffiliateSignUpp_DU_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC187_GuestAffiliateSignUpp_DU_ToArbic","Guest at DU payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC187_GuestAffiliateSignUpp_DU_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC188_GuestAffiliateSignUpp_DU_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC188_GuestAffiliateSignUpp_DU_ToFrench","Guest at DU Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC188_GuestAffiliateSignUpp_DU_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC189_GuestAffiliateSignUpp_DU_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC189_GuestAffiliateSignUpp_DU_Billing_Weekly","Guest select billing frequency as weekly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a weekly subscription payment of AED 10 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC189_GuestAffiliateSignUpp_DU_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC190_GuestAffiliateSignUpp_With_Active_DU_Registration: Execution Started");
		   Exttest = Extreport.startTest("TC190_GuestAffiliateSignUpp_With_Active_DU_Registration","Guest enters a valid du number and requests activation code OTP");
		   String DuNumber = Futil.fgetData("Du - Mbile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValidDU_Registration(DuNumber,Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC190_GuestAffiliateSignUpp_With_Active_DU_Registration : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC191_199_GuestAffiliateSignUp_WithEmail_MoP_Du_Monthly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC191_GuestAffiliateSignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC191_GuestAffiliateSignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC191_GuestAffiliateSignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC192_GuestAffiliateSignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC192_GuestAffiliateSignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC192_GuestAffiliateSignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC193_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC193_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC193_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC194_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC194_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC194_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC195_GuestAffiliateSignUpp_PaymentPage_MoP_DU: Execution Started");
		   Exttest = Extreport.startTest("TC195_GuestAffiliateSignUpp_PaymentPage_MoP_DU","Guest chooses DU as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_DU_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC195_GuestAffiliateSignUpp_PaymentPage_MoP_DU : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC196_GuestAffiliateSignUpp_DU_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC196_GuestAffiliateSignUpp_DU_ToArbic","Guest at DU payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC196_GuestAffiliateSignUpp_DU_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC197_GuestAffiliateSignUpp_DU_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC197_GuestAffiliateSignUpp_DU_ToFrench","Guest at DU Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC197_GuestAffiliateSignUpp_DU_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC198_GuestAffiliateSignUpp_DU_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC198_GuestAffiliateSignUpp_DU_Billing_Monthly","Guest select billing frequency as Monthly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyDuPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a monthly subscription payment of AED 30 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC198_GuestAffiliateSignUpp_DU_Billing_Monthly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC199_GuestAffiliateSignUpp_With_Active_DU_Registration: Execution Started");
		   Exttest = Extreport.startTest("TC199_GuestAffiliateSignUpp_With_Active_DU_Registration","Guest enters a valid du number and requests activation code OTP");
		   String DuNumber = Futil.fgetData("Du - Mbile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValidDU_Registration(DuNumber,Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC199_GuestAffiliateSignUpp_With_Active_DU_Registration : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC201_TC219_SignUP_Email_DU_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		String DuNumber = Futil.fgetData("Du - Mbile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		int StepNum = 1;
		System.out.println("TC201_SignUp_with_Email: Execution Started");
		  Exttest = Extreport.startTest("TC201_SignUp_with_Email","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC201_SignUp_with_Email : Execution Finished  ");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		  Exttest = Extreport.startTest("TC202_SignUp_User_All_MOPs_verifcation","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC202_SignUp_User_All_MOPs_verifcation : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		  System.out.println("TC203_GuestAffiliateSignUpp_PaymentPage_MoP_DU: Execution Started");
		   Exttest = Extreport.startTest("TC203_GuestAffiliateSignUpp_PaymentPage_MoP_DU","Guest chooses DU as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_DU_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC203_GuestAffiliateSignUpp_PaymentPage_MoP_DU : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC204_GuestAffiliateSignUpp_DU_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC204_GuestAffiliateSignUpp_DU_Billing_Weekly","Guest select billing frequency as weekly");
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a weekly subscription payment of AED 10 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC204_GuestAffiliateSignUpp_DU_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC205_GuestAffiliateSignUp_MoP_DU_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC205_GuestAffiliateSignUp_MoP_DU_EmptyNumber","Select Weekly Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyDuNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC205_GuestAffiliateSignUp_MoP_DU_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC206_GuestAffiliateSignUp_MoP_DU_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC206_GuestAffiliateSignUp_MoP_DU_ShortNumber","Select Weekly Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithDu_ShortDuNumber(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC206_GuestAffiliateSignUp_MoP_DU_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC207_GuestAffiliateSignUp_MoP_DU_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC207_GuestAffiliateSignUp_MoP_DU_LongNumber","Select Weekly Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithDu_LongDuNumber(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC207_GuestAffiliateSignUp_MoP_DU_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC208_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC208_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber","Select Weekly Payment And enter wrong format du number");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongDuNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC208_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC209_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC209_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode","Select Weekly Payment And enter correct du number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC209_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC210_GuestAffiliateSignUp_MoP_DU_WrongActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC210_GuestAffiliateSignUp_MoP_DU_WrongActivationCode","Select Weekly Payment And enter correct du number, then provide wrong activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC210_GuestAffiliateSignUp_MoP_DU_WrongActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC211_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC211_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode","Select Weekly Payment And enter correct du number, then provide expired activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_ExpiredActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC211_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC212_GuestAffiliateSignUpp_DU_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC212_GuestAffiliateSignUpp_DU_Billing_Monthly","Guest select billing frequency as Monthly");
		   WebElement BtnChangePlan= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-form-back-button']");
		   StepNum = Genf.fClickOnObject(BtnChangePlan, "Click On ChangePlan", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg2, "Disclaimer Text", "By proceeding you agree that a monthly subscription payment of AED 30 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC212_GuestAffiliateSignUpp_DU_Billing_Monthly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC213_GuestAffiliateSignUp_MoP_DU_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC213_GuestAffiliateSignUp_MoP_DU_EmptyNumber","Select Monthly Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyDuNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC213_GuestAffiliateSignUp_MoP_DU_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC214_GuestAffiliateSignUp_MoP_DU_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC214_GuestAffiliateSignUp_MoP_DU_ShortNumber","Select Monthly Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithDu_ShortDuNumber(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC214_GuestAffiliateSignUp_MoP_DU_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC215_GuestAffiliateSignUp_MoP_DU_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC215_GuestAffiliateSignUp_MoP_DU_LongNumber","Select Monthly Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithDu_LongDuNumber(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC215_GuestAffiliateSignUp_MoP_DU_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC216_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC216_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber","Select Monthly Payment And enter wrong format du number");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongDuNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC216_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC217_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC217_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode","Select Monthly Payment And enter correct du number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC217_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC218_GuestAffiliateSignUp_MoP_DU_WrongActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC218_GuestAffiliateSignUp_MoP_DU_WrongActivationCode","Select Monthly Payment And enter correct du number, then provide wrong activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC218_GuestAffiliateSignUp_MoP_DU_WrongActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC219_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC219_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode","Select Monthly Payment And enter correct du number, then provide expired activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_ExpiredActivationCode(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC219_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode : Execution Finished");
		   StepNum = Genf.gfDeleteGigyaTraces(DuNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Extreport.flush();
		   Extreport.endTest(Exttest);
	  
	  Driver.close();
}
	
	public void TC220_228_GuestAffiliateSignUp_WithEmail_MoP_Etisalat_Daily_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC220_GuestAffiliateSignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC220_GuestAffiliateSignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC220_GuestAffiliateSignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC221_GuestAffiliateSignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC221_GuestAffiliateSignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC221_GuestAffiliateSignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC222_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC222_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC222_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC223_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC223_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC223_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC224_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat: Execution Started");
		   Exttest = Extreport.startTest("TC224_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat","Guest chooses Etislat as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Etisalat_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC224_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC225_GuestAffiliateSignUpp_Etisalat_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC225_GuestAffiliateSignUpp_Etisalat_ToArbic","Guest at Etislat payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC225_GuestAffiliateSignUpp_Etisalat_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC226_GuestAffiliateSignUpp_Etisalat_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC226_GuestAffiliateSignUpp_Etisalat_ToFrench","Guest at Etisalat Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC226_GuestAffiliateSignUpp_Etisalat_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC227_GuestAffiliateSignUpp_Etisalat_Billing_Daily: Execution Started");
		   Exttest = Extreport.startTest("TC227_GuestAffiliateSignUpp_Etisalat_Billing_Daily","Guest select billing frequency as Daily");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Daily(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a daily subscription payment of AED 3 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC227_GuestAffiliateSignUpp_Etisalat_Billing_Daily : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC228_GuestAffiliateSignUpp_With_Active_Etisalat_Registration_Daily_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC228_GuestAffiliateSignUpp_With_Active_Etisalat_Registration_Daily_Subscription","Guest enters a valid Etisalat number, provide access(OTP) code and register ");
		   String EtisalatNumber = Futil.fgetData("Etisalat - Mobile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_Etisalat_Registration(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC228_GuestAffiliateSignUpp_With_Active_Etisalat_Registration_Daily_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC229_237_GuestAffiliateSignUp_WithEmail_MoP_Etisalat_Weekly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC229_GuestAffiliateSignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC229_GuestAffiliateSignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC229_GuestAffiliateSignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC230_GuestAffiliateSignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC230_GuestAffiliateSignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC230_GuestAffiliateSignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC231_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC231_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC231_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC232_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC232_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC232_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC233_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat: Execution Started");
		   Exttest = Extreport.startTest("TC233_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat","Guest chooses Etislat as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Etisalat_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC233_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC234_GuestAffiliateSignUpp_Etisalat_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC234_GuestAffiliateSignUpp_Etisalat_ToArbic","Guest at Etislat payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC234_GuestAffiliateSignUpp_Etisalat_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC235_GuestAffiliateSignUpp_Etisalat_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC235_GuestAffiliateSignUpp_Etisalat_ToFrench","Guest at Etisalat Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC235_GuestAffiliateSignUpp_Etisalat_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC236_GuestAffiliateSignUpp_Etisalat_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC236_GuestAffiliateSignUpp_Etisalat_Billing_Weekly","Guest select billing frequency as Weekly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a weekly subscription payment of AED 10 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC236_GuestAffiliateSignUpp_Etisalat_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC237_GuestAffiliateSignUpp_With_Active_Etisalat_Registration_Weekly_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC237_GuestAffiliateSignUpp_With_Active_Etisalat_Registration_Weekly_Subscription","Guest enters a valid Etisalat number, provide access(OTP) code and register ");
		   String EtisalatNumber = Futil.fgetData("Etisalat - Mobile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_Etisalat_Registration(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC237_GuestAffiliateSignUpp_With_Active_Etisalat_Registration_Weekly_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC238_246_GuestAffiliateSignUp_WithEmail_MoP_Etisalat_Monthly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC238_GuestAffiliateSignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC238_GuestAffiliateSignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC238_GuestAffiliateSignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC239_GuestAffiliateSignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC239_GuestAffiliateSignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC239_GuestAffiliateSignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC240_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC240_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC240_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC241_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC241_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC241_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC242_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat: Execution Started");
		   Exttest = Extreport.startTest("TC242_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat","Guest chooses Etislat as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Etisalat_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC242_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC243_GuestAffiliateSignUpp_Etisalat_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC243_GuestAffiliateSignUpp_Etisalat_ToArbic","Guest at Etislat payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC243_GuestAffiliateSignUpp_Etisalat_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC244_GuestAffiliateSignUpp_Etisalat_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC244_GuestAffiliateSignUpp_Etisalat_ToFrench","Guest at Etisalat Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC244_GuestAffiliateSignUpp_Etisalat_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC245_GuestAffiliateSignUpp_Etisalat_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC245_GuestAffiliateSignUpp_Etisalat_Billing_Weekly","Guest select billing frequency as Weekly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyEtisalatPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a monthly subscription payment of AED 30 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC245_GuestAffiliateSignUpp_Etisalat_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC246_GuestAffiliateSignUpp_With_Active_Etisalat_Registration_Monthly_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC246_GuestAffiliateSignUpp_With_Active_Etisalat_Registration_Monthly_Subscription","Guest enters a valid Etisalat number, provide access(OTP) code and register ");
		   String EtisalatNumber = Futil.fgetData("Etisalat - Mobile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_Etisalat_Registration(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC246_GuestAffiliateSignUpp_With_Active_Etisalat_Registration_Monthly_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC247_TC267_SignUP_Email_Etisalat_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		String EtisalatNumber = Futil.fgetData("Etisalat - Mobile Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		int StepNum = 1;
		System.out.println("TC247_SignUp_with_Email: Execution Started");
		  Exttest = Extreport.startTest("TC247_SignUp_with_Email","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC247_SignUp_with_Email : Execution Finished  ");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		  Exttest = Extreport.startTest("TC248_SignUp_User_All_MOPs_verifcation","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC248_SignUp_User_All_MOPs_verifcation : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		  System.out.println("TC249_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat: Execution Started");
		   Exttest = Extreport.startTest("TC249_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat","Guest chooses Etisalat as MoP at Payment page");
		   StepNum = CMoPMethods.afMoP_Etisalat_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC249_GuestAffiliateSignUpp_PaymentPage_MoP_Etisalat : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC250_GuestAffiliateSignUpp_Etisalat_Billing_Daily: Execution Started");
		   Exttest = Extreport.startTest("TC250_GuestAffiliateSignUpp_Etisalat_Billing_Daily","Guest select billing frequency as Daily");
		   StepNum = CMoPMethods.afMoP_Billing_Daily(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By proceeding you agree that a daily subscription payment of AED 3 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC250_GuestAffiliateSignUpp_Etisalat_Billing_Daily : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC251_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC251_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber","Select daily Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithEtislata_EmptyEtisalatNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC251_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC252_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC252_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber","Select daily Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_ShortEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC252_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC253_GuestAffiliateSignUp_MoP_Etisalat_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC253_GuestAffiliateSignUp_MoP_Etisalat_LongNumber","Select daily Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_LongEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC253_GuestAffiliateSignUp_MoP_Etisalat_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC254_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC254_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber","Select daily Payment And enter wrong format Etisalat number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_WrongEtisalatNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC254_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC255_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC255_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode","Select daily Payment And enter correct Etisalat number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_EmptyActivationCode(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC255_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC256_GuestAffiliateSignUpp_Etisalat_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC256_GuestAffiliateSignUpp_Etisalat_Billing_Weekly","Guest select billing frequency as weekly");
		   WebElement BtnChangePlan= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='etisalat-form-back-button']");
		   StepNum = Genf.fClickOnObject(BtnChangePlan, "Click On ChangePlan", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Genf.gfSleep(3);
		   WebElement objDisclaimerMsgDaily= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsgDaily, "Disclaimer Text", "By proceeding you agree that a weekly subscription payment of AED 10 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC256_GuestAffiliateSignUpp_Etisalat_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC257_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC257_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber","Select weekly Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithEtislata_EmptyEtisalatNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC257_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC258_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC258_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber","Select weekly Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_ShortEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC258_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC259_GuestAffiliateSignUp_MoP_Etisalat_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC259_GuestAffiliateSignUp_MoP_Etisalat_LongNumber","Select weekly Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_LongEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC259_GuestAffiliateSignUp_MoP_Etisalat_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC260_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC260_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber","Select weekly Payment And enter wrong format Etisalat number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_WrongEtisalatNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC260_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC261_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC261_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode","Select weekly Payment And enter correct Etisalat number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_EmptyActivationCode(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC261_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   
		   System.out.println("TC262_GuestAffiliateSignUpp_Etisalat_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC262_GuestAffiliateSignUpp_Etisalat_Billing_Monthly","Guest select billing frequency as Monthly");
		   StepNum = Genf.fClickOnObject(BtnChangePlan, "Click On ChangePlan", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Genf.gfSleep(3);
		   WebElement objDisclaimerMsg2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg2, "Disclaimer Text", "By proceeding you agree that a monthly subscription payment of AED 30 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC262_GuestAffiliateSignUpp_Etisalat_Billing_Monthly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC263_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC263_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber","Select Monthly Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithEtislata_EmptyEtisalatNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC263_GuestAffiliateSignUp_MoP_Etisalat_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC264_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC264_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber","Select Monthly Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_ShortEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC264_GuestAffiliateSignUp_MoP_Etisalat_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC265_GuestAffiliateSignUp_MoP_Etisalat_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC265_GuestAffiliateSignUp_MoP_Etisalat_LongNumber","Select Monthly Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_LongEtisalatNumber(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC265_GuestAffiliateSignUp_MoP_Etisalat_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC266_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC266_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber","Select Monthly Payment And enter wrong format Etisalat number");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_WrongEtisalatNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC266_GuestAffiliateSignUp_MoP_Etisalat_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC267_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC267_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode","Select Monthly Payment And enter correct Etisalat number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithEtisalat_EmptyActivationCode(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC267_GuestAffiliateSignUp_MoP_Etisalat_EmptyActivationCode : Execution Finished");
		   StepNum = Genf.gfDeleteGigyaTraces(EtisalatNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Extreport.flush();
		   Extreport.endTest(Exttest);
	       Driver.close();
}
	
	public void TC268_GuestAffiliateSignUp_GoToSignUP_FromGreatEntertainment(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC268_GuestAffiliateSignUp_GoToSignUP_FromGreatEntertainment: Execution Started");
		  Exttest = Extreport.startTest("TC268_GuestAffiliateSignUp_GoToSignUP_FromGreatEntertainment","Guest at the landing page go to signup page by clicking START YOUR FREE TRIAL from the section Great Entertainment");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afVerifySignUpPage_FromGreatEntertainment(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC268_GuestAffiliateSignUp_GoToSignUP_FromGreatEntertainment : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC269_GuestAffiliateSignUp_GoToSignUP_FromKidsSection(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CFMethods = new CommonFooterMethods();
		CHMethods = new CommonHeaderMethods();
		CSignUpMethods = new CommonSignUpMethods();
		int StepNum = 1;
		System.out.println("TC269_GuestAffiliateSignUp_GoToSignUP_FromKidsSection: Execution Started");
		  Exttest = Extreport.startTest("TC269_GuestAffiliateSignUp_GoToSignUP_FromKidsSection","Guest at the landing page go to signup page by clicking START YOUR FREE TRIAL from the section Great Entertainment");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afVerifySignUpPage_FromKids(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  System.out.println("TC269_GuestAffiliateSignUp_GoToSignUP_FromKidsSection : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	}
	
	public void TC270_278_GuestAffiliateSignUp_WithEmail_MoP_MarocTelecom_Weekly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC270_GuestAffiliateSignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC270_GuestAffiliateSignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC270_GuestAffiliateSignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC271_GuestAffiliateSignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC271_GuestAffiliateSignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC271_GuestAffiliateSignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC272_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC272_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC272_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC273_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC273_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC273_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC274_GuestAffiliateSignUpp_PaymentPage_MoP_MarocTelecom: Execution Started");
		   Exttest = Extreport.startTest("TC274_GuestAffiliateSignUpp_PaymentPage_MoP_MarocTelecom","Guest chooses Maroc Telecom as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_MarocTelecom_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC274_GuestAffiliateSignUpp_PaymentPage_MoP_MarocTelecom : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC275_GuestAffiliateSignUpp_MarocTelecom_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC275_GuestAffiliateSignUpp_MarocTelecom_ToArbic","Guest at Maroc Telecom payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC275_GuestAffiliateSignUpp_MarocTelecom_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC276_GuestAffiliateSignUpp_MarocTelecom_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC276_GuestAffiliateSignUpp_MarocTelecom_ToFrench","Guest at Maroc telecom Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC276_GuestAffiliateSignUpp_MarocTelecom_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC277_GuestAffiliateSignUpp_MarocTelecom_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC277_GuestAffiliateSignUpp_MarocTelecom_Billing_Weekly","Guest select billing frequency as Weekly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Genf.gfSleep(3);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By clicking continue you agree to be charged 15 DH TTC per week after your trial ends. It will be deducted from your account balance or your monthly bill if you are post paid.");
		   System.out.println("TC277_GuestAffiliateSignUpp_MarocTelecom_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC278_GuestAffiliateSignUpp_With_Active_MarocTelecom_Registration_Weekly_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC278_GuestAffiliateSignUpp_With_Active_MarocTelecom_Registration_Weekly_Subscription","Guest enters a valid Maroc Telecom number, provide access(OTP) code and register ");
		   String MarocNumber = Futil.fgetData("Maroc Telecom Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_MarocTelecom_Registration(MarocNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC278_GuestAffiliateSignUpp_With_Active_MarocTelecom_Registration_Weekly_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC279_287_GuestAffiliateSignUp_WithEmail_MoP_MarocTelecom_Monthly_HappyEndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException
	{
		  Futil = new FrameWorkUtility();
		  Genf = new GeneralMethods();
		  CHMethods = new CommonHeaderMethods();
		  CSignUpMethods = new CommonSignUpMethods();
		  CMoPMethods = new CommonMoPMethods();
		  int StepNum = 1;
		  System.out.println("TC279_GuestAffiliateSignUp_With_Correct_Email_Password: Execution Started");
		  Exttest = Extreport.startTest("TC279_GuestAffiliateSignUp_With_Correct_Email_Password","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		   StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC279_GuestAffiliateSignUp_With_Correct_Email_Password : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC280_GuestAffiliateSignUpp_PaymentPage_Verification: Execution Started");
		   Exttest = Extreport.startTest("TC280_GuestAffiliateSignUpp_PaymentPage_Verification","Guest at Payment page should see correct MoPs as per country of guest");
		   StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC280_GuestAffiliateSignUpp_PaymentPage_Verification : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC281_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC281_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic","Guest at Payment page changes language to Arabic");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC281_GuestAffiliateSignUpp_PaymentPage_LanguageToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC282_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC282_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench","Guest at Payment page changes language to French");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CSignUpMethods.afVerifyRegistrationPaymentPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC282_GuestAffiliateSignUpp_PaymentPage_LanguageToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC283_GuestAffiliateSignUpp_PaymentPage_MoP_MarocTelecom: Execution Started");
		   Exttest = Extreport.startTest("TC283_GuestAffiliateSignUpp_PaymentPage_MoP_MarocTelecom","Guest chooses Maroc Telecom as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_MarocTelecom_Selection(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC283_GuestAffiliateSignUpp_PaymentPage_MoP_MarocTelecom : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC284_GuestAffiliateSignUpp_MarocTelecom_ToArbic: Execution Started");
		   Exttest = Extreport.startTest("TC284_GuestAffiliateSignUpp_MarocTelecom_ToArbic","Guest at Maroc Telecom payment page changes language to Arabic(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageTorabic(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC284_GuestAffiliateSignUpp_MarocTelecom_ToArbic : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC285_GuestAffiliateSignUpp_MarocTelecom_ToFrench: Execution Started");
		   Exttest = Extreport.startTest("TC285_GuestAffiliateSignUpp_MarocTelecom_ToFrench","Guest at Maroc telecom Payment page changes language to French(from the header)");
		   StepNum = CHMethods.afGuestChangeLanguageToFrench(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC285_GuestAffiliateSignUpp_MarocTelecom_ToFrench : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC286_GuestAffiliateSignUpp_MarocTelecom_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC286_GuestAffiliateSignUpp_MarocTelecom_Billing_Monthly","Guest select billing frequency as Monthly");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afVerifyMarocTelcomPageURL( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Genf.gfSleep(3);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By clicking continue you agree to be charged 50 DH TTC per month after your trial ends. It will be deducted from your account balance or your monthly bill if you are post paid.");
		   System.out.println("TC286_GuestAffiliateSignUpp_MarocTelecom_Billing_Monthly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC287_GuestAffiliateSignUpp_With_Active_MarocTelecom_Registration_Monthly_Subscription: Execution Started");
		   Exttest = Extreport.startTest("TC287_GuestAffiliateSignUpp_With_Active_MarocTelecom_Registration_Monthly_Subscription","Guest enters a valid Maroc Telecom number, provide access(OTP) code and register ");
		   String MarocNumber = Futil.fgetData("Maroc Telecom Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		   StepNum = CMoPMethods.afMoP_WithValid_MarocTelecom_Registration(MarocNumber, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC287_GuestAffiliateSignUpp_With_Active_MarocTelecom_Registration_Monthly_Subscription : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   Driver.close();
	}
	
	public void TC288_TC2XX_SignUP_Email_MarocTelecom_UnHappy_EndToEnd(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap) throws MalformedURLException{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CSignUpMethods = new CommonSignUpMethods();
		CHMethods = new CommonHeaderMethods();
		CMoPMethods = new CommonMoPMethods();
		String Number = Futil.fgetData("Maroc Telecom Number", "CommonData", "Common", Driver, Extreport, Exttest, ResultLocation);
		int StepNum = 1;
		System.out.println("TC288_SignUp_with_Email: Execution Started");
		  Exttest = Extreport.startTest("TC288_SignUp_with_Email","Guest signs up with correct email and password");
		  Driver = Genf.gnLaunchBrowser(Driver, Browser, AppURL,TestType,NodeURL,Cap);
		  StepNum = afVerifyAffiliateSignUpPage(Extreport, Exttest, Driver, ResultLocation, StepNum);
		  StepNum = CSignUpMethods.afSignUpWith_Correct_Email_ValidPwd(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC288_SignUp_with_Email : Execution Finished  ");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		  Exttest = Extreport.startTest("TC289_SignUp_User_All_MOPs_verifcation","Guest at Payment page should see correct MoPs as per country of guest");
		  StepNum = CSignUpMethods.afVerifyPaymentPage(Extreport, Exttest, Driver, StepNum, ResultLocation);
		  Extreport.endTest(Exttest);
		  System.out.println("TC289_SignUp_User_All_MOPs_verifcation : Execution Finished  "); 
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  
		  System.out.println("TC290_GuestAffiliateSignUpp_PaymentPage_MoP_MarocTelecom: Execution Started");
		   Exttest = Extreport.startTest("TC290_GuestAffiliateSignUpp_PaymentPage_MoP_MarocTelecom","Guest chooses Maroc Telecom as MoP at Payment page");
		   StepNum = CHMethods.afGuestChangeLanguageToEnglish(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   StepNum = CMoPMethods.afMoP_MarocTelecom_Selection( Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC290_GuestAffiliateSignUpp_PaymentPage_MoP_MarocTelecom : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC291_GuestAffiliateSignUpp_MarocTelecom_Billing_Weekly: Execution Started");
		   Exttest = Extreport.startTest("TC291_GuestAffiliateSignUpp_MarocTelecom_Billing_Weekly","Guest select billing frequency as weekly");
		   StepNum = CMoPMethods.afMoP_Billing_Weekly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg, "Disclaimer Text", "By clicking continue you agree to be charged 15 DH TTC per week after your trial ends. It will be deducted from your account balance or your monthly bill if you are post paid.");
		   System.out.println("TC291_GuestAffiliateSignUpp_MarocTelecom_Billing_Weekly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC292_GuestAffiliateSignUp_MoP_MarocTelecom_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC292_GuestAffiliateSignUp_MoP_MarocTelecom_EmptyNumber","Select Weekly Payment And enter empty Maroc Telecom number");
		   StepNum = CMoPMethods.afSignUPWith_EmptyMarocTelcomNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC292_GuestAffiliateSignUp_MoP_MarocTelecom_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC293_GuestAffiliateSignUp_MoP_MarocTelecom_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC293_GuestAffiliateSignUp_MoP_MarocTelecom_ShortNumber","Select Weekly Payment And enter short MarocTelecom number");
		   StepNum = CMoPMethods.afSignUPWith_ShortMarocTelecomNumber(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC293_GuestAffiliateSignUp_MoP_MarocTelecom_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC294_GuestAffiliateSignUp_MoP_MarocTelecom_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC294_GuestAffiliateSignUp_MoP_MarocTelecom_LongNumber","Select Weekly Payment And enter long MarocTelecom number");
		   StepNum = CMoPMethods.afSignUPWith_LongMarocTelecomNumber(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC294_GuestAffiliateSignUp_MoP_MarocTelecom_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC295_GuestAffiliateSignUp_MoP_MarocTelecom_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC295_GuestAffiliateSignUp_MoP_MarocTelecom_WrongFormatNumber","Select Weekly Payment And enter wrong format Maroc Telecom number");
		   StepNum = CMoPMethods.afSignUPWith_WrongMarocTelecomNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC295_GuestAffiliateSignUp_MoP_MarocTelecom_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC296_GuestAffiliateSignUp_MoP_MarocTelecom_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC296_GuestAffiliateSignUp_MoP_MarocTelecom_EmptyActivationCode","Select Weekly Payment And enter correct Maroc telecom number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithMarocTelecom_EmptyActivationCode(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC296_GuestAffiliateSignUp_MoP_MarocTelecom_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   /*System.out.println("TC210_GuestAffiliateSignUp_MoP_DU_WrongActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC210_GuestAffiliateSignUp_MoP_DU_WrongActivationCode","Select Weekly Payment And enter correct du number, then provide wrong activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongActivationCode(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC210_GuestAffiliateSignUp_MoP_DU_WrongActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC211_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC211_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode","Select Weekly Payment And enter correct du number, then provide expired activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_ExpiredActivationCode(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC211_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC212_GuestAffiliateSignUpp_DU_Billing_Monthly: Execution Started");
		   Exttest = Extreport.startTest("TC212_GuestAffiliateSignUpp_DU_Billing_Monthly","Guest select billing frequency as Monthly");
		   WebElement BtnChangePlan= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='du-form-back-button']");
		   StepNum = Genf.fClickOnObject(BtnChangePlan, "Click On ChangePlan", "Critical", Extreport, Exttest, Driver, StepNum, ResultLocation, "No");
		   StepNum = CMoPMethods.afMoP_Billing_Monthly(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   WebElement objDisclaimerMsg2= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='disclaimerText']");
		   StepNum = Genf.gfVerifyAlertMessage(Extreport, Exttest, Driver, StepNum, ResultLocation, objDisclaimerMsg2, "Disclaimer Text", "By proceeding you agree that a monthly subscription payment of AED 30 will be made from your prepaid or postpaid mobile account.");
		   System.out.println("TC212_GuestAffiliateSignUpp_DU_Billing_Monthly : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC213_GuestAffiliateSignUp_MoP_DU_EmptyNumber: Execution Started");
		   Exttest = Extreport.startTest("TC213_GuestAffiliateSignUp_MoP_DU_EmptyNumber","Select Monthly Payment And enter empty du number");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyDuNumber(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC213_GuestAffiliateSignUp_MoP_DU_EmptyNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC214_GuestAffiliateSignUp_MoP_DU_ShortNumber: Execution Started");
		   Exttest = Extreport.startTest("TC214_GuestAffiliateSignUp_MoP_DU_ShortNumber","Select Monthly Payment And enter short du number");
		   StepNum = CMoPMethods.afSignUPWithDu_ShortDuNumber(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC214_GuestAffiliateSignUp_MoP_DU_ShortNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC215_GuestAffiliateSignUp_MoP_DU_LongNumber: Execution Started");
		   Exttest = Extreport.startTest("TC215_GuestAffiliateSignUp_MoP_DU_LongNumber","Select Monthly Payment And enter long du number");
		   StepNum = CMoPMethods.afSignUPWithDu_LongDuNumber(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC215_GuestAffiliateSignUp_MoP_DU_LongNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC216_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber: Execution Started");
		   Exttest = Extreport.startTest("TC216_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber","Select Monthly Payment And enter wrong format du number");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongDuNumberFormat(Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC216_GuestAffiliateSignUp_MoP_DU_WrongFormatNumber : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC217_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC217_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode","Select Monthly Payment And enter correct du number, then provide empty activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_EmptyActivationCode(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC217_GuestAffiliateSignUp_MoP_DU_EmptyActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC218_GuestAffiliateSignUp_MoP_DU_WrongActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC218_GuestAffiliateSignUp_MoP_DU_WrongActivationCode","Select Monthly Payment And enter correct du number, then provide wrong activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_WrongActivationCode(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC218_GuestAffiliateSignUp_MoP_DU_WrongActivationCode : Execution Finished");
		   Extreport.flush();
		   Extreport.endTest(Exttest);
		   
		   System.out.println("TC219_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode: Execution Started");
		   Exttest = Extreport.startTest("TC219_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode","Select Monthly Payment And enter correct du number, then provide expired activation code");
		   StepNum = CMoPMethods.afSignUPWithDu_ExpiredActivationCode(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   System.out.println("TC219_GuestAffiliateSignUp_MoP_DU_ExpiredActivationCode : Execution Finished");
		   StepNum = Genf.gfDeleteGigyaTraces(Number, Extreport, Exttest, Driver, StepNum, ResultLocation);
		   Extreport.flush();
		   Extreport.endTest(Exttest);*/
	  
	  Driver.close();
}
	public int RedirectToSignNUPafterLogin(String URL,ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,int StepNum,String ResultLocation)
	{
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		try{
			Driver.navigate().to(URL);
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Try to Redirect To Affiliate Signu UP Page", "Successfully Tried", "No");
			Genf.gfSleep(4);
			if(Driver.getCurrentUrl().contains("en/start")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Starz Play Home page", "Successfully Verified the Home Page, The home Page loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Verified the Home Page With URL :"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Starz Play Home page", "Failed To Verify home page And The URL was"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Failed To Verify home page And The URL was"+Driver.getCurrentUrl()); 
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Starz Play Home page", "Failed To Verify home page And The URL was"+Driver.getCurrentUrl(), "Yes");
			  System.out.println("Failed To Verify home page And The URL was"+Driver.getCurrentUrl()); 
			  StepNum = StepNum+1;
		}
		return 0;
		
	}
	public int afNaviagteToLoginFromAffilatePageHeader(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String ResultLocation,int StepNum) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			CLoginMethods = new CommonLoginMethods();
			  WebElement lnkLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='login-button']");
			  if(lnkLogin!=null){
				  lnkLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login", "Successfully Clicked on Login from Header", "No");
				  System.out.println("Successfully Clicked on Login from header");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login from Header", "Failed To Click On  Login", "No");   
				  System.out.println("Failed To Click On Login from header"); 
				  Driver.close();
				  Assert.fail("Failed To Click On Login from SignUP Box");
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  String strHomeUrl = Driver.getCurrentUrl();
			  if(strHomeUrl.contains("/login")&&(strHomeUrl.contains("error")!=true)){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login page", "Successfully Verified the Login, The Login page for guest loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Verified the Login Page URL is :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login page", "Failed To Launch login Page And The URL was"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Launch Login page And The URL was"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login functionality", "Verification failed for The login functionality", "No");
			  System.out.println("Verification failed for The login functinality");
			  StepNum = StepNum+1;
		}
		
		 return StepNum;
		  
	}
	
	public int afNavigateToLoginFromAffilatePageSignUpBox(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String ResultLocation,int StepNum) 
	{
		try{
			Futil = new FrameWorkUtility();
			Genf = new GeneralMethods();
			CLoginMethods = new CommonLoginMethods();
			  WebElement lnkLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='signup-link']");
			  if(lnkLogin!=null){
				  lnkLogin.click();
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Click On Login", "Successfully Clicked on Login from SignUP Box", "No");
				  System.out.println("Successfully Clicked on Login from SignUP Box");  
				  StepNum = StepNum+1;
			  }else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Click On Login from SignUP Box", "Failed To Click On  SignUP Box", "No");   
				  System.out.println("Failed To Click On Login from SignUP Box"); 
				  Driver.close();
				  Assert.fail("Failed To Click On Login from SignUP Box");
				  StepNum = StepNum+1;
				   }
			  
			  Genf.gfSleep(5);
			  String strHomeUrl = Driver.getCurrentUrl();
			  if(strHomeUrl.contains("/login")&&(strHomeUrl.contains("error")!=true)){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Login page", "Successfully Verified the Login, The Login page for guest loaded with URL :"+Driver.getCurrentUrl(), "Yes");
				  System.out.println("Verified the Login Page URL is :"+Driver.getCurrentUrl());  
				  StepNum = StepNum+1;
			  }
			  else{
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Login page", "Failed To Launch login Page And The URL was"+Driver.getCurrentUrl(), "No");
				  System.out.println("Failed To Launch Login page And The URL was"+Driver.getCurrentUrl());
				  StepNum = StepNum+1;
			  }
			  
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify The login functionality", "Verification failed for The login functionality", "No");
			  System.out.println("Verification failed for The login functinality");
			  StepNum = StepNum+1;
		}
		
		 return StepNum;
		  
	}
	
	
	public int afVerifyAffiliateSignUpPage(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String ResultLocation,int StepNum)
	{
		String strCurrURL = null;
		try{
			Futil = new FrameWorkUtility();
			  Genf = new GeneralMethods();
			  CHMethods = new CommonHeaderMethods();
			  CSignUpMethods = new CommonSignUpMethods();
			  strCurrURL = Driver.getCurrentUrl();
			  if(strCurrURL.contains("/affiliate")){
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step "+StepNum+". Verify Affiliate Sign Up Page", "Successfully verified the Affiliate Sign Up page With URL :"+strCurrURL, "No");
				  System.out.println("Successfully verified the Affiliate Sign Up page With URL :"+strCurrURL);  
				  StepNum = StepNum+1;
			  }else if(strCurrURL.equals("about:blank"))
			  {
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Affiliate Sign Up Page", "Affiliate Sign Up page verification failed And The URL was"+strCurrURL, "No");
				  System.out.println("Affiliate Sign Up page verification failed And The URL was"+strCurrURL); 
				  StepNum = StepNum+1;
				  Driver.close();
				  Assert.fail("Affiliate Sign Up page verification failed And The URL was"+strCurrURL);
			  }else if(strCurrURL.contains("error"))
			  {
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Affiliate Sign Up Page", "Affiliate Sign Up page verification failed And The URL was"+strCurrURL, "No");
				  System.out.println("Affiliate Sign Up page verification failed And The URL was"+strCurrURL); 
				  StepNum = StepNum+1;
				  Driver.close();
				  Assert.fail("Affiliate Sign Up page verification failed And The URL was"+strCurrURL);
			  }
			  else
			  {
				  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Affiliate Sign Up Page", "Affiliate Sign Up page verification failed And The URL was"+strCurrURL, "No");
				  System.out.println("Affiliate Sign Up page verification failed And The URL was"+strCurrURL); 
				  StepNum = StepNum+1;
			  }
			
		}catch(Exception e){
			Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step "+StepNum+". Verify Affiliate Sign Up Page", "Affiliate Sign Up page verification failed And The URL was"+strCurrURL, "No");
			  System.out.println("Affiliate Sign Up page verification failed And The URL was"+strCurrURL); 
			  StepNum = StepNum+1;
		}
		
		   return StepNum;
	}
	
	public void DataBaseTest(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap)
	{
		try{
			Futil = new FrameWorkUtility();
			//String SqlQuery = "select * from starzplayarabia_staging.`sms_verification_codes` where phone_number = '971529752712' order by id DESC";
			String SqlQuery = "select verification_code from sms_verification_codes where phone_number = '212612345555' order by id DESC";
			ResultSet rs = Futil.fGetDataFromDB(SqlQuery, Driver, Extreport, Exttest, ResultLocation);
			rs.next();
			String OTP = rs.getString(1);
			System.out.println("The OTP is:"+OTP);
		}catch(Exception e){
			
		}
		
	}
	
	public void GuigyaTrace(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap)
	{
		try{
			Genf = new GeneralMethods();
			Futil = new FrameWorkUtility();
			int StepNum = 1;
			String USERID = "e1910e6b316f4309b97d6e60328858b4";
				String endpoint1 = "https://accounts.eu1.gigya.com/accounts.setAccountInfo?format=json&UID="+USERID+"&apiKey=3_dFt8EnQzDmeB6DwcuF2lf2E6JIsbjiwSZolwwwCw58kBDmouvah9cyMxFxOOEtAl&secret=rRiE4LPpZ8FJZzWoBUsPFAH5TF2vo6Vjdt5PtULWR/c=&username="+Genf.GenrateRandomName();
				Genf.doGet(endpoint1,Extreport, Exttest, Driver, StepNum, ResultLocation);
				String endpoint2 = "https://socialize.eu1.gigya.com/accounts.setAccountInfo?UID="+USERID+"&profile=%7B%22phones.number%22:null%7D&apiKey=3_dFt8EnQzDmeB6DwcuF2lf2E6JIsbjiwSZolwwwCw58kBDmouvah9cyMxFxOOEtAl%20&secret=rRiE4LPpZ8FJZzWoBUsPFAH5TF2vo6Vjdt5PtULWR%2Fc%3D";
				Genf.doGet(endpoint2,Extreport, Exttest, Driver, StepNum+1, ResultLocation);
				String spxModifyBillingPhone = "https://test-api.aws.playco.com/api/v0.2/userAccounts/" + USERID;
				String AccessTocken = Genf.getAccessToken(USERID, Extreport, Exttest, Driver, StepNum,ResultLocation );
				Genf.doPut(spxModifyBillingPhone, USERID, Genf.updateUserAccount(), AccessTocken);
			
		}catch(Exception e){
			
		}
	}
	
	public void GuigyaTrace_RD(ExtentReports Extreport,ExtentTest Exttest,WebDriver Driver,String AppURL,String AppTitle,String ResultLocation,String Browser,String TestType,String NodeURL,DesiredCapabilities Cap)
	{
		try{
			Genf = new GeneralMethods();
			Futil = new FrameWorkUtility();
			int StepNum = 1;
			String strSQLQuerry = "select user_id from sms_verification_codes where phone_number = '971529752712' order by id DESC";
			ResultSet rs = Futil.fGetDataFromDB(strSQLQuerry, Driver, Extreport, Exttest, ResultLocation);
			while (rs.next()) {
				rs.next();
				String USERID = rs.getString(1);
				//String USERID = "926dbbdf00c0410a8d30831293fde71d";
					String endpoint1 = "https://accounts.eu1.gigya.com/accounts.setAccountInfo?format=json&UID="+USERID+"&apiKey=3_dFt8EnQzDmeB6DwcuF2lf2E6JIsbjiwSZolwwwCw58kBDmouvah9cyMxFxOOEtAl&secret=rRiE4LPpZ8FJZzWoBUsPFAH5TF2vo6Vjdt5PtULWR/c=&username="+Genf.GenrateRandomName();
					Genf.doGet(endpoint1,Extreport, Exttest, Driver, StepNum, ResultLocation);
					String endpoint2 = "https://socialize.eu1.gigya.com/accounts.setAccountInfo?UID="+USERID+"&profile=%7B%22phones.number%22:null%7D&apiKey=3_dFt8EnQzDmeB6DwcuF2lf2E6JIsbjiwSZolwwwCw58kBDmouvah9cyMxFxOOEtAl%20&secret=rRiE4LPpZ8FJZzWoBUsPFAH5TF2vo6Vjdt5PtULWR%2Fc%3D";
					Genf.doGet(endpoint2,Extreport, Exttest, Driver, StepNum+1, ResultLocation);
					String spxModifyBillingPhone = "https://test-api.aws.playco.com/api/v0.2/userAccounts/" + USERID;
					String AccessTocken = Genf.getAccessToken(USERID, Extreport, Exttest, Driver, StepNum,ResultLocation );
					Genf.doPut(spxModifyBillingPhone, USERID, Genf.updateUserAccount(), AccessTocken);
		    }
			
			
		}catch(Exception e){
			
		}
		
		
		
	}

}
