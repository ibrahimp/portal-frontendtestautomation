package portalfrontend;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class FrameWorkUtility {
	
	public BufferedImage resizeImage(final Image image, int width, int height) {
        final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        //below three lines are for RenderingHints for better image quality at cost of higher processing time
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();
        return bufferedImage;
    }
	
	public void flogResult_embed_resize(WebDriver Driver,ExtentReports Extreport,ExtentTest Exttest,String ResultLocation,String Status, String Step, String Decripion, String ScreenShot) 
	{
		try{
			switch (ScreenShot) {
			case "Yes":
	File scrFile = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
	String DestFile = ResultLocation + "/Images/img" + fGetRandomNumUsingTime() + ".jpeg";
	try {
		FileUtils.copyFile(scrFile,new File(DestFile));
	} catch (IOException e) {
		e.printStackTrace();
	}
	Image img = null;
	BufferedImage tempJPG = null;
	img = ImageIO.read(new File(DestFile));
	double aspectRatio = (double) img.getWidth(null)/(double) img.getHeight(null);
	tempJPG = resizeImage(img, 1200,(int)(1200/aspectRatio));
	ImageIO.write(tempJPG, "jpg", new File(DestFile));
	InputStream is = new FileInputStream(DestFile);
	byte[] imageBytes = IOUtils.toByteArray(is);
	String base64 = Base64.getEncoder().encodeToString(imageBytes);
	String image = Exttest.addBase64ScreenShot("data:image/png;base64,"+base64);
	if (Status.equals("Passed")) {
	Exttest.log(LogStatus.PASS, Step + " -- The Actual Is : " + Decripion, image);
	} else {
		Exttest.log(LogStatus.FAIL, Step + "-- The Actual Is : " + Decripion, image);
		}
		break;
	case "No":
	if (Status.equals("Passed")) {
			Exttest.log(LogStatus.PASS, Step, Decripion);
		} else {
			Exttest.log(LogStatus.FAIL, Step, Decripion);
		}
		break;
	}	
	
	Extreport.flush();
		}catch(Exception e){
			
		}
			
			
	}
		
		public void flogResult_embed(WebDriver Driver,ExtentReports Extreport,ExtentTest Exttest,String ResultLocation,String Status, String Step, String Decripion, String ScreenShot) 
		{
			try{
				switch (ScreenShot) {
				case "Yes":
		File scrFile = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
		String DestFile = ResultLocation + "/Images/img" + fGetRandomNumUsingTime() + ".jpeg";
		try {
			FileUtils.copyFile(scrFile,new File(DestFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
		InputStream is = new FileInputStream(DestFile);
		byte[] imageBytes = IOUtils.toByteArray(is);
		String base64 = Base64.getEncoder().encodeToString(imageBytes);
		String image = Exttest.addBase64ScreenShot("data:image/png;base64,"+base64);
		if (Status.equals("Passed")) {
		Exttest.log(LogStatus.PASS, Step + " -- The Actual Is : " + Decripion, image);
		} else {
			Exttest.log(LogStatus.FAIL, Step + "-- The Actual Is : " + Decripion, image);
			}
			break;
		case "No":
		if (Status.equals("Passed")) {
				Exttest.log(LogStatus.PASS, Step, Decripion);
			} else {
				Exttest.log(LogStatus.FAIL, Step, Decripion);
			}
			break;
		}	
		
		Extreport.flush();
			}catch(Exception e){
				
			}
				
				
		}
		
		public void flogResult(WebDriver Driver,ExtentReports Extreport,ExtentTest Exttest,String ResultLocation,String Status, String Step, String Decripion, String ScreenShot) 
		{
			try{
				switch (ScreenShot) {
				case "Yes":
		File scrFile = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
		String DestFile = ResultLocation + "/Images/img" + fGetRandomNumUsingTime() + ".png";
		try {
			FileUtils.copyFile(scrFile,new File(DestFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Image img = null;
		BufferedImage tempJPG = null;
		img = ImageIO.read(new File(DestFile));
		double aspectRatio = (double) img.getWidth(null)/(double) img.getHeight(null);
		tempJPG = resizeImage(img, 2000,(int)(2000/aspectRatio));
		ImageIO.write(tempJPG, "jpg", new File(DestFile));
		String image = Exttest.addScreenCapture(DestFile);
		if(image.contains("jenkins")){
			//image = image.replace("file:////var/lib/jenkins/workspace/Selenium-Automation/", "http://54.154.61.48:8080/job/Selenium-Automation/ws/");
			//image = image.replaceAll("/var/lib/jenkins/workspace/Selenium-Automation/", "http://54.154.61.48:8080/job/Selenium-Automation/ws/");
			image = image.replace("file:////var/lib/jenkins/workspace/Portal-AffiliateSignUpTest-Automation/", "http://54.154.61.48:8080/job/Portal-AffiliateSignUpTest-Automation/ws/");
			image = image.replaceAll("/var/lib/jenkins/workspace/Portal-AffiliateSignUpTest-Automation/", "http://54.154.61.48:8080/job/Portal-AffiliateSignUpTest-Automation/ws/");
			image = image.replace("file:////var/lib/jenkins/workspace/Portal-LandingPageTest-Automation/", "http://54.154.61.48:8080/job/Portal-LandingPageTest-Automation/ws/");
			image = image.replaceAll("/var/lib/jenkins/workspace/Portal-LandingPageTest-Automation/", "http://54.154.61.48:8080/job/Portal-LandingPageTest-Automation/ws/");
			image = image.replace("file:////var/lib/jenkins/workspace/Portal-SettingsPageTest-Automation/", "http://54.154.61.48:8080/job/Portal-SettingsPageTest-Automation/ws/");
			image = image.replaceAll("/var/lib/jenkins/workspace/Portal-SettingsPageTest-Automation/", "http://54.154.61.48:8080/job/Portal-SettingsPageTest-Automation/ws/");
		}
		if (Status.equals("Passed")) {
		Exttest.log(LogStatus.PASS, Step + " -- The Actual Is : " + Decripion, image);
		} else if(Status.equals("Warning")){
			Exttest.log(LogStatus.WARNING, Step + " -- The Actual Is : " + Decripion, image);
		}
		else {
			Exttest.log(LogStatus.FAIL, Step + "-- The Actual Is : " + Decripion, image);
			}
			break;
		case "No":
		if (Status.equals("Passed")) {
				Exttest.log(LogStatus.PASS, Step, Decripion);
			}else if(Status.equals("Warning")){
				Exttest.log(LogStatus.WARNING, Step, Decripion);
			} else {
				Exttest.log(LogStatus.FAIL, Step, Decripion);
			}
			break;
		}	
		
		Extreport.flush();
				
			}catch(Exception e){
				if (Status.equals("Passed")) {
					Exttest.log(LogStatus.PASS, Step, Decripion);
				} else {
					Exttest.log(LogStatus.FAIL, Step, Decripion);
				}	
				Extreport.flush();
			}
				
				
		}
	/*public String fGetRandomNumUsingTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(calendar.getTime());
		int month = Calendar.MONTH;
		int day = Calendar.DAY_OF_MONTH;
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);
		String Rand = Integer.toString(month) + Integer.toString(day) + Integer.toString(hours)
		+ Integer.toString(minutes) + Integer.toString(seconds);
		return Rand;
	}*/
	
	public String fGetRandomNumUsingTime() {
		   
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-YYYY_HH_mm_ss");
		//System.out.println(formatter.format(new Date()));
		return formatter.format(new Date());
	
	}
	 private  void getAllFiles(File curDir) {

	        File[] filesList = curDir.listFiles();
	        for(File f : filesList){
	            if(f.isDirectory())
	                System.out.println(f.getName());
	            if(f.isFile()){
	                System.out.println(f.getName());
	            }
	        }
	 }
	 
	 public  String getfilePath(File curDir,String fileName) {
		 String Filepath = null;
		 try{
			 File[] filesList = curDir.listFiles();
		        for(File f : filesList){
		        	if(f.getName().equals(fileName)){
		        		Filepath =  f.getAbsolutePath();
	            	}
		        }
		 }catch(Exception e){
			 Filepath =  null;
		 }
		
		return  Filepath;
	 }
	        

	public String fgetData(String attribute,String SheetName,String TestcaseName,WebDriver Driver,ExtentReports Extreport,ExtentTest Exttest,String ResultLocation) {
		FileInputStream TestDataFile;
		File curDir = new File(".");
		String DataSheetpath = getfilePath(curDir,"Datasheet.xlsx");
		String Data = null;
		try {
			TestDataFile = new FileInputStream(new File(DataSheetpath));
			XSSFWorkbook workbook = new XSSFWorkbook(TestDataFile);
			XSSFSheet worksheet = workbook.getSheet(SheetName);
			XSSFRow firstRow = worksheet.getRow(0);
			int intColumnNum = firstRow.getLastCellNum();
			int intRowNum = worksheet.getPhysicalNumberOfRows();
			int TestCaseColNum = 0;
			int i, j;
			for (j = 0; j < intColumnNum; j++) {
				String HeaderValue = firstRow.getCell(j).getStringCellValue();
				if (HeaderValue.equals(TestcaseName)) {
					TestCaseColNum = j;
					break;
				}
			}

			for (i = 1; i < intRowNum; i++) {
				if (((worksheet).getRow(i).getCell(TestCaseColNum).getStringCellValue()).equals(attribute)) {

					Cell cell = (worksheet).getRow(i).getCell(TestCaseColNum+1);
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						Data = (worksheet).getRow(i).getCell(TestCaseColNum+1)
						.getStringCellValue();
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						Data = Boolean.toString((worksheet).getRow(i)
								.getCell(TestCaseColNum+1).getBooleanCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						cell.setCellType(Cell.CELL_TYPE_STRING);
						Data = (worksheet).getRow(i).getCell(TestCaseColNum+1)
								.getStringCellValue();
						break;
					case Cell.CELL_TYPE_FORMULA:

						if (HSSFDateUtil.isCellDateFormatted(
								(worksheet).getRow(i).getCell(TestCaseColNum+1))) {
							DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
							Data = df.format((worksheet).getRow(i)
									.getCell(TestCaseColNum+1).getDateCellValue());
						} else {
							cell.setCellType(Cell.CELL_TYPE_STRING);
							Data = (worksheet).getRow(i).getCell(TestCaseColNum+1)
									.getStringCellValue();
						}

					}
					break;
				}
			}

			TestDataFile.close();

		} catch (NullPointerException e) {
			System.out.println("NullPointer");
			flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "No data present", "Data not provided for :" + attribute, "No");
			
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException");
			flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", " Failed to load Data file- File Not found","Check file path or extension :", "No");
		} catch (IOException e) {
			System.out.println("IOException");
			flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", " Runtime Exception while reading data file", "Check data file :","No");
		} catch (Exception ex) {
			System.out.println(ex);
			flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", " Runtime/Unknown Exception while reading data file", "Check data file :","No");
		}
		return Data;

	}
	
	public ResultSet fGetDataFromDB(String query,WebDriver Driver,ExtentReports Extreport,ExtentTest Exttest,String ResultLocation) {
      
        String databaseURL = "jdbc:mysql://dev-mysql.aws.playco.com:3306/starzplayarabia_staging";
        String user = "qa_automation";
        String password = "eQgdg6sp";
        ResultSet rs = null;
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connecting to Database...");
            connection = DriverManager.getConnection(databaseURL, user, password);
            if (connection != null) {
                System.out.println("Connected to the Database...");
                Statement statement = connection.createStatement();
                rs = statement.executeQuery(query);
            }
        } catch (SQLException ex) {
        	flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Dats Base fetching", "Failed with SQL Exception :"+ex, "No");
           return null;
        }
        catch (Exception ex) {
        	flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Dats Base fetching", "Failed with Exception :"+ex, "No");
        	return null;
        }
        return rs;
}
	
	public ResultSet fGetDataFromDB_Base() {
        //String databaseURL = "jdbc:mysql://54.229.10.244:3306"; // Parameterize 
        String databaseURL = "jdbc:mysql://dev-mysql.aws.playco.com:3306/starzplayarabia_staging"; // Parameterize 
        String user = "qa_automation";//"spxUsr";
        String password = "eQgdg6sp";//vqkWWHptpCwmkgDw8twQuExC";
        ResultSet rs = null;
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connecting to Database...");
            connection = DriverManager.getConnection(databaseURL, user, password);
            if (connection != null) {
                System.out.println("Connected to the Database...");
                String query = "select transaction_id from sms_logs";//Parameterize - this is just an example
                Statement statement = connection.createStatement();
                rs = statement.executeQuery(query);
            }
        } catch (SQLException ex) {
           return null;
        }
        catch (ClassNotFoundException ex) {
        	return null;
        }
        return rs;
}
	
	public static void reduceImageQuality(int sizeThreshold, String srcImg, String destImg) throws Exception {  
        float quality = 1.0f;  
  
        File file = new File(srcImg);  
  
        long fileSize = file.length();  
  
        if (fileSize <= sizeThreshold) {  
            System.out.println("Image file size is under threshold");  
            return;  
        }  
  
        Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");  
  
        ImageWriter writer = (ImageWriter)iter.next();  
  
        ImageWriteParam iwp = writer.getDefaultWriteParam();  
  
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);  
  
        FileInputStream inputStream = new FileInputStream(file);  
  
        BufferedImage originalImage = ImageIO.read(inputStream);  
        IIOImage image = new IIOImage(originalImage, null, null);  
  
        float percent = 0.1f;   // 10% of 1  
  
        while (fileSize > sizeThreshold) {  
            if (percent >= quality) {  
                percent = percent * 0.1f;  
            }  
  
            quality -= percent;  
  
            File fileOut = new File(destImg);  
            if (fileOut.exists()) {  
                fileOut.delete();  
            }  
            FileImageOutputStream output = new FileImageOutputStream(fileOut);  
  
            writer.setOutput(output);  
  
            iwp.setCompressionQuality(quality);  
  
            writer.write(null, image, iwp);  
  
            File fileOut2 = new File(destImg);  
            long newFileSize = fileOut2.length();  
            if (newFileSize == fileSize) {  
                // cannot reduce more, return  
                break;  
            } else {  
                fileSize = newFileSize;  
            }  
            System.out.println("quality = " + quality + ", new file size = " + fileSize);  
            output.close();  
        }  
  
        writer.dispose();  
    }  
	
	public void rduceSize(String src,String dest,float fvalue){
		
		try{
			String srcPath = src;  
	        String destPath = dest;  
	        float quality = fvalue;  
	  
	        Iterator iter = ImageIO.getImageWritersByFormatName("jpg");  
	  
	        ImageWriter writer = (ImageWriter)iter.next();  
	  
	        ImageWriteParam iwp = writer.getDefaultWriteParam();  
	  
	        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);  
	  
	        iwp.setCompressionQuality(quality);  
	  
	        File file = new File(destPath);  
	        FileImageOutputStream output = new FileImageOutputStream(file);  
	        writer.setOutput(output);  
	  
	        FileInputStream inputStream = new FileInputStream(srcPath);  
	        BufferedImage originalImage = ImageIO.read(inputStream);  
	  
	        IIOImage image = new IIOImage(originalImage, null, null);  
	        writer.write(null, image, iwp);  
	        writer.dispose();  
	  
	        System.out.println("DONE"); 
		}catch(Exception e)
		{
			System.out.println("Exception"+ e); 
		}
		 
	}
}
