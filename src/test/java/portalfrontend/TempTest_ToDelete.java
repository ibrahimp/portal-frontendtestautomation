package portalfrontend;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TempTest_ToDelete {
	public ExtentReports Extreport;
	public ExtentTest Exttest;
	public WebDriver Driver;
	public HtmlUnitDriver Hdriver; 
	public String ResultLocation;
	public FrameWorkUtility Futil;
	public GeneralMethods Genf;
	public DesiredCapabilities Cap;
	public CommonFooterMethods CTMethods;
	public File PhantomJsSrc;
	
	
	@Parameters({"Browser"})
	@BeforeTest
	public void startTest(String BrowserName,final ITestContext testContext) {
		Futil = new FrameWorkUtility();
		Genf = new GeneralMethods();
		CTMethods = new CommonFooterMethods();
	    ResultLocation = System.getProperty("user.dir")+"/Custom_Report/"+testContext.getName();
	    Extreport = new ExtentReports(ResultLocation+"/"+testContext.getName()+"-"+Futil.fGetRandomNumUsingTime()+".html", true);
	      Cap = DesiredCapabilities.chrome();
		  Cap.setBrowserName(BrowserName);
		  Cap.setCapability("ignoreZoomSetting", true);
		  Cap.setPlatform(Platform.MAC);
		  Cap.setCapability("nativeEvents","false");
		  PhantomJsSrc = new File(System.getProperty("user.dir")+"/Resources/phantomjs/bin/phantomjs");
	}
	
	@Parameters({"NodeURL","ApplnURL","AppTitle"})
	  @Test(enabled=false)//working with grid - chrome And firefox
	  public void TC_Login_Logout0001(String nodeURL,String AppURL,String AppTitle) throws MalformedURLException {
		System.out.println("TC_Login_Logout0001 : Execution Started");
		  Exttest = Extreport.startTest("TC_Login_0001","This Test case is to verify The Login Functionality With Positive Credentials");
		  Driver = new RemoteWebDriver(new URL(nodeURL),Cap);
		  Driver.get(AppURL);
		  System.out.println(Driver.getTitle());
		  System.out.println("Successfully Launcehd the browser And Navigated To"+Driver.getTitle());
		  if(AppTitle.equals(Driver.getTitle())){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
			  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "Yes");
		  }
		  
		  WebElement btnLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//a[@id='login-button']");
		  if(btnLogin.isDisplayed()){
			  btnLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Login Button", "Successfully Clicked on the button Login", "No");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Login Button", "Failed To Click On  Clicked on the button Login", "Yes");   
			   }
		  
		  WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
		  if(EdtUserName.isDisplayed()){
			  EdtUserName.clear();
			  String EmailId = "ibrahim.poovakkattil@starzplayarabia.com";
			  EdtUserName.sendKeys(EmailId);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Enter Email Address", "Successfully Entered The Email id :"+EmailId, "No");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Enter Email Address", "Failed To Enter Email ID", "Yes");   
			   }
		  
		  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
		  if(EdtPassword.isDisplayed()){
			  EdtPassword.clear();
			  String Password = "123456";
			  EdtPassword.sendKeys(Password);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Enter The Password", "Successfully Entered The Email id :"+Password, "No");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Enter The Password", "Failed To Enter Password", "Yes");   
			   }
		  
		  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
		  if(btnSubmitLogin.isDisplayed()){
			  btnSubmitLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 4. Click On Login Submit BUtton", "Successfully Clicked on the button Login", "No");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 4. Click On Login Submit Button", "Failed To Click On  Clicked on the button Login", "Yes");   
			   }
		  
		  WebElement lnkMOvies= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='movies-link']");
		  if(lnkMOvies.isDisplayed()){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 5. Verify Home Page", "Successfully Verified The home page by confirming The Link Movies", "Yes");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 5. Verify Home Page", "Failed To Verify the home page, The Link for Movies Not Present", "Yes");   
			   }
		  
		  WebElement btnSetting= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='settingsUser']");
		  if(btnSetting.isDisplayed()){
			  btnSetting.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 6. Click On Setting Icon", "Successfully Clicked on Setting Icon", "No");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 6. Click On Setting Icon", "Failed To Click On Setting Icon", "Yes");   
			   }
		  
		  WebElement lnkLogout= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='logoutSettings']");
		  if(lnkLogout.isDisplayed()){
			  lnkLogout.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 7. Click On Setting Icon", "Successfully Clicked on Setting Icon", "No");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 7. Click On Setting Icon", "Failed To Click On Setting Icon", "Yes");   
			   }
		  
		  WebElement lnkLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='login-button']");
		  if(lnkLogin.isDisplayed()){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 8. Verify Home Page After Logout", "Successfully Verified The home page by confirming The Link Login", "Yes");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 8. Verify Home Page After Logout", "Failed To Verify the home page, The Link for Login Not Present", "Yes");   
			   }
		
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	  }
	
	@Parameters({"ApplnURL","AppTitle"})
	  @Test(enabled=false) //working
	  public void TC_Login_Logout00001(String AppURL,String AppTitle) throws MalformedURLException {
		System.out.println("TC_Login_Logout00001: Execution Started");
		  Exttest = Extreport.startTest("TC_Login_0001","This Test case is to verify The Login Functionality With Positive Credentials");
		  //Driver = new RemoteWebDriver(new URL(nodeURL),Cap);
		  //System. setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/Resources/chromedriver");
		  Driver = new FirefoxDriver(); 
		  Driver.get(AppURL);
		  //System.out.println(Driver.getTitle());
		  //System.out.println("Successfully Launcehd the browser And Navigated To"+Driver.getTitle());
		  if(AppTitle.equals(Driver.getTitle())){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
			  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "Yes");
		  }
		  
		  WebElement btnLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//a[@id='login-button']");
		  if(btnLogin.isDisplayed()){
			  btnLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Login Button", "Successfully Clicked on the button Login", "No");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Login Button", "Failed To Click On  Clicked on the button Login", "Yes");   
			  System.out.println("Successfully Clicked on the button Login"); 
			   }
		  
		  WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
		  if(EdtUserName.isDisplayed()){
			  EdtUserName.clear();
			  String EmailId = "ibrahim.poovakkattil@starzplayarabia.com";
			  EdtUserName.sendKeys(EmailId);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Enter Email Address", "Successfully Entered The Email id :"+EmailId, "No");
			  System.out.println("Successfully Entered The Email id :"+EmailId);   
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Enter Email Address", "Failed To Enter Email ID", "Yes");   
			   }
		  
		  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
		  if(EdtPassword.isDisplayed()){
			  EdtPassword.clear();
			  String Password = "123456";
			  EdtPassword.sendKeys(Password);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Enter The Password", "Successfully Entered The Password :", "No");
			  System.out.println("Successfully Entered The Password");  
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Enter The Password", "Failed To Enter Password", "Yes");   
			   }
		  
		  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='ButtonLogin']");
		  if(btnSubmitLogin.isDisplayed()){
			  btnSubmitLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 4. Click On Login Submit BUtton", "Successfully Clicked on the button Login", "No");
			  System.out.println("Successfully Clicked on the button Login");    
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 4. Click On Login Submit Button", "Failed To Click On  Clicked on the button Login", "Yes");   
			   }
		  
		  WebElement lnkMOvies= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='movies-link']");
		  if(lnkMOvies.isDisplayed()){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 5. Verify Home Page", "Successfully Verified The home page by confirming The Link Movies", "Yes");
			  System.out.println("Successfully Verified The home page by confirming The Link Movies");    
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 5. Verify Home Page", "Failed To Verify the home page, The Link for Movies Not Present", "Yes");   
			   }
		  
		  WebElement btnSetting= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='settingsUser']");
		  if(btnSetting.isDisplayed()){
			  btnSetting.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 6. Click On Setting Icon", "Successfully Clicked on Setting Icon", "No");
			  System.out.println("Successfully Clicked on Setting Icon");      
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 6. Click On Setting Icon", "Failed To Click On Setting Icon", "Yes");   
			   }
		  
		  WebElement lnkLogout= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='logoutSettings']");
		  if(lnkLogout.isDisplayed()){
			  lnkLogout.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 7. Click On Setting Icon", "Successfully Clicked on Logout Link", "No");
			  System.out.println("Successfully Clicked on Logout Link");   
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 7. Click On Setting Icon", "Failed To Click On Lohout Link", "Yes");   
			   }
		  
		  WebElement lnkLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='login-button']");
		  if(lnkLogin.isDisplayed()){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 8. Verify Home Page After Logout", "Successfully Verified The home page by confirming The Link Login", "Yes");
			  System.out.println("Successfully Verified The home page by confirming The Link Login");   
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 8. Verify Home Page After Logout", "Failed To Verify the home page, The Link for Login Not Present", "Yes");   
			   }
		  System.out.println("TC_Login_Logout00001 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	  }
	
	@Parameters({"ApplnURL","AppTitle"})
	  @Test(enabled=false)// Working only till Login - unable to verify home page
	  public void TC_Login_Logout00001_HTmlUnitDriver(String AppURL,String AppTitle) throws MalformedURLException {
		System.out.println("TC_Login_Logout00001: Execution Started");
		  Exttest = Extreport.startTest("TC_Login_0001","This Test case is to verify The Login Functionality With Positive Credentials");
		  //Driver = new RemoteWebDriver(new URL(nodeURL),Cap);
		  //System. setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/Resources/chromedriver");
		  Hdriver = new HtmlUnitDriver(); 
		  Hdriver.get(AppURL);
		  //System.out.println(Driver.getTitle());
		  //System.out.println("Successfully Launcehd the browser And Navigated To"+Driver.getTitle());
		  if(AppTitle.equals(Hdriver.getTitle())){
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment", "Successfully Launched With Title :"+Hdriver.getTitle(), "No");
			  System.out.println("Successfully Launched With Title :"+Hdriver.getTitle());  
		  }
		  else{
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Hdriver.getTitle(), "No");
		  }
		  
		  WebElement btnLogin= Genf.setObject(50, Hdriver, Extreport, Exttest, ResultLocation, "xpath", "//a[@id='login-button']");
		  if(btnLogin.isDisplayed()){
			  btnLogin.click();
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Login Button", "Successfully Clicked on the button Login", "No");
			   }else{
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Login Button", "Failed To Click On  Clicked on the button Login", "No");   
			  System.out.println("Successfully Clicked on the button Login"); 
			   }
		  
		  WebElement EdtUserName= Genf.setObject(50, Hdriver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
		  if(EdtUserName.isDisplayed()){
			  EdtUserName.clear();
			  String EmailId = "ibrahim.poovakkattil@starzplayarabia.com";
			  EdtUserName.sendKeys(EmailId);
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Enter Email Address", "Successfully Entered The Email id :"+EmailId, "No");
			  System.out.println("Successfully Entered The Email id :"+EmailId);   
		  }else{
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Enter Email Address", "Failed To Enter Email ID", "No");   
			   }
		  
		  WebElement EdtPassword= Genf.setObject(50, Hdriver, Extreport, Exttest, ResultLocation, "id", "password");
		  if(EdtPassword.isDisplayed()){
			  EdtPassword.clear();
			  String Password = "123456";
			  EdtPassword.sendKeys(Password);
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Enter The Password", "Successfully Entered The Password :", "No");
			  System.out.println("Successfully Entered The Password");  
		  }else{
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Enter The Password", "Failed To Enter Password", "No");   
			   }
		  
		  WebElement btnSubmitLogin= Genf.setObject(50, Hdriver, Extreport, Exttest, ResultLocation, "id", "ButtonLogin");
		  if(btnSubmitLogin.isDisplayed()){
			  btnSubmitLogin.click();
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Passed", "Step 4. Click On Login Submit BUtton", "Successfully Clicked on the button Login", "No");
			  System.out.println("Successfully Clicked on the button Login");    
		  }else{
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Failed", "Step 4. Click On Login Submit Button", "Failed To Click On  Clicked on the button Login", "No");   
			   }
		  
		  
		  try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(Hdriver.getTitle());
		  
		  WebElement lnkMOvies= Genf.setObject(50, Hdriver, Extreport, Exttest, ResultLocation, "id", "movies-link");
		  if(lnkMOvies.isDisplayed()){
			  
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Passed", "Step 5. Verify Home Page", "Successfully Verified The home page by confirming The Link Movies", "No");
			  System.out.println("Successfully Verified The home page by confirming The Link Movies");    
		  }else{
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Failed", "Step 5. Verify Home Page", "Failed To Verify the home page, The Link for Movies Not Present", "No");   
			   }
		  
		  WebElement btnSetting= Genf.setObject(50, Hdriver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
		  if(btnSetting.isDisplayed()){
			  btnSetting.click();
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Passed", "Step 6. Click On Setting Icon", "Successfully Clicked on Setting Icon", "No");
			  System.out.println("Successfully Clicked on Setting Icon");      
		  }else{
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Failed", "Step 6. Click On Setting Icon", "Failed To Click On Setting Icon", "No");   
			   }
		  
		  WebElement lnkLogout= Genf.setObject(50, Hdriver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='dropDownUser']//a[@id='logoutSettings']");
		  if(lnkLogout.isDisplayed()){
			  lnkLogout.click();
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Passed", "Step 7. Click On Setting Icon", "Successfully Clicked on Logout Link", "No");
			  System.out.println("Successfully Clicked on Logout Link");   
		  }else{
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Failed", "Step 7. Click On Setting Icon", "Failed To Click On Lohout Link", "No");   
			   }
		  
		  WebElement lnkLogin= Genf.setObject(50, Hdriver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='login-button']");
		  if(lnkLogin.isDisplayed()){
			  
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Passed", "Step 8. Verify Home Page After Logout", "Successfully Verified The home page by confirming The Link Login", "No");
			  System.out.println("Successfully Verified The home page by confirming The Link Login");   
		  }else{
			  Futil.flogResult(Hdriver,Extreport,Exttest,ResultLocation,"Failed", "Step 8. Verify Home Page After Logout", "Failed To Verify the home page, The Link for Login Not Present", "No");   
			   }
		  System.out.println("TC_Login_Logout00001 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	  }
	
	@Parameters({"ApplnURL","AppTitle"})
	  @Test(enabled=false)//WORKING END TO END IN JENKIN
	  public void TC_Login_Logout00001_PhantomJs(String AppURL,String AppTitle) throws MalformedURLException {
		System.out.println("TC_Login_Logout00001: Execution Started");
		  Exttest = Extreport.startTest("TC_Login_Logout00001","This Test case is to verify The Login Functionality With Positive Credentials");
		  //Driver = new RemoteWebDriver(new URL(nodeURL),Cap);
		  //System. setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/Resources/chromedriver");
		  //System.setProperty("phantomjs.binary.path", PhantomJsSrc.getAbsolutePath());
		  //System.setProperty("phantomjs.binary.path", "/usr/local/bin/phantomjs");
		  Driver = new PhantomJSDriver(); 
		  Driver.manage().window().setSize(new Dimension(1920, 1080));
		  Driver.get(AppURL);
		  //System.out.println(Driver.getTitle());
		  //System.out.println("Successfully Launcehd the browser And Navigated To"+Driver.getTitle());
		  if(AppTitle.equals(Driver.getTitle())){
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Test Environment", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
			  System.out.println("Successfully Launched With Title :"+Driver.getTitle());  
		  }
		  else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 1. Launch Starz Play Test Environment", "Failed To Launch And The title was"+Driver.getTitle(), "No");
		  }
		  //WebElement btnLogin = Driver.findElement(By.xpath("//a[@id='login-button']"));
		  WebElement btnLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//a[@id='login-button']");
		  if(btnLogin!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", btnLogin);
			  btnLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Click On Login Button", "Successfully Clicked on the button Login", "No");
			   }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Click On Login Button", "Failed To Click On  Clicked on the button Login", "No");   
			  System.out.println("Successfully Clicked on the button Login"); 
			   }
		  
		  WebElement EdtUserName= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", "//input[@id='emailLogin']");
		  if(EdtUserName!=null){
			  EdtUserName.clear();
			  String EmailId = "ibrahim.poovakkattil@starzplayarabia.com";
			  EdtUserName.sendKeys(EmailId);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Enter Email Address", "Successfully Entered The Email id :"+EmailId, "No");
			  System.out.println("Successfully Entered The Email id :"+EmailId);   
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 2. Enter Email Address", "Failed To Enter Email ID", "No");   
			   }
		  
		  WebElement EdtPassword= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "password");
		  if(EdtPassword!=null){
			  EdtPassword.clear();
			  String Password = "123456";
			  EdtPassword.sendKeys(Password);
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 3. Enter The Password", "Successfully Entered The Password :", "No");
			  System.out.println("Successfully Entered The Password");  
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 3. Enter The Password", "Failed To Enter Password", "No");   
			   }
		  
		  try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  WebElement btnSubmitLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "ButtonLogin");
		  if(btnSubmitLogin!=null){
			  JavascriptExecutor js = (JavascriptExecutor)Driver;
			  js.executeScript("arguments[0].click();", btnSubmitLogin);
			  //btnSubmitLogin.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 4. Click On Login Submit BUtton", "Successfully Clicked on the button Login", "No");
			  System.out.println("Successfully Clicked on the button Login");    
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 4. Click On Login Submit Button", "Failed To Click On  Clicked on the button Login", "No");   
			   }
		  
		  
		  try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		  WebElement lnkMOvies= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "id", "movies-link");
		  if(lnkMOvies!=null){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 5. Verify Home Page", "Successfully Verified The home page by confirming The Link Movies", "Yes");
			  System.out.println("Successfully Verified The home page by confirming The Link Movies");    
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 5. Verify Home Page", "Failed To Verify the home page, The Link for Movies Not Present", "No");   
			   }
		  
		  WebElement btnSetting= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='navbarHeader']//a[@id='settingsUser']");
		  if(btnSetting!=null){
			  btnSetting.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 6. Click On Setting Icon", "Successfully Clicked on Setting Icon", "No");
			  System.out.println("Successfully Clicked on Setting Icon");      
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 6. Click On Setting Icon", "Failed To Click On Setting Icon", "No");   
			   }
		  
		  WebElement lnkLogout= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='dropDownUser']//a[@id='logoutSettings']");
		  if(lnkLogout!=null){
			  lnkLogout.click();
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 7. Click On Setting Icon", "Successfully Clicked on Logout Link", "No");
			  System.out.println("Successfully Clicked on Logout Link");   
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 7. Click On Setting Icon", "Failed To Click On Lohout Link", "No");   
			   }
		  
		  try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  WebElement lnkLogin= Genf.setObject(50, Driver, Extreport, Exttest, ResultLocation, "xpath", ".//*[@id='login-button']");
		  if(lnkLogin!=null){
			  
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 8. Verify Home Page After Logout", "Successfully Verified The home page by confirming The Link Login", "Yes");
			  System.out.println("Successfully Logged out and Verified The home page by confirming The Link Login");   
		  }else{
			  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Failed", "Step 8. Verify Home Page After Logout", "Failed To Verify the home page, The Link for Login Not Present", "Yes");   
			   }
		  System.out.println("TC_Login_Logout00001 : Execution Finished");
		  Extreport.flush();
		  Extreport.endTest(Exttest);
		  Driver.close();
	  }
	
	@Parameters({"NodeURL","ApplnURL"})
  @Test(enabled=false)
  public void TC_0001(String nodeURL,String AppURL) throws MalformedURLException {
	  Exttest = Extreport.startTest("TC_0001","Starz Play Sample Test");
	  System.out.println("To Health Check");
	  Driver = new RemoteWebDriver(new URL(nodeURL),Cap);
	  Driver.get(AppURL);
	  System.out.println("Successfully Launcehd the browser And Navigated To"+Driver.getTitle());
	  
	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Starz Play Arabia", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
	  
	  Driver.close();
	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Close The Browser", "Successfully Closed", "No");
	  
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	  
  }
  
  
  @Parameters({"NodeURL"})
  @Test(enabled=false)
  public void TC_0002(String nodeURL) throws MalformedURLException{
	  //FrameWorkUtility Futil = new FrameWorkUtility();
	  Exttest = Extreport.startTest("TC_0002","Google Sample Test");
	  Driver = new RemoteWebDriver(new URL(nodeURL),Cap);
	  Driver.get("https://google.com/");
	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 1. Launch Google", "Successfully Launched With Title :"+Driver.getTitle(), "Yes");
	  Extreport.flush(); 
	  Driver.close();
	  Futil.flogResult(Driver,Extreport,Exttest,ResultLocation,"Passed", "Step 2. Close The Browser", "Successfully Closed", "No");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
  }
  
  @Test(enabled=false)
  public void TC_003(){
	  System.out.println("I reached TC_0003");
	  Exttest = Extreport.startTest("TC_0003","Sample Description");
	  Exttest.log(LogStatus.PASS, "This is a sample Step Name", "This is a sample Description");
	  Extreport.flush();
	  Extreport.endTest(Exttest);
	 
  }
  
  
}
